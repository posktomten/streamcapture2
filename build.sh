#!/bin/bash


      QT=6.8.2
      EXECUTABLE=streamcapture2
      GLIBC=$(glibcversion);
      PATH=/opt/Qt/Tools/Ninja:${PATH}

        HERE=$(pwd)


        cd ${EXECUTABLE}-zsyncmake
        rm -f *"${EXECUTABLE}"*
        rm -f ".invisible_"*


      cd ..
      rm -f *"${EXECUTABLE}-x86_64.AppImage"*
      rm -f ".invisible_"*


      cd code

        export CXX=/bin/g++
        export CMAKE_CXX_OUTPUT_EXTENSION=.o
        export CMAKE_C_COMPILER=/bin/gcc
        export CMAKE_PREFIX_PATH=/opt/Qt/${QT}/gcc_64
        export QT_DIR=/opt/Qt/${QT}//gcc_64/lib/cmake/Qt6
        export QT_QMAKE_EXECUTABLE=/opt/Qt/${QT}//gcc_64/bin/qmake
        export Qt6CoreTools_DIR=/opt/Qt/${QT}//gcc_64/lib/cmake/Qt6CoreTools
        export Qt6Core_DIR=/opt/Qt/${QT}//gcc_64/lib/cmake/Qt6Core
        export Qt6LinguistTools_DIR=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6LinguistTools
        export Qt6Network_DIR=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6Network
        export Qt6_DIR=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6
        export CMAKE_BUILD_TYPE=Release
        export CMAKE_GENERATOR=Ninja
        export CMAKE_PREFIX_PATH=/opt/Qt/${QT}/gcc_64
        export QT_DIR=/opt/Qt/${QT}/gcc_64
        export Qt6_DIR=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6
        export CMAKE_MODULE_PATH=/opt/Qt/${QT}/gcc_64/lib/cmake/Qt6





        cmake -S "." -B "../build" -G "Ninja" -DCMAKE_BUILD_TYPE=Release
        cmake --build "../build" --target all
        cd ..

        echo $(pwd)
        echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("streamcapture2-x86_64.AppImage" "streamcapture2-ffmpeg-x86_64.AppImage" "Quit")

select opt in "${options[@]}"; do
    case $opt in
    "streamcapture2-x86_64.AppImage")
        cp build-executable6/* linuxdeploy/
        rm -f -R build
        rm -f -R build-executable6
        cd linuxdeploy
        ./instructions.sh
        break
        ;;
    "streamcapture2-ffmpeg-x86_64.AppImage")
    
    
        cp build-executable6/* linuxdeploy-ffmpeg/
        rm -f -R build
        rm -f -R build-executable6
        cd linuxdeploy-ffmpeg
        if [[ "${GLIBC}" == "2.39" ]]; then
			./instructions-ffmpeg.sh
		fi
		
	   if [[ "${GLIBC}" == "2.35" ]]; then
			./instructions-ffmpeg_glibc2.35.sh
		fi
		
	   if [[ "${GLIBC}" == "2.31" ]]; then
			./instructions-ffmpeg_glibc2.31.sh
		fi
		
        break
        ;;

    "Quit")
        echo "Goodbye!"
        exit 0
        break
        ;;
    *) echo "Invalid option $REPLY"
    ;;
    esac
done









echo "Finished!"
