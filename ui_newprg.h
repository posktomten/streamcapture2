/********************************************************************************
** Form generated from reading UI file 'newprg.ui'
**
** Created by: Qt User Interface Compiler version 6.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_NEWPRG_H
#define UI_NEWPRG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QIcon>
#include <QtWidgets/QApplication>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_newprg
{
public:
    QAction *actionEnglish;
    QAction *actionSwedish;
    QAction *actionAbout;
    QAction *actionCheckForUpdates;
    QAction *actionExit;
    QAction *actionAboutSvtplayDl;
    QAction *actionAboutFfmpeg;
    QAction *actionSearch;
    QAction *actionPast;
    QAction *actionDownload;
    QAction *actionLicense;
    QAction *actionLicenseSvtplayDl;
    QAction *actionLicenseFfmpeg;
    QAction *actionRecentFiles;
    QAction *actionHelp;
    QAction *actionViewDownloadList;
    QAction *actionDeleteDownloadList;
    QAction *actionDeleteDownloadList_menu;
    QAction *actionItalian;
    QAction *actionVersionHistory;
    QAction *actionCreateNew;
    QAction *actionPasswordX;
    QAction *actionVerboseOutput;
    QAction *actionAdd;
    QAction *actionDownloadAll;
    QAction *actionCreateFolder;
    QAction *actionEditDownloadList;
    QAction *actionShowMore;
    QAction *actionPassword;
    QAction *actionUninstall_streamCapture;
    QAction *actionDownloadAllEpisodes;
    QAction *actionDownloadAfterDate;
    QAction *actionStopAllDownloads;
    QAction *actionListAllEpisodes;
    QAction *actionDeleteAllSettings2;
    QAction *actionCopyToDefaultLocation;
    QAction *actionSetDefaultCopyLocation;
    QAction *actionSetDefaultDownloadLocation;
    QAction *actionDownloadToDefaultLocation;
    QAction *actionAddAllEpisodesToDownloadList;
    QAction *actionSelectFont;
    QAction *actionSvtplayDlForum;
    QAction *actionMaintenanceTool;
    QAction *actionDeleteAllSettings;
    QAction *actionNotifications;
    QAction *actionCreateDestopShortcut;
    QAction *actionDesktopShortcut;
    QAction *actionApplicationsMenuShortcut;
    QAction *actionPreCheckLanguagefile;
    QAction *actionStCookie;
    QAction *actionIn;
    QAction *actionDefault;
    QAction *actionOut;
    QAction *actionCheckLatestsvtplaydl;
    QAction *actionDownloadMicrosoftRuntime;
    QAction *actionLicense7zip;
    QAction *actionCheckOnStart;
    QAction *actionCheckSvtplayDlForUpdatesAtStart;
    QAction *actionStreamCapture2Settings;
    QAction *actionDownloadSvtplayDlSettings;
    QAction *actionNfoInfo;
    QAction *actionSelectFileName;
    QAction *actionAboutQt;
    QAction *actionTVToken;
    QAction *actionHowTo;
    QAction *actionHowToVideo;
    QAction *action4k;
    QAction *actionSubtitle;
    QAction *actionMergeSubtitle;
    QAction *actionNoSubtitles;
    QAction *actionImportant;
    QAction *actionLightTheme;
    QAction *actionDarkFusionStyle;
    QAction *actionOpenFolder;
    QAction *actionSvtplayDlSystem;
    QAction *actionSvtplayDlStable;
    QAction *actionSvtplayDlBleedingEdge;
    QAction *actionSvtPlayDlManuallySelect;
    QAction *actionSvtPlayDlManuallySelected;
    QAction *actionDownloadSvtplayDlFromBinCeicerCom;
    QAction *actionAllSubtitles;
    QAction *actionNoSubtitles_2;
    QAction *actionMergeSubtitle_2;
    QAction *actionSubtitle_2;
    QAction *actionAllSubtitles_2;
    QAction *actionDownloadRaw_2;
    QAction *actionDontUseNativeDialogs;
    QAction *actionSystemDefaultTheme;
    QAction *actionFusionStyle;
    QAction *actionDefaultStyle;
    QAction *actionExportDownloadList;
    QAction *actionImportDownloadList;
    QAction *actionDownloadList;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_3;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *pbPast;
    QLineEdit *leSok;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *pbSok;
    QPushButton *pbDownload;
    QPushButton *pbAdd;
    QPushButton *pbDownloadAll;
    QCheckBox *chb4k;
    QSpacerItem *horizontalSpacer;
    QTextEdit *teOut;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout;
    QLabel *lblQuality;
    QLabel *leQuality;
    QSpacerItem *horizontalSpacer_4;
    QVBoxLayout *verticalLayout_2;
    QLabel *lblMethod;
    QLabel *leMethod;
    QSpacerItem *horizontalSpacer_2;
    QLabel *lblQualityBitrate;
    QHBoxLayout *horizontalLayout_4;
    QComboBox *comboBox;
    QComboBox *comboResolution;
    QComboBox *comboAmount;
    QComboBox *comboPayTV;
    QPushButton *pbPassword;
    QSpacerItem *horizontalSpacer_3;
    QProgressBar *progressBar;
    QMenuBar *menuBar;
    QMenu *menuFile;
    QMenu *menuLanguage;
    QMenu *menuTools;
    QMenu *menuCheckForUpdates;
    QMenu *menuEditSettingsAdvanced;
    QMenu *menuHelp;
    QMenu *menuRecent;
    QMenu *menuDownloadList;
    QMenu *menuPayTV;
    QMenu *menuDownload_all_Episodes;
    QMenu *menuStCookies;
    QMenu *menuView;
    QMenu *menuTV4;
    QMenu *menuSvtplayDl;
    QMenu *menuSubtitles;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *newprg)
    {
        if (newprg->objectName().isEmpty())
            newprg->setObjectName("newprg");
        newprg->setEnabled(true);
        newprg->resize(988, 548);
        QSizePolicy sizePolicy(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(newprg->sizePolicy().hasHeightForWidth());
        newprg->setSizePolicy(sizePolicy);
        newprg->setMinimumSize(QSize(0, 0));
        newprg->setMaximumSize(QSize(16777215, 16777215));
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/images/icon.ico"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        newprg->setWindowIcon(icon);
        newprg->setAutoFillBackground(true);
        actionEnglish = new QAction(newprg);
        actionEnglish->setObjectName("actionEnglish");
        QIcon icon1;
        icon1.addFile(QString::fromUtf8(":/images/english"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionEnglish->setIcon(icon1);
        actionSwedish = new QAction(newprg);
        actionSwedish->setObjectName("actionSwedish");
        QIcon icon2;
        icon2.addFile(QString::fromUtf8(":/images/swedish"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionSwedish->setIcon(icon2);
        actionAbout = new QAction(newprg);
        actionAbout->setObjectName("actionAbout");
        QIcon icon3;
        icon3.addFile(QString::fromUtf8(":/images/appiconsmal.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionAbout->setIcon(icon3);
        actionCheckForUpdates = new QAction(newprg);
        actionCheckForUpdates->setObjectName("actionCheckForUpdates");
        QIcon icon4;
        icon4.addFile(QString::fromUtf8(":/images/update"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionCheckForUpdates->setIcon(icon4);
        actionExit = new QAction(newprg);
        actionExit->setObjectName("actionExit");
        QIcon icon5;
        icon5.addFile(QString::fromUtf8(":/images/exit"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionExit->setIcon(icon5);
        actionAboutSvtplayDl = new QAction(newprg);
        actionAboutSvtplayDl->setObjectName("actionAboutSvtplayDl");
        QIcon icon6;
        icon6.addFile(QString::fromUtf8(":/images/svtplay-dl2.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionAboutSvtplayDl->setIcon(icon6);
        actionAboutFfmpeg = new QAction(newprg);
        actionAboutFfmpeg->setObjectName("actionAboutFfmpeg");
        QIcon icon7;
        icon7.addFile(QString::fromUtf8(":/images/FFmpeg"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionAboutFfmpeg->setIcon(icon7);
        actionSearch = new QAction(newprg);
        actionSearch->setObjectName("actionSearch");
        QIcon icon8;
        icon8.addFile(QString::fromUtf8(":/images/search"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionSearch->setIcon(icon8);
        actionPast = new QAction(newprg);
        actionPast->setObjectName("actionPast");
        QIcon icon9;
        icon9.addFile(QString::fromUtf8(":/images/past"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionPast->setIcon(icon9);
        actionDownload = new QAction(newprg);
        actionDownload->setObjectName("actionDownload");
        actionDownload->setEnabled(false);
        QIcon icon10;
        icon10.addFile(QString::fromUtf8(":/images/download"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionDownload->setIcon(icon10);
        actionLicense = new QAction(newprg);
        actionLicense->setObjectName("actionLicense");
        QIcon icon11;
        icon11.addFile(QString::fromUtf8(":/images/gpl.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionLicense->setIcon(icon11);
        actionLicenseSvtplayDl = new QAction(newprg);
        actionLicenseSvtplayDl->setObjectName("actionLicenseSvtplayDl");
        QIcon icon12;
        icon12.addFile(QString::fromUtf8(":/images/mit.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionLicenseSvtplayDl->setIcon(icon12);
        actionLicenseFfmpeg = new QAction(newprg);
        actionLicenseFfmpeg->setObjectName("actionLicenseFfmpeg");
        actionLicenseFfmpeg->setIcon(icon11);
        actionRecentFiles = new QAction(newprg);
        actionRecentFiles->setObjectName("actionRecentFiles");
        actionHelp = new QAction(newprg);
        actionHelp->setObjectName("actionHelp");
        actionHelp->setEnabled(true);
        QIcon icon13;
        icon13.addFile(QString::fromUtf8(":/images/help"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionHelp->setIcon(icon13);
        actionHelp->setVisible(true);
        actionViewDownloadList = new QAction(newprg);
        actionViewDownloadList->setObjectName("actionViewDownloadList");
        QIcon icon14;
        icon14.addFile(QString::fromUtf8(":/images/list.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionViewDownloadList->setIcon(icon14);
        actionDeleteDownloadList = new QAction(newprg);
        actionDeleteDownloadList->setObjectName("actionDeleteDownloadList");
        actionDeleteDownloadList->setIcon(icon14);
        actionDeleteDownloadList_menu = new QAction(newprg);
        actionDeleteDownloadList_menu->setObjectName("actionDeleteDownloadList_menu");
        actionItalian = new QAction(newprg);
        actionItalian->setObjectName("actionItalian");
        QIcon icon15;
        icon15.addFile(QString::fromUtf8(":/images/italian.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionItalian->setIcon(icon15);
        actionVersionHistory = new QAction(newprg);
        actionVersionHistory->setObjectName("actionVersionHistory");
        QIcon icon16;
        icon16.addFile(QString::fromUtf8(":/images/versionhistory2.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionVersionHistory->setIcon(icon16);
        actionCreateNew = new QAction(newprg);
        actionCreateNew->setObjectName("actionCreateNew");
        QIcon icon17;
        icon17.addFile(QString::fromUtf8(":/images/new.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionCreateNew->setIcon(icon17);
        actionPasswordX = new QAction(newprg);
        actionPasswordX->setObjectName("actionPasswordX");
        QIcon icon18;
        icon18.addFile(QString::fromUtf8(":/images/password.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionPasswordX->setIcon(icon18);
        actionVerboseOutput = new QAction(newprg);
        actionVerboseOutput->setObjectName("actionVerboseOutput");
        actionVerboseOutput->setCheckable(true);
        actionAdd = new QAction(newprg);
        actionAdd->setObjectName("actionAdd");
        actionAdd->setEnabled(false);
        actionAdd->setIcon(icon14);
        actionDownloadAll = new QAction(newprg);
        actionDownloadAll->setObjectName("actionDownloadAll");
        actionDownloadAll->setEnabled(false);
        actionDownloadAll->setIcon(icon14);
        actionCreateFolder = new QAction(newprg);
        actionCreateFolder->setObjectName("actionCreateFolder");
        actionCreateFolder->setCheckable(true);
        actionEditDownloadList = new QAction(newprg);
        actionEditDownloadList->setObjectName("actionEditDownloadList");
        actionEditDownloadList->setIcon(icon14);
        actionShowMore = new QAction(newprg);
        actionShowMore->setObjectName("actionShowMore");
        actionShowMore->setCheckable(true);
        actionShowMore->setShortcutContext(Qt::ShortcutContext::WidgetShortcut);
        actionShowMore->setIconVisibleInMenu(true);
        actionPassword = new QAction(newprg);
        actionPassword->setObjectName("actionPassword");
        actionPassword->setIcon(icon18);
        actionUninstall_streamCapture = new QAction(newprg);
        actionUninstall_streamCapture->setObjectName("actionUninstall_streamCapture");
        actionDownloadAllEpisodes = new QAction(newprg);
        actionDownloadAllEpisodes->setObjectName("actionDownloadAllEpisodes");
        actionDownloadAllEpisodes->setEnabled(false);
        actionDownloadAfterDate = new QAction(newprg);
        actionDownloadAfterDate->setObjectName("actionDownloadAfterDate");
        actionStopAllDownloads = new QAction(newprg);
        actionStopAllDownloads->setObjectName("actionStopAllDownloads");
        QIcon icon19;
        icon19.addFile(QString::fromUtf8(":/images/kill.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionStopAllDownloads->setIcon(icon19);
        actionListAllEpisodes = new QAction(newprg);
        actionListAllEpisodes->setObjectName("actionListAllEpisodes");
        actionDeleteAllSettings2 = new QAction(newprg);
        actionDeleteAllSettings2->setObjectName("actionDeleteAllSettings2");
        actionCopyToDefaultLocation = new QAction(newprg);
        actionCopyToDefaultLocation->setObjectName("actionCopyToDefaultLocation");
        actionCopyToDefaultLocation->setCheckable(true);
        actionSetDefaultCopyLocation = new QAction(newprg);
        actionSetDefaultCopyLocation->setObjectName("actionSetDefaultCopyLocation");
        QIcon icon20;
        icon20.addFile(QString::fromUtf8(":/images/folder.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionSetDefaultCopyLocation->setIcon(icon20);
        actionSetDefaultDownloadLocation = new QAction(newprg);
        actionSetDefaultDownloadLocation->setObjectName("actionSetDefaultDownloadLocation");
        actionSetDefaultDownloadLocation->setIcon(icon20);
        actionDownloadToDefaultLocation = new QAction(newprg);
        actionDownloadToDefaultLocation->setObjectName("actionDownloadToDefaultLocation");
        actionDownloadToDefaultLocation->setCheckable(true);
        actionAddAllEpisodesToDownloadList = new QAction(newprg);
        actionAddAllEpisodesToDownloadList->setObjectName("actionAddAllEpisodesToDownloadList");
        actionAddAllEpisodesToDownloadList->setEnabled(false);
        actionSelectFont = new QAction(newprg);
        actionSelectFont->setObjectName("actionSelectFont");
        QIcon icon21;
        icon21.addFile(QString::fromUtf8(":/images/font2.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionSelectFont->setIcon(icon21);
        actionSvtplayDlForum = new QAction(newprg);
        actionSvtplayDlForum->setObjectName("actionSvtplayDlForum");
        QIcon icon22;
        icon22.addFile(QString::fromUtf8(":/images/forum.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionSvtplayDlForum->setIcon(icon22);
        actionMaintenanceTool = new QAction(newprg);
        actionMaintenanceTool->setObjectName("actionMaintenanceTool");
        actionMaintenanceTool->setEnabled(false);
        QIcon icon23;
        icon23.addFile(QString::fromUtf8(":/images/maintenancetool.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionMaintenanceTool->setIcon(icon23);
        actionMaintenanceTool->setVisible(true);
        actionDeleteAllSettings = new QAction(newprg);
        actionDeleteAllSettings->setObjectName("actionDeleteAllSettings");
        QIcon icon24;
        icon24.addFile(QString::fromUtf8(":/images/removesettings.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionDeleteAllSettings->setIcon(icon24);
        actionNotifications = new QAction(newprg);
        actionNotifications->setObjectName("actionNotifications");
        actionNotifications->setCheckable(true);
        actionCreateDestopShortcut = new QAction(newprg);
        actionCreateDestopShortcut->setObjectName("actionCreateDestopShortcut");
        actionCreateDestopShortcut->setEnabled(true);
        QIcon icon25;
        icon25.addFile(QString::fromUtf8(":/images/link.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionCreateDestopShortcut->setIcon(icon25);
        actionCreateDestopShortcut->setVisible(true);
        actionDesktopShortcut = new QAction(newprg);
        actionDesktopShortcut->setObjectName("actionDesktopShortcut");
        actionDesktopShortcut->setCheckable(true);
        actionApplicationsMenuShortcut = new QAction(newprg);
        actionApplicationsMenuShortcut->setObjectName("actionApplicationsMenuShortcut");
        actionApplicationsMenuShortcut->setCheckable(true);
        actionPreCheckLanguagefile = new QAction(newprg);
        actionPreCheckLanguagefile->setObjectName("actionPreCheckLanguagefile");
        actionPreCheckLanguagefile->setAutoRepeat(true);
        actionStCookie = new QAction(newprg);
        actionStCookie->setObjectName("actionStCookie");
        actionIn = new QAction(newprg);
        actionIn->setObjectName("actionIn");
        QIcon icon26;
        icon26.addFile(QString::fromUtf8(":/images/zoompluss.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionIn->setIcon(icon26);
        actionDefault = new QAction(newprg);
        actionDefault->setObjectName("actionDefault");
        actionOut = new QAction(newprg);
        actionOut->setObjectName("actionOut");
        QIcon icon27;
        icon27.addFile(QString::fromUtf8(":/images/zoomminus.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionOut->setIcon(icon27);
        actionCheckLatestsvtplaydl = new QAction(newprg);
        actionCheckLatestsvtplaydl->setObjectName("actionCheckLatestsvtplaydl");
        QIcon icon28;
        icon28.addFile(QString::fromUtf8(":/images/update.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionCheckLatestsvtplaydl->setIcon(icon28);
        actionDownloadMicrosoftRuntime = new QAction(newprg);
        actionDownloadMicrosoftRuntime->setObjectName("actionDownloadMicrosoftRuntime");
        QIcon icon29;
        icon29.addFile(QString::fromUtf8(":/images/redistuable.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionDownloadMicrosoftRuntime->setIcon(icon29);
        actionDownloadMicrosoftRuntime->setVisible(false);
        actionLicense7zip = new QAction(newprg);
        actionLicense7zip->setObjectName("actionLicense7zip");
        QIcon icon30;
        icon30.addFile(QString::fromUtf8(":/images/lgpl.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionLicense7zip->setIcon(icon30);
        actionLicense7zip->setVisible(false);
        actionCheckOnStart = new QAction(newprg);
        actionCheckOnStart->setObjectName("actionCheckOnStart");
        actionCheckOnStart->setCheckable(true);
        actionCheckSvtplayDlForUpdatesAtStart = new QAction(newprg);
        actionCheckSvtplayDlForUpdatesAtStart->setObjectName("actionCheckSvtplayDlForUpdatesAtStart");
        actionCheckSvtplayDlForUpdatesAtStart->setCheckable(true);
        actionStreamCapture2Settings = new QAction(newprg);
        actionStreamCapture2Settings->setObjectName("actionStreamCapture2Settings");
        QIcon icon31;
        icon31.addFile(QString::fromUtf8(":/images/edit.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionStreamCapture2Settings->setIcon(icon31);
        actionDownloadSvtplayDlSettings = new QAction(newprg);
        actionDownloadSvtplayDlSettings->setObjectName("actionDownloadSvtplayDlSettings");
        actionDownloadSvtplayDlSettings->setIcon(icon31);
        actionNfoInfo = new QAction(newprg);
        actionNfoInfo->setObjectName("actionNfoInfo");
        QIcon icon32;
        icon32.addFile(QString::fromUtf8(":/images/nfo.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionNfoInfo->setIcon(icon32);
        actionSelectFileName = new QAction(newprg);
        actionSelectFileName->setObjectName("actionSelectFileName");
        actionSelectFileName->setCheckable(true);
        actionAboutQt = new QAction(newprg);
        actionAboutQt->setObjectName("actionAboutQt");
        QIcon icon33;
        icon33.addFile(QString::fromUtf8(":/images/qt256.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionAboutQt->setIcon(icon33);
        actionTVToken = new QAction(newprg);
        actionTVToken->setObjectName("actionTVToken");
        QIcon icon34;
        icon34.addFile(QString::fromUtf8(":/images/tv4.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionTVToken->setIcon(icon34);
        actionHowTo = new QAction(newprg);
        actionHowTo->setObjectName("actionHowTo");
        actionHowToVideo = new QAction(newprg);
        actionHowToVideo->setObjectName("actionHowToVideo");
        action4k = new QAction(newprg);
        action4k->setObjectName("action4k");
        action4k->setCheckable(true);
        actionSubtitle = new QAction(newprg);
        actionSubtitle->setObjectName("actionSubtitle");
        actionSubtitle->setCheckable(true);
        actionSubtitle->setChecked(true);
        actionMergeSubtitle = new QAction(newprg);
        actionMergeSubtitle->setObjectName("actionMergeSubtitle");
        actionMergeSubtitle->setCheckable(true);
        actionNoSubtitles = new QAction(newprg);
        actionNoSubtitles->setObjectName("actionNoSubtitles");
        actionNoSubtitles->setCheckable(true);
        actionImportant = new QAction(newprg);
        actionImportant->setObjectName("actionImportant");
        QIcon icon35;
        icon35.addFile(QString::fromUtf8(":/images/important.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionImportant->setIcon(icon35);
        actionLightTheme = new QAction(newprg);
        actionLightTheme->setObjectName("actionLightTheme");
        actionLightTheme->setCheckable(true);
        actionLightTheme->setVisible(true);
        actionDarkFusionStyle = new QAction(newprg);
        actionDarkFusionStyle->setObjectName("actionDarkFusionStyle");
        actionDarkFusionStyle->setCheckable(true);
        actionDarkFusionStyle->setVisible(true);
        actionOpenFolder = new QAction(newprg);
        actionOpenFolder->setObjectName("actionOpenFolder");
        QIcon icon36;
        icon36.addFile(QString::fromUtf8(":/images/open.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionOpenFolder->setIcon(icon36);
        actionSvtplayDlSystem = new QAction(newprg);
        actionSvtplayDlSystem->setObjectName("actionSvtplayDlSystem");
        actionSvtplayDlSystem->setCheckable(true);
        actionSvtplayDlStable = new QAction(newprg);
        actionSvtplayDlStable->setObjectName("actionSvtplayDlStable");
        actionSvtplayDlStable->setCheckable(true);
        actionSvtplayDlBleedingEdge = new QAction(newprg);
        actionSvtplayDlBleedingEdge->setObjectName("actionSvtplayDlBleedingEdge");
        actionSvtplayDlBleedingEdge->setCheckable(true);
        actionSvtPlayDlManuallySelect = new QAction(newprg);
        actionSvtPlayDlManuallySelect->setObjectName("actionSvtPlayDlManuallySelect");
        actionSvtPlayDlManuallySelect->setIcon(icon6);
        actionSvtPlayDlManuallySelected = new QAction(newprg);
        actionSvtPlayDlManuallySelected->setObjectName("actionSvtPlayDlManuallySelected");
        actionSvtPlayDlManuallySelected->setCheckable(true);
        actionDownloadSvtplayDlFromBinCeicerCom = new QAction(newprg);
        actionDownloadSvtplayDlFromBinCeicerCom->setObjectName("actionDownloadSvtplayDlFromBinCeicerCom");
        QIcon icon37;
        icon37.addFile(QString::fromUtf8(":/images/download.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        actionDownloadSvtplayDlFromBinCeicerCom->setIcon(icon37);
        actionAllSubtitles = new QAction(newprg);
        actionAllSubtitles->setObjectName("actionAllSubtitles");
        actionAllSubtitles->setCheckable(true);
        actionNoSubtitles_2 = new QAction(newprg);
        actionNoSubtitles_2->setObjectName("actionNoSubtitles_2");
        actionNoSubtitles_2->setCheckable(true);
        actionMergeSubtitle_2 = new QAction(newprg);
        actionMergeSubtitle_2->setObjectName("actionMergeSubtitle_2");
        actionMergeSubtitle_2->setCheckable(true);
        actionSubtitle_2 = new QAction(newprg);
        actionSubtitle_2->setObjectName("actionSubtitle_2");
        actionSubtitle_2->setCheckable(true);
        actionAllSubtitles_2 = new QAction(newprg);
        actionAllSubtitles_2->setObjectName("actionAllSubtitles_2");
        actionAllSubtitles_2->setCheckable(true);
        actionDownloadRaw_2 = new QAction(newprg);
        actionDownloadRaw_2->setObjectName("actionDownloadRaw_2");
        actionDownloadRaw_2->setCheckable(true);
        actionDontUseNativeDialogs = new QAction(newprg);
        actionDontUseNativeDialogs->setObjectName("actionDontUseNativeDialogs");
        actionDontUseNativeDialogs->setCheckable(true);
        actionSystemDefaultTheme = new QAction(newprg);
        actionSystemDefaultTheme->setObjectName("actionSystemDefaultTheme");
        actionSystemDefaultTheme->setCheckable(true);
        actionFusionStyle = new QAction(newprg);
        actionFusionStyle->setObjectName("actionFusionStyle");
        actionFusionStyle->setCheckable(true);
        actionDefaultStyle = new QAction(newprg);
        actionDefaultStyle->setObjectName("actionDefaultStyle");
        actionDefaultStyle->setCheckable(true);
        actionExportDownloadList = new QAction(newprg);
        actionExportDownloadList->setObjectName("actionExportDownloadList");
        actionExportDownloadList->setIcon(icon14);
        actionImportDownloadList = new QAction(newprg);
        actionImportDownloadList->setObjectName("actionImportDownloadList");
        actionImportDownloadList->setIcon(icon14);
        actionDownloadList = new QAction(newprg);
        actionDownloadList->setObjectName("actionDownloadList");
        actionDownloadList->setIcon(icon14);
        centralWidget = new QWidget(newprg);
        centralWidget->setObjectName("centralWidget");
        QSizePolicy sizePolicy1(QSizePolicy::Policy::Minimum, QSizePolicy::Policy::Minimum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(centralWidget->sizePolicy().hasHeightForWidth());
        centralWidget->setSizePolicy(sizePolicy1);
        verticalLayout_3 = new QVBoxLayout(centralWidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName("verticalLayout_3");
        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setObjectName("verticalLayout_4");
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setObjectName("horizontalLayout_2");
        pbPast = new QPushButton(centralWidget);
        pbPast->setObjectName("pbPast");
        pbPast->setMinimumSize(QSize(150, 30));
        pbPast->setMaximumSize(QSize(150, 30));
        pbPast->setIcon(icon9);

        horizontalLayout_2->addWidget(pbPast);

        leSok = new QLineEdit(centralWidget);
        leSok->setObjectName("leSok");
        leSok->setMinimumSize(QSize(0, 30));
        leSok->setMaximumSize(QSize(16777215, 30));

        horizontalLayout_2->addWidget(leSok);


        verticalLayout_4->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName("horizontalLayout_3");
        pbSok = new QPushButton(centralWidget);
        pbSok->setObjectName("pbSok");
        pbSok->setMinimumSize(QSize(150, 30));
        pbSok->setMaximumSize(QSize(150, 30));
        pbSok->setIcon(icon8);

        horizontalLayout_3->addWidget(pbSok);

        pbDownload = new QPushButton(centralWidget);
        pbDownload->setObjectName("pbDownload");
        pbDownload->setEnabled(false);
        QSizePolicy sizePolicy2(QSizePolicy::Policy::Fixed, QSizePolicy::Policy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(pbDownload->sizePolicy().hasHeightForWidth());
        pbDownload->setSizePolicy(sizePolicy2);
        pbDownload->setMinimumSize(QSize(0, 30));
        pbDownload->setMaximumSize(QSize(16777215, 30));
        pbDownload->setAutoFillBackground(false);
        pbDownload->setIcon(icon10);
        pbDownload->setAutoDefault(false);

        horizontalLayout_3->addWidget(pbDownload);

        pbAdd = new QPushButton(centralWidget);
        pbAdd->setObjectName("pbAdd");
        pbAdd->setEnabled(false);
        pbAdd->setMinimumSize(QSize(0, 30));
        pbAdd->setMaximumSize(QSize(16777215, 30));
        pbAdd->setAutoFillBackground(false);
        QIcon icon38;
        icon38.addFile(QString::fromUtf8(":/images/list"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        pbAdd->setIcon(icon38);
        pbAdd->setAutoDefault(false);

        horizontalLayout_3->addWidget(pbAdd);

        pbDownloadAll = new QPushButton(centralWidget);
        pbDownloadAll->setObjectName("pbDownloadAll");
        pbDownloadAll->setEnabled(false);
        QSizePolicy sizePolicy3(QSizePolicy::Policy::Fixed, QSizePolicy::Policy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(10);
        sizePolicy3.setHeightForWidth(pbDownloadAll->sizePolicy().hasHeightForWidth());
        pbDownloadAll->setSizePolicy(sizePolicy3);
        pbDownloadAll->setMinimumSize(QSize(0, 30));
        pbDownloadAll->setMaximumSize(QSize(16777215, 30));
        pbDownloadAll->setAutoFillBackground(false);
        pbDownloadAll->setIcon(icon38);
        pbDownloadAll->setAutoRepeatDelay(282);
        pbDownloadAll->setAutoDefault(false);

        horizontalLayout_3->addWidget(pbDownloadAll);

        chb4k = new QCheckBox(centralWidget);
        chb4k->setObjectName("chb4k");
        chb4k->setLayoutDirection(Qt::LayoutDirection::RightToLeft);
        QIcon icon39;
        icon39.addFile(QString::fromUtf8(":/images/4k.png"), QSize(), QIcon::Mode::Normal, QIcon::State::Off);
        chb4k->setIcon(icon39);
        chb4k->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(chb4k);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);


        verticalLayout_4->addLayout(horizontalLayout_3);


        verticalLayout_3->addLayout(verticalLayout_4);

        teOut = new QTextEdit(centralWidget);
        teOut->setObjectName("teOut");
        QSizePolicy sizePolicy4(QSizePolicy::Policy::Preferred, QSizePolicy::Policy::Expanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(teOut->sizePolicy().hasHeightForWidth());
        teOut->setSizePolicy(sizePolicy4);
        teOut->setMaximumSize(QSize(16777215, 16777215));
        teOut->setBaseSize(QSize(500, 500));
        teOut->setAcceptDrops(false);
        teOut->setAutoFillBackground(false);
        teOut->setStyleSheet(QString::fromUtf8(""));
        teOut->setInputMethodHints(Qt::InputMethodHint::ImhNone);
        teOut->setFrameShape(QFrame::Shape::Box);
        teOut->setFrameShadow(QFrame::Shadow::Plain);
        teOut->setLineWidth(1);
        teOut->setMidLineWidth(0);
        teOut->setUndoRedoEnabled(false);
        teOut->setLineWrapMode(QTextEdit::LineWrapMode::FixedColumnWidth);
        teOut->setLineWrapColumnOrWidth(200);
        teOut->setReadOnly(false);
        teOut->setHtml(QString::fromUtf8("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><meta charset=\"utf-8\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"hr { height: 1px; border-width: 0; }\n"
"li.unchecked::marker { content: \"\\2610\"; }\n"
"li.checked::marker { content: \"\\2612\"; }\n"
"</style></head><body style=\" font-family:'Segoe UI'; font-size:9pt; font-weight:400; font-style:normal;\">\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-family:'MS Shell Dlg 2'; font-size:7.8pt;\"><br /></p></body></html>"));
        teOut->setOverwriteMode(true);
        teOut->setCursorWidth(1);
        teOut->setPlaceholderText(QString::fromUtf8(""));

        verticalLayout_3->addWidget(teOut);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName("horizontalLayout");
        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(0);
        verticalLayout->setObjectName("verticalLayout");
        lblQuality = new QLabel(centralWidget);
        lblQuality->setObjectName("lblQuality");
        lblQuality->setFrameShape(QFrame::Shape::Box);
        lblQuality->setLineWidth(0);
        lblQuality->setTextFormat(Qt::TextFormat::PlainText);
        lblQuality->setAlignment(Qt::AlignmentFlag::AlignLeading|Qt::AlignmentFlag::AlignLeft|Qt::AlignmentFlag::AlignVCenter);
        lblQuality->setWordWrap(false);
        lblQuality->setIndent(0);

        verticalLayout->addWidget(lblQuality);

        leQuality = new QLabel(centralWidget);
        leQuality->setObjectName("leQuality");
        leQuality->setInputMethodHints(Qt::InputMethodHint::ImhPreferNumbers);
        leQuality->setFrameShape(QFrame::Shape::Box);
        leQuality->setFrameShadow(QFrame::Shadow::Plain);
        leQuality->setLineWidth(0);
        leQuality->setTextFormat(Qt::TextFormat::PlainText);
        leQuality->setScaledContents(false);
        leQuality->setAlignment(Qt::AlignmentFlag::AlignLeading|Qt::AlignmentFlag::AlignLeft|Qt::AlignmentFlag::AlignVCenter);
        leQuality->setMargin(0);
        leQuality->setIndent(0);

        verticalLayout->addWidget(leQuality, 0, Qt::AlignmentFlag::Qt::AlignmentFlag::AlignVCenter);


        horizontalLayout->addLayout(verticalLayout);

        horizontalSpacer_4 = new QSpacerItem(12, 20, QSizePolicy::Policy::Fixed, QSizePolicy::Policy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(0);
        verticalLayout_2->setObjectName("verticalLayout_2");
        verticalLayout_2->setSizeConstraint(QLayout::SizeConstraint::SetDefaultConstraint);
        lblMethod = new QLabel(centralWidget);
        lblMethod->setObjectName("lblMethod");
        lblMethod->setLayoutDirection(Qt::LayoutDirection::LeftToRight);
        lblMethod->setAutoFillBackground(false);
        lblMethod->setFrameShape(QFrame::Shape::Box);
        lblMethod->setLineWidth(0);
        lblMethod->setTextFormat(Qt::TextFormat::PlainText);
        lblMethod->setIndent(0);

        verticalLayout_2->addWidget(lblMethod);

        leMethod = new QLabel(centralWidget);
        leMethod->setObjectName("leMethod");
        leMethod->setFrameShape(QFrame::Shape::Box);
        leMethod->setLineWidth(0);
        leMethod->setMargin(0);
        leMethod->setIndent(0);

        verticalLayout_2->addWidget(leMethod);


        horizontalLayout->addLayout(verticalLayout_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);


        verticalLayout_3->addLayout(horizontalLayout);

        lblQualityBitrate = new QLabel(centralWidget);
        lblQualityBitrate->setObjectName("lblQualityBitrate");
        lblQualityBitrate->setMinimumSize(QSize(60, 30));
        lblQualityBitrate->setFrameShape(QFrame::Shape::Box);
        lblQualityBitrate->setLineWidth(0);

        verticalLayout_3->addWidget(lblQualityBitrate);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName("horizontalLayout_4");
        comboBox = new QComboBox(centralWidget);
        comboBox->setObjectName("comboBox");
        sizePolicy.setHeightForWidth(comboBox->sizePolicy().hasHeightForWidth());
        comboBox->setSizePolicy(sizePolicy);
        comboBox->setMinimumSize(QSize(0, 30));
        comboBox->setMaximumSize(QSize(200, 16777215));
        comboBox->setFocusPolicy(Qt::FocusPolicy::NoFocus);
        comboBox->setSizeAdjustPolicy(QComboBox::SizeAdjustPolicy::AdjustToContents);
        comboBox->setMinimumContentsLength(10);
        comboBox->setFrame(true);

        horizontalLayout_4->addWidget(comboBox);

        comboResolution = new QComboBox(centralWidget);
        comboResolution->setObjectName("comboResolution");
        comboResolution->setEnabled(false);
        sizePolicy.setHeightForWidth(comboResolution->sizePolicy().hasHeightForWidth());
        comboResolution->setSizePolicy(sizePolicy);
        comboResolution->setMinimumSize(QSize(0, 30));
        comboResolution->setMaximumSize(QSize(300, 16777215));
        comboResolution->setAutoFillBackground(false);
        comboResolution->setSizeAdjustPolicy(QComboBox::SizeAdjustPolicy::AdjustToContents);
        comboResolution->setMinimumContentsLength(10);
        comboResolution->setFrame(true);

        horizontalLayout_4->addWidget(comboResolution);

        comboAmount = new QComboBox(centralWidget);
        comboAmount->setObjectName("comboAmount");
        comboAmount->setEnabled(false);
        sizePolicy.setHeightForWidth(comboAmount->sizePolicy().hasHeightForWidth());
        comboAmount->setSizePolicy(sizePolicy);
        comboAmount->setMinimumSize(QSize(0, 30));
        comboAmount->setMaximumSize(QSize(200, 16777215));
        comboAmount->setAutoFillBackground(false);
        comboAmount->setSizeAdjustPolicy(QComboBox::SizeAdjustPolicy::AdjustToContents);
        comboAmount->setMinimumContentsLength(7);
        comboAmount->setFrame(true);

        horizontalLayout_4->addWidget(comboAmount);

        comboPayTV = new QComboBox(centralWidget);
        comboPayTV->setObjectName("comboPayTV");
        sizePolicy.setHeightForWidth(comboPayTV->sizePolicy().hasHeightForWidth());
        comboPayTV->setSizePolicy(sizePolicy);
        comboPayTV->setMinimumSize(QSize(0, 30));
        comboPayTV->setMaximumSize(QSize(200, 16777215));
        comboPayTV->setSizeAdjustPolicy(QComboBox::SizeAdjustPolicy::AdjustToContents);
        comboPayTV->setMinimumContentsLength(13);
        comboPayTV->setFrame(true);

        horizontalLayout_4->addWidget(comboPayTV);

        pbPassword = new QPushButton(centralWidget);
        pbPassword->setObjectName("pbPassword");
        sizePolicy.setHeightForWidth(pbPassword->sizePolicy().hasHeightForWidth());
        pbPassword->setSizePolicy(sizePolicy);
        pbPassword->setMinimumSize(QSize(0, 30));
        pbPassword->setMaximumSize(QSize(16777215, 16777215));
        pbPassword->setIcon(icon18);
        pbPassword->setFlat(false);

        horizontalLayout_4->addWidget(pbPassword);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout_3->addLayout(horizontalLayout_4);

        progressBar = new QProgressBar(centralWidget);
        progressBar->setObjectName("progressBar");
        QSizePolicy sizePolicy5(QSizePolicy::Policy::Expanding, QSizePolicy::Policy::Fixed);
        sizePolicy5.setHorizontalStretch(10);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(progressBar->sizePolicy().hasHeightForWidth());
        progressBar->setSizePolicy(sizePolicy5);
        progressBar->setMinimumSize(QSize(307, 0));
        progressBar->setMaximum(100);
        progressBar->setValue(0);
        progressBar->setTextVisible(false);
        progressBar->setOrientation(Qt::Orientation::Horizontal);

        verticalLayout_3->addWidget(progressBar);

        newprg->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(newprg);
        menuBar->setObjectName("menuBar");
        menuBar->setGeometry(QRect(0, 0, 988, 22));
        sizePolicy.setHeightForWidth(menuBar->sizePolicy().hasHeightForWidth());
        menuBar->setSizePolicy(sizePolicy);
        menuBar->setDefaultUp(false);
        menuBar->setNativeMenuBar(true);
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName("menuFile");
        menuLanguage = new QMenu(menuBar);
        menuLanguage->setObjectName("menuLanguage");
        menuTools = new QMenu(menuBar);
        menuTools->setObjectName("menuTools");
        menuTools->setGeometry(QRect(2684, 119, 337, 519));
        sizePolicy.setHeightForWidth(menuTools->sizePolicy().hasHeightForWidth());
        menuTools->setSizePolicy(sizePolicy);
        menuTools->setMinimumSize(QSize(0, 0));
        menuTools->setSizeIncrement(QSize(0, 0));
        menuTools->setBaseSize(QSize(0, 0));
        menuTools->setAutoFillBackground(false);
        menuCheckForUpdates = new QMenu(menuTools);
        menuCheckForUpdates->setObjectName("menuCheckForUpdates");
        menuCheckForUpdates->setIcon(icon28);
        menuEditSettingsAdvanced = new QMenu(menuTools);
        menuEditSettingsAdvanced->setObjectName("menuEditSettingsAdvanced");
        menuEditSettingsAdvanced->setIcon(icon31);
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName("menuHelp");
        menuRecent = new QMenu(menuBar);
        menuRecent->setObjectName("menuRecent");
        menuDownloadList = new QMenu(menuBar);
        menuDownloadList->setObjectName("menuDownloadList");
        menuPayTV = new QMenu(menuBar);
        menuPayTV->setObjectName("menuPayTV");
        menuDownload_all_Episodes = new QMenu(menuBar);
        menuDownload_all_Episodes->setObjectName("menuDownload_all_Episodes");
        menuStCookies = new QMenu(menuBar);
        menuStCookies->setObjectName("menuStCookies");
        menuView = new QMenu(menuBar);
        menuView->setObjectName("menuView");
        menuTV4 = new QMenu(menuBar);
        menuTV4->setObjectName("menuTV4");
        menuSvtplayDl = new QMenu(menuBar);
        menuSvtplayDl->setObjectName("menuSvtplayDl");
        menuSubtitles = new QMenu(menuBar);
        menuSubtitles->setObjectName("menuSubtitles");
        menuSubtitles->setToolTipsVisible(false);
        newprg->setMenuBar(menuBar);
        statusBar = new QStatusBar(newprg);
        statusBar->setObjectName("statusBar");
        newprg->setStatusBar(statusBar);
#if QT_CONFIG(shortcut)
        lblQualityBitrate->setBuddy(comboBox);
#endif // QT_CONFIG(shortcut)
        QWidget::setTabOrder(pbPast, pbSok);
        QWidget::setTabOrder(pbSok, leSok);
        QWidget::setTabOrder(leSok, teOut);
        QWidget::setTabOrder(teOut, pbAdd);
        QWidget::setTabOrder(pbAdd, comboPayTV);
        QWidget::setTabOrder(comboPayTV, pbPassword);

        menuBar->addAction(menuFile->menuAction());
        menuBar->addAction(menuRecent->menuAction());
        menuBar->addAction(menuDownloadList->menuAction());
        menuBar->addAction(menuLanguage->menuAction());
        menuBar->addAction(menuView->menuAction());
        menuBar->addAction(menuSvtplayDl->menuAction());
        menuBar->addAction(menuSubtitles->menuAction());
        menuBar->addAction(menuTools->menuAction());
        menuBar->addAction(menuPayTV->menuAction());
        menuBar->addAction(menuTV4->menuAction());
        menuBar->addAction(menuStCookies->menuAction());
        menuBar->addAction(menuDownload_all_Episodes->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFile->addAction(actionOpenFolder);
        menuFile->addAction(actionPast);
        menuFile->addAction(actionSearch);
        menuFile->addAction(actionNfoInfo);
        menuFile->addAction(actionDownload);
        menuFile->addSeparator();
        menuFile->addSeparator();
        menuFile->addAction(actionStopAllDownloads);
        menuFile->addAction(actionExit);
        menuLanguage->addAction(actionEnglish);
        menuLanguage->addAction(actionItalian);
        menuLanguage->addAction(actionSwedish);
        menuLanguage->addAction(actionPreCheckLanguagefile);
        menuTools->addAction(menuCheckForUpdates->menuAction());
        menuTools->addAction(actionSelectFileName);
        menuTools->addAction(actionCreateFolder);
        menuTools->addAction(actionShowMore);
        menuTools->addSeparator();
        menuTools->addAction(actionSetDefaultDownloadLocation);
        menuTools->addAction(actionDownloadToDefaultLocation);
        menuTools->addSeparator();
        menuTools->addAction(actionSetDefaultCopyLocation);
        menuTools->addAction(actionCopyToDefaultLocation);
        menuTools->addSeparator();
        menuTools->addSeparator();
        menuTools->addSeparator();
        menuTools->addAction(action4k);
        menuTools->addSeparator();
        menuTools->addAction(actionNotifications);
        menuTools->addSeparator();
        menuTools->addAction(actionDesktopShortcut);
        menuTools->addAction(actionApplicationsMenuShortcut);
        menuTools->addSeparator();
        menuTools->addAction(actionSelectFont);
        menuTools->addSeparator();
        menuTools->addAction(actionDontUseNativeDialogs);
        menuTools->addAction(actionMaintenanceTool);
        menuTools->addAction(menuEditSettingsAdvanced->menuAction());
        menuTools->addAction(actionDeleteAllSettings);
        menuTools->addAction(actionDownloadMicrosoftRuntime);
        menuCheckForUpdates->addAction(actionCheckOnStart);
        menuCheckForUpdates->addAction(actionCheckSvtplayDlForUpdatesAtStart);
        menuEditSettingsAdvanced->addAction(actionStreamCapture2Settings);
        menuEditSettingsAdvanced->addAction(actionDownloadSvtplayDlSettings);
        menuHelp->addAction(actionHelp);
        menuHelp->addAction(actionImportant);
        menuHelp->addAction(actionCheckForUpdates);
        menuHelp->addAction(actionCheckLatestsvtplaydl);
        menuHelp->addAction(actionVersionHistory);
        menuHelp->addAction(actionAbout);
        menuHelp->addAction(actionAboutFfmpeg);
        menuHelp->addAction(actionAboutSvtplayDl);
        menuHelp->addAction(actionAboutQt);
        menuHelp->addAction(actionSvtplayDlForum);
        menuHelp->addAction(actionLicense);
        menuHelp->addAction(actionLicenseFfmpeg);
        menuHelp->addAction(actionLicenseSvtplayDl);
        menuHelp->addAction(actionLicense7zip);
        menuHelp->addSeparator();
        menuDownloadList->addAction(actionAdd);
        menuDownloadList->addAction(actionDownloadAll);
        menuDownloadList->addAction(actionViewDownloadList);
        menuDownloadList->addAction(actionEditDownloadList);
        menuDownloadList->addAction(actionDeleteDownloadList);
        menuDownloadList->addAction(actionDownloadList);
        menuDownloadList->addAction(actionImportDownloadList);
        menuDownloadList->addAction(actionExportDownloadList);
        menuPayTV->addAction(actionPassword);
        menuPayTV->addSeparator();
        menuPayTV->addAction(actionCreateNew);
        menuDownload_all_Episodes->addAction(actionListAllEpisodes);
        menuDownload_all_Episodes->addAction(actionAddAllEpisodesToDownloadList);
        menuDownload_all_Episodes->addAction(actionDownloadAllEpisodes);
        menuView->addAction(actionIn);
        menuView->addAction(actionDefault);
        menuView->addAction(actionOut);
        menuView->addSeparator();
        menuView->addAction(actionDarkFusionStyle);
        menuView->addAction(actionFusionStyle);
        menuView->addAction(actionDefaultStyle);
        menuTV4->addAction(actionTVToken);
        menuTV4->addAction(actionHowTo);
        menuTV4->addAction(actionHowToVideo);
        menuSvtplayDl->addAction(actionSvtplayDlSystem);
        menuSvtplayDl->addAction(actionSvtplayDlStable);
        menuSvtplayDl->addAction(actionSvtplayDlBleedingEdge);
        menuSvtplayDl->addSeparator();
        menuSvtplayDl->addAction(actionSvtPlayDlManuallySelect);
        menuSvtplayDl->addAction(actionSvtPlayDlManuallySelected);
        menuSvtplayDl->addSeparator();
        menuSvtplayDl->addAction(actionDownloadSvtplayDlFromBinCeicerCom);
        menuSubtitles->addAction(actionNoSubtitles_2);
        menuSubtitles->addAction(actionSubtitle_2);
        menuSubtitles->addAction(actionMergeSubtitle_2);
        menuSubtitles->addAction(actionAllSubtitles_2);
        menuSubtitles->addAction(actionDownloadRaw_2);

        retranslateUi(newprg);

        pbPassword->setDefault(false);


        QMetaObject::connectSlotsByName(newprg);
    } // setupUi

    void retranslateUi(QMainWindow *newprg)
    {
        newprg->setWindowTitle(QCoreApplication::translate("newprg", "TEST", nullptr));
        actionEnglish->setText(QCoreApplication::translate("newprg", "English", nullptr));
        actionSwedish->setText(QCoreApplication::translate("newprg", "Swedish", nullptr));
        actionAbout->setText(QCoreApplication::translate("newprg", "About...", nullptr));
        actionCheckForUpdates->setText(QCoreApplication::translate("newprg", "Check for updates...", nullptr));
        actionExit->setText(QCoreApplication::translate("newprg", "Exit", nullptr));
#if QT_CONFIG(statustip)
        actionExit->setStatusTip(QCoreApplication::translate("newprg", "Exits the program.", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(shortcut)
        actionExit->setShortcut(QCoreApplication::translate("newprg", "F4", nullptr));
#endif // QT_CONFIG(shortcut)
        actionAboutSvtplayDl->setText(QCoreApplication::translate("newprg", "About svtplay-dl...", nullptr));
        actionAboutFfmpeg->setText(QCoreApplication::translate("newprg", "About FFmpeg...", nullptr));
        actionSearch->setText(QCoreApplication::translate("newprg", "Search", nullptr));
#if QT_CONFIG(statustip)
        actionSearch->setStatusTip(QCoreApplication::translate("newprg", "Search for video streams.", nullptr));
#endif // QT_CONFIG(statustip)
        actionPast->setText(QCoreApplication::translate("newprg", "Paste", nullptr));
#if QT_CONFIG(statustip)
        actionPast->setStatusTip(QCoreApplication::translate("newprg", "Paste the link to the page where the video is displayed.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownload->setText(QCoreApplication::translate("newprg", "Download", nullptr));
#if QT_CONFIG(statustip)
        actionDownload->setStatusTip(QCoreApplication::translate("newprg", "Download the stream you just searched for.", nullptr));
#endif // QT_CONFIG(statustip)
        actionLicense->setText(QCoreApplication::translate("newprg", "License streamCapture2...", nullptr));
        actionLicenseSvtplayDl->setText(QCoreApplication::translate("newprg", "License svtplay-dl...", nullptr));
        actionLicenseFfmpeg->setText(QCoreApplication::translate("newprg", "License FFmpeg...", nullptr));
        actionRecentFiles->setText(QCoreApplication::translate("newprg", "Recent files", nullptr));
        actionHelp->setText(QCoreApplication::translate("newprg", "Help...", nullptr));
        actionViewDownloadList->setText(QCoreApplication::translate("newprg", "View Download List", nullptr));
#if QT_CONFIG(statustip)
        actionViewDownloadList->setStatusTip(QCoreApplication::translate("newprg", "Look at the list of all the streams to download.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDeleteDownloadList->setText(QCoreApplication::translate("newprg", "Delete Download List", nullptr));
#if QT_CONFIG(statustip)
        actionDeleteDownloadList->setStatusTip(QCoreApplication::translate("newprg", "All saved streams in the download list are deleted.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDeleteDownloadList_menu->setText(QCoreApplication::translate("newprg", "Delete download list", nullptr));
        actionItalian->setText(QCoreApplication::translate("newprg", "Italian", nullptr));
        actionVersionHistory->setText(QCoreApplication::translate("newprg", "Version history...", nullptr));
        actionCreateNew->setText(QCoreApplication::translate("newprg", "Create new user", nullptr));
#if QT_CONFIG(statustip)
        actionCreateNew->setStatusTip(QCoreApplication::translate("newprg", "Save the name of a video stream provider, your username and, if you want, your password.", nullptr));
#endif // QT_CONFIG(statustip)
        actionPasswordX->setText(QCoreApplication::translate("newprg", "Password", nullptr));
#if QT_CONFIG(statustip)
        actionPasswordX->setStatusTip(QCoreApplication::translate("newprg", "If no saved password is found, click here.", nullptr));
#endif // QT_CONFIG(statustip)
        actionVerboseOutput->setText(QCoreApplication::translate("newprg", "Explain what is going on", nullptr));
        actionAdd->setText(QCoreApplication::translate("newprg", "Add to Download List", nullptr));
#if QT_CONFIG(statustip)
        actionAdd->setStatusTip(QCoreApplication::translate("newprg", "Add current video to the list of streams that will be downloaded.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownloadAll->setText(QCoreApplication::translate("newprg", "Download all on the list", nullptr));
#if QT_CONFIG(statustip)
        actionDownloadAll->setStatusTip(QCoreApplication::translate("newprg", "Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.", nullptr));
#endif // QT_CONFIG(statustip)
        actionCreateFolder->setText(QCoreApplication::translate("newprg", "Create folder \"method_quality_amount_resolution\"", nullptr));
#if QT_CONFIG(tooltip)
        actionCreateFolder->setToolTip(QCoreApplication::translate("newprg", "Create folder \"method_quality_amount\"", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        actionCreateFolder->setStatusTip(QCoreApplication::translate("newprg", "Automatically creates a folder for each downloaded video stream. If you use \"Direct download of all...\" no folders are ever created.", nullptr));
#endif // QT_CONFIG(statustip)
        actionEditDownloadList->setText(QCoreApplication::translate("newprg", "Edit Download List (Advanced)", nullptr));
#if QT_CONFIG(statustip)
        actionEditDownloadList->setStatusTip(QCoreApplication::translate("newprg", "Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.", nullptr));
#endif // QT_CONFIG(statustip)
        actionShowMore->setText(QCoreApplication::translate("newprg", "Show more", nullptr));
#if QT_CONFIG(statustip)
        actionShowMore->setStatusTip(QCoreApplication::translate("newprg", "View more information from svtplay-dl. Appears in purple text color.", nullptr));
#endif // QT_CONFIG(statustip)
        actionPassword->setText(QCoreApplication::translate("newprg", "Password", nullptr));
#if QT_CONFIG(statustip)
        actionPassword->setStatusTip(QCoreApplication::translate("newprg", "If no saved password is found, click here.", nullptr));
#endif // QT_CONFIG(statustip)
        actionUninstall_streamCapture->setText(QCoreApplication::translate("newprg", "Uninstall streamCapture", nullptr));
#if QT_CONFIG(statustip)
        actionUninstall_streamCapture->setStatusTip(QCoreApplication::translate("newprg", "Uninstall and remove all components", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownloadAllEpisodes->setText(QCoreApplication::translate("newprg", "Direct Download of all Episodes", nullptr));
#if QT_CONFIG(statustip)
        actionDownloadAllEpisodes->setStatusTip(QCoreApplication::translate("newprg", "Trying to immediately download all episodes. Unable to create folders or select quality.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownloadAfterDate->setText(QCoreApplication::translate("newprg", "Download after Date...", nullptr));
        actionStopAllDownloads->setText(QCoreApplication::translate("newprg", "Stop all downloads", nullptr));
#if QT_CONFIG(statustip)
        actionStopAllDownloads->setStatusTip(QCoreApplication::translate("newprg", "Trying to stop svtplay-dl.", nullptr));
#endif // QT_CONFIG(statustip)
        actionListAllEpisodes->setText(QCoreApplication::translate("newprg", "List all Episodes", nullptr));
#if QT_CONFIG(statustip)
        actionListAllEpisodes->setStatusTip(QCoreApplication::translate("newprg", "Looking for video streams for all episodes.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDeleteAllSettings2->setText(QCoreApplication::translate("newprg", "Delete all settings and Exit", nullptr));
#if QT_CONFIG(statustip)
        actionDeleteAllSettings2->setStatusTip(QCoreApplication::translate("newprg", "All saved searches and the list of streams to be downloaded will be deleted.", nullptr));
#endif // QT_CONFIG(statustip)
        actionCopyToDefaultLocation->setText(QCoreApplication::translate("newprg", "Copy to Selected Location", nullptr));
#if QT_CONFIG(statustip)
        actionCopyToDefaultLocation->setStatusTip(QCoreApplication::translate("newprg", "Direct copy to the default copy location.", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(whatsthis)
        actionCopyToDefaultLocation->setWhatsThis(QString());
#endif // QT_CONFIG(whatsthis)
        actionSetDefaultCopyLocation->setText(QCoreApplication::translate("newprg", "Select Copy Location...", nullptr));
#if QT_CONFIG(statustip)
        actionSetDefaultCopyLocation->setStatusTip(QCoreApplication::translate("newprg", "Save the location where the finished video file is copied. If you use \"Direct download of all...\" no files are ever copied.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSetDefaultDownloadLocation->setText(QCoreApplication::translate("newprg", "Select Default Download Location...", nullptr));
#if QT_CONFIG(statustip)
        actionSetDefaultDownloadLocation->setStatusTip(QCoreApplication::translate("newprg", "Save the location for direct download.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownloadToDefaultLocation->setText(QCoreApplication::translate("newprg", "Download to Default Location", nullptr));
#if QT_CONFIG(statustip)
        actionDownloadToDefaultLocation->setStatusTip(QCoreApplication::translate("newprg", "Direct download to the default location.", nullptr));
#endif // QT_CONFIG(statustip)
        actionAddAllEpisodesToDownloadList->setText(QCoreApplication::translate("newprg", "Add all Episodes to Download List", nullptr));
#if QT_CONFIG(statustip)
        actionAddAllEpisodesToDownloadList->setStatusTip(QCoreApplication::translate("newprg", "Adds all episodes to the download list.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSelectFont->setText(QCoreApplication::translate("newprg", "Select font...", nullptr));
        actionSvtplayDlForum->setText(QCoreApplication::translate("newprg", "Visit svtplay-dl forum for issues...", nullptr));
        actionMaintenanceTool->setText(QCoreApplication::translate("newprg", "Maintenance Tool...", nullptr));
#if QT_CONFIG(statustip)
        actionMaintenanceTool->setStatusTip(QCoreApplication::translate("newprg", "Starts the Maintenance Tool. To update or uninstall.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDeleteAllSettings->setText(QCoreApplication::translate("newprg", "Delete all settings and Exit", nullptr));
#if QT_CONFIG(statustip)
        actionDeleteAllSettings->setStatusTip(QCoreApplication::translate("newprg", "All saved searches, download list and settings are deleted.", nullptr));
#endif // QT_CONFIG(statustip)
        actionNotifications->setText(QCoreApplication::translate("newprg", "Don't show notifications", nullptr));
#if QT_CONFIG(statustip)
        actionNotifications->setStatusTip(QCoreApplication::translate("newprg", "Do not show notifications when the download is complete.", nullptr));
#endif // QT_CONFIG(statustip)
        actionCreateDestopShortcut->setText(QCoreApplication::translate("newprg", "Create a shortcut", nullptr));
        actionDesktopShortcut->setText(QCoreApplication::translate("newprg", "Desktop Shortcut", nullptr));
#if QT_CONFIG(statustip)
        actionDesktopShortcut->setStatusTip(QCoreApplication::translate("newprg", "Create a shortcut to streamCapture2 on the desktop.", nullptr));
#endif // QT_CONFIG(statustip)
        actionApplicationsMenuShortcut->setText(QCoreApplication::translate("newprg", "Applications menu Shortcut", nullptr));
#if QT_CONFIG(statustip)
        actionApplicationsMenuShortcut->setStatusTip(QCoreApplication::translate("newprg", "Create a shortcut to streamCapture2 in the operating system menu.", nullptr));
#endif // QT_CONFIG(statustip)
        actionPreCheckLanguagefile->setText(QCoreApplication::translate("newprg", "Load external language file...", nullptr));
#if QT_CONFIG(statustip)
        actionPreCheckLanguagefile->setStatusTip(QCoreApplication::translate("newprg", "Useful when testing your own translation.", nullptr));
#endif // QT_CONFIG(statustip)
        actionStCookie->setText(QCoreApplication::translate("newprg", "Set new 'st' cookie", nullptr));
        actionIn->setText(QCoreApplication::translate("newprg", "Zoom In", nullptr));
#if QT_CONFIG(statustip)
        actionIn->setStatusTip(QCoreApplication::translate("newprg", "Increase the font size.", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(shortcut)
        actionIn->setShortcut(QCoreApplication::translate("newprg", "Ctrl++", nullptr));
#endif // QT_CONFIG(shortcut)
        actionDefault->setText(QCoreApplication::translate("newprg", "Zoom Default", nullptr));
#if QT_CONFIG(shortcut)
        actionDefault->setShortcut(QCoreApplication::translate("newprg", "Ctrl+0", nullptr));
#endif // QT_CONFIG(shortcut)
        actionOut->setText(QCoreApplication::translate("newprg", "Zoom Out", nullptr));
#if QT_CONFIG(statustip)
        actionOut->setStatusTip(QCoreApplication::translate("newprg", "Decrease the font size.", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(shortcut)
        actionOut->setShortcut(QCoreApplication::translate("newprg", "Ctrl+-", nullptr));
#endif // QT_CONFIG(shortcut)
        actionCheckLatestsvtplaydl->setText(QCoreApplication::translate("newprg", "Check for the latest svtplay-dl from bin.ceicer.com...", nullptr));
        actionDownloadMicrosoftRuntime->setText(QCoreApplication::translate("newprg", "Download Microsoft runtime (required for svtplay-dl)...", nullptr));
        actionDownloadMicrosoftRuntime->setIconText(QCoreApplication::translate("newprg", "Download Microsoft runtime (required for svtplay-dl)...", nullptr));
#if QT_CONFIG(tooltip)
        actionDownloadMicrosoftRuntime->setToolTip(QCoreApplication::translate("newprg", "Download Microsoft runtime (required for svtplay-dl)...", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        actionDownloadMicrosoftRuntime->setStatusTip(QCoreApplication::translate("newprg", "Download the runtime file from bin.ceicer.com.", nullptr));
#endif // QT_CONFIG(statustip)
        actionLicense7zip->setText(QCoreApplication::translate("newprg", "License 7-Zip...", nullptr));
        actionCheckOnStart->setText(QCoreApplication::translate("newprg", "Check streamCapture2 for updates at start", nullptr));
        actionCheckSvtplayDlForUpdatesAtStart->setText(QCoreApplication::translate("newprg", "Check for new versions of svtplay-dl at start", nullptr));
        actionStreamCapture2Settings->setText(QCoreApplication::translate("newprg", "streamCapture2 settings", nullptr));
        actionDownloadSvtplayDlSettings->setText(QCoreApplication::translate("newprg", "download svtplay-dl settings", nullptr));
        actionNfoInfo->setText(QCoreApplication::translate("newprg", "NFO info", nullptr));
#if QT_CONFIG(statustip)
        actionNfoInfo->setStatusTip(QCoreApplication::translate("newprg", "NFO files contain media release information. Available at svtplay.se, among other places.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSelectFileName->setText(QCoreApplication::translate("newprg", "Select file name", nullptr));
#if QT_CONFIG(statustip)
        actionSelectFileName->setStatusTip(QCoreApplication::translate("newprg", "You choose the name of the downloaded video file.", nullptr));
#endif // QT_CONFIG(statustip)
        actionAboutQt->setText(QCoreApplication::translate("newprg", "About Qt...", nullptr));
        actionTVToken->setText(QCoreApplication::translate("newprg", "Set TV4 Token", nullptr));
        actionHowTo->setText(QCoreApplication::translate("newprg", "How-to", nullptr));
        actionHowToVideo->setText(QCoreApplication::translate("newprg", "How-to Video", nullptr));
        action4k->setText(QCoreApplication::translate("newprg", "Dolby Vision 4K", nullptr));
#if QT_CONFIG(statustip)
        action4k->setStatusTip(QCoreApplication::translate("newprg", "If Dolby Vision 4K video streams are not found, the download will fail.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSubtitle->setText(QCoreApplication::translate("newprg", "Download subtitles", nullptr));
#if QT_CONFIG(statustip)
        actionSubtitle->setStatusTip(QCoreApplication::translate("newprg", "The subtitle is saved in a text file (*.srt).", nullptr));
#endif // QT_CONFIG(statustip)
        actionMergeSubtitle->setText(QCoreApplication::translate("newprg", "Download subtitles and try to merge with the video file", nullptr));
#if QT_CONFIG(statustip)
        actionMergeSubtitle->setStatusTip(QCoreApplication::translate("newprg", "If embedding in the video file does not work, try using the text file with the subtitle.", nullptr));
#endif // QT_CONFIG(statustip)
        actionNoSubtitles->setText(QCoreApplication::translate("newprg", "No subtitles", nullptr));
        actionImportant->setText(QCoreApplication::translate("newprg", "If the download does not work...", nullptr));
        actionLightTheme->setText(QCoreApplication::translate("newprg", "Light Theme", nullptr));
        actionDarkFusionStyle->setText(QCoreApplication::translate("newprg", "Dark Fusion Style", nullptr));
        actionOpenFolder->setText(QCoreApplication::translate("newprg", "Open...", nullptr));
        actionSvtplayDlSystem->setText(QCoreApplication::translate("newprg", "Use svtplay-dl from the system path", nullptr));
#if QT_CONFIG(statustip)
        actionSvtplayDlSystem->setStatusTip(QCoreApplication::translate("newprg", "Uses (if available) svtplay-dl in the system path.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSvtplayDlStable->setText(QCoreApplication::translate("newprg", "Use svtplay-dl stable", nullptr));
#if QT_CONFIG(statustip)
        actionSvtplayDlStable->setStatusTip(QCoreApplication::translate("newprg", "Note that the latest stable version may be newer than the latest beta version. Check version by clicking \"Help\" -> \"About svtplay-dl...\"", nullptr));
#endif // QT_CONFIG(statustip)
        actionSvtplayDlBleedingEdge->setText(QCoreApplication::translate("newprg", "Use svtplay-dl beta", nullptr));
#if QT_CONFIG(statustip)
        actionSvtplayDlBleedingEdge->setStatusTip(QCoreApplication::translate("newprg", "Note that the latest stable version may be newer than the latest beta version. Check version by clicking \"Help\" -> \"About svtplay-dl...\"", nullptr));
#endif // QT_CONFIG(statustip)
        actionSvtPlayDlManuallySelect->setText(QCoreApplication::translate("newprg", "Select svtplay-dl...", nullptr));
#if QT_CONFIG(statustip)
        actionSvtPlayDlManuallySelect->setStatusTip(QCoreApplication::translate("newprg", "Select svtplay-dl that you have in your computer.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSvtPlayDlManuallySelected->setText(QCoreApplication::translate("newprg", "Use the selected svtplay-dl", nullptr));
#if QT_CONFIG(statustip)
        actionSvtPlayDlManuallySelected->setStatusTip(QCoreApplication::translate("newprg", "Use svtplay-dl that you selected in your computer.", nullptr));
#endif // QT_CONFIG(statustip)
        actionDownloadSvtplayDlFromBinCeicerCom->setText(QCoreApplication::translate("newprg", "Download svtplay-dl...", nullptr));
#if QT_CONFIG(statustip)
        actionDownloadSvtplayDlFromBinCeicerCom->setStatusTip(QCoreApplication::translate("newprg", "Download and decompress svtplay-dl from bin.ceicer.com", nullptr));
#endif // QT_CONFIG(statustip)
        actionAllSubtitles->setText(QCoreApplication::translate("newprg", "Download all subtitles", nullptr));
        actionNoSubtitles_2->setText(QCoreApplication::translate("newprg", "No subtitles", nullptr));
        actionMergeSubtitle_2->setText(QCoreApplication::translate("newprg", "Merge subtitle", nullptr));
#if QT_CONFIG(statustip)
        actionMergeSubtitle_2->setStatusTip(QCoreApplication::translate("newprg", "If embedding in the video file does not work, try using the text file with the subtitle.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSubtitle_2->setText(QCoreApplication::translate("newprg", "Download subtitle", nullptr));
#if QT_CONFIG(statustip)
        actionSubtitle_2->setStatusTip(QCoreApplication::translate("newprg", "The subtitle is saved in a text file (*.srt).", nullptr));
#endif // QT_CONFIG(statustip)
        actionAllSubtitles_2->setText(QCoreApplication::translate("newprg", "Download all subtitles", nullptr));
        actionDownloadRaw_2->setText(QCoreApplication::translate("newprg", "Download the subtitles in their native format", nullptr));
        actionDontUseNativeDialogs->setText(QCoreApplication::translate("newprg", "Don't Use Native Dialogs", nullptr));
#if QT_CONFIG(statustip)
        actionDontUseNativeDialogs->setStatusTip(QCoreApplication::translate("newprg", "Do not use the operating system's file dialog.", nullptr));
#endif // QT_CONFIG(statustip)
        actionSystemDefaultTheme->setText(QCoreApplication::translate("newprg", "System Default Theme (Light or Dark)", nullptr));
        actionFusionStyle->setText(QCoreApplication::translate("newprg", "Fusion Style", nullptr));
        actionDefaultStyle->setText(QCoreApplication::translate("newprg", "Default Style", nullptr));
        actionExportDownloadList->setText(QCoreApplication::translate("newprg", "Export download list...", nullptr));
        actionImportDownloadList->setText(QCoreApplication::translate("newprg", "Import download list...", nullptr));
        actionDownloadList->setText(QCoreApplication::translate("newprg", "New downloadlist...", nullptr));
#if QT_CONFIG(tooltip)
        pbPast->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        pbPast->setStatusTip(QCoreApplication::translate("newprg", "Paste the link to the page where the video is displayed", nullptr));
#endif // QT_CONFIG(statustip)
        pbPast->setText(QCoreApplication::translate("newprg", "Paste", nullptr));
        leSok->setPlaceholderText(QCoreApplication::translate("newprg", "https://", nullptr));
#if QT_CONFIG(tooltip)
        pbSok->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        pbSok->setStatusTip(QCoreApplication::translate("newprg", "Search for video streams.", nullptr));
#endif // QT_CONFIG(statustip)
        pbSok->setText(QCoreApplication::translate("newprg", "Search", nullptr));
#if QT_CONFIG(tooltip)
        pbDownload->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        pbDownload->setStatusTip(QCoreApplication::translate("newprg", "Download the file you just searched for.", nullptr));
#endif // QT_CONFIG(statustip)
        pbDownload->setText(QCoreApplication::translate("newprg", "Download", nullptr));
#if QT_CONFIG(tooltip)
        pbAdd->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        pbAdd->setStatusTip(QCoreApplication::translate("newprg", "Add current video to the list of files that will be downloaded.", nullptr));
#endif // QT_CONFIG(statustip)
        pbAdd->setText(QCoreApplication::translate("newprg", "Add to Download List", nullptr));
#if QT_CONFIG(statustip)
        pbDownloadAll->setStatusTip(QCoreApplication::translate("newprg", "Download all files you added to the list.", nullptr));
#endif // QT_CONFIG(statustip)
        pbDownloadAll->setText(QCoreApplication::translate("newprg", "Download all on the list", nullptr));
#if QT_CONFIG(statustip)
        chb4k->setStatusTip(QCoreApplication::translate("newprg", "If Dolby Vision 4K video streams are not found, the download will probably fail.", nullptr));
#endif // QT_CONFIG(statustip)
        chb4k->setText(QCoreApplication::translate("newprg", "Dolby Vision 4K", nullptr));
#if QT_CONFIG(statustip)
        lblQuality->setStatusTip(QCoreApplication::translate("newprg", "The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..", nullptr));
#endif // QT_CONFIG(statustip)
        lblQuality->setText(QCoreApplication::translate("newprg", "Quality (Bitrate)", nullptr));
        leQuality->setText(QString());
#if QT_CONFIG(statustip)
        lblMethod->setStatusTip(QCoreApplication::translate("newprg", "Media streaming communications protocol.", nullptr));
#endif // QT_CONFIG(statustip)
        lblMethod->setText(QCoreApplication::translate("newprg", "Method", nullptr));
        leMethod->setText(QString());
#if QT_CONFIG(statustip)
        lblQualityBitrate->setStatusTip(QCoreApplication::translate("newprg", "Select quality on the video you download", nullptr));
#endif // QT_CONFIG(statustip)
        lblQualityBitrate->setText(QCoreApplication::translate("newprg", "Quality (bitrate) and method. Higher bitrate gives better quality and larger file.", nullptr));
#if QT_CONFIG(tooltip)
        comboBox->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        comboBox->setStatusTip(QCoreApplication::translate("newprg", "Select quality on the video you download.", nullptr));
#endif // QT_CONFIG(statustip)
        comboBox->setCurrentText(QString());
#if QT_CONFIG(statustip)
        comboResolution->setStatusTip(QCoreApplication::translate("newprg", "Video resolution: 480p=640x480, 720p=1280x720, 1080p=1920x1080", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(statustip)
        comboAmount->setStatusTip(QCoreApplication::translate("newprg", "Allows given quality to differ by an amount. 300 usually works well. (Bit rate +/- 300).", nullptr));
#endif // QT_CONFIG(statustip)
        comboAmount->setCurrentText(QString());
#if QT_CONFIG(tooltip)
        comboPayTV->setToolTip(QString());
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        comboPayTV->setStatusTip(QCoreApplication::translate("newprg", "Select provider. If yoy need a password.", nullptr));
#endif // QT_CONFIG(statustip)
#if QT_CONFIG(statustip)
        pbPassword->setStatusTip(QCoreApplication::translate("newprg", "If no saved password is found, click here.", nullptr));
#endif // QT_CONFIG(statustip)
        pbPassword->setText(QCoreApplication::translate("newprg", "Password", nullptr));
        menuFile->setTitle(QCoreApplication::translate("newprg", "&File", nullptr));
        menuLanguage->setTitle(QCoreApplication::translate("newprg", "&Language", nullptr));
        menuTools->setTitle(QCoreApplication::translate("newprg", "&Tools", nullptr));
        menuCheckForUpdates->setTitle(QCoreApplication::translate("newprg", "Check for updates at program start", nullptr));
        menuEditSettingsAdvanced->setTitle(QCoreApplication::translate("newprg", "Edit settings (Advanced)", nullptr));
        menuHelp->setTitle(QCoreApplication::translate("newprg", "&Help", nullptr));
        menuRecent->setTitle(QCoreApplication::translate("newprg", "&Recent", nullptr));
        menuDownloadList->setTitle(QCoreApplication::translate("newprg", "&Download List", nullptr));
        menuPayTV->setTitle(QCoreApplication::translate("newprg", "L&ogin", nullptr));
        menuDownload_all_Episodes->setTitle(QCoreApplication::translate("newprg", "&All Episodes", nullptr));
        menuStCookies->setTitle(QCoreApplication::translate("newprg", "'st' &cookies", nullptr));
        menuView->setTitle(QCoreApplication::translate("newprg", "&View", nullptr));
        menuTV4->setTitle(QCoreApplication::translate("newprg", "TV&4", nullptr));
        menuSvtplayDl->setTitle(QCoreApplication::translate("newprg", "&svtplay-dl", nullptr));
#if QT_CONFIG(tooltip)
        menuSubtitles->setToolTip(QCoreApplication::translate("newprg", "The download will fail if no subtitles are found. You can try different menu options.", nullptr));
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(statustip)
        menuSubtitles->setStatusTip(QCoreApplication::translate("newprg", "The download will fail if no subtitles are found. You can try different menu options.", nullptr));
#endif // QT_CONFIG(statustip)
        menuSubtitles->setTitle(QCoreApplication::translate("newprg", "S&ubtitle", nullptr));
    } // retranslateUi

};

namespace Ui {
    class newprg: public Ui_newprg {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_NEWPRG_H

