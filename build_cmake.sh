#!/bin/bash

QT6=6.8.1
CURRENT_PATH=$(pwd)
DIR=build
EXECUTABLE=streamcapture2
PATH=/opt/Qt/Tools/Ninja:${PATH}

#DELETE

files=(./${EXECUTABLE}-zsyncmake/${EXECUTABLE}-*)

if [[ -f "$files" ]]; then

for value in "${files[@]}"
do
  rm $value
  echo "$value deleted"
done

fi


files=(./build/)
if [ -d "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

files=(./${EXECUTABLE}-i386.AppImage)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi


files=(./${EXECUTABLE}-i386.AppImage-SHA256.txt-i386)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

files=(./${EXECUTABLE}-x86_64.AppImage)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi


files=(./${EXECUTABLE}-x86_64.AppImage-SHA256.txt)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

files=(./${EXECUTABLE}-zsyncmake/${EXECUTABLE}-*AppImage*)
if [ -f "$files" ]; then
for value in "${files[@]}"
do
  rm $value
  echo "$value deleted"
done
  rm -Rf $files
fi

files=(./linuxdeploy/${EXECUTABLE})
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

#FFMPEG




files=(./${EXECUTABLE}-ffmpeg-i386.AppImage)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi


files=(./${EXECUTABLE}-ffmpeg-i386.AppImage-SHA256.txt-i386)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

files=(./${EXECUTABLE}-ffmpeg-x86_64.AppImage)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi


files=(./${EXECUTABLE}-ffmpeg-x86_64.AppImage-SHA256.txt)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi
#END DELETE

PATH=/opt/Qt/${QT6}/gcc_64/bin:${PATH}
LD_LIBRARY_PATH=/opt/Qt/${QT6}/gcc_64/bin:${LD_LIBRARY_PATH}

echo Your current path: ${CURRENT_PATH}
echo Build directory: ${DIR}




cmake -S ${CURRENT_PATH}/code -B ${CURRENT_PATH}/${DIR} -G "Ninja" -DCMAKE_BUILD_TYPE=Release

cmake --build ${CURRENT_PATH}/${DIR} --target all

cp -f ${CURRENT_PATH}/build-executable6/${EXECUTABLE} linuxdeploy/

cd linuxdeploy
./instructions.sh
