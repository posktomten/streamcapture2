#!/bin/sh

find ./streamcapture2_2.19.2-1_amd64 -path '*/DEBIAN' -prune -o -type f -exec md5sum {} \; | 
awk '{ print $1 "  " substr($2, 10) }'
