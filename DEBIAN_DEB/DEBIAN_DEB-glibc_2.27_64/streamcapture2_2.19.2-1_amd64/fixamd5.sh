#!/bin/sh

find ./usr -path '*/DEBIAN' -prune -o -type f -exec md5sum {} \; | 
awk '{ print $1 "  " substr($2, 10) }'
