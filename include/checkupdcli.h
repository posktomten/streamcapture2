//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Program name
//          Copyright (C) 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef LIBCHECKUPDCLI_H
#define LIBCHECKUPDCLI_H
#include <QObject>
#include <QNetworkAccessManager>
#include "checkupdcli_global.h"
class LIBCHECKUPDCLI_EXPORT Libcheckupdcli : public QObject
{
    Q_OBJECT
public:
    explicit Libcheckupdcli(QObject *parent = 0);
    void getVersionRequest(QString address);

signals:
    void sendVersion(const QString version);

private:
    QNetworkAccessManager *nam;  // Gör den till en medlem

};


#endif // LIBCHECKUPDCLI_H

