
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          downloadinstall
//          Copyright (C)  Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_streamcapture2@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>#ifndef DOWNLOAD_INSTALL_H
#ifndef DOWNLOAD_INSTALL_H
#define DOWNLOAD_INSTALL_H

#include <QDialog>
#include <QNetworkReply>

QT_BEGIN_NAMESPACE
namespace Ui
{
class DownloadInstall;
}
QT_END_NAMESPACE


#if defined(Q_OS_LINUX)
#if defined(DOWNLOAD_INSTALL_LIBRARY)
#include "download_install_global.h"
class DOWNLOAD_INSTALL_EXPORT DownloadInstall : public QDialog
#else
class DownloadInstall : public QDialog
#endif // DOWNLOAD_INSTALL_LIBRARY
#else
class DownloadInstall : public QDialog
#endif // Q_OS_LINUX
{
    Q_OBJECT

public:
    explicit DownloadInstall(QWidget *parent = nullptr);
    ~DownloadInstall();
    void setValues(const QString *installer_path, const QString *installer_filename, const QString *display_name, const QString *version);

private:
    Ui::DownloadInstall *ui;
    QString networkErrorMessages(QNetworkReply::NetworkError error);

};

#endif // DOWNLOAD_INSTALL_H
