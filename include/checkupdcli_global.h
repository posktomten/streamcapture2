//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//    		Program name
//    		Copyright (C) 2024 Ingemar Ceicer
//    		https://gitlab.com/posktomten
//    		programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef LIBCHECKUPDCLI_GLOBAL_H
#define LIBCHECKUPDCLI_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(LIBCHECKUPDCLI_LIBRARY)
#  define LIBCHECKUPDCLI_EXPORT Q_DECL_EXPORT
#else
#  define LIBCHECKUPDCLI_EXPORT Q_DECL_IMPORT
#endif

#endif // LIBCHECKUPDCLI_GLOBAL_H
