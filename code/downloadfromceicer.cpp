// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <QProcess>
#include "newprg.h"
// #include "downloadunpack.h"

void Newprg::downloadFromCeicer()
{
    // QProcess *process = new QProcess(nullptr);
#if defined(Q_OS_LINUX)
    QString EXECUTABLE = QCoreApplication::applicationDirPath() + "/download-svtplay-dl";
#elif defined(Q_OS_WINDOWS)
    QString EXECUTABLE = QCoreApplication::applicationDirPath() + "/download-svtplay-dl.exe";
#endif
    QProcess::startDetached(EXECUTABLE);
    // process->deleteLater();
// #if defined(Q_OS_WINDOWS)
// {
// #if defined(PYTHON38)
//     DownloadUnpack *du = new DownloadUnpack(true);
// #else
//     DownloadUnpack *du = new DownloadUnpack(false);
//     // du->deleteLater();
// #endif
// #else
// {
//     DownloadUnpack *du = new DownloadUnpack;
// #endif
//     connect(this, &Newprg::sendNewFont, du, &DownloadUnpack::getNewFont);
//     // du->ensurePolished();
//     du->show();
}
