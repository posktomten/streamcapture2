// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "downloadlistdialog.h"
#include "selectfont.h"
#include "checkupdate.h"
#include "about.h"
#include "ui_newprg.h"
#include <QSslSocket>
#include <QActionGroup>
#include <QStyleFactory>
#include <QDir>
#include <QFontDatabase>
#include <QApplication>
#include <QtGlobal>
#include <QFileDialog>
#include <QStandardPaths>
#include <QInputDialog>
#include <QDesktopServices>
#include <QClipboard>
#include <QTimer>
// #include <qforeach.h>
#if defined(Q_OS_LINUX) // Linux
#include "updatedialog.h"
#include "update.h"
#endif // Q_OS_LINUX
Newprg::Newprg(QWidget *parent) : QMainWindow(parent), ui(new Ui::newprg)
{
    ui->setupUi(this);
    // setWindowModality(Qt::WindowModal);
    // qDebug() << _MSC_VER;
    zoom();
    ui->actionAboutQt->setVisible(false);
#if defined (Q_OS_LINUX)
    ui->actionFusionStyle->setVisible(false);
    ui->actionDontUseNativeDialogs->setVisible(false);
#endif
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
#if defined(Q_OS_WINDOWS)
    connect(ui->actionDontUseNativeDialogs, &QAction::triggered, this, [this]() {
        QMessageBox *msgBox = new QMessageBox(this);
        QIcon iconYes(QPixmap(":/images/restart.png"));
        QIcon iconLater(QPixmap(":/images/later.png"));
        QIcon iconReject(QPixmap(":/images/application-exit.png"));
        QPushButton  *yesButton = new QPushButton(iconYes, tr("Restart Now"), msgBox);
        yesButton->setFixedSize(QSize(150, 40));
        QPushButton  *laterButton = new QPushButton(iconLater, tr("Change settings on next start"), msgBox);
        laterButton->setFixedSize(QSize(350, 40));
        QPushButton  *rejectButton = new QPushButton(iconReject, tr("Cancel"), msgBox);
        rejectButton->setFixedSize(QSize(150, 40));
        msgBox->addButton(yesButton, QMessageBox::YesRole);
        msgBox->addButton(laterButton, QMessageBox::YesRole);
        msgBox->addButton(rejectButton, QMessageBox::RejectRole);
        msgBox->setDefaultButton(laterButton);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The program must be restarted for the new settings to take effect."));
        msgBox->exec();
        /***/

        if(msgBox->clickedButton() == yesButton) {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);

            if(ui->actionDontUseNativeDialogs->isChecked()) {
                settings.beginGroup("Settings");
                settings.setValue("dontusenativedialogs", true);
                settings.endGroup();
            } else {
                settings.beginGroup("Settings");
                settings.setValue("dontusenativedialogs", false);
                settings.endGroup();
            }

            settings.sync();
            delete msgBox;
            const QString EXECUTE =
                QCoreApplication::applicationDirPath() + "/" +
                QFileInfo(QCoreApplication::applicationFilePath()).fileName();
            QProcess p;
            p.setProgram(EXECUTE);
            p.startDetached();
            close();
        } else if(msgBox->clickedButton() == laterButton) {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);

            if(ui->actionDontUseNativeDialogs->isChecked()) {
                settings.beginGroup("Settings");
                settings.setValue("dontusenativedialogs", true);
                settings.endGroup();
            } else {
                settings.beginGroup("Settings");
                settings.setValue("dontusenativedialogs", false);
                settings.endGroup();
            }

            delete msgBox;
        } else {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");
            bool dontusenativedialogs = settings.value("dontusenativedialogs", false).toBool();
            settings.endGroup();
            ui->actionDontUseNativeDialogs->setChecked(dontusenativedialogs);
            delete msgBox;
            return;
        }

        /***/
    });

#endif
    connect(ui->actionNotifications, &QAction::hovered, this, [this]() {
        ui->actionNotifications->setStatusTip(tr("Whether notifications are displayed depends on the settings in your operating system."));
    });

    QActionGroup *actionGroupStyle = new QActionGroup(this);
    actionGroupStyle->addAction(ui->actionDefaultStyle);
    actionGroupStyle->addAction(ui->actionFusionStyle);
    actionGroupStyle->addAction(ui->actionDarkFusionStyle);
    actionGroupStyle->setExclusionPolicy(QActionGroup::ExclusionPolicy::Exclusive);
    QObject::connect(ui->actionDarkFusionStyle, &QAction::triggered, this, &::Newprg::fusionDarkStyle);
    QObject::connect(ui->actionFusionStyle, &QAction::triggered, this, &::Newprg::fusionStyle);
    QObject::connect(ui->actionDefaultStyle, &QAction::triggered, this, &::Newprg::defaultStyle);
// #endif
    QActionGroup *actionGroup = new QActionGroup(this);
    actionGroup->addAction(ui->actionNoSubtitles);
    actionGroup->addAction(ui->actionSubtitle);
    actionGroup->addAction(ui->actionAllSubtitles);
    actionGroup->addAction(ui->actionMergeSubtitle);
    actionGroup->addAction(ui->actionDownloadRaw);
    actionGroupStyle->setExclusionPolicy(QActionGroup::ExclusionPolicy::Exclusive);
    QActionGroup *svtplayGroup = new QActionGroup(this);
    svtplayGroup->addAction(ui->actionSvtplayDlStable);
    svtplayGroup->addAction(ui->actionSvtplayDlBleedingEdge);
    svtplayGroup->addAction(ui->actionSvtplayDlSystem);
    svtplayGroup->addAction(ui->actionSvtPlayDlManuallySelected);
    actionGroupStyle->setExclusionPolicy(QActionGroup::ExclusionPolicy::Exclusive);
#if defined(Q_OS_WINDOWS)
    int id = QFontDatabase::addApplicationFont(":/fonts/segoeui.ttf");
#endif
#if defined(Q_OS_LINUX)
    int id = QFontDatabase::addApplicationFont(":/fonts/Ubuntu-M.ttf");
#endif
    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont _font(family, FONTSIZE);
    QApplication::setFont(_font);

    foreach(QWidget *widget, QApplication::allWidgets()) {
        widget->setFont(QApplication::font());
        widget->update();
    }

    // connect(ui->actionAboutQt, &QAction::triggered, [this]() {
    //     QMessageBox::aboutQt(this, DISPLAY_NAME " " DISPLAY_VERSION);
    // });
    QSettings settings2(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                        LIBRARY_NAME);
    connect(ui->actionOpenFolder, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Path");
        QString savepath = settings.value("savepath", QStandardPaths::writableLocation(QStandardPaths::MoviesLocation)).toString();
        settings.endGroup();
        QFileDialog filedialog(this);
        int x = this->x();
        int y = this->y();
        filedialog.setDirectory(savepath);
        filedialog.setLabelText(QFileDialog::Accept, tr("Open"));
        filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
        filedialog.setWindowTitle(tr("Open downloaded file"));
        filedialog.setGeometry(x + 50, y + 50, 900, 550);
        filedialog.setFileMode(QFileDialog::ExistingFile);//Folder name
        filedialog.setNameFilter(tr("Video files") + " (*.mp4)");

        if(!filedialog.exec()) {
            return;
        }

        filedialog.selectedFiles().at(0);
        QDesktopServices::openUrl(filedialog.selectedFiles().at(0));
    });

    /*** IMPORT DOWNLOADLIST ***/
    connect(ui->actionImportDownloadList, &QAction::triggered, this, &Newprg::_importDownloadList);
    /*** EXPORT DOWNLOADLIST ***/
    connect(ui->actionExportDownloadList, &QAction::triggered, this, &Newprg::_exportDownloadList);
    connect(ui->actionExportDownloadList, &QAction::hovered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Path");
        QString downloadlistfile = settings.value("downloadlistfile").toString();
        settings.endGroup();
        QFileInfo fi(downloadlistfile);

        if(fi.exists()) {
            ui->actionExportDownloadList->setStatusTip(tr("File: ") + downloadlistfile);
        } else {
            ui->actionExportDownloadList->setStatusTip(tr("There is no file."));
        }
    });

    /*** NEW DOWNLOADLIST ***/
    connect(ui->actionDownloadList, &QAction::triggered, this, &Newprg::_newDownloadList);
// important!
    connect(ui->actionImportant, &QAction::triggered, this, [this]() {
        QString currentLocale(QLocale::system().name());
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Language");
        QString sprak = settings.value("language", currentLocale).toString();
        QString important;
        settings.endGroup();

        if(sprak == "sv_SE") {
            important = "important_sv.txt";
        } else if(sprak == "it_IT") {
            important = "important_it.txt";
        } else {
            important = "important_en.txt";
        }

        QString format(QStringLiteral("table{border-width: 8px; border-color: transparent;}  td{border: none;}"));
        // tdoc->setDefaultStyleSheet(instructions->at(2));
        QStringList *infoList = new QStringList;
        *infoList << tr("If the download does not work");
        *infoList << QString("wordwrap");
        *infoList << format;
        QString *filename = new QString(":/txt/" + important);
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
        // connect(mDownloadListDialog, &DownloadListDialog::isfinished, this, &Newprg::importantFinished);
        mDownloadListDialog->openFile(infoList, filename, true);
    });

    connect(ui->action4k, &QAction::triggered, this, [this]() {
        if(ui->action4k->isChecked()) {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");
            settings.setValue("try4k", true);
            settings.endGroup();
            ui->chb4k->setChecked(true);
        } else {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");
            settings.setValue("try4k", false);
            settings.endGroup();
            ui->chb4k->setChecked(false);
        }
    });

#if (QT_VERSION >= QT_VERSION_CHECK(6,8,0))
    connect(ui->chb4k, &QCheckBox::checkStateChanged, this, [this](int state) {
#else
    connect(ui->chb4k, &QCheckBox::stateChanged, this, [this](int state) {
#endif

        if(state == Qt::Checked) {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");
            settings.setValue("try4k", true);
            settings.endGroup();
            ui->action4k->setChecked(true);
        } else {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Settings");
            settings.setValue("try4k", false);
            settings.endGroup();
            ui->chb4k->setChecked(false);
            ui->action4k->setChecked(false);
        }
    });

// tv4token
    connect(ui->actionTVToken, &QAction::triggered, this, []() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        settings.beginGroup("Stcookies");
        QString tv4token = settings.value("tv4token").toString();
        settings.endGroup();
        bool ok;
        QInputDialog inputdialog;
        inputdialog.setWindowFlags(inputdialog.windowFlags() & ~Qt::WindowContextHelpButtonHint);
        inputdialog.resize(600, inputdialog.rect().height());
        inputdialog.setSizeGripEnabled(true);
        inputdialog.setOkButtonText(tr("Ok"));
        inputdialog.setCancelButtonText(tr("Cancel"));
        inputdialog.setWindowTitle(tr("Enter TV4 token"));
        inputdialog.setInputMode(QInputDialog::TextInput);
        inputdialog.setTextValue(tv4token);
        inputdialog.setLabelText(tr("You must paste the exact TV4 token."));
        ok = inputdialog.exec();

        if(ok) {
            tv4token = inputdialog.textValue();

            if(tv4token.size() < 600) {
                tv4token = tv4token.simplified();
                tv4token.replace("\"", "");
                tv4token.replace("\'", "");
                settings.beginGroup("Stcookies");
                settings.setValue("tv4token", tv4token);
                settings.endGroup();
            }
        }
    });

    connect(ui->actionHowTo, &QAction::triggered, []() {
        QDesktopServices::openUrl(
            QUrl(TV4_HOWTO, QUrl::TolerantMode));
    });

    connect(ui->actionHowToVideo, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Language");
        QString language = settings.value("language", "en_US").toString();
        settings.endGroup();
        ui->chb4k->setChecked(true);

        if(language == "sv_SE") {
            QDesktopServices::openUrl(
                QUrl(TV4_HOWTO_VIDEO_SV, QUrl::TolerantMode));
        } else {
            QDesktopServices::openUrl(
                QUrl(TV4_HOWTO_VIDEO, QUrl::TolerantMode));
        }
    });

#ifdef Q_OS_LINUX
    settings.beginGroup("FirstRun");
    bool firstrun = settings.value("firstrun", true).toBool();
    settings.endGroup();

    if(firstrun) {
        firstRun();
    } else {
        // settings.beginGroup("Created");
        // bool important_shown = settings.value("important_shown", false).toBool();
        // bool created = settings.value("created", false).toBool();
        // settings.endGroup();
        // if(!important_shown && created) {
        //     ui->actionImportant->trigger();
        // }
        // settings.beginGroup("FirstRun");
        // bool firstrun = settings.value("firstrun", true).toBool();
        // settings.endGroup();
        settings.beginGroup("FirstRun");
        settings.setValue("firstrun", false);
        settings.endGroup();
    }

#endif
// }
    setStartConfig();

    if(ui->actionCheckSvtplayDlForUpdatesAtStart->isChecked()) {
        checkSvtplaydlAtStart();
    }

    connect(ui->actionNfoInfo, &QAction::triggered, this, [this]() {
        nfo();
    });

    connect(ui->actionCheckLatestsvtplaydl, &QAction::triggered, this, [this]() {
        checkSvtplaydlForUpdatesHelp();
    });

    connect(ui->actionStreamCapture2Settings, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
        QString inifile = settings.fileName();
        QFile fil(inifile);
        QString *content = new QString;
        content->clear();

        if(fil.open(QIODevice::ReadOnly)) {
            QTextStream in(&fil);

            while(!in.atEnd()) {
                *content += in.readLine() + "<br>";
            }

            fil.close();
        }

        QStringList *infoList = new QStringList;
        *infoList << tr("Edit streamCapture2 settings");
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
        connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::getSettings);
        mDownloadListDialog->initiate(infoList, content, false);
    });

    connect(ui->actionDownloadSvtplayDlSettings, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, LIBRARY_NAME);
        QString inifile = settings.fileName();
        QFile fil(inifile);
        QString *content = new QString;
        content->clear();

        if(fil.open(QIODevice::ReadOnly)) {
            QTextStream in(&fil);

            while(!in.atEnd()) {
                *content += in.readLine() + "<br>";
            }

            fil.close();
        }

        QStringList *infoList = new QStringList;
        *infoList << tr("Edit download svtplay-dl settings");
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
        connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::getSettingsDownloadunpack);
        mDownloadListDialog->initiate(infoList, content, false);
    });

    /*  */
    /* KOLLAR VILKEN svtplay-dl SOM SKA ANVÄNDAS */
#if defined(Q_OS_WIN) // Windows 32- and 64-bit
#ifndef PORTABLE
    ui->actionApplicationsMenuShortcut->setVisible(false);
#endif
#endif
#if defined(Q_OS_LINUX)
    ui->actionApplicationsMenuShortcut->setVisible(true);
#endif

    if(ui->actionSvtplayDlStable->isChecked()) {
        svtplaydl = QDir::toNativeSeparators(
                        QCoreApplication::applicationDirPath() + "/stable/svtplay-dl.exe");
    } else if(ui->actionSvtplayDlSystem->isChecked()) {
        QString path = findSvtplayDl();

        if(path == QStringLiteral("NOTFIND")) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("svtplay-dl is not found in the system path.\nYou can download any svtplay-dl of your choice."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
        } else {
            svtplaydl = path;
        }

        svtplaydl = QDir::toNativeSeparators(QStringLiteral(u"svtplay-dl.exe"));
    } else if(ui->actionSvtPlayDlManuallySelected->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString svtplaydlpath =
            settings.value("svtplaydlpath", "NOTFIND").toString();
        settings.endGroup();
        svtplaydl = QDir::toNativeSeparators(svtplaydlpath);
    }

// #endif // Q_OS_WINDOWS

    if(ui->actionSvtplayDlStable->isChecked()) {
        settings2.beginGroup("Path");
        QString stablepath = settings2.value("stablepath").toString();
        settings2.endGroup();
#if defined(Q_OS_WIN)
        svtplaydl = stablepath + "/stable/svtplay-dl.exe";
#endif
#if defined(Q_OS_LINUX)
        svtplaydl = stablepath + QStringLiteral("/stable/svtplay-dl");
#endif
    }  else if(ui->actionSvtPlayDlManuallySelected->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
        QString svtplaydlpath =
            settings.value(QStringLiteral(u"svtplaydlpath"), "NOTFIND").toString();
        settings.endGroup();
        svtplaydl = QDir::toNativeSeparators(svtplaydlpath);
    } else if(ui->actionSvtplayDlBleedingEdge->isChecked()) {
        settings2.beginGroup("Path");
        QString betapath = settings2.value("betapath").toString();
        settings2.endGroup();
#if defined(Q_OS_WIN)
        svtplaydl = betapath + "/beta/svtplay-dl.exe";
#endif
#if defined(Q_OS_LINUX)
        svtplaydl = betapath + "/beta/svtplay-dl";
#endif
    } else {
        settings2.beginGroup("Path");
        QString stablepath = settings2.value("stablepath", "svtplay-dl").toString();
        settings2.endGroup();
#if defined(Q_OS_WIN)
        svtplaydl = stablepath + "/stable/svtplay-dl.exe";
#endif
#if defined(Q_OS_LINUX)
        svtplaydl = stablepath + "/stable/svtplay-dl";
#endif
        // if(!QFile::exists(svtplaydl)) {
        // ui->teOut->setText("svtplay-dl" +
        //                    tr(" cannot be found or is not an executable program."));
        // ui->teOut->append(tr("Please click on \"svtplay-dl\" and select svtplay-dl..."));
        // QMessageBox msgBox1;
        // msgBox1.setIcon(QMessageBox::Critical);
        // msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
        // msgBox1.setText(tr("svtplay-dl cannot be found.") + '\n' + tr("Or is not an executable program.") + '\n' + tr("Please click on \"svtplay-dl\" and select svtplay-dl."));
        // msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
        // msgBox1.exec();
        // return;
        // }
    }

// Download from ceicer
    connect(ui->actionDownloadSvtplayDlFromBinCeicerCom, &QAction::triggered, this, &Newprg::downloadFromCeicer);
// File
    connect(ui->actionSelectFont, &QAction::hovered, this, [this]() {
        QFont f = ui->teOut->currentFont();
        QString sf = f.family();
        int size = f.pointSize();
        bool bold = f.bold();
        bool italic = f.italic();
        QString stil = " , " + tr("normal");

        if(bold && italic) {
            stil = " , " + tr("bold and italic");
        } else {
            if(bold) {
                stil = ", " + tr("bold");
            } else if(italic) {
                stil = " , " + tr("italic");
            }
        }

        ui->actionSelectFont->setStatusTip(tr("Current font:") + " \"" + sf +
                                           "\", " + tr("size:") + " " +
                                           QString::number(size) + stil);
    });

    connect(ui->pbPast, &QPushButton::clicked, this, [this]() {
        ui->teOut->setReadOnly(true);
        QClipboard *clipboard = QApplication::clipboard();
        QString originalText = clipboard->text();
        ui->leSok->setText(originalText);
    });

    connect(ui->actionPast, &QAction::triggered, this, [this]() {
        ui->teOut->setReadOnly(true);
        QClipboard *clipboard = QApplication::clipboard();
        QString originalText = clipboard->text();
        ui->leSok->setText(originalText);
    });

    connect(ui->actionExit, &QAction::triggered, this, [this]() {
        close();
    });

    connect(ui->menuRecent, &QMenu::aboutToShow, this, &Newprg::slotRecent);
    /*** READ DOWNLOAD LIST ***/
    // connect(ui->actionViewDownloadList, &QAction::hovered, [this]() {
    //     QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
    //                        EXECUTABLE_NAME);
    //     settings.beginGroup("Path");
    //     QString downloadlistfile = settings.value("downloadlistfile").toString();
    //     settings.endGroup();
    //     ui->statusBar->showMessage(tr("File: ") + downloadlistfile);
    // });
    // ipac
    connect(ui->actionViewDownloadList, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Path");
        QString downloadlistfile = settings.value("downloadlistfile").toString();
        settings.endGroup();

        if(downloadlistfile.isEmpty()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("There is no file.") + '\n' + tr("Select \"Download List\" and \"New download List...\".\nOr select \"Download List\" and \"Import a download List...\"."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            settings.beginGroup("Path");
            settings.setValue("downloadlistfile", "");
            settings.endGroup();
            return;
        }

        QFile file(downloadlistfile);
        QFileInfo fi(downloadlistfile);
        QString info;

        if(!fi.isFile()) {
            info = tr("Cannot find the file") + "\n\"" + downloadlistfile + "\"" + '\n' + tr("with download links listed.\nCheck if the file has been removed or moved.");
        } else if(!fi.isReadable()) {
            info =  tr("Cannot open the file") + "\n\"" + downloadlistfile + "\"" + '\n' + tr("with download links listed.\nCheck if the file permissions have changed.");
        }

        if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(info);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            file.close();
            return;
        }

        QStringList contents;

        while(!file.atEnd()) {
            QByteArray line = file.readLine();
            contents << line;
        }

        file.close();
        /*****/
        static QRegularExpression pattern(QStringLiteral("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)"));
        QString *nomatch = new QString;

        foreach(QString s, contents) {
            QRegularExpressionMatch match = pattern.match(s);

            if(!match.hasMatch()) {
                nomatch->append(s);
            }
        }

        int comma = 0, hitta = 0;
        QString is = "";

        foreach(QString s, contents) {
            hitta = s.indexOf(",");
            is = s.mid(hitta);
            comma = is.count(QString(","));

            if(comma != 7) {
                nomatch->append(s);
            }
        }

        /***/
        // QString *content = new QString;
        QString content;
        int amount_good = 0;
        contents.removeDuplicates();

        foreach(QString s, contents) {
            content += s;
            amount_good++;
        }

        if(!nomatch->isEmpty()) {
            QStringList list_bad = nomatch->split('\n');
            list_bad.removeDuplicates();
            list_bad.removeAll(QString(""));
            int amount_bad = list_bad.size();
            QStringList *infoList = new QStringList;
            const QString amountofstreams = (amount_bad  == 1) ? tr("Invalid list item found.") : tr("Invalid list items found.");
            const QString reminder = (amount_bad  == 1) ? QString::number(amount_bad) + tr(" Incorrect link found. Please click \"Download List\", \"Edit the download List...\" to remove incorrect link.") : QString::number(amount_bad) + tr(" Incorrect links found.  Please click \"Download List\", \"Edit the download List...\" to remove incorrect links.");
            *infoList  << QString::number(amount_bad) + " " + amountofstreams;
            *infoList  << downloadlistfile;
            *infoList  << reminder;
            DownloadListDialog *mDownloadListDialog = new DownloadListDialog(this);
            connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
            // connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::saveDownloadList);
            QStringList nomatch_list = nomatch->split('\n');
            nomatch_list.removeDuplicates();
            nomatch->clear();

            foreach(QString s, nomatch_list) {
                *nomatch += s + '\n';
            }

            mDownloadListDialog->initiate(infoList, nomatch, true);
            mDownloadListDialog->deleteLater();
        } else {
            QStringList *infoList = new QStringList;
            const QString amountofstreams = (amount_good == 1) ? tr("Video stream") : tr("Video streams");
            *infoList << tr("View download list") + " (" + QString::number(amount_good)  + " " + amountofstreams + ")";
            *infoList << downloadlistfile;
            DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
            connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
            // connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &::Newprg::newDownloadList);
            mDownloadListDialog->initiate(infoList, &content, true);
            // mDownloadListDialog->deleteLater();
        }
    });

    /*** EDIT DOWNLOAD LIST ***/
    connect(ui->actionEditDownloadList, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Path");
        QString downloadlistfile = settings.value("downloadlistfile").toString();
        settings.endGroup();

        if(downloadlistfile.isEmpty()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("There is no file.") + '\n' + tr("Select \"Download List\" and \"New download List...\".\nOr select \"Download List\" and \"Import a download List...\"."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            settings.beginGroup("Path");
            settings.setValue("downloadlistfile", "");
            settings.endGroup();
            return;
        }

        QFile file(downloadlistfile);
        QFileInfo fi(downloadlistfile);
        QString info;

        if(!fi.exists()) {
            info = tr("Cannot find the file") + "\n\"" + downloadlistfile + "\"" + '\n' + tr("with download links listed.\nCheck if the file has been removed or moved.");
        } else {
            info =  tr("Cannot open the file") + "\n\"" + downloadlistfile + "\"" + '\n' + tr("with download links listed.\nCheck if the file permissions have changed.");
        }

        if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("Unable to open file") + "\n\"" + downloadlistfile + "\"" + '\n' + tr("with download links listed.\nCheck if the file has been deleted\nor if the file permissions have changed."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

        QStringList contents2;
        int items = 0;

        while(!file.atEnd()) {
            QByteArray line = file.readLine();
            contents2 << line;
        }

        QString *content = new QString;
        contents2.removeDuplicates();

        foreach(QString s, contents2) {
            *content += s;
            items++;
        }

        QStringList *infoList = new QStringList;
        const QString amountofstreams = (items == 1) ? tr("Video stream") : tr("Video streams");
        *infoList << tr("Edit the download list") + " (" + QString::number(items)  + " " + amountofstreams + ")";
        *infoList << downloadlistfile;
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
        connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::saveDownloadList);
        mDownloadListDialog->initiate(infoList, content, false, true);
    });

// ipac
    connect(ui->actionDeleteDownloadList, &QAction::hovered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Path");
        QString downloadlistfile = settings.value("downloadlistfile").toString();
        settings.endGroup();
        QFileInfo fi(downloadlistfile);

        if(fi.exists()) {
            ui->actionDeleteDownloadList->setStatusTip(tr("File: ") + downloadlistfile);
        } else {
            ui->actionDeleteDownloadList->setStatusTip(tr("There is no file."));
        }
    });

    connect(ui->actionDeleteDownloadList, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Path");
        QString downloadlistfile = settings.value("downloadlistfile").toString();
        settings.endGroup();

        if(downloadlistfile.isEmpty()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("There is no file.") + '\n' + tr("Select \"Download List\" and \"New download List...\".\nOr select \"Download List\" and \"Import a download List...\"."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            settings.beginGroup("Path");
            settings.setValue("downloadlistfile", "");
            settings.endGroup();
            return;
        }

        QString info;
        QFileInfo fi(downloadlistfile);

        if(!fi.isFile()) {
            info = tr("Cannot delete the file") + "\n\"" + downloadlistfile + "\"" + '\n' + tr("with download links listed.\nCheck if the file has been removed or moved.");
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(info);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            settings.beginGroup("Path");
            settings.setValue("downloadlistfile", "");
            settings.endGroup();
            return;
        } else if(!fi.isWritable()) {
            info =  tr("Cannot delete the file") + "\n\"" + downloadlistfile + "\"" + '\n' + tr("with download links listed.\nCheck if the file permissions have changed.");
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(info);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            settings.beginGroup("Path");
            settings.setValue("downloadlistfile", "");
            settings.endGroup();
            return;
        }

        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setIcon(QMessageBox::Warning);
        QPushButton  *yesButton = new QPushButton(tr("Yes"), msgBox);
        // yesButton->setFixedSize(QSize(150, 40));
        QPushButton  *noButton = new QPushButton(tr("No"), msgBox);
        // noButton->setFixedSize(QSize(150, 40));
        msgBox->addButton(yesButton, QMessageBox::YesRole);
        msgBox->addButton(noButton, QMessageBox::RejectRole);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The file") + "\n\"" + QDir::toNativeSeparators(downloadlistfile) + "\"" + '\n' + tr("will be deleted and can not be "
                           "restored.\nDo you want to continue?"));
        msgBox->exec();

        if(msgBox->clickedButton() == yesButton) {
            // downloadList.clear();
            QFile file(downloadlistfile);

            if(!file.remove()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Can't delete ") + downloadlistfile);
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                return;
            }

            settings.beginGroup("Path");
            settings.setValue("downloadlistfile", "");
            settings.endGroup();
        }
    });

#if defined(Q_OS_WIN)
    connect(ui->actionDownloadMicrosoftRuntime, &QAction::triggered, this, &Newprg::downloadRuntime);
#endif
    connect(ui->actionCheckOnStart, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Update");

        if(ui->actionCheckOnStart->isChecked()) {
            settings.setValue("checkonstart", "true");
        } else {
            settings.setValue("checkonstart", "false");
        }

        settings.endGroup();
    });

    connect(ui->actionCheckSvtplayDlForUpdatesAtStart, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Update");

        if(ui->actionCheckSvtplayDlForUpdatesAtStart->isChecked()) {
            settings.setValue("checksvtplaydlonstart", "true");
        } else {
            settings.setValue("checksvtplaydlonstart", "false");
        }

        settings.endGroup();
    });

// SELECT FILE NAME
    connect(ui->actionSelectFileName, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Settings");

        if(ui->actionSelectFileName->isChecked()) {
            settings.setValue("selectfilename", "true");
        } else {
            settings.setValue("selectfilename", "false");
        }

        settings.endGroup();
    });

    connect(ui->actionShowMore, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Settings");

        if(ui->actionShowMore->isChecked()) {
            settings.setValue("showmore", "true");
        } else {
            settings.setValue("showmore", "false");
        }

        settings.endGroup();
    });

    connect(ui->actionCreateFolder, &QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Settings");

        if(ui->actionCreateFolder->isChecked()) {
            settings.setValue("createfolder", "true");
        } else {
            settings.setValue("createfolder", "false");
        }

        settings.endGroup();
    });

// Help
    connect(ui->actionHelp, &QAction::triggered, this, []() -> void {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Language");
        QString sp = settings.value("language", "").toString();
        settings.endGroup();

        if(sp == "") {
            sp = QLocale::system().name();
        } else if(sp == "de_DE") {
            sp = "en_US";
        }

        QDesktopServices::openUrl(QUrl(MANUAL_PATH + sp + ".html", QUrl::TolerantMode));

    });
// HIT (checkforupdates)
#if defined(Q_OS_WIN)
    connect(ui->actionCheckForUpdates, &QAction::triggered, this, [ = ]() {
#endif
#if defined(Q_OS_LINUX)
        connect(ui->actionCheckForUpdates, &QAction::triggered, this, [this]() {
            QString *updateinstructions =
//                new QString(tr("Please click on \"svtplay-dl\" and \"Download svtplay-dl...\""));
                new QString(tr("Please click on \"Tools\" and \"Update\""));
#endif
#if defined(Q_OS_WIN)
#ifndef PORTABLE
            QString *updateinstructions = new QString(
                tr("Select \"Tools\", \"Maintenance Tool\" and \"Update components\"."));
#else
            QString *updateinstructions = new QString(
                tr("Download a new") + " <a href=\"" DOWNLOAD_PATH "\"> portable</a>");
#endif
#endif
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Language");
            QString language = settings.value("language", "en_US").toString();
            settings.endGroup();

            if(language == "sv_SE") {
                language = VERSION_PATH_SV;
            } else {
                language = VERSION_PATH;
            }

            auto *cfu = new CheckUpdate;
            cfu->check(DISPLAY_NAME, VERSION, language, *updateinstructions, windowicon);
            connect(cfu, &CheckUpdate::foundUpdate, this, [ = ](bool isupdated) {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                                   EXECUTABLE_NAME);
                // settings.setIniCodec("UTF-8");
                settings.beginGroup("Update");

                if(isupdated) {
                    settings.setValue("readyupdate", true);
#if defined(Q_OS_LINUX)
                    ui->actionMaintenanceTool->setEnabled(true);
#endif
                } else {
                    settings.setValue("readyupdate", false);
#if defined(Q_OS_LINUX)
                    ui->actionMaintenanceTool->setDisabled(true);
#endif
                }

                settings.endGroup();
            });
        });

// TILL HIT (checkforupdates)
        connect(ui->actionVersionHistory, &QAction::triggered, this, &Newprg::versionHistory);
        connect(ui->actionAbout, &QAction::triggered, this, []() -> void {
            const QString *purpose = new QString(tr("A graphical shell for ") +
                                                 "<a style=\"text-decoration:none;\" href=\"" SVTPLAYDL_HOMEPAGE
                                                 "\">svtplay-dl</a>" + tr(" and ") +
                                                 "<a style=\"text-decoration:none;\" href=\"" FFMPEG_HOMEPAGE
                                                 "\">FFmpeg</a>.<br>" +
                                                 tr(" streamCapture2 handles downloads of video streams.")
                                                );
            const QString *copyright_year = new QString(COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year()));
            const QString *translator = new QString(tr("Many thanks to ") + ITALIAN_TRANSLATER + tr(" for the Italian translation. And for many good ideas that have made the program better."));
            //        const QString *translator = new QString("");
            const QPixmap *pixmap = new QPixmap(":/images/appicon.png");
            // const QPixmap *pixmap = nullptr;
            About *about = new About(QApplication::font(),
                                     QStringLiteral(DISPLAY_NAME),
                                     QStringLiteral(DISPLAY_VERSION),
                                     QStringLiteral(COPYRIGHT),
                                     QStringLiteral(EMAIL),
                                     copyright_year,
                                     QStringLiteral(BUILD_DATE_TIME),
                                     QStringLiteral(LICENSE),
                                     QStringLiteral(LICENSE_LINK),
                                     QStringLiteral(CHANGELOG),
                                     QStringLiteral(SOURCECODE),
                                     QStringLiteral(WEBSITE),
                                     QStringLiteral(COMPILEDON),
                                     purpose,
                                     QStringLiteral(MANUAL),
                                     translator,
                                     pixmap,
                                     false
                                    );
            delete about;
        });
//        connect(ui->actionAboutSvtplayDl, SIGNAL(triggered()), this,
//                SLOT(aboutSvtplayDl())); // about.cpp
        connect(ui->actionAboutSvtplayDl, &QAction::triggered, this, &Newprg::aboutSvtplayDl);
        /* About svtplay-dl HOOVER */
        connect(ui->actionAboutSvtplayDl, &QAction::hovered, this, [this]() -> void {

            if(ui->actionSvtplayDlSystem->isChecked()) {
                QString path = findSvtplayDl();

                if(path != QStringLiteral("NOTFIND")) {
#if defined(Q_OS_LINUX)
                    ui->actionAboutSvtplayDl->setStatusTip(
                        tr("svtplay-dl is in the system path."));
                    svtplaydl = path;
#endif
#if defined(Q_OS_WIN)
                    ui->actionAboutSvtplayDl->setStatusTip(
                        tr("svtplay-dl.exe is in the system path."));
                    svtplaydl = path;
#endif
                } else {
#if defined(Q_OS_LINUX)
                    ui->actionAboutSvtplayDl->setStatusTip(
                        tr("ERROR! svtplay-dl is not in the system path."));
#endif
#if defined(Q_OS_WIN)
                    ui->actionAboutSvtplayDl->setStatusTip(
                        tr("ERROR! svtplay-dl.exe is not in the system path."));
#endif
                }

                return;
            }

            if(fileExists(svtplaydl))
            {
#if defined(Q_OS_LINUX)
                ui->actionAboutSvtplayDl->setStatusTip(tr("Path to svtplay-dl: ") + "\"" +
                                                       svtplaydl + "\"");
#endif
#if defined(Q_OS_WIN)
                ui->actionAboutSvtplayDl->setStatusTip(tr("Path to svtplay-dl.exe: ") +
                                                       "\"" + svtplaydl + "\"");
#endif
            } else
            {
#if defined(Q_OS_LINUX)
                ui->actionAboutSvtplayDl->setStatusTip(
                    tr("ERROR! svtplay-dl could not be found."));
#endif
#if defined(Q_OS_WIN)
                ui->actionAboutSvtplayDl->setStatusTip(
                    tr("ERROR! svtplay-dl.exe could not be found."));
#endif
            }
        });
        connect(ui->actionSvtplayDlForum, &QAction::triggered, []() -> void {
            QDesktopServices::openUrl(
                QUrl(SVTPLAYDL_ISSUES, QUrl::TolerantMode));
        });
        // about.cpp
        connect(ui->actionAboutFfmpeg, &QAction::triggered, this, &Newprg::aboutFfmpeg);
        /* HOOVERED ffmpeg */
        connect(ui->actionAboutFfmpeg, &QAction::hovered, this, [this]() -> void {
            QString pathtoffmpeg = selectFfmpeg();

            if(pathtoffmpeg == "NOTFIND") {
                ui->actionAboutFfmpeg->setStatusTip(tr("ERROR! FFmpeg cannot be found."));
            } else {
#if defined(Q_OS_WIN)
                ui->actionAboutFfmpeg->setStatusTip(tr("Path to ffmpeg.exe: ") + "\"" + QDir::toNativeSeparators(pathtoffmpeg) + "\"");
#endif
#if defined(Q_OS_LINUX)
                ui->actionAboutFfmpeg->setStatusTip(tr("Path to ffmpeg: ") + "\"" + QDir::toNativeSeparators(pathtoffmpeg) + "\"");
#endif
            }
        });
        connect(ui->actionLicense, &QAction::triggered, this, &Newprg::license);
        // about.cpp
#ifdef FFMPEG
        connect(ui->actionLicenseFfmpeg, &QAction::triggered, this, &Newprg::licenseFfmpeg);
#endif
        connect(ui->actionLicenseSvtplayDl, &QAction::triggered, this, &Newprg::licenseSvtplayDl);
#if defined(Q_OS_WIN)
        connect(ui->actionLicense7zip, &QAction::triggered, this, &Newprg::license7zip);
#endif
        // Language
        connect(ui->actionSwedish, &QAction::triggered, this, &Newprg::swedish);
        connect(ui->actionEnglish, &QAction::triggered, this, &Newprg::english);
        connect(ui->actionItalian, &QAction::triggered, this, &Newprg::italian);
        connect(ui->pbSok, &QPushButton::pressed, this, &Newprg::initSok);
        connect(ui->actionSearch, &QAction::triggered, this, &Newprg::initSok);
        connect(ui->pbSok, &QPushButton::clicked, this, &Newprg::sok);
        connect(ui->actionSearch, &QAction::triggered, this, &Newprg::sok);
        // list all
        connect(ui->actionListAllEpisodes, &QAction::triggered, this, &Newprg::listAllEpisodes);
        //
        connect(ui->pbDownload, &QPushButton::clicked, this, &Newprg::download);
        connect(ui->actionDownload, &QAction::triggered, this, &Newprg::download);
        connect(ui->pbAdd, &QPushButton::clicked, ui->actionAdd, &QAction::triggered);
        // ADD ALL EPISODES
        connect(ui->actionAddAllEpisodesToDownloadList, &QAction::triggered, this, [ this ]() {
            /* DOWNLOAD LIST AddAllEpisodes ipac */
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Path");
            QString downloadlistfile = settings.value("downloadlistfile").toString();
            settings.endGroup();
            QFileInfo fi(downloadlistfile);

            if(!fi.exists()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("To add downloads to the download list,\nyou need to create a list") + '\n' + tr("\"Downlod List\" and \"New download List...\"") + '\n' + tr(" or import a list") + '\n' + tr("\"Downlod List\" and \"Import a download List...\""));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                return;
            }

            QString in, provider, sub;

            if((ui->comboPayTV->currentText().indexOf("-", 0)) == 0) {
                provider = QStringLiteral("npassword");
            } else {
                provider = ui->comboPayTV->currentText();
            }

            if(ui->actionSubtitle->isChecked()) {
                sub = QStringLiteral("ysub");
            } else if(ui->actionAllSubtitles->isChecked()) {
                sub = QStringLiteral("asub");
            } else if(ui->actionMergeSubtitle->isChecked()) {
                sub = QStringLiteral("ymsub");
            } else if(ui->actionNoSubtitles->isChecked()) {
                sub = QStringLiteral("nsub");
            } else if(ui->actionDownloadRaw->isChecked()) {
                sub = QStringLiteral("rsub");
            }

            QString soktext = ui->leSok->text();

            //  Bugg V0.15.0
            if(soktext.right(1) == '/') {
                int storlek = soktext.size();
                soktext.remove(storlek - 1, 1);
            }

            /* ADD 'st' COOKIE TO DOWNLOAD LIST */
            // QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
            settings.beginGroup("Stcookies");
            QString stcookie = settings.value("stcookie", "").toString();
            //settings.endGroup();

            if(stcookie.isEmpty()) {
                stcookie = "nst";
            }

            settings.value("stcookie", "").toString();
            settings.endGroup();
            int hittat = soktext.indexOf('?');
            soktext = soktext.left(hittat);
            QString unknown = QStringLiteral("unknown");
            // if(ui->actionAddAllEpisodesToDownloadList->isEnabled()) {
            QString data = ui->teOut->toPlainText();
            static QRegularExpression qre("[\n]");
            QStringList addresses = data.split(qre, Qt::SkipEmptyParts);
            QString resolution;

            switch(ui->comboResolution->currentIndex()) {
                case 0:
                    resolution = QStringLiteral("unknown");
                    break;

                case 1:
                    resolution = QStringLiteral("less_than_720p");
                    break;

                case 2:
                    resolution = QStringLiteral("less_or_equal_720p");
                    break;

                case 3:
                    resolution = QStringLiteral("less_or_equal_1080p");
                    break;

                case 4:
                    resolution = QStringLiteral("more_than_1080p");
                    break;
            }

            QFile file(downloadlistfile);
            QTextStream line(&file);

            if(!file.open(QIODevice::WriteOnly | QIODevice::Text |  QIODevice::Append)) {
                return;
            } else {
                /**** ****/
                foreach(QString tmp, addresses) {
                    if(tmp.left(7) == QStringLiteral("http://")) {
                        tmp.insert(4, 's');
                    }

                    if((tmp.left(8) == QStringLiteral("https://")) || (tmp.left(7) == QStringLiteral("http://"))) {
                        // in = tmp + "," + unknown + "," + unknown + "," + provider + "," + sub + "," + unknown + "," + stcookie + "," + resolution;
                        in = tmp + "," + unknown + "," + unknown + "," + provider + "," + sub + "," + stcookie + "," + unknown  + "," + resolution;
                        in = in.trimmed();

                        if(!in.isEmpty()) {
                            line << in + '\n';
                        }
                    }
                }
            }

            file.close();

            if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                return;
            } else {
                QByteArray line;
                QStringList contents;

                while(!file.atEnd()) {
                    line = file.readLine();
                    contents << QString(line);
                }

                file.close();
                file.resize(0);
                contents.removeDuplicates();
                contents.removeAll(QString(""));
                QString content;

                foreach(QString s, contents) {
                    content += s;
                }

                saveDownloadList(&content);
            }

            ui->pbDownloadAll->setEnabled(true);
            ui->actionDownloadAll->setEnabled(true);
            // }
        });

        // ADD EPISODE
        connect(ui->actionAdd, &QAction::triggered, this, [ = ]() {
            /* DOWNLOAD LIST AddEpisodes ipac */
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Path");
            QString downloadlistfile = settings.value("downloadlistfile").toString();
            settings.endGroup();
            QFileInfo fi(downloadlistfile);

            if(!fi.exists()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("To add downloads to the download list,\nyou need to create a list") + '\n' + tr("\"Downlod List\" and \"New download List...\"") + '\n' + tr(" or import a list") + '\n' + tr("\"Downlod List\" and \"Import a download List...\""));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                return;
            }

            QString soktext = ui->leSok->text();

            if(soktext.isEmpty()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("There is no search to add."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                return;
            }

            /*****/
            static QRegularExpression pattern(QStringLiteral("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)"));
            QRegularExpressionMatch match = pattern.match(soktext);

            if(!match.hasMatch()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Invalid link. Unable to add to download list."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                return;
            }

            /*****/
            QString resolution;
            // QStringList resolutionlist;
            //  resolutionlist    << "unknown" << "<720" << "<= 720"  << "<= 720" << "<= 1080" << ">1080";

            switch(ui->comboResolution->currentIndex()) {
                case 0:
                    resolution = QStringLiteral("unknown");
                    break;

                case 1:
                    resolution = QStringLiteral("less_than_720p");
                    break;

                case 2:
                    resolution = QStringLiteral("less_or_equal_720p");
                    break;

                case 3:
                    resolution = QStringLiteral("less_or_equal_1080p");
                    break;

                case 4:
                    resolution = QStringLiteral("more_than_1080p");
                    break;
            }

            QString in, provider, sub;

            if((ui->comboPayTV->currentText().indexOf(QStringLiteral(u"-"), 0)) == 0) {
                provider = QStringLiteral("npassword");
            } else {
                provider = ui->comboPayTV->currentText();
            }

            if(ui->actionSubtitle->isChecked()) {
                sub = QStringLiteral("ysub");
            } else if(ui->actionAllSubtitles->isChecked()) {
                sub = QStringLiteral("asub");
            } else if(ui->actionMergeSubtitle->isChecked()) {
                sub = QStringLiteral("ymsub");
            } else if(ui->actionDownloadRaw->isChecked()) {
                sub = QStringLiteral("rawsub");
            } else {
                sub = QStringLiteral("nsub");
            }

            //  Bugg V0.15.0
            if(soktext.right(1) == '/') {
                int storlek = soktext.size();
                soktext.remove(storlek - 1, 1);
            }

            int hittat = soktext.indexOf('?');
            soktext = soktext.left(hittat);
            /* ADD 'st' COOKIE TO DOWNLOAD LIST */
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Stcookies");
            QString stcookie = settings.value("stcookie", "").toString();
            settings.endGroup();

            if(stcookie.isEmpty()) {
                stcookie = "nst";
            }

            QString amount;

            if(ui->comboAmount->currentText().at(0) == QStringLiteral("-")) {
                amount = "unknown";
            } else {
                amount = ui->comboAmount->currentText();
            }

            QString method, quality;

            if((ui->comboBox->currentText().at(0)) == QStringLiteral("-")) {
                method = QStringLiteral("unknown");
                quality = QStringLiteral("unknown");
            } else {
                method = ui->leMethod->text();
                method = method.trimmed();
                method = method.toLower();
                quality = ui->leQuality->text();
                quality = quality.trimmed();
                quality = quality.toLower();
            }

            in = soktext + "," + method + "," + quality + "," + provider + "," + sub + "," + stcookie + "," + amount + "," + QString(resolution);
            QFile file(downloadlistfile);
            QTextStream infile(&file);

            if(!file.open(QIODevice::WriteOnly | QIODevice::Text |  QIODevice::Append)) {
            } else {
                infile << in;
            }

            file.close();
            QByteArray line;
            QStringList stringlist;

            if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
            } else {
                while(!file.atEnd()) {
                    line = file.readLine();
                    stringlist << QString(line);
                }
            }

// ipac 2
            file.close();
            file.resize(0);
            QStringList tmp_list = stringlist;
            stringlist.clear();

            foreach(QString s, tmp_list) {
                s.remove('\n');
                stringlist << s;
            }

            stringlist.removeDuplicates();
            // QString data = stringlist.join('\n');
            QString data;

            foreach(QString s, stringlist) {
                data += s + '\n';
            }

            saveDownloadList(&data);
            ui->pbDownloadAll->setEnabled(true);
            ui->actionDownloadAll->setEnabled(true);
        });

        connect(ui->pbDownloadAll, &QPushButton::clicked, this, &Newprg::downloadAll);
        connect(ui->actionDownloadAll, &QAction::triggered, this, &Newprg::downloadAll);
        connect(ui->comboBox, QOverload<int>::of(&QComboBox::currentIndexChanged),
                this, &Newprg::comboBoxChanged);
        connect(ui->actionCreateNew, &QAction::triggered, this,
        [this]() {
            newSupplier();
        });

        connect(ui->comboPayTV, QOverload<int>::of(&QComboBox::currentIndexChanged), this,
        [ = ](int index) {
            if(index == 0) {
                ui->pbPassword->setEnabled(false);
                ui->actionPassword->setEnabled(false);
            } else {
                ui->pbPassword->setEnabled(true);
                ui->actionPassword->setEnabled(true);
            }
        });

        connect(ui->pbPassword, &QPushButton::clicked, this, [this]() {
            QString provider = ui->comboPayTV->currentText();
            bool ok;
            QString newpassword = QInputDialog::getText(
                                      nullptr,
                                      provider + " " +
                                      QCoreApplication::translate("MainWindow", "Enter your password"),
                                      QCoreApplication::translate(
                                          "MainWindow",
                                          "Spaces are not allowed. Use only the characters your streaming "
                                          "provider approves.\nThe Password will not be saved."),
                                      QLineEdit::Password, QLatin1String(""), &ok);

            if(newpassword.indexOf(' ') >= 0) {
                return false;
            }

            if(ok) {
                secretpassword = newpassword;
                return true;
            }

            return true;
        });

        connect(ui->actionPassword, &QAction::triggered, this, [this]() {
            QString provider = ui->comboPayTV->currentText();
            bool ok;
            QString newpassword = QInputDialog::getText(
                                      nullptr,
                                      provider + " " +
                                      QCoreApplication::translate("MainWindow", "Enter your password"),
                                      QCoreApplication::translate(
                                          "MainWindow",
                                          "Spaces are not allowed. Use only the characters your streaming "
                                          "provider approves.\nThe Password will not be saved."),
                                      QLineEdit::Password, "", &ok);

            if(newpassword.indexOf(' ') >= 0) {
                return false;
            }

            if(ok) {
                secretpassword = newpassword;
                return true;
            }

            return true;
        });

        /* Delete all settings */
        connect(ui->actionDeleteAllSettings, &QAction::triggered, this,
        [this]() {
            deleteAllSettings();
        });

#if defined(Q_OS_WIN) // QT5
        connect(ui->actionStopAllDownloads, &QAction::triggered, this, [this]() {
            if(processpid > 0) {
                //#if defined(Q_OS_LINUX)
                // int rv = QProcess::execute("kill -9 " + QString::number(processpid));
                // int rv = QProcess::execute("pkill svtplay-dl");
                // int rv = QProcess::startDetached("pkill", {"svtplay-dl"});
                // int rv = QProcess::startDetached("pkill",
                // {QString::number(processpid)}); int rv = system("pkill svtplay-dl");
                //#endif
                //#if defined(Q_OS_WIN) // QT5
                QStringList argument;
                argument << "/IM" << svtplaydl << "/F";
                int rv = QProcess::execute("taskkill", argument);
                //            int rv = QProcess::execute("taskkill /IM " + svtplaydl + "
                //            /F");
                //#endif
                // QString txtcolor = ui->teOut->textColor().name(QColor::HexRgb);

                switch(rv) {
                    case -1:
                        ui->teOut->append(
                            QCoreApplication::translate("MainWindow", "svtplay-dl crashed."));
                        break;

                    case -2:
                        ui->teOut->append(QCoreApplication::translate(
                                              "MainWindow", "Could not stop svtplay-dl."));
                        break;

                    default:
                        ui->teOut->append(
                            QCoreApplication::translate("MainWindow",
                                                        "svtplay-dl stopped. Exit code ") +
                            QString::number(rv) + '\n' +
                            QCoreApplication::translate(
                                "MainWindow",
                                "Delete any files that may have already been downloaded."));
                        avbrutet = true;
                        processpid = 0;
                }
            }
        });

#endif
// SET DEFAULT LOCATION
//
        connect(ui->actionCopyToDefaultLocation, &QAction::triggered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString copypath = settings.value("copypath").toString();

            if(ui->actionCopyToDefaultLocation->isChecked()) {
                settings.setValue("copytodefaultlocation", "true");
                ui->statusBar->showMessage(
                    QCoreApplication::translate("MainWindow", "Copy to: ") + copypath);
            } else {
                settings.setValue("copytodefaultlocation", "false");
            }

            settings.endGroup();
        });

        connect(ui->actionSetDefaultCopyLocation, &QAction::triggered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString defaultdownloadlocation = settings.value("copypath", QStandardPaths::writableLocation(QStandardPaths::MoviesLocation)).toString();
            settings.endGroup();
            QFileDialog filedialog(this);
            int x = this->x();
            int y = this->y();
            //        filedialog.setOption(QFileDialog::DontUseNativeDialog, true);
            filedialog.setDirectory(defaultdownloadlocation);
            filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
            filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
            filedialog.setWindowTitle(tr("Copy streaming media to directory"));
            filedialog.setGeometry(x + 50, y + 50, 900, 550);
            filedialog.setFileMode(QFileDialog::Directory);//Folder name

            if(filedialog.exec() == 1) {
                QDir dir = filedialog.directory();
                QString sdir = dir.path();
#if defined(Q_OS_WIN)
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
                // NTFS
                qt_ntfs_permission_lookup++; // turn checking on
#else
                qEnableNtfsPermissionChecks();  // turn checking on
#endif
#endif
                QFileInfo fi(sdir);

                if(!fi.isWritable()) {
                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                    msgBox.setText(tr("You do not have the right to save to") + '\n' + QDir::toNativeSeparators(sdir) + '\n' + tr("and it cannot be used as the default folder to copy to."));
                    msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                    msgBox.exec();
                    ui->teOut->clear();
#if defined(Q_OS_WIN)
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
                    // NTFS
                    qt_ntfs_permission_lookup--; // turn checking off
#else
                    qDisableNtfsPermissionChecks();  // turn checking off
#endif
#endif
                    return;
                }

#if defined(Q_OS_WIN)
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
                // NTFS
                qt_ntfs_permission_lookup--; // turn checking off
#else
                qDisableNtfsPermissionChecks();  // turn checking off
#endif
#endif
                settings.beginGroup("Path");
                settings.setValue("copypath", sdir);
                settings.endGroup();
            }
        });

        /* DOWNLOAD LOCATION */
        connect(ui->actionSetDefaultDownloadLocation, &QAction::triggered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString defaultdownloadlocation = settings.value("savepath", QStandardPaths::writableLocation(QStandardPaths::MoviesLocation)).toString();
            settings.endGroup();
            QFileDialog filedialog(this);
            int x = this->x();
            int y = this->y();
            //        filedialog.setOption(QFileDialog::DontUseNativeDialog, true);
            filedialog.setDirectory(defaultdownloadlocation);
            filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
            filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
            filedialog.setWindowTitle(tr("Download streaming media to directory"));
            filedialog.setGeometry(x + 50, y + 50, 900, 550);
            filedialog.setFileMode(QFileDialog::Directory);//Folder name

            if(filedialog.exec() == 1) {
                QDir dir = filedialog.directory();
                QString sdir = dir.path();
#if defined(Q_OS_WIN)
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
                // NTFS
                qt_ntfs_permission_lookup++; // turn checking on
#else
                qEnableNtfsPermissionChecks();  // turn checking on
#endif
#endif
                QFileInfo fi(sdir);

                if(!fi.isWritable()) {
                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                    msgBox.setText(tr("You do not have the right to save to") + '\n' + QDir::toNativeSeparators(sdir) + '\n' + tr("and it cannot be used as the default folder for downloads."));
//                        msgBox.addButton(QMessageBox::Ok);
//                        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                    msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                    msgBox.exec();
                    ui->teOut->clear();
#if defined(Q_OS_WIN)
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
                    // NTFS
                    qt_ntfs_permission_lookup--; // turn checking off
#else
                    qDisableNtfsPermissionChecks();  // turn checking off
#endif
#endif
                    return;
                }

#if defined(Q_OS_WIN)
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
                // NTFS
                qt_ntfs_permission_lookup--; // turn checking off
#else
                qDisableNtfsPermissionChecks();  // turn checking off
#endif
#endif
                settings.beginGroup("Path");
                settings.setValue("savepath", sdir);
                settings.endGroup();
            }
        });

        /* DOWNLOAD LOCATION  HOOVER */
        connect(ui->actionSetDefaultDownloadLocation, &QAction::hovered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString defaultdownloadlocation =
                settings.value("defaultdownloadlocation", "NOTFIND").toString();
            settings.endGroup();

            if(defaultdownloadlocation != "NOTFIND")
                ui->actionSetDefaultDownloadLocation->setStatusTip(
                    defaultdownloadlocation);
        });

        connect(ui->actionDownloadToDefaultLocation, &QAction::triggered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString defaultdownloadlocation =
                settings.value("defaultdownloadlocation").toString();

            if(ui->actionDownloadToDefaultLocation->isChecked()) {
                settings.setValue("downloadtodefaultlocation", "true");
                ui->statusBar->showMessage(
                    QCoreApplication::translate("MainWindow", "Download to: ") +
                    defaultdownloadlocation);
            } else {
                settings.setValue("downloadtodefaultlocation", "false");
            }

            settings.endGroup();
        });

        /* Font */
        connect(ui->actionSelectFont, &QAction::triggered, this, [this] {
            // QFont font = ui->teOut->font();
            SelectFont *mSelectFont;
            mSelectFont = new SelectFont(this);

            // mSelectFont->setAttribute(Qt::WA_DeleteOnClose);
            QIcon icon(QPixmap(":/images/appicon.png"));

            mSelectFont->sFont(DISPLAY_NAME, EXECUTABLE_NAME, icon);

            connect(mSelectFont, &SelectFont::valueChanged, this, &::Newprg::setFontValue);
            connect(mSelectFont, &SelectFont::isClosing, this, &::Newprg::selectFontIsClosing);
            ui->actionSelectFont->setEnabled(false);
            // ui->teOut->setFont(font);
        });
        /* Väljer svtplay-dl version */
        /* STABLE */
        connect(ui->actionSvtplayDlStable, &QAction::triggered, this, [this]() -> void {

            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, LIBRARY_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString path = settings.value(QStringLiteral("stablepath")).toString();
            settings.endGroup();

#if defined(Q_OS_LINUX) // Linux
            svtplaydl = path + QStringLiteral("/stable/svtplay-dl");
#endif
#if defined(Q_OS_WIN)
            svtplaydl = QDir::toNativeSeparators(path + QStringLiteral("/stable/svtplay-dl.exe"));

#endif


            if(!fileExists(svtplaydl)) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("svtplay-dl stable cannot be found.") + '\n' + tr("Or is not an executable program.") + '\n' + tr("Please go to \"svtplay-dl\" and \"Download svtplay-dl...\""));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                svtplaydl = "NOTFIND";
            }

            ui->actionSvtplayDlStable->setChecked(true);
            // ui->actionSvtplayDlBleedingEdge->setChecked(false);
            // ui->actionSvtplayDlSystem->setChecked(false);
            // ui->actionSvtPlayDlManuallySelected->setChecked(false);
        });
        /* BLEEDING EDGE */
        connect(
        ui->actionSvtplayDlBleedingEdge, &QAction::triggered, this, [this]() -> void {

            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, LIBRARY_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString path = settings.value("betapath").toString();
            settings.endGroup();

#if defined(Q_OS_LINUX) // Linux
            svtplaydl = path + "/beta/svtplay-dl";
#endif
#if defined(Q_OS_WIN)
            svtplaydl = path + "/beta/svtplay-dl.exe";

#endif

            if(!fileExists(svtplaydl)) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("svtplay-dl beta cannot be found.") + '\n' + tr("Or is not an executable program.") + '\n' + tr("Please go to \"svtplay-dl\" and \"Download svtplay-dl...\""));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                svtplaydl = "NOTFIND";
            }

            // ui->actionSvtplayDlStable->setChecked(false);
            // ui->actionSvtplayDlSystem->setChecked(false);
            // ui->actionSvtPlayDlManuallySelected->setChecked(false);
            ui->actionSvtplayDlBleedingEdge->setChecked(true);

            /* SYSTEM */
        });
        connect(ui->actionSvtplayDlSystem, &QAction::triggered, this, [this]() -> void {
            QString EXECUTE = findSvtplayDl();

            // ui->actionSvtplayDlStable->setChecked(false);
            // ui->actionSvtPlayDlManuallySelected->setChecked(false);
            ui->actionSvtplayDlSystem->setChecked(true);
            // ui->actionSvtplayDlBleedingEdge->setChecked(false);
// ipac
            if(EXECUTE == QStringLiteral("NOTFIND")) {
                svtplaydl = "NOTFIND";
                QMessageBox msgBox1;
                msgBox1.setIcon(QMessageBox::Critical);
                msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox1.setText(tr("svtplay-dl cannot be found in the system path.") + '\n' + tr("Or is not an executable program.") + '\n' + tr("Please click on \"svtplay-dl\" and select svtplay-dl."));
                msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox1.exec();
                svtplaydl = "NOTFIND";
                return;
            }



            if(EXECUTE != "NOTFIND")
            {
#if defined(Q_OS_LINUX) // Linux
                svtplaydl = QDir::toNativeSeparators(EXECUTE);
#endif
#if defined(Q_OS_WIN) // Windows
                svtplaydl = QDir::toNativeSeparators(EXECUTE);
#endif
            }
        });
        /* VÄLJA svtplay-dl */
        connect(ui->actionSvtPlayDlManuallySelect, &QAction::triggered, this, [this]() -> void {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                               DISPLAY_NAME, EXECUTABLE_NAME);
            // settings.setIniCodec("UTF-8");
            settings.beginGroup("Path");
            QString svtplaydlpath = settings.value("svtplaydlpath", QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toString();
            settings.endGroup();
            QFileInfo info(svtplaydlpath);
            QString path = info.absolutePath();
#if defined(Q_OS_LINUX) // Linux


            QFileDialog filedialog(this);
            int x = this->x();
            int y = this->y();

            filedialog.setDirectory(path);
            filedialog.setLabelText(QFileDialog::Accept, tr("Choose"));
            filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
            filedialog.setWindowTitle(tr("Select svtplay-dl"));
            filedialog.setGeometry(x + 50, y + 50, 900, 550);
            filedialog.setFileMode(QFileDialog::ExistingFile);//Folder name
            QString fileName;

            if(filedialog.exec() == 1) {
                fileName = filedialog.selectedFiles().at(0);
                settings.beginGroup(QStringLiteral("Path"));
                settings.setValue(QStringLiteral("svtplaydlpath"), fileName);
                settings.endGroup();
                settings.sync();
            }

#endif
#if defined(Q_OS_WIN) // Windows 32- and 64-bit
            QString fileName = QFileDialog::getOpenFileName(
                this, tr("Select svtplay-dl"), path, tr("*.exe (svtplay-dl.exe)"));
#endif

            if(fileName.isNull())
            {
                return;
            }
            if(fileExists(fileName))
            {
                settings.beginGroup("Path");
                settings.setValue("svtplaydlpath", fileName);
                settings.endGroup();
                svtplaydl = fileName;
            } else
            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
#if defined(Q_OS_LINUX)
                msgBox.setText(tr("svtplay-dl is not an executable program."));
#endif
#if defined(Q_OS_WIN)
                msgBox.setText(tr("svtplay-dl.exe is not an executable program."));
#endif
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            }

            ui->actionSvtplayDlStable->setChecked(false);
            ui->actionSvtPlayDlManuallySelected->setChecked(true);
            ui->actionSvtplayDlSystem->setChecked(false);
            ui->actionSvtplayDlBleedingEdge->setChecked(false);
        });
        /* SLUT VÄLJA svtplay-dl */
        /* ANVÄNDA VALD svtplay-dl */
        connect(ui->actionSvtPlayDlManuallySelected, &QAction::triggered, this,
        [this]() -> void {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                               DISPLAY_NAME, EXECUTABLE_NAME);
            settings.beginGroup("Path");
            QString svtplaydlpath = settings.value("svtplaydlpath").toString();

            settings.endGroup();

            if(!fileExists(svtplaydlpath)) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                // if(svtplaydlpath == "NOTFIND") {
                //     svtplaydlpath = "svtplay-dl";
                // }
                msgBox.setText(svtplaydlpath + tr("The selected svtplay-dl cannot be found.") + '\n' + tr("Or is not an executable program.") + '\n' + tr("Please click \"svtplay-dl\" and \"Select svtplay-dl...\"") + '\n' + tr("to select svtplay-dl."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                svtplaydl = "NOTFIND";
            } else {
                svtplaydl = svtplaydlpath;
            }
            ui->actionSvtplayDlStable->setChecked(false);
            ui->actionSvtPlayDlManuallySelected->setChecked(true);
            ui->actionSvtplayDlSystem->setChecked(false);
            ui->actionSvtplayDlBleedingEdge->setChecked(false);
        });
        connect(ui->actionSvtPlayDlManuallySelect, &QAction::hovered, this, [this]() {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Path");
            QString svtplaydlpath = settings.value("svtplaydlpath", "NOTFIND").toString();
            settings.endGroup();

            if(svtplaydlpath != QStringLiteral("NOTFIND")) {
                ui->actionSvtPlayDlManuallySelect->setStatusTip(svtplaydlpath);
            }
        });

        /* End Väljer svtplay-dl */
// ceicer2
        connect(ui->actionMaintenanceTool, &QAction::triggered, this, [ = ]() -> void {
#if defined(Q_OS_LINUX) // Linux
// zsync
#ifdef EXPERIMENTAL
            QMessageBox msgBox;
            msgBox.setTextFormat(Qt::RichText);
            msgBox.setIcon(QMessageBox::Information);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("This is a BETA version. This AppImage can't be updated.") + "<br>" + tr("You can find more versions") + "<a href=" + EXPERIMENTAL_VERSION_LINK + " > " + tr("here") + ". < / a > ");
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
#endif
#ifndef EXPERIMENTAL
            // QObject class
            Update *update = new Update;
            connect(update, &Update::isUpdated, this, &Newprg::isUpdated);
            QPixmap *pixmap = new QPixmap(QStringLiteral(":/images/appicon.png"));
            // QDialog class
            updatedialog = new UpdateDialog;
            updatedialog->viewUpdate(pixmap);
            updatedialog->show();

            update->doUpdate(ARG1, ARG2, DISPLAY_NAME, updatedialog, pixmap);

#endif

#endif // Linux
#if defined(Q_OS_WIN) // Windows 32- and 64-bit
            //FROM
#ifndef OFFLINE_INSTALLER
            QString path =
            QDir::toNativeSeparators(QCoreApplication::applicationDirPath() +
                                     QStringLiteral("/streamCapture2MaintenanceTool.exe"));

            bool b = QProcess::startDetached(path, QStringList() << QLatin1String("--updater"));

            if(!b)

            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Maintenance Tool cannot be found.\nOnly if you install") + '\n' +
                               DISPLAY_NAME + '\n' +
                               tr("is it possible to update and uninstall the program."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            }
#endif
#ifdef OFFLINE_INSTALLER

            offlineInstaller();

#endif // END OFFLINE_INSTALLER
#endif
        });
#if defined(Q_OS_WIN)
//#ifdef PORTABLE
        connect(ui->actionDesktopShortcut, &QAction::triggered, this, []() -> void {
            QString source =
            QDir::toNativeSeparators(QCoreApplication::applicationFilePath());
#ifdef PORTABLE
            QString target = QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation);
            QString target2 = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);
#else
            QString target2 = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation);

#endif
            QString appname = DISPLAY_NAME;

            appname.append(QStringLiteral(u".lnk"));

            if(QFile::exists(appname)) {
                QFile::remove(appname);
            }

            if(!QFile::link(QDir::toNativeSeparators(source), QDir::toNativeSeparators(target2 + "/" + appname)))
            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Failed to create desktop shortcut.\nCheck your file permissions."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            } else
            {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                                   DISPLAY_NAME, EXECUTABLE_NAME);
                // settings.setIniCodec("UTF-8");
                settings.beginGroup(QStringLiteral(u"Path"));
                settings.setValue(
                    QStringLiteral(u"targetDesktopLocation"),
                    QDir::toNativeSeparators(target2 + QStringLiteral(u"/streamCapture2.lnk")));
                settings.endGroup();
            }

#ifdef PORTABLE

            if(!QFile::link(QDir::toNativeSeparators(source), QDir::toNativeSeparators(target + "/" + appname)))
            {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Failed to create desktop shortcut.\nCheck your file permissions."));
//                    msgBox.addButton(QMessageBox::Ok);
//                    msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
            } else
            {
                QSettings settings(QSettings::IniFormat, QSettings::UserScope,
                                   DISPLAY_NAME, EXECUTABLE_NAME);
                // settings.setIniCodec("UTF-8");
                settings.beginGroup(QStringLiteral("Path"));
                settings.setValue("targetApplicationLocation",
                                  QDir::toNativeSeparators(target + QStringLiteral("/streamCapture2.lnk")));
                settings.endGroup();
            }

#endif
        });
//#endif
#endif
        connect(ui->actionDesktopShortcut, &QAction::triggered, this, [this](bool checked) -> void {

            if(checked) {
                chortcutdesktop(2);
            } else {
                chortcutdesktop(1);
            }

        });
        connect(ui->actionApplicationsMenuShortcut, &QAction::triggered, this, [this](bool checked) -> void {

            if(checked) {
                chortcutapplications(2);
            } else {
                chortcutapplications(1);
            }

        });
    }

#if defined(Q_OS_LINUX) // Linux
    void Newprg::isUpdated(bool isupdated) {
//    If the update is successful, the old program will close.
        if(isupdated) {
            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               EXECUTABLE_NAME);
            settings.beginGroup("Update");
            settings.setValue("readyupdate", false);
            settings.endGroup();
            close();
        }
    }

#endif
// slots
    void Newprg::setFontValue(QFont font) {
        ui->teOut->setFont(font);
        emit sendNewFont(font);
    }

    void Newprg::selectFontIsClosing(bool is_closing) {
        if(is_closing) {
            ui->actionSelectFont->setEnabled(true);
        }
    }

// private slots:
    void Newprg::comboBoxChanged() {
        if(ui->comboBox->currentIndex() != 0) {
            QString val = ui->comboBox->currentText();
//            QRegularExpression number(QStringLiteral(u"\\d+"));
            QString method = val;
            QString quality = val;
            static QRegularExpression qre1("[a-zA-Z]");
            static QRegularExpression qre2("[0-9]");
            quality.remove(qre1);
            method.remove(qre2);
            method = method.trimmed();
            method = method.toLower();
            quality = quality.trimmed();
            ui->leQuality->setText(quality);
            ui->leMethod->setText(method.toUpper());
            ui->comboAmount->setEnabled(true);
        } else {
            ui->leQuality->setText(tr("Best quality is selected automatically"));
            ui->leMethod->setText(tr("Best method is selected automatically"));
            ui->comboAmount->setEnabled(false);
        }
    }

    void Newprg::onCommProcessStart() {
        ui->teOut->append(tr("Downloading..."));
    }

    void Newprg::onCommProcessExit_sok(int exitCode,
                                       QProcess::ExitStatus exitStatus) {
        CommProcess2->waitForFinished(-1);
        ui->progressBar->setValue(0);
        timer->stop();
        // ui->teOut->append("<br>" + tr("An error occurred while searching. Check that the address is correct"));

        if(exitCode == 0) {
            QTextStream stream(CommProcess2->readAllStandardOutput());
            QString s = "", sf = "", line = "";
            QStringList varannan;

            while(!stream.atEnd()) {
                line = stream.readLine();
                line = line.simplified();
                varannan << line;
            }

            varannan.removeDuplicates();
#if QT_VERSION  > QT_VERSION_CHECK(6, 0, 0)

            for(const QString &listitm : std::as_const(varannan)) {
                ui->comboBox->addItem(listitm);
                ui->teOut->append(listitm);
            }

#else

            for(const QString &listitm : qAsConst(varannan)) {
                ui->comboBox->addItem(listitm);
                ui->teOut->append(listitm);
            }

#endif
            // ui->pbDownload->setEnabled(true);
            // ui->actionDownload->setEnabled(true);
            ui->actionDownloadAllEpisodes->setEnabled(true);
            // ui->actionAddAllEpisodesToDownloadList->setEnabled(false);
            QString val = ui->comboBox->currentText();
            static QRegularExpression number("\\d+");
            int first_number = val.indexOf(number);
            int last_number = val.lastIndexOf(number);
            QString quality = val.mid(first_number, last_number - first_number + 1);
            quality = quality.trimmed();
//        first_number = val.indexOf(" ", -1);
//        QString method = val.mid(first_number, val.size() - first_number);
            static QRegularExpression qre("[0-9]");
            QString method = val.remove(qre);
            method = method.trimmed();
            method = method.toLower();
            ui->leQuality->setText(quality);
            method = method.trimmed();
            ui->leMethod->setText(method);
            bool lyckatsok = true;

            if(sf != "") {
                ui->teOut->setText(
                    tr("Can not find any video streams, please check the address.\n\n"));
                ui->teOut->append(sf);
                lyckatsok = false;
            }

            if(s.contains("ERROR:")) {
                ui->leQuality->clear();
                ui->leMethod->clear();
                ui->pbDownload->setDisabled(true);
                ui->actionDownload->setDisabled(true);
            }

            if((s.left(6) == QStringLiteral("ERROR:")) || (!lyckatsok)) {
                ui->leQuality->clear();
                ui->leMethod->clear();
                ui->pbDownload->setDisabled(true);
                ui->actionDownload->setDisabled(true);
            } else {
                recentFiles.append(address);

                if(ui->comboBox->currentText().isEmpty()) {
                    ui->pbAdd->setDisabled(true);
                    ui->actionAdd->setDisabled(true);
                } else {
                    ui->pbAdd->setEnabled(true);
                    ui->actionAdd->setEnabled(true);
                }
            }

            ui->teOut->append('\n' + tr("The search is complete") + '\n');
            ui->actionDownload->setEnabled(true);
        } else {
            ui->teOut->append('\n' + tr("The search failed") + '\n');
            ui->leMethod->setText("");
            ui->leQuality->setText("");
            ui->actionDownload->setEnabled(false);
        }

        statusExit(exitStatus, exitCode);
        delete CommProcess2;
        ui->comboBox->insertItem(0, "- " + tr("Select"));
        ui->comboBox->setCurrentIndex(0);
    }

    void Newprg::slotRecent() {
        recentFiles.removeDuplicates();

        while(recentFiles.size() > MAX_RECENT) {
            recentFiles.removeFirst();
        }

        ui->menuRecent->clear();
        QMenu *cmenu = new QMenu;
        cmenu->setContextMenuPolicy(Qt::CustomContextMenu);

        for(int i = recentFiles.size() - 1; i >= 0; i--) {
            QString path = recentFiles.at(i);
            auto actionRF = new QAction(path, this);
            actionRF->installEventFilter(this);
            actionRF->setShortcutVisibleInContextMenu(true);
            ui->menuRecent->addAction(actionRF);
            actionRF->setStatusTip(tr("Click to copy to the search box."));
            QAction *tabort = new QAction("Ta bort");
            cmenu->addAction(tabort);
            connect(tabort, &QAction::triggered, this,
            [cmenu]() {
                cmenu->exec();
            });

            /********************************
             * deprected
            QSignalMapper* signalMapperRF  = new QSignalMapper(this);
            connect(actionRF, SIGNAL(triggered()), signalMapperRF, SLOT(map()));
            signalMapperRF->setMapping(actionRF, path);
            connect(signalMapperRF, SIGNAL(mapped(const QString)), this,
            SLOT(recentRequested(const QString)));
            ***************************************************************/
            connect(actionRF, &QAction::triggered, this,
            [this, path]() {
                ui->leSok->setText(path);
            });
        }

        ui->menuRecent->addSeparator();
        QAction *actionAntalRecentFiles =
            new QAction(tr("The number of previous searches to be saved..."), this);
        ui->menuRecent->addAction(actionAntalRecentFiles);
        actionAntalRecentFiles->setStatusTip(tr(
                "Specify how many previous searches you want to save. If the number of "
                "searches exceeds the specified number, the oldest search is "
                "deleted."));
        connect(actionAntalRecentFiles, &QAction::triggered, this, [this]() {
            bool ok;
            QInputDialog inputdialog;
            inputdialog.setWindowFlags(inputdialog.windowFlags() & ~Qt::WindowContextHelpButtonHint);
            inputdialog.setOkButtonText(tr("Ok"));
            inputdialog.setCancelButtonText(tr("Cancel"));
            inputdialog.setWindowTitle(DISPLAY_NAME " " VERSION);
            inputdialog.setInputMode(QInputDialog::IntInput);
            inputdialog.setIntValue(MAX_RECENT);
            inputdialog.setIntMinimum(0);
            inputdialog.setIntStep(1);
            inputdialog.setIntMaximum(SIZE_OF_RECENT);
            inputdialog.setLabelText(tr("The number of searches to be saved: ") +
                                     "(0-" + QString::number(SIZE_OF_RECENT) + + ")");
            ok = inputdialog.exec();
            int antal = inputdialog.intValue();

            if(ok) {
                MAX_RECENT = antal;
            }
        });

        QAction *actionEditAntalRecentFiles = new QAction(tr("Edit saved searches..."), this);
        ui->menuRecent->addAction(actionEditAntalRecentFiles);
        actionEditAntalRecentFiles->setStatusTip(
            tr("Click to edit all saved searches."));
        connect(actionEditAntalRecentFiles, &QAction::triggered, this,
        [this]() {
            QString *content = new QString(recentFiles.join('\n'));
            QStringList *infoList = new QStringList;
            *infoList << tr("Edit saved searches");
            DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
            connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
            connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::getRecentSearches);
            mDownloadListDialog->initiate(infoList, content, false);
        });

        QAction *actionClearAntalRecentFiles =
            new QAction(tr("Remove all saved searches"), this);
        ui->menuRecent->addAction(actionClearAntalRecentFiles);
        actionClearAntalRecentFiles->setStatusTip(
            tr("Click to delete all saved searches."));
        connect(actionClearAntalRecentFiles, &QAction::triggered, this,
        [this]() {
            recentFiles.clear();
        });
    }

// private:
    void Newprg::deleteAllSettings() {
        // Delete chortcuts, if anny
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Path");
#if defined(Q_OS_LINUX)
#ifdef PORTABLE
        QString targetApplicationsLocation = settings.value("targetApplicationsLocation", "NOTHING").toString();
#endif
//    QString targetDesktopLocation = settings.value("targetDesktopLocation", "NOTHING").toString();
#endif
#if defined(Q_OS_WIN)
        QString targetDesktopLocation = settings.value("targetDesktopLocation").toString();
#endif
#ifdef PORTABLE
        QString targetApplicationLocation = settings.value("targetApplicationLocation").toString();
#endif
        settings.endGroup();
        const QString settingsFile = settings.fileName();
        int hitta = settingsFile.indexOf(QStringLiteral("/streamcapture2.ini"));
        int lagom = settingsFile.size() - hitta;
        QString settingsPath = settingsFile.mid(0, settingsFile.size() - lagom);
        QDir dir(settingsPath);
        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setIcon(QMessageBox::Warning);
        QPushButton  *yesButton = new QPushButton(tr("Yes"), msgBox);
        QPushButton  *noButton = new QPushButton(tr("Cancel"), msgBox);
        msgBox->setDefaultButton(noButton);
        msgBox->addButton(yesButton, QMessageBox::AcceptRole);
        msgBox->addButton(noButton, QMessageBox::RejectRole);
        msgBox->setDefaultButton(noButton);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("All your saved settings will be deleted.\nAll lists of files to "
                           "download will disappear.\nDo you want to continue?"));
        msgBox->exec();

        if(msgBox->clickedButton() == yesButton)  {
#if defined(Q_OS_LINUX)
            QString applicationslocations = QDir::toNativeSeparators(
                                                QStandardPaths::writableLocation(QStandardPaths::ApplicationsLocation) + "/streamCapture2.desktop");

            if(QFile::exists(applicationslocations)) {
                QFile::remove(applicationslocations);
            }

            QString desktopLocation = QDir::toNativeSeparators(
                                          QStandardPaths::writableLocation(QStandardPaths::DesktopLocation) + "/streamCapture2.desktop");

            if(QFile::exists(desktopLocation)) {
                QFile::remove(desktopLocation);
            }

#endif
#if defined(Q_OS_WIN)

            if(QFile::exists(targetDesktopLocation)) {
                QFile::remove(targetDesktopLocation);
            }

#endif
#ifdef PORTABLE

            if(QFile::exists(targetApplicationLocation)) {
                QFile::remove(targetApplicationLocation);
            }

#endif

            if(dir.removeRecursively()) {
                deleteSettings = true;
                exit(0);
            } else {
                // cppcheck
                QMessageBox msgBox1;
                msgBox1.setIcon(QMessageBox::Critical);
                msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox1.setText(tr("Failed to delete your configuration files.\n"
                                   "Check your file permissions."));
                msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox1.exec();
            }
        }
    }

    void Newprg::statusExit(QProcess::ExitStatus exitStatus, int exitCode) {
        if(ui->actionShowMore->isChecked()) {
            QString info;

            switch(exitStatus) {
                case QProcess::NormalExit:
                    info = QStringLiteral(u"svtplay-dl") + " " + tr("was normally terminated. Exit code = ") + QString::number(exitCode);
                    break;

                case QProcess::CrashExit:
                    info = QStringLiteral(u"svtplay-dl") + " " + tr("crashed. Exit code = ") + QString::number(exitCode);
                    break;
            }

            ui->statusBar->showMessage(info);
        }
    }
