// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include <QtGlobal>
#include <QMessageBox>
#include <QSettings>
#include <QDir>
#include <QFileInfo>
#include <QInputDialog>
#include <QStandardPaths>
/* DOWNLOAD */
void Newprg::download()
{
    getSvtplaydlVersion(&svtplaydl);

    if(!fileExists(svtplaydl)) {
        QMessageBox msgBox1;
        msgBox1.setIcon(QMessageBox::Critical);
        msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox1.setText(tr("svtplay-dl cannot be found.") + '\n' + tr("Or is not an executable program.") + '\n' + tr("Please click on \"svtplay-dl\" and select svtplay-dl."));
        msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox1.exec();
        return;
    }

    QString preferred, quality, amount, resolution;
    ui->teOut->clear();
    QStringList ARG;
    ARG << QStringLiteral("--verbose");
    ARG << QStringLiteral("--force");

    switch(ui->comboResolution->currentIndex()) {
        case 1:
            ARG << QStringLiteral("--resolution") << QStringLiteral("<720");
            resolution = QStringLiteral("less_than_720p");
            break;

        case 2:
            ARG << QStringLiteral("--resolution") << QStringLiteral("<=720");
            resolution = QStringLiteral("less_or_equal_720p");
            break;

        case 3:
            ARG << QStringLiteral("--resolution") << QStringLiteral("<=1080");
            resolution = QStringLiteral("less_or_equal_1080p");
            break;

        case 4:
            ARG << QStringLiteral("--resolution") << QStringLiteral(">1080");
            resolution = QStringLiteral("less_or_equal_1080p");
            break;
    }

    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup(QStringLiteral("Stcookies"));
    QString stcookie = settings.value(QStringLiteral("stcookie"), "").toString();
    settings.endGroup();

    if(ui->leSok->text().contains(QStringLiteral("https://www.tv4play.se"))) {
        settings.beginGroup("Stcookies");
        QString tv4token = settings.value(QStringLiteral("tv4token"), "").toString();
        settings.endGroup();

        if(!tv4token.isEmpty()) {
            ARG << "--token" << tv4token;
        } else {
            QMessageBox msgBox1;
            msgBox1.setIcon(QMessageBox::Critical);
            msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox1.setText(tr("You need to copy the TV4 token from your browser and save it in streamCapture2."));
            msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox1.exec();
            return;
        }
    }

    settings.beginGroup(QStringLiteral("Settings"));
    bool try4k = settings.value(QStringLiteral("try4k"), false).toBool();
    settings.endGroup();

    if(try4k) {
        ARG << QStringLiteral("--format") << QStringLiteral("dvhevc-51,dvhevc,hevc-51,hevc");
    }

    if(!stcookie.isEmpty()) {
        ARG << QStringLiteral("--cookies")  << QStringLiteral("st=") + stcookie;
    }

    avbrutet = false;
    ui->teOut->setReadOnly(true);
    QString explanation =  tr("The request is processed...") +  '\n' + tr("Preparing to download...");
    /*  */
    settings.beginGroup(QStringLiteral("Path"));
    QString savepath =
        settings.value(QStringLiteral("savepath")).toString();
    QString copypath = settings.value(QStringLiteral("copypath")).toString();
    settings.endGroup();
    settings.beginGroup("Settings");
    bool selectfilename = settings.value(QStringLiteral("selectfilename"), QStringLiteral("false")).toBool();
    settings.endGroup();
    QString save_path;
    QString save_path2;
    QString folderaddress;

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        save_path = savepath;
    } else {
        save_path = save(tr("Download streaming media to folder"));
    }

    if(save_path != "nothing") {
        QString kopierastill;
        amount = ui->comboAmount->currentText();

        if(amount.at(0) == '-') {
            amount = QStringLiteral("unknown");
        }

        if(ui->comboBox->currentText().at(0) == '-') {
            preferred = QStringLiteral("unknown");
            quality = QStringLiteral("unknown");
        } else {
            preferred = ui->leMethod->text();
            preferred = preferred.trimmed();
            preferred = preferred.toLower();
            quality = ui->leQuality->text();
            quality = quality.trimmed();
        }

        /* CREATE FOLDER */
        folderaddress = address;

        if(folderaddress.right(1) == '/') {
            int storlek = folderaddress.size();
            folderaddress.remove(storlek - 1, 1);
        }

        int hittat = folderaddress.lastIndexOf('/');
        int hittat2 = folderaddress.indexOf('?');
        folderaddress = folderaddress.mid(hittat, hittat2 - hittat);
//            save_path2 = QLatin1String("");

        if(ui->actionCreateFolder->isChecked()) {
            switch(ui->comboResolution->currentIndex()) {
                case 1:
                    resolution = QStringLiteral("less_than_720p");
                    break;

                case 2:
                    resolution = QStringLiteral("less_or_equal_720p");
                    break;

                case 3:
                    resolution = QStringLiteral("less_or_equal_1080p");
                    break;

                case 4:
                    resolution = QStringLiteral("more_than_1080p");
                    break;

                default:
                    resolution = QStringLiteral("unknown");
            }

            QDir dir(save_path + folderaddress + QStringLiteral("/") + preferred + '_' + quality + '_' + amount + '_' + resolution);

            if(!dir.exists()) {
                dir.mkpath(".");
            }

            save_path2 = dir.path();
        } else {
            save_path2 = save_path;
        }

#ifdef Q_OS_WIN
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
        // NTFS
        qt_ntfs_permission_lookup++; // turn checking on
#else
        qEnableNtfsPermissionChecks();  // turn checking on
#endif
#endif
        QFileInfo fi(save_path2);

        if(!fi.isWritable()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("You do not have the right to save to the "
                              "folder.\nDownload is interrupted."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
#ifdef Q_OS_WIN
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
            // NTFS
            qt_ntfs_permission_lookup--; // turn checking off
#else
            qDisableNtfsPermissionChecks();  // turn checking off
#endif
#endif
            return;
        }

#ifdef Q_OS_WIN
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
        // NTFS
        qt_ntfs_permission_lookup--; // turn checking off
#else
        qDisableNtfsPermissionChecks();  // turn checking off
#endif
#endif
        ui->teOut->append(tr("The video stream is saved in ") + "\"" + QDir::toNativeSeparators(save_path2 + "\""));

        if(ui->actionDownloadToDefaultLocation->isChecked()) {
            const QFileInfo downloadlocation(savepath);

            if(!downloadlocation.exists()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("The default folder for downloading video streams cannot be "
                                  "found.\nDownload is interrupted."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                ui->teOut->clear();
                return;
            }

#ifdef Q_OSS_WIN
            qt_ntfs_permission_lookup++; // turn checking on
#endif

            if(!downloadlocation.isWritable()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("You do not have the right to save to the "
                                  "default folder.\nDownload is interrupted."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                ui->teOut->clear();
#ifdef Q_OSS_WIN
                qt_ntfs_permission_lookup--; // turn checking of
#endif
                return;
            }
        } else {
#ifdef Q_OSS_WIN
            qt_ntfs_permission_lookup--; // turn checking of
#endif
            const QFileInfo downloadlocation(save_path);

            if(!downloadlocation.exists()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("The folder for downloading video streams cannot be "
                                  "found.\nDownload is interrupted."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                ui->teOut->clear();
                return;
            }

            if(!downloadlocation.isWritable()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("You do not have the right to save to the "
                                  "folder.\nDownload is interrupted."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                ui->teOut->clear();
                return;
            }
        }

        kopierastill = tr("Selected folder to copy to is ") + "\"" +
                       QDir::toNativeSeparators(copypath + "\"");
        QProcess *CommProcess2 = new QProcess(nullptr);
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        const QString path = QCoreApplication::applicationDirPath();
        const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
        env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN                               // QT5
        env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
        CommProcess2->setProcessEnvironment(env);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess2->setWorkingDirectory(save_path2);

        if(ui->actionSubtitle->isChecked()) {
            ARG << QStringLiteral("--subtitle");
        } else if(ui->actionAllSubtitles->isChecked()) {
            ARG << QStringLiteral("--all-subtitles");
        } else if(ui->actionMergeSubtitle->isChecked()) {
            ARG << QStringLiteral("--subtitle") << QStringLiteral("--merge-subtitle");
        } else if(ui->actionDownloadRaw->isChecked()) {
            ARG << QStringLiteral("--raw-subtitles");
        }

        if(ui->comboBox->currentText().at(0) != '-') {
            if(amount != QStringLiteral("unknown")) {
                ARG << QStringLiteral("--quality") << quality << QStringLiteral("--preferred") << preferred << QStringLiteral("--flexible-quality") << amount;
            } else {
                ARG << QStringLiteral("--quality") << quality << QStringLiteral("--preferred") << preferred;
            }
        }

        if(ui->comboPayTV->currentIndex() != 0) {  // Password
            {
                /* */
// cppcheck
//                    QSettings settings(QSettings::IniFormat, QSettings::UserScope,
//                                       DISPLAY_NAME, EXECUTABLE_NAME);
//                    // settings.setIniCodec("UTF-8");
                QString provider = ui->comboPayTV->currentText();
                settings.beginGroup("Provider");
                QString username = settings.value(provider + QStringLiteral("/username")).toString();
                QString password = settings.value(provider + QStringLiteral("/password")).toString();
                settings.endGroup();

                if(password == "") {
                    password = secretpassword;

                    if(password == "") {
                        bool ok;
                        QInputDialog inputdialog;
                        inputdialog.setWindowFlags(inputdialog.windowFlags() & ~Qt::WindowContextHelpButtonHint);
                        inputdialog.setOkButtonText(tr("Ok"));
                        inputdialog.setCancelButtonText(tr("Cancel"));
                        inputdialog.setWindowTitle(tr("Enter your password"));
                        inputdialog.setInputMode(QInputDialog::TextInput);
                        inputdialog.setTextEchoMode(QLineEdit::Password);
                        inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
                        ok = inputdialog.exec();
                        QString newpassword = inputdialog.textValue();

                        if(newpassword.indexOf(' ') >= 0) {
                            return;
                        }

                        if(ok) {
                            secretpassword = newpassword;
                        }
                    }
                }

                /* */
                ARG << QStringLiteral("--username") << username << QStringLiteral("--password") << password;
            }
        }

        /* */
        auto *filnamn = new QString;
        ui->teOut->append(explanation);
        ui->teOut->append(tr("Starts downloading: ") + "\"" + address + "\"");
        connect(CommProcess2, &QProcess::readyReadStandardOutput, this,
        [this, filnamn, CommProcess2]() {
            QString result(CommProcess2->readAllStandardOutput());

            if(result.trimmed().contains("Can't find any streams with that video resolution")) {
                ui->teOut->append(tr("Unable to find any streams with the selected video resolution."));
            }

            if(result.contains(QStringLiteral(".mp4"))) {
                int stopp = result.indexOf(QStringLiteral(".mp4")) + 4;
                int start = result.lastIndexOf(" ", stopp) + 1;
                int langd = result.size();
                int storlek = langd - start - stopp;
                QString tmp = result.mid(start, storlek);
                tmp = tmp.trimmed();
                *filnamn = tmp;
            }

            if(ui->actionShowMore->isChecked()) {
                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                ui->progressBar->setValue(ui->progressBar->value() + 1);
                ui->teOut->append(result);
            } else {
                // progressbar
                if(ui->progressBar->value() >= 100) {
                    ui->progressBar->setValue(0);
                }

                if(result.trimmed().contains(QStringLiteral("Merge audio and video"))) {
                    ui->teOut->append(tr("Merge audio and video..."));
                }

                if(result.trimmed().contains(QStringLiteral("removing old files"))) {
                    ui->teOut->append(tr("Removing old files, if there are any..."));
                }

                ui->progressBar->setValue(ui->progressBar->value() + 1);
            }
        });

#if (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
        QObject::connect(CommProcess2, &QProcess::finished, this, [this, filnamn, preferred, quality, amount, resolution, kopierastill, folderaddress, save_path2](int exitCode, QProcess::ExitStatus exitStatus) {
#else
        connect(
            CommProcess2,
            QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
        [ this, filnamn, preferred, quality, amount, resolution, kopierastill, folderaddress, save_path2 ](int exitCode, QProcess::ExitStatus exitStatus) {
#endif

            if(!avbrutet) {
                if(filnamn->isEmpty()) {
                    QString *x_filnamn = new QString;
                    *x_filnamn = *filnamn;
                    x_filnamn->replace("\\\\", "\\", Qt::CaseSensitivity::CaseInsensitive);
                    ui->teOut->append(tr("The download failed.") + "\"" + preferred + ", " + quality + ", " + amount + ", " + resolution + "\"");

                    if(!ui->actionNotifications->isChecked()) {
                        showNotification(tr("The download failed.") + "\"" + preferred + ", " + quality + ", " + amount + ", " + resolution + "\"", 3);
                    }
                } else {
                    QString *x_filnamn = new QString;
                    *x_filnamn = *filnamn;
                    x_filnamn->replace("\\\\", "\\", Qt::CaseSensitivity::CaseInsensitive);
                    ui->teOut->append(tr("Download succeeded") + " \"" + *x_filnamn + "\" " + preferred + ", " + quality + ", " + amount + "," + resolution);
                    ui->progressBar->setValue(10);
                    QString undertexten = *filnamn;
                    int langd = undertexten.size();
                    undertexten = undertexten.replace(langd - 3, 3, "srt");

                    if(QFile::exists(save_path2 + "/" + undertexten)) {
                        ui->teOut->append(tr("Download succeeded") + " \"" + undertexten + "\"");
                    }

// copy

                    if(ui->actionCopyToDefaultLocation->isChecked()) {
                        QString *copyto = new QString;

                        if(ui->actionCreateFolder->isChecked()) {
                            QString resolution;

                            switch(ui->comboResolution->currentIndex()) {
                                case 1:
                                    resolution = QStringLiteral("less_than_720p");
                                    break;

                                case 2:
                                    resolution = QStringLiteral("less_or_equal_720p");
                                    break;

                                case 3:
                                    resolution = QStringLiteral("less_or_equal_1080p");
                                    break;

                                case 4:
                                    resolution = QStringLiteral("more_than_1080p");
                                    break;
                            }

                            *copyto = folderaddress + "/" + preferred + '_' + quality + '_' + amount + '_' + resolution;
                            copyToDefaultLocation(*filnamn, &save_path2, copyto);
                        } else {
                            copyto->clear();
                            copyToDefaultLocation(*filnamn, &save_path2, copyto);
                        }
                    }

                    ui->teOut->append(tr("Download completed"));
                    ui->progressBar->setValue(0);

                    if(!ui->actionNotifications->isChecked()) {
                        // showNotification
                        showNotification(tr("Download completed"), 1);
                    }

                    if(ui->actionCopyToDefaultLocation->isChecked()) {
                        ui->teOut->append(kopierastill);
                    }

                    // hit
                }
            } else {
                ui->teOut->append(tr("The download failed "));

                if(!ui->actionNotifications->isChecked()) {
                    // showNotification
                    showNotification(tr("The download failed "), 3);
                }

                avbrutet = false;
            }

            statusExit(exitStatus, exitCode);
            /* ... */
        });

        if(selectfilename) {
            // SELECT FILE NAME
            QInputDialog selectFilename;
            selectFilename.setWindowFlags(selectFilename.windowFlags() & ~Qt::WindowContextHelpButtonHint);
            selectFilename.setSizeGripEnabled(true);
            selectFilename.setCancelButtonText(tr("Cancel"));
            selectFilename.setOkButtonText(tr("Ok"));
            selectFilename.setWindowTitle(tr("Save as..."));
            bool ok;
            QString filename = folderaddress.remove("/");
            selectFilename.setInputMode(QInputDialog::TextInput);
            selectFilename.setLabelText(tr("Select file name"));
            selectFilename.setTextValue(filename);
            selectFilename.setModal(true);
            ok = selectFilename.exec();

            if(ok) {
                folderaddress = "/" + selectFilename.textValue().trimmed();
            }
        }

        // END SELECT FILE NAME
//        QString tmp = save_path2  + folderaddress + "-x-";
        QString tmp = save_path2  + folderaddress;
        ARG << QStringLiteral("--output") << tmp;
        ARG << address;
//        CommProcess2->setWorkingDirectory(save_path2);
        CommProcess2->start(svtplaydl, ARG);
//        CommProcess2->start(svtplaydl, QStringList() << "--help");
        ARG.clear();
    } else {
        ui->teOut->setText(tr("No folder is selected"));
    }
//    save_path2.clear();
//    folderaddress.clear();
//    CommProcess2->deleteLater();
}

/* END DOWNLOAD */
void Newprg::initSok()
{
    if(ui->actionSvtplayDlSystem->isChecked()) {
#if defined(Q_OS_LINUX)
        QString executable = QStandardPaths::findExecutable(QStringLiteral("svtplay-dl"));
#elif defined(Q_OS_WIN)
        QString executable = QStandardPaths::findExecutable(QStringLiteral("svtplay-dl.exe"));
#endif

        if(!fileExists(executable)) {
            ui->teOut->clear();
            svtplaydl = QStringLiteral("NOTFIND");
            return;
        } else {
            svtplaydl = QStringLiteral("svtplay-dl");
        }
    }

    ui->teOut->setReadOnly(true);
    ui->comboBox->clear();
    ui->leQuality->setText(tr("Searching..."));
    ui->leMethod->setText(tr("Searching..."));
    ui->teOut->setText(tr("The request is processed...\nStarting search..."));
}
