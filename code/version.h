
#ifndef VERSION_H
#define VERSION_H

#define VER_FILEVERSION             3,0,1,0
#define VER_FILEVERSION_STR         "3.0.1.0\0"

#define VER_PRODUCTVERSION          3,0,1,0
#define VER_PRODUCTVERSION_STR      "3.0.1\0"

#define VER_COMPANYNAME_STR         "Ingemar Ceicer"
#define VER_FILEDESCRIPTION_STR     "Downloads video streams."
#define VER_INTERNALNAME_STR        "streamCapture2"
#define VER_LEGALCOPYRIGHT_STR      "Copyright (C) 2016 - 2025 Ingemar Ceicer"
#define VER_LEGALTRADEMARKS1_STR    "All Rights Reserved"
#define VER_LEGALTRADEMARKS2_STR    VER_LEGALTRADEMARKS1_STR
#define VER_ORIGINALFILENAME_STR    "streamcapture2.exe"
#define VER_PRODUCTNAME_STR         "streamCapture2"

#define VER_COMPANYDOMAIN_STR       "https://gitlab.com/posktomten/streamcapture2"

#endif // VERSION_H

