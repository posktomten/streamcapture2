
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#include "newprg.h"
#include <QMessageBox>
#include <QFileInfo>
#include <QDir>
#ifdef Q_OS_WINDOWS
#include "download_install.h"
#endif

#ifdef OFFLINE_INSTALLER

void Newprg::offlineInstaller()
{
    QMessageBox *msgBox = new QMessageBox(this);
    msgBox->setInformativeText("<a style=\"text-decoration:none\" href = \""  APPLICATION_HOMEPAGE_ENG   "\">" + tr("To the website") + "</a>");
    msgBox->setIcon(QMessageBox::Information);
    msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox->setText(tr("Download the latest version"));
    QAbstractButton *pbCancel = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
    pbCancel->setFixedSize(QSize(150, 40));
    QAbstractButton *pbUninstall = msgBox->addButton(tr("Uninstall"), QMessageBox::AcceptRole);
    pbUninstall->setFixedSize(QSize(150, 40));
    QAbstractButton *pbUninstallInstall = msgBox->addButton(tr("Download the new version"), QMessageBox::AcceptRole);
    pbUninstallInstall->setFixedSize(QSize(300, 40));
    pbUninstallInstall->setFocus();
    msgBox->exec();

    if(msgBox->clickedButton() == pbCancel) {
        return;
    }

    if(msgBox->clickedButton() == pbUninstall) {
        const QString EXECUTE =
            QCoreApplication::applicationDirPath() + "/" +
            "uninstall.exe";
        //
        QFileInfo fi(EXECUTE);

        if(!fi.isExecutable()) {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("An unexpected error occurred.<br>") + "\"" + QDir::toNativeSeparators(EXECUTE) + "\"" + tr("<br>can not be found or is not an executable program."));
            msgBox->exec();
            return;
        }

        //
        QProcess p;
        p.setProgram(EXECUTE);
        p.startDetached();
        close();
        return;
    } else if(msgBox->clickedButton() == pbUninstallInstall) {
        close();
        DownloadInstall *mDownloadInstall(nullptr);
        mDownloadInstall = new DownloadInstall;
        QString *path = new QString(QStringLiteral(PATH));
        QString *filename = new QString(QStringLiteral(FILENAME));
        QString *display_name = new QString(QStringLiteral(DISPLAY_NAME));
        QString *version = new QString(QStringLiteral(VERSION));
        mDownloadInstall->setValues(path, filename, display_name, version);
        mDownloadInstall->show();
    }
}

#endif // OFFLINE_INSTALLER
