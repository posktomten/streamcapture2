// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

//          CLI
//          Copyright (C) 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/template
//          programming@ceicer.com

// A template program with frequently used functions.
// Change language, change style, show "About" dialog.
// View the license, check ether updates.
// The program can update itself.
// Works with Qt5 and Qt6.
// With Linux and with Windows operating systems.
// Works with 32-bit and with 64-bit operating systems.

// CLI is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
// And NO LATER versions are allowed.
// GPL Version 3.0 only.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef CLI_H
#define CLI_H
#include "checkupdcli.h"
#include <QCoreApplication>
#include <QObject>
#include <QApplication>
#include <QNetworkReply>
#include <qprocess.h>


class Cli : public QObject
{
    Q_OBJECT
public:
    explicit Cli(QObject *parent = nullptr);

    void parseOptions(QStringList list);

private:
    bool fileExists(QString & path);
    Libcheckupdcli *mLibcheckupdcli;
    QString globalhashsum;

public slots:

    void getVersion(QString version);
    void getCHECKSUM(QString checksum);


    void doupdate(bool update);


};

#endif // CLI_H
