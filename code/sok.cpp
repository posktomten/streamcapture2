// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; withfout even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include <QFontMetrics>
#include "newprg.h"
//#include "qforeach.h"
#include "ui_newprg.h"
#include <QSettings>
#include <QTimer>
#include <QStandardPaths>
#include <QFileInfo>
#include <QMessageBox>
#include <QInputDialog>
#include <QProcess>
#include <QStringListModel>
/* SOK */

void Newprg::sok()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    QSettings settings2(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                        LIBRARY_NAME);
    settings.beginGroup(QStringLiteral("Settings"));
    QString svtplaydlversion = settings.value(QStringLiteral("svtplaydlversion"), QStringLiteral("stable")).toString();
    settings.endGroup();
    settings.beginGroup(QStringLiteral("Path"));
    QString svtplaydlpath = settings.value(QStringLiteral("svtplaydlpath")).toString();
    settings.endGroup();
    settings2.beginGroup(QStringLiteral("Path"));
    QString stablepath = settings2.value(QStringLiteral("stablepath")).toString();
    // QString betapath = settings2.value(QStringLiteral("betapath")).toString();
    settings2.endGroup();

    if(svtplaydlpath.isEmpty()) {
        if(svtplaydlversion == QStringLiteral("stable")) {
#if defined(Q_OS_LINUX)
            svtplaydl = stablepath + QStringLiteral("/stable/svtplay-dl");
#elif defined(Q_OS_WIN)
            svtplaydl = stablepath + QStringLiteral("/stable/svtplay-dl.exe");
#endif
        } else if(svtplaydlversion == QStringLiteral("beta")) {
#if defined(Q_OS_LINUX)
            svtplaydl = stablepath + QStringLiteral("/beta/svtplay-dl");
#elif defined(Q_OS_WIN)
            svtplaydl = stablepath + QStringLiteral("/beta/svtplay-dl.exe");
#endif
        }
    } else {
        svtplaydl = svtplaydlpath;
    }

    ui->comboBox->clear();
    ui->actionAddAllEpisodesToDownloadList->setEnabled(false);
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, [this]() {
        if(ui->progressBar->value() >= 100) {
            ui->progressBar->setValue(0);
        }

        ui->progressBar->setValue(ui->progressBar->value() + 1);
    });

    timer->start(100);

    if(ui->actionSvtplayDlSystem->isChecked()) {
#if defined(Q_OS_LINUX)
        QString executable = QStandardPaths::findExecutable(QStringLiteral("svtplay-dl"));
#elif defined(Q_OS_WIN)
        QString executable = QStandardPaths::findExecutable(QStringLiteral("svtplay-dl.exe"));
#endif

        if(!fileExists(executable)) {
            ui->teOut->clear();
            svtplaydl = QStringLiteral("NOTFIND");
        } else {
            QFileInfo fi(executable);
            svtplaydl = fi.absoluteFilePath();
        }
    }

    if(!fileExists(svtplaydl)) {
        QMessageBox msgBox1;
        msgBox1.setIcon(QMessageBox::Critical);
        msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox1.setText(tr("svtplay-dl cannot be found") + '\n' + tr("Or is not an executable program.") + '\n' + tr("Please click on \"svtplay-dl\" and select svtplay-dl."));
        msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
        timer->stop();
        ui->progressBar->setValue(0);
        msgBox1.exec();
        ui->pbDownload->setDisabled(true);
        return;
    }

    // }
    ui->teOut->append(tr("The information from svtplay-dl may or may not contain:"));
    ui->teOut->append(tr("Quality, Method, Codec, Resolution, Language and Role") + '\n');
    QStringList ARG;
    // QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
// settings.setIniCodec("UTF-8");
    settings.beginGroup("Stcookies");
    QString stcookie = settings.value("stcookie", "").toString();
    settings.endGroup();

    if(ui->leSok->text().contains(QStringLiteral("https://www.tv4play.se"))) {
        settings.beginGroup("Stcookies");
        QString tv4token = settings.value(QStringLiteral("tv4token"), "").toString();
        settings.endGroup();

        if(!tv4token.isEmpty()) {
            ARG << QStringLiteral("--token") << tv4token;
        } else {
            QMessageBox msgBox1;
            msgBox1.setIcon(QMessageBox::Critical);
            msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox1.setText(tr("You need to copy the TV4 token from your browser and save it in streamCapture2."));
            msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox1.exec();
            timer->stop();
            ui->progressBar->setValue(0);
            return;
        }
    }

    settings.beginGroup("Settings");
    bool try4k = settings.value("try4k", false).toBool();
    settings.endGroup();

    if(try4k) {
        ARG << QStringLiteral("--format") << QStringLiteral("dvhevc-51,dvhevc,hevc-51,hevc");
    }

    if(!stcookie.isEmpty()) {
        ARG << QStringLiteral("--cookies")  << QStringLiteral("st=") + stcookie;
    }

    ui->pbSok->setEnabled(false);
    ui->actionSearch->setEnabled(false);
    // ui->actionListAllEpisodes->setEnabled(false);
    QTimer::singleShot(2000, this, [this] {
        ui->pbSok->setEnabled(true);
        ui->actionSearch->setEnabled(true);


    });
    ui->teOut->setReadOnly(true);
    QString sok = ui->leSok->text();
    sok = sok.simplified();

    if(sok.isEmpty()) {
        ui->teOut->setText(tr("The search field is empty!"));
        timer->stop();
        ui->progressBar->setValue(0);
        ui->leQuality->setText(QLatin1String(""));
        ui->leMethod->setText(QLatin1String(""));
        ui->actionDownload->setEnabled(false);
        ui->pbDownload->setEnabled(false);
        return;
    } else if((sok.left(7) != QStringLiteral("http://")) && (sok.left(8) != QStringLiteral("https://"))) {
        ui->teOut->setText(tr("Invalid link"));
        ui->leQuality->setText(QLatin1String(""));
        ui->leMethod->setText(QLatin1String(""));
        timer->stop();
        ui->progressBar->setValue(0);
    } else {
        address = ui->leSok->text().trimmed();
        int questionmark = address.indexOf('?');
        address = address.left(questionmark);
        // Hit

        if(ui->comboPayTV->currentIndex() == 0) {  // No Password
            ARG << "--list-quality" << address;
        } else { // Password
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup("Provider");
            QString username = settings.value(provider + QStringLiteral("/username")).toString();
            QString password = settings.value(provider + QStringLiteral("/password")).toString();
            settings.endGroup();

            if(password.isEmpty()) {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(
                                              nullptr, provider + " " + tr("Enter your password"),
                                              tr("Spaces are not allowed. Use only the characters your "
                       "streaming provider approves.\nThe Password will not be "
                       "saved."),
                                              QLineEdit::Password, QLatin1String(""), &ok);

                    if(newpassword.indexOf(' ') >= 0) {
                        timer->stop();
                        ui->progressBar->setValue(0);
                        return;
                    }

                    if(ok) {
                        secretpassword = newpassword;
                        password = newpassword;
                    }
                }
            }

            ARG << QStringLiteral("--list-quality") << QStringLiteral("--username") << username << QStringLiteral("--password") << password << address;
        }

        CommProcess2 = new QProcess(nullptr);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess2->start(svtplaydl, ARG);
        processpid = CommProcess2->processId();
        connect(CommProcess2, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
                this, &Newprg::onCommProcessExit_sok);
        connect(CommProcess2, &QProcess::readyReadStandardOutput, this, [this]() {
            QString result(CommProcess2->readAllStandardOutput());

            if(ui->actionShowMore->isChecked()) {
                ui->teOut->append(result);
            }

            if(result.mid(0, 25) == QStringLiteral("ERROR: svtplay-dl crashed")) {
                ui->teOut->append(svtplaydl + tr(" crashed."));
                ui->teOut->append(
                    tr("Can not find any video streams, please check the address."));
                ui->actionDownload->setEnabled(false);
                ui->pbDownload->setEnabled(false);
                return;
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. You need to login"))) {
                ui->teOut->append(tr("Unable to log in. Forgot your username and password?"));
                ui->actionDownload->setEnabled(false);
                ui->pbDownload->setEnabled(false);
                return;
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. You need a token to access the website. see https://svtplay-dl.se/tv4play/"))) {
                ui->teOut->append(tr("ERROR: No videos found. You need a token to access the website. see https://svtplay-dl.se/tv4play/"));
                ui->actionDownload->setEnabled(false);
                ui->pbDownload->setEnabled(false);
                return;
            }

            if(result.contains(QStringLiteral("ERROR: That site is not supported. Make a ticket or send a message"))) {
                ui->teOut->append(tr("ERROR: That site is not supported. Please visit https://github.com/spaam/svtplay-dl/issues"));
                ui->actionDownload->setEnabled(false);
                ui->pbDownload->setEnabled(false);
                return;
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. This mode is not supported anymore. Need the url with the video."))) {
                ui->teOut->append(tr("ERROR: No videos found. This mode is not supported anymore. Need the url with the video."));
                ui->actionDownload->setEnabled(false);
                ui->pbDownload->setEnabled(false);
                return;
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. Can't find any videos"))) {
                ui->teOut->append(tr("ERROR: No videos found. Can't find any videos."));
                ui->actionDownload->setEnabled(false);
                ui->pbDownload->setEnabled(false);
                return;
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. We can't download DRM protected content from this site."))) {
                ui->teOut->append(tr("ERROR: No videos found. We can't download DRM protected content from this site."));
                ui->actionDownload->setEnabled(false);
                ui->pbDownload->setEnabled(false);
                return;
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. Wrong url, need to be video url"))) {
                ui->teOut->append(tr("ERROR: No videos found. Wrong url, need to be video url."));
                ui->actionDownload->setEnabled(false);
                ui->pbDownload->setEnabled(false);
                return;
            }

            static QStringList qualitylist;

            if(result.mid(0, 6) != QStringLiteral("ERROR:")) {
                ui->pbAdd->setEnabled(true);
                ui->actionAdd->setEnabled(true);
                // ui->actionAdd->setEnabled(true);
                // ui->pbDownload->setEnabled(true);
                result = result.simplified();
                // 2021-07-07
                // Adaptation for 4.0-5-ga162357

                if(result.contains("{'url':")) {
                    result.clear();
                }

                // QStringList list = result.split(QStringLiteral("INFO:"));
                // qDebug() << "result " << result;
                // ipac Version 3.0.1
                QStringList list = result.split(QStringLiteral("main"));
                list.removeDuplicates();

                for(auto& str : list)
                    str = str.trimmed();

                list.removeAll("");

                for(auto &s : list) {
                    if(s.contains(QStringLiteral("Quality:"))) {
                        continue;
                    }

                    s.replace(' ', '\t');
                    QStringList newList = s.split('\t');
                    QString ut;

                    for(QString &ones :  newList) {
                        ut.append(ones + "  ");
                    }

                    ui->teOut->append(ut);
                    ut.clear();
                    int hittat = s.indexOf('\t');
                    hittat = s.indexOf('\t', hittat + 1);
                    s.replace('\t', ' ');
                    // ui->comboBox->addItem(s.mid(0, hittat));
                    qualitylist.append(s.mid(0, hittat));
                    ui->comboAmount->clear();
                    ui->comboAmount->addItem("- " + tr("Deviation"));

                    for(int i = 0; i <= 10; i++) {
                        ui->comboAmount->addItem(QString::number(i * 100));
                    }

                    ui->comboAmount->setCurrentIndex(0);
                    ui->comboResolution->setEnabled(true);
                }
            } else {
                if(result.contains(QStringLiteral("ERROR: No videos found. Can't find video id for the video"))) {
                    QString notfind = tr("ERROR: No videos found. Can't find video id for the video.");
                    ui->teOut->append(notfind);
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    return;
                }

                if(result.contains(QStringLiteral("ERROR: No videos found. Can't find video info"))) {
                    QString notfind = tr("ERROR: No videos found. Can't find video info.");
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    ui->teOut->append(notfind);
                    return;
                } else if(result.contains(QStringLiteral("ERROR: No videos found. Can't find video id for the video"))) {
                    QString notfind = tr("ERROR: No videos found. Can't find video id for the video.");
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    ui->teOut->append(notfind);
                    return;
                } else if(result.contains(QStringLiteral("ERROR: No videos found. Cant find video id for the video"))) {
                    QString notfind = tr("ERROR: No videos found. Can't find video id for the video.");
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    ui->teOut->append(notfind);
                    return;
                }

                if(result.contains(QStringLiteral("ERROR: No videos found. Can't find video id"))) {
                    QString notfind = tr("ERROR: No videos found. Can't find video id for the video.");
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    ui->teOut->append(notfind);
                    return;
                } else if(result.contains(QStringLiteral("ERROR: No videos found. Can't decode api request:"))) {
                    QString notfind = tr("ERROR: No videos found. Can't decode api request.");
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    ui->teOut->append(notfind);
                    return;
                } else if(result.contains(QStringLiteral("ERROR: No videos found. Use the video page not the series page"))) {
                    QString notfind = tr("ERROR: No videos found. Use the video page not the series page.");
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    ui->teOut->append(notfind);
                    return;
                }

                if(result.contains(QStringLiteral("ERROR: No videos found. Can't find any videos. Is it removed?"))) {
                    QString notfind = tr("ERROR: No videos found. Can't find any videos. Is it removed?");
                    ui->teOut->append(notfind);
                    return;
                } else if(result.contains(QStringLiteral("ERROR: No videos found. Can't find the video file"))) {
                    QString notfind = tr("ERROR: No videos found. Can't find the video file.");
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    ui->teOut->append(notfind);
                    return;
                }

                // newerror
                if(result.contains(QStringLiteral("ERROR: Run again and add --verbose as an argument, to get more information"))) {
                    QString notfind = tr("ERROR: Click on \"Tools\", \"Show more\" and run again to get more information.");
                    ui->teOut->append(notfind);
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    return;
                }
                // newerror
                else if(result.contains(QStringLiteral("ERROR: If the error persists, you can report it at https://github.com/spaam/svtplay-dl/issues"))) {
                    QString notfind = tr("ERROR: If the error persists, you can report it at https://github.com/spaam/svtplay-dl/issues");
                    ui->teOut->append(notfind);
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    return;
                }
                // newerror
                else if(result.contains(QStringLiteral("ERROR: Include the URL used, the stack trace and the output of svtplay-dl --version in the issue"))) {
                    QString notfind = tr("ERROR: Include URL and error message in problem description.");
                    ui->teOut->append(notfind);
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    return;
                } else if(result.contains(QStringLiteral("ERROR: Can't find any videos."))) {
                    QString notfind = tr("ERROR: Can't find any videos.");
                    ui->teOut->append(notfind);
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    return;
                }

                if(result.contains(QStringLiteral("WARNING: --all-episodes not implemented for this service"))) {
                    QString notfind = tr("WARNING: --all-episodes not implemented for this service.");
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    ui->teOut->append(notfind);
                    return;
                } else if(result.contains(QStringLiteral("WARNING: Use program page instead of the clip / video page."))) {
                    QString notfind = tr("WARNING: Use program page instead of the clip / video page.");
                    ui->teOut->append(notfind);
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    return;
                }
                /**/
                else if(result.contains(QStringLiteral("ERROR: No videos found. Media doesn't have any associated videos"))) {
                    QString notfind = tr("ERROR: No videos found. Media doesn't have any associated videos.");
                    ui->teOut->append(notfind);
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    return;
                }
                /**/
                /**/
                else if(result.contains(QStringLiteral("ERROR: No videos found. Can't find audio info"))) {
                    QString notfind = tr("ERROR: No videos found. Can't find audio info.");
                    ui->teOut->append(notfind);
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    return;
                }
                /**/
                else if(result.contains(QStringLiteral("ERROR: No videos found. Cant find video id"))) {
                    QString notfind = tr("ERROR: No videos found. Cant find video id.");
                    ui->teOut->append(notfind);
                    ui->actionDownload->setEnabled(false);
                    ui->pbDownload->setEnabled(false);
                    return;
                } else {
                    ui->teOut->append(result);
                }
            }

            qualitylist.removeDuplicates();
            QStringListModel *model = new QStringListModel();
            model->setStringList(qualitylist);
            ui->comboBox->setModel(model);
            ui->actionDownload->setEnabled(true);
            ui->pbDownload->setEnabled(true);
            return;
        });
    }
}

/* END SOK */
