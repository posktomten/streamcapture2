//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          Copyright (C) 2016 - 2021 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          ic_0002 (at) ceicer (dot) com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "ui_newprg.h"

QString Newprg::doTestSearch(const QString &address)
{
    QStringList ARG;
    ARG << "--silent-semi"
        << "--list-quality" << address;
    auto *result = new QString;
    auto *process = new QProcess(this);
    process->waitForFinished(-1);
    process->setProcessChannelMode(QProcess::MergedChannels);
    process->start(svtplaydl, ARG);
    connect(process, &QProcess::readyReadStandardOutput,
    [process, result]() {
        *result = *process->readAllStandardOutput();
    });
    process->kill();
    delete process;
    return *result;
}

QString Newprg::doTestSearch(const QString &address, const QString &username,
                             const QString &password)
{
    QStringList ARG;
    ARG << "--username=" + username << "--password=" + password << "--silent-semi"
        << "--list-quality" << address;
    auto *result = new QString;
    auto *process = new QProcess;
    process->waitForFinished(-1);
    process->setProcessChannelMode(QProcess::MergedChannels);
    process->start(svtplaydl, ARG);
    connect(process, &QProcess::readyReadStandardOutput,
    [process, result]() {
        *result = *process->readAllStandardOutput();
    });
    process->kill();
    delete process;
    return *result;
}
