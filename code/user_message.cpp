// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include <QNetworkReply>
#include "newprg.h"
#include "downloadlistdialog.h"
#include <QSettings>
#include <QMessageBox>
#include <QTemporaryFile>
void Newprg::userMessageStart()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Language");
    QString language = settings.value("language", "en_US").toString();
    settings.endGroup();

    if(language == "sv_SE") {
        language = USER_MESSAGE_SV;
    } else {
        language = USER_MESSAGE;
    }

    QUrl url(language);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
//    reply->waitForBytesWritten(1000);

    if(reply->error() != QNetworkReply::NoError) {
        return;
    }

    QObject::connect(
        reply, &QNetworkReply::finished, this,
    [reply, this] {
        QByteArray *bytes = new QByteArray;
        *bytes = reply->readAll();

        if(reply->error() != QNetworkReply::NoError) {
            return;
        }

        if(bytes->length() > 0)
        {
            QTemporaryFile file;
            QString filename;

            if(file.open()) {
                filename = file.fileName();
                file.write(*bytes);
                file.close();
            }

            QStringList *infoList = new QStringList;
            *infoList << tr("Message from the developer");
            *infoList << QString("wordwrap");
            DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
            connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
            mDownloadListDialog->openFile(infoList, &filename, true);
        }

    });
}

void Newprg::userMessage()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Language");
    QString language = settings.value("language", "en_US").toString();
    settings.endGroup();

    if(language == "sv_SE") {
        language = USER_MESSAGE_SV;
    } else {
        language = USER_MESSAGE;
    }

    QUrl url(language);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    //    reply->waitForBytesWritten(1000);

    if(reply->error() != QNetworkReply::NoError) {
        return;
    }

    QObject::connect(
        reply, &QNetworkReply::finished, this,
    [reply, this] {
        QByteArray *bytes = new QByteArray;
        *bytes = reply->readAll();

        if(reply->error() != QNetworkReply::NoError) {
            return;
        }

        if(bytes->length() < 1)
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Information);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("There is no message."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
        } else
        {
            QTemporaryFile file;
            QString filename;

            if(file.open()) {
                filename = file.fileName();
                file.write(*bytes);
                file.close();
                QStringList *infoList = new QStringList;
                *infoList << tr("Message from the developer");
                *infoList << QString("wordwrap");
                DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
                connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
                mDownloadListDialog->openFile(infoList, &filename, true);
            }
        }
    });
}
