// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include <QSettings>
#include <QMessageBox>
#include <QFileInfo>
void Newprg::english()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Language"));
    QString sp = settings.value(QStringLiteral("language"), "").toString();
    settings.endGroup();

    if(sp != "en_US") {
        /***/
        QMessageBox *msgBox = new QMessageBox(this);
        QIcon iconYes(QPixmap(QStringLiteral(":/images/restart.png")));
        QIcon iconLater(QPixmap(QStringLiteral(":/images/later.png")));
        QIcon iconReject(QPixmap(QStringLiteral(":/images/application-exit.png")));
        QPushButton  *yesButton = new QPushButton(iconYes, tr("Restart Now"), msgBox);
        yesButton->setFixedSize(QSize(150, 40));
        QPushButton  *laterButton = new QPushButton(iconLater, tr("Change language on next start"), msgBox);
        laterButton->setFixedSize(QSize(350, 40));
        QPushButton  *rejectButton = new QPushButton(iconReject, tr("Cancel"), msgBox);
        rejectButton->setFixedSize(QSize(150, 40));
        msgBox->addButton(yesButton, QMessageBox::YesRole);
        msgBox->addButton(laterButton, QMessageBox::YesRole);
        msgBox->addButton(rejectButton, QMessageBox::RejectRole);
        msgBox->setDefaultButton(laterButton);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The program needs to be restarted to switch to English."));
        msgBox->exec();
        /***/

        if(msgBox->clickedButton() == yesButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("en_US"));
            settings.endGroup();
            settings.sync();
            const QString EXECUTE =
                QCoreApplication::applicationDirPath() + "/" +
                QFileInfo(QCoreApplication::applicationFilePath()).fileName();
            // QProcess p;
            // p.setProgram(EXECUTE);
            // p.startDetached();
            QProcess::startDetached(EXECUTE);
            close();
        } else if(msgBox->clickedButton() == laterButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("en_US"));
            settings.endGroup();
        } else {
            delete msgBox;
            return;
        }

        /***/
    }
}

void Newprg::swedish()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Language"));
    QString sp = settings.value(QStringLiteral("language"), "").toString();
    settings.endGroup();

    if(sp != QStringLiteral("sv_SE")) {
        /***/
        QMessageBox *msgBox = new QMessageBox(this);
        QIcon iconYes(QPixmap(QStringLiteral(":/images/restart.png")));
        QIcon iconLater(QPixmap(QStringLiteral(":/images/later.png")));
        QIcon iconReject(QPixmap(QStringLiteral(":/images/application-exit.png")));
        QPushButton  *yesButton = new QPushButton(iconYes, tr("Restart Now"), msgBox);
        yesButton->setFixedSize(QSize(150, 40));
        QPushButton  *laterButton = new QPushButton(iconLater, tr("Change language on next start"), msgBox);
        laterButton->setFixedSize(QSize(350, 40));
        QPushButton  *rejectButton = new QPushButton(iconReject, tr("Cancel"), msgBox);
        rejectButton->setFixedSize(QSize(150, 40));
        msgBox->addButton(yesButton, QMessageBox::YesRole);
        msgBox->addButton(laterButton, QMessageBox::YesRole);
        msgBox->addButton(rejectButton, QMessageBox::RejectRole);
        msgBox->setDefaultButton(laterButton);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The program needs to be restarted to switch to Swedish."));
        msgBox->exec();
        /***/

        if(msgBox->clickedButton() == yesButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("sv_SE"));
            settings.endGroup();
            settings.sync();
            const QString EXECUTE =
                QCoreApplication::applicationDirPath() + "/" +
                QFileInfo(QCoreApplication::applicationFilePath()).fileName();
            // QProcess p;
            // p.setProgram(EXECUTE);
            // p.startDetached();
            QProcess::startDetached(EXECUTE);
            close();
        } else if(msgBox->clickedButton() == laterButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("sv_SE"));
            settings.endGroup();
        } else {
            delete msgBox;
            return;
        }
    }
}

void Newprg::italian()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Language"));
    QString sp = settings.value(QStringLiteral("language"), "").toString();
    settings.endGroup();

    if(sp != QStringLiteral("it_IT")) {
        /***/
        QMessageBox *msgBox = new QMessageBox(this);
        QIcon iconYes(QPixmap(QStringLiteral(":/images/restart.png")));
        QIcon iconLater(QPixmap(QStringLiteral(":/images/later.png")));
        QIcon iconReject(QPixmap(QStringLiteral(":/images/application-exit.png")));
        QPushButton  *yesButton = new QPushButton(iconYes, tr("Restart Now"), msgBox);
        yesButton->setFixedSize(QSize(150, 40));
        QPushButton  *laterButton = new QPushButton(iconLater, tr("Change language on next start"), msgBox);
        laterButton->setFixedSize(QSize(350, 40));
        QPushButton  *rejectButton = new QPushButton(iconReject, tr("Cancel"), msgBox);
        rejectButton->setFixedSize(QSize(150, 40));
        msgBox->addButton(yesButton, QMessageBox::YesRole);
        msgBox->addButton(laterButton, QMessageBox::YesRole);
        msgBox->addButton(rejectButton, QMessageBox::RejectRole);
        msgBox->setDefaultButton(laterButton);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("The program needs to be restarted to switch to Italian."));
        msgBox->exec();
        /***/

        if(msgBox->clickedButton() == yesButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("it_IT"));
            settings.endGroup();
            settings.sync();
            const QString EXECUTE =
                QCoreApplication::applicationDirPath() + "/" +
                QFileInfo(QCoreApplication::applicationFilePath()).fileName();
            // QProcess p;
            // p.setProgram(EXECUTE);
            // p.startDetached();
            QProcess::startDetached(EXECUTE);
            close();
        } else if(msgBox->clickedButton() == laterButton) {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("it_IT"));
            settings.endGroup();
        } else {
            delete msgBox;
            return;
        }
    }
}
