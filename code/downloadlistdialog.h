// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2 (DownloadListDialog)
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef DOWNLOADLISTDIALOG_H
#define DOWNLOADLISTDIALOG_H

#include <QDialog>
namespace Ui
{
class DownloadListDialog;
}

class DownloadListDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DownloadListDialog(QWidget *parent = nullptr);
    ~DownloadListDialog();
    void initiate(const QStringList *instruktions, const QString *content, bool readonly);
    void initiate(const QStringList *instruktions, const QString *content, bool readonly, bool removeinvalidlinks);
    void openFile(const QStringList *instruktions, const QString *filename, bool readonly);
private:
    Ui::DownloadListDialog *ui;


protected:
    void closeEvent(QCloseEvent * e) override;
    //  void wheelEvent(QWheelEvent *event) override;

signals:
    void sendContent(QString *modefiedContent);
    // void isfinished(bool isFinished);

public slots:
    // Zoom
    //    Zoom
    void textBrowsertextEditTextChanged();
    void zoomMinus();
    void zoomPlus();
    void zoomDefault();
    void getNewFont(QFont font);


};

#endif // DOWNLOADLISTDIALOG_H
