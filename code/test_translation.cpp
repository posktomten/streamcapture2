// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include <QSettings>
#include <QFileDialog>
void Newprg::testTranslation()
{
    QObject::connect(ui->actionPreCheckLanguagefile, & QAction::triggered, this, [this]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Language");
        QString testlanguage = settings.value("testlanguage", QCoreApplication::applicationDirPath()).toString();
        QString testlanguagepath = settings.value("testlanguagepath", QCoreApplication::applicationDirPath()).toString();
        settings.endGroup();
        //
//        QFileDialog dialog(this,
//                           tr("Open your language file"), testlanguage, tr("Compiled language file (*.qm)"));
        QFileDialog filedialog(this);
        int x = this->x();
        int y = this->y();
        filedialog.setDirectory(testlanguagepath);
        filedialog.setLabelText(QFileDialog::Accept, tr("Open"));
        filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
        filedialog.setWindowTitle(tr("Open your language file"));
        filedialog.setGeometry(x + 50, y + 50, 900, 550);
        filedialog.setFileMode(QFileDialog::ExistingFile);//Folder name
        filedialog.setNameFilter(tr("Compiled language file") + " (*.qm)");

        if(!filedialog.exec()) {
            return;
        }

        QFileInfo fi(filedialog.selectedFiles().at(0));
        testlanguage = fi.absoluteFilePath();
        testlanguagepath = fi.absolutePath();
        settings.beginGroup("Language");
        settings.setValue("testlanguage", testlanguage);
        settings.setValue("testlanguagepath", testlanguagepath);
        settings.setValue("language", "");
        settings.endGroup();
        settings.sync();
        const QString EXECUTE = QDir::toNativeSeparators(
                                    QCoreApplication::applicationDirPath() + "/" +
                                    QFileInfo(QCoreApplication::applicationFilePath()).fileName());
        close();
        QProcess p;
        p.setProgram(EXECUTE);
        p.startDetached();
    });
}
