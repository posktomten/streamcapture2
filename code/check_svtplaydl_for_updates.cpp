
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QMessageBox>
#include <QSettings>
#include <QDir>
void Newprg::checkSvtplaydlForUpdatesHelp()
{
    QString *selectedsvtplaydl = new QString(hittaSvtplaydl());
    QString *currentVersion =  new QString(getSvtplaydlVersion(&svtplaydl));
    QStringList currenStableBleedingedge = checkSvtplaydlForUpdates();
    QString inuse = QString("<b>" + tr("You have downloaded") + QStringLiteral("</b>"));

    if(currenStableBleedingedge.at(0).isEmpty()) {
        currenStableBleedingedge.replace(0, tr("Nothing"));
        inuse += QString(QStringLiteral("<br>") + tr("To folder \"stable\": Nothing."));
    } else {
        inuse += QString(QStringLiteral("<br>") + tr("To folder \"stable\": ") + currenStableBleedingedge.at(0));
    }

    if(currenStableBleedingedge.at(1).isEmpty()) {
        inuse += QString(QStringLiteral("<br>") + tr("To folder \"beta\": Nothing."));
    } else {
        inuse += QString(QStringLiteral("<br>") + tr("To folder \"beta\": ") + currenStableBleedingedge.at(1));
    }

    QString *pinuse = new QString(inuse);
    /*     */
    QByteArray *bytes = new QByteArray;
    QUrl url(CHECK_LATEST);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    reply->waitForBytesWritten(1000);

    if(reply->error() != QNetworkReply::NoError) {
        QMessageBox msgBox(this);
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("An unexpected error occurred.<br>Information about svtplay-dl beta can not be found.<br>Check your internet connection."));
        msgBox.exec();
        delete bytes;
        delete selectedsvtplaydl;
        delete currentVersion;
        delete pinuse;
        return;
    }

    QObject::connect(
        reply, &QNetworkReply::finished, this,
    [this, reply,  bytes, currentVersion, pinuse, selectedsvtplaydl]() {
        /*   */
        QUrl url_stable(CHECK_LATEST_STABLE);
        auto *nam_stable = new QNetworkAccessManager(nullptr);
        auto *reply_stable = nam_stable->get(QNetworkRequest(url_stable));
        reply_stable->waitForBytesWritten(1000);
        *bytes = reply->readAll(); // bytes

        if(reply->error() != QNetworkReply::NoError) {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl stable can not be found.<br>Check your internet connection."));
            msgBox->exec();
            return;
        }

        QObject::connect(
            reply_stable, &QNetworkReply::finished, this,
        [this, reply_stable, bytes, currentVersion, pinuse, selectedsvtplaydl] {
            *bytes += reply_stable->readAll(); // bytes

            if(reply_stable->error() == QNetworkReply::ContentNotFoundError) {
                QMessageBox *msgBox = new QMessageBox(this);
                msgBox->setIcon(QMessageBox::Critical);
                msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox->setText(tr("An unexpected error occurred.") + QStringLiteral("<br>") + tr("Information about svtplay-dl stable can not be found.") + QStringLiteral("<br>") + tr("Check your internet connection."));
                msgBox->exec();
                return;
            }
            QString latestVersion = (QString) * bytes;

            latestVersion = latestVersion.trimmed();

            QStringList versions = latestVersion.split('\n');
            versions.removeAll(QString(""));

            QString ut;
//            QString youAreUsing, yourVersion;
            QString youAreUsing;

            if(!currentVersion->isEmpty())
            {
                youAreUsing = tr("You are using version:") + " " + *currentVersion;
//                yourVersion = tr("You are NOT using the latest version.");
            } else
            {
                youAreUsing = tr("You are not using svtplay-dl.");
//                yourVersion = "";
            }

//            ut = "<b>" + tr("The latest svtplay-dl<br>available for download at bin.ceicer.com is") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br>" + tr("Beta:") + " " + versions.at(0) + "<br><br>" + *pinuse + "<br><br>" + tr("You have selected the option") + "<br>" + *selectedsvtplaydl + "<br>" + youAreUsing + "<br><b>" + tr("You are using the beta version.") + " </b > ";
            ut = "<b>" + tr("The latest svtplay-dl available for<br>download from bin.ceicer.com are") + "</b><br>" + tr("Stable:") + " " + versions.at(1) + "<br>" + tr("Beta:") + " " + versions.at(0) + "<br><br>" + *pinuse + "<br><br><b>" + tr("You have selected") + "</b><br>" + *selectedsvtplaydl + "<br>" + youAreUsing + "<br>";

            ut.replace(" ", "&nbsp;");
            ut.replace("-", "&#8209;");
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIconPixmap(QPixmap(QStringLiteral(":/images/download.png")));
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(ut);
            QPushButton *pbDownload = msgBox->addButton(tr("Download"), QMessageBox::AcceptRole);
            pbDownload->setIcon(QPixmap(QStringLiteral(":/images/download.png")));
            QPushButton *pbCancel = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
            pbCancel->setIcon(QPixmap(QStringLiteral(":/images/application-exit.png")));
            msgBox->setDefaultButton(pbCancel);
            msgBox->exec();

            if(msgBox->clickedButton() == pbDownload)
            {
                downloadFromCeicer();
            }

        });
        /*   */
    });

    /*    */
}

QStringList Newprg::checkSvtplaydlForUpdates()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       LIBRARY_NAME);
    settings.beginGroup(QStringLiteral("Path"));
    QString stablepath = settings.value(QStringLiteral("stablepath")).toString();
    QString betapath = settings.value(QStringLiteral("betapath")).toString();
    settings.endGroup();
#ifdef Q_OS_WINDOWS
    QString *ls = new QString(QDir::toNativeSeparators(stablepath + QStringLiteral("/stable/svtplay-dl.exe")));
    QString *l = new QString(QDir::toNativeSeparators(betapath + QStringLiteral("/beta/svtplay-dl.exe")));
#endif
#ifdef Q_OS_LINUX
    QString *ls = new QString(QDir::toNativeSeparators(stablepath + QStringLiteral("/stable/svtplay-dl")));
    QString *l = new QString(QDir::toNativeSeparators(betapath + QStringLiteral("/beta/svtplay-dl")));
#endif
    QStringList versions;
    versions << getSvtplaydlVersion(ls);
    versions << getSvtplaydlVersion(l);
    /*    */
    return versions;
}

QString Newprg::hittaSvtplaydl()
{
    QString tempSvtplaydl;

    if(ui->actionSvtplayDlStable->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           LIBRARY_NAME);
        settings.beginGroup(QStringLiteral("Path"));
        QString stablepath = settings.value(QStringLiteral("stablepath"), QStringLiteral("NOTFIND")).toString();
        settings.endGroup();
#ifdef Q_OS_WINDOWS
        tempSvtplaydl = stablepath + QStringLiteral("/stable/svtplay-dl.exe");
#endif
#ifdef Q_OS_LINUX
        tempSvtplaydl = stablepath + QStringLiteral("/stable/svtplay-dl");
#endif

        if(fileExists(tempSvtplaydl)) {
            svtplaydl = tempSvtplaydl;
            return tr("\"Use svtplay-dl stable\"");
        } else {
            return tr("\"Use svtplay-dl stable\", but it can not be found.");
        }
    }

    if(ui->actionSvtplayDlBleedingEdge->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           LIBRARY_NAME);
        settings.beginGroup(QStringLiteral("Path"));
        QString betapath = settings.value(QStringLiteral("betapath")).toString();
        settings.endGroup();
#ifdef Q_OS_WINDOWS
        tempSvtplaydl = betapath + QStringLiteral("/beta/svtplay-dl.exe");
#endif
#ifdef Q_OS_LINUX
        tempSvtplaydl = betapath + QStringLiteral("/beta/svtplay-dl");
#endif

        if(fileExists(tempSvtplaydl)) {
            svtplaydl = tempSvtplaydl;
            return tr("\"Use svtplay-dl beta\"");
        } else {
            return tr("\"Use svtplay-dl beta\", but it can not be found.");
        }
    }

    if(ui->actionSvtPlayDlManuallySelected->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup(QStringLiteral("Path"));
        QString svtplaypath = settings.value(QStringLiteral("svtplaydlpath")).toString();
        settings.endGroup();

        if(fileExists(svtplaypath)) {
            svtplaydl = svtplaypath;
            return tr("\"Use the selected svtplay-dl\"");
        } else {
            return tr("\"Use the selected svtplay-dl\", but it can not be found.");
        }
    }

    return tr("svtplay-dl is not found in the specified path.");
}

void Newprg::checkSvtplaydlAtStart()
{
    QByteArray *bytes = new QByteArray;
    QUrl url(CHECK_LATEST);
    auto *nam = new QNetworkAccessManager(nullptr);
    auto *reply = nam->get(QNetworkRequest(url));
    reply->waitForBytesWritten(1000);

    if(reply->error() != QNetworkReply::NoError)  {
        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setIcon(QMessageBox::Critical);
        msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl stable can not be found.<br>Check your internet connection."));
        msgBox->exec();
        delete bytes;
        return;
    }

    QObject::connect(
        reply, &QNetworkReply::finished, this,
    [ reply, bytes, this]() {
        /*   */
        QUrl url_stable(CHECK_LATEST_STABLE);
        auto *nam_stable = new QNetworkAccessManager(nullptr);
        auto *reply_stable = nam_stable->get(QNetworkRequest(url_stable));
        reply_stable->waitForBytesWritten(1000);
        *bytes = reply->readAll(); // bytes

        if(reply->error() != QNetworkReply::NoError)  {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl stable can not be found.<br>Check your internet connection."));
            msgBox->exec();
            return;
        }

        QObject::connect(
            reply_stable, &QNetworkReply::finished, this,
        [ reply_stable, bytes, this] {

            /*   */
            *bytes += reply_stable->readAll(); // bytes

            if(reply_stable->error() == QNetworkReply::ContentNotFoundError) {
                QMessageBox *msgBox = new QMessageBox(this);
                msgBox->setIcon(QMessageBox::Critical);
                msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox->setText(tr("An unexpected error occurred.<br>Information about svtplay-dl stable can not be found.<br>Check your internet connection."));
                msgBox->exec();
                return;
            }
            QString latestVersion = (QString) * bytes;
            latestVersion.remove("\r");

            QStringList latestVersions = latestVersion.split('\n');
            latestVersions.removeAll(QString(""));

            QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                               LIBRARY_NAME);
            settings.beginGroup(QStringLiteral("Path"));
            QString stablepath = settings.value(QStringLiteral("stablepath"), QStringLiteral("nothing")).toString();
            QString betapath = settings.value(QStringLiteral("betapath"), QStringLiteral("nothing")).toString();
            settings.endGroup();

#ifdef Q_OS_LINUX
            QFile fileStable(stablepath + QStringLiteral("/stable/svtplay-dl"));
            QFile filebeta(betapath + QStringLiteral("/beta/svtplay-dl"));
#endif
#ifdef Q_OS_WINDOWS
            QFile fileStable(stablepath + QStringLiteral("/stable/svtplay-dl.exe"));
            QFile filebeta(betapath + QStringLiteral("/beta/svtplay-dl.exe"));
#endif

#ifdef Q_OS_LINUX
            QString latest_stable = getSvtplaydlVersion(stablepath + QStringLiteral("/stable/svtplay-dl"));
            QString latest = getSvtplaydlVersion(betapath + QStringLiteral("/beta/svtplay-dl"));
#endif
#ifdef Q_OS_WINDOWS
            QString latest_stable = getSvtplaydlVersion(stablepath + QStringLiteral("/stable/svtplay-dl.exe"));
            QString latest = getSvtplaydlVersion(betapath + QStringLiteral("/beta/svtplay-dl.exe"));
#endif

            bool betapathExists = filebeta.exists();
            bool stablepathExists = fileStable.exists();
            QString ut = "";
            QSettings settings2(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                                EXECUTABLE_NAME);
            settings2.beginGroup(QStringLiteral("FirstRun"));
            bool firstrun = settings2.value(QStringLiteral("firstrun"), true).toBool();
            settings2.endGroup();

            if(firstrun)
            {
                ut += tr("<b>Welcome to streamCapture2!</b>");
                settings2.beginGroup(QStringLiteral("FirstRun"));
                settings2.setValue("firstrun", false);
                settings2.endGroup();
                ut += QStringLiteral("<br><br>");
            } else
            {
                // ipac
                if(!stablepathExists) {
                    ut += tr("<b>svtplay-dl stable</b> can not be found. You have entered an incorrect path or no path has been specified at all.<br>Or the files have been moved or deleted.");
                    ut += "<br><br>" + tr("Click <i>Download</i> and <i>Latest stable svtplay-dl") + "</i>";
                    ut += "<br><br>";
                }

                if(!betapathExists) {
//                    if(latestVersions.at(0) > latestVersions.at(1)) {
                    ut += tr("<b>svtplay-dl beta</b> can not be found. You have entered an incorrect path or no path has been specified at all.<br>Or the files have been moved or deleted.");
                    ut += "<br><br>" + tr("Click <i>Download</i> and <i>Latest svtplay-dl beta") + "</i>";
                    ut += "<br><br>";
//                    }
                }
            }

            if((latest_stable != latestVersions.at(1)) && (latest != latestVersions.at(0)))
            {
                ut += tr("<b>svtplay-dl stable is available for download.</b><br>Version: ") + latestVersions.at(1) + "<br><br>";
                ut += tr("<b>svtplay-dl beta is available for download.</b><br>Version: ") + latestVersions.at(0) + "<br>";

// ipac
                if(firstrun) {
                    ut += "<br>" + tr("Click <i>Download</i>, then click<br><i>Latest stable svtplay-dl ") + tr("</i>and<br><i>Latest svtplay-dl beta") + "</i>";
                }
            } else if(latest_stable != latestVersions.at(1))
            {
                ut += tr("<b>svtplay-dl stable is available for download.</b><br>Version: ") + latestVersions.at(1);

                if(firstrun) {
                    ut += "<br>" + tr("<i>Click \"Download\" and \"Latest stable svtplay-dl\"</i>");
                }

                ut += "<br><br>";
            } else if(latest != latestVersions.at(0))
            {
                ut += tr("<b>svtplay-dl beta is available for download.</b><br>Version: ") + latestVersions.at(0);

                if(firstrun) {
                    ut += "<br>" + tr("<i>Click \"Download\" and \"Latest svtplay-dl beta\"</i>");
                }
            }
            if(!ut.isEmpty())
            {
                QMessageBox *msgBox = new QMessageBox(this);
                msgBox->setIconPixmap(QPixmap(QStringLiteral(":/images/download.png")));
                msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox->setText(ut);
                QPushButton *pbDownload = msgBox->addButton(tr("Download"), QMessageBox::AcceptRole);
                pbDownload->setIcon(QPixmap(QStringLiteral(":/images/download.png")));
                QPushButton *pbCancel = msgBox->addButton(tr("Cancel"), QMessageBox::RejectRole);
                pbCancel->setIcon(QPixmap(QStringLiteral(":/images/application-exit.png")));
                msgBox->setDefaultButton(pbDownload);
                msgBox->exec();

                if(msgBox->clickedButton() == pbDownload) {
                    downloadFromCeicer();
                }
            }

        });
//
    });
}

QString Newprg::getSvtplaydlVersion(QString path)
{
    auto *CommProcess = new QProcess(this);
    CommProcess->setProcessChannelMode(QProcess::MergedChannels);
    CommProcess->start(path, QStringList() << QStringLiteral("--version"));
    CommProcess->waitForFinished(-1);
    QTextStream stream(CommProcess->readAllStandardOutput());
    QString line;

    while(!stream.atEnd()) {
        line += stream.readLine();
    }

    line.remove(0, 11);
    CommProcess->deleteLater();
    return line;
}
