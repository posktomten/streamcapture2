// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "downloadlistdialog.h"
#include "ui_newprg.h"
#include <QSettings>
#include <QFileInfo>
#include <QDir>
void Newprg::versionHistory()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Language"));
    QString sp = settings.value(QStringLiteral("language"), QStringLiteral("en_US")).toString();
    settings.endGroup();
    QString readme_sp(QStringLiteral(":/txt/CHANGELOG_") + sp);
    QFileInfo fi(readme_sp);

    if(!fi.exists()) {
        readme_sp = QStringLiteral(":/txt/CHANGELOG_en_US");
    }

    QStringList *infoList = new QStringList;
    *infoList << tr("Version history");
    *infoList << QString(QStringLiteral("wordwrap"));
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
    mDownloadListDialog->openFile(infoList, &readme_sp, true);
}

void Newprg::license()
{
    QStringList *infoList = new QStringList;
    *infoList << tr("License streamCapture2");
    *infoList << QStringLiteral("wordwrap");
    const QString *filename = new QString(QStringLiteral(":/txt/LICENSE"));
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
    mDownloadListDialog->openFile(infoList, filename, true);
}

void Newprg::licenseSvtplayDl()
{
    QStringList *infoList = new QStringList;
    *infoList << tr("License svtplay-dl");
    *infoList << QStringLiteral("wordwrap");
    const QString *filename = new QString(QStringLiteral(":/txt/license_svtplay-dl.txt"));
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
    mDownloadListDialog->openFile(infoList, filename, true);
}

#ifdef FFMPEG
void Newprg::licenseFfmpeg()
{
    QStringList *infoList = new QStringList;
    *infoList << tr("License FFmpeg");
    *infoList << QStringLiteral("wordwrap");
    const QString *filename = new QString(QStringLiteral(":/txt/license_FFmpeg.txt"));
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
    mDownloadListDialog->openFile(infoList, filename, true);
}

#endif
#ifdef  Q_OS_WINDOWS
void Newprg::license7zip()
{
    QStringList *infoList = new QStringList;
    *infoList << tr("License 7zip");
    *infoList << QString("wordwrap");
    const QString *filename = new QString(QStringLiteral(":/txt/license_7zip.txt"));
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
    mDownloadListDialog->openFile(infoList, filename, true);
}

#endif

void Newprg::aboutFfmpeg()
{
    QString *instring = new QString;
    QString EXECUTE = selectFfmpeg();

    if(EXECUTE == QStringLiteral("NOTFIND")) {
#ifdef Q_OS_WINDOWS
#ifdef PORTABLE
        *instring = "FFmpeg" + tr(" could not be found. Please download a portable streamCapture2 where FFmpeg is included.");
#endif
#ifndef PORTABLE
        *instring = "FFmpeg" + tr(" could not be found. Please go to \"Tools\", \"Maintenance Tool\" to install FFmpeg.");
#endif
        * instring += " " + tr("Or install ") + "FFmpeg" + tr(" in your system.");
#endif
#ifdef Q_OS_LINUX
        *instring = "FFmpeg" + tr(" could not be found. Please download an AppImage where FFmpeg is included.");
        *instring += " " + tr("Or install ") + "FFmpeg" + tr(" in your system.");
#endif
        EXECUTE = "FFmpeg";
    } else {
        auto *CommProcess = new QProcess(this);
        CommProcess->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess->start(EXECUTE, QStringList() << "--help");
        CommProcess->waitForFinished(-1);
        QTextStream stream(CommProcess->readAllStandardOutput());
        QString line;
        int i = 0;

        while(!stream.atEnd()) {
            line = stream.readLine();

            if(i != 0) {
                *instring += QStringLiteral("<br>") + line;
            } else {
                *instring += QStringLiteral("<b>") + line + QStringLiteral("</b><br>");
            }

            i++;
        }
    }

    QStringList *infoList = new QStringList;
    *infoList << tr("About ") + QDir::toNativeSeparators(EXECUTE);
    *infoList << QStringLiteral("wordwrap");
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
    mDownloadListDialog->initiate(infoList, instring, true);
    delete instring;
}

void Newprg::aboutSvtplayDl()
{
    QString *s = new QString;

    if(ui->actionSvtplayDlSystem->isChecked()) {
        if(findSvtplayDl() == QStringLiteral("NOTFIND")) {
#ifdef Q_OS_LINUX
            *s = tr("svtplay-dl is not found in the system path.");
//            *s += QStringLiteral(" ");
            *s += tr(" You can download svtplay-dl from bin.ceicer.com. Select \"svtplay-dl\", \"Download svtplay-dl...\"");
#endif
#ifdef Q_OS_WIN
            *s += tr("svtplay-dl.exe is not found in the system path.");
            *s += tr(" You can download svtplay-dl from bin.ceicer.com. Select \"svtplay-dl\", \"Download svtplay-dl...\"");
#endif
            QStringList *infoList = new QStringList;
            *infoList << tr("About svtplay-dl (In the system path)");
            DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
            connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
            mDownloadListDialog->initiate(infoList, s, true);
            return;
        }
    } else  if(ui->actionSvtplayDlStable->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           LIBRARY_NAME);
        settings.beginGroup(QStringLiteral("Path"));
        QString stablepath = settings.value(QStringLiteral("stablepath")).toString();
        settings.endGroup();
#ifdef Q_OS_WINDOWS
        svtplaydl = stablepath + QDir::toNativeSeparators(QStringLiteral("/stable/svtplay-dl.exe"));
#endif
#ifdef Q_OS_LINUX
        svtplaydl = stablepath + QDir::toNativeSeparators(QStringLiteral("/stable/svtplay-dl"));
#endif
    } else if(ui->actionSvtplayDlBleedingEdge->isChecked()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, EXECUTABLE_DISPLAY_NAME,
                           LIBRARY_NAME);
        settings.beginGroup(QStringLiteral("Path"));
        QString betapath = settings.value(QStringLiteral("betapath")).toString();
        settings.endGroup();
#ifdef Q_OS_WINDOWS
        svtplaydl = betapath + QDir::toNativeSeparators(QStringLiteral("/beta/svtplay-dl.exe"));
#endif
#ifdef Q_OS_LINUX
        svtplaydl = betapath + QDir::toNativeSeparators(QStringLiteral("/beta/svtplay-dl"));
#endif
    }

    if(!fileExists(svtplaydl)) {
#ifdef Q_OS_WIN

        if(!ui->actionSvtPlayDlManuallySelected->isChecked()) {
            QDir::toNativeSeparators(*s += tr(" svtplay-dl.exe  cannot be found. Go to \"svtplay-dl\", ""\"Download svtplay-dl...\" to download."));
        } else {
            QDir::toNativeSeparators(*s += tr("Please click \"svtplay-dl\" and select svtplay-dl."));
        }

#endif
#ifdef Q_OS_LINUX

        if(!ui->actionSvtPlayDlManuallySelected->isChecked()) {
            *s +=  tr(" svtplay-dl cannot be found. Go to \"svtplay-dl\", ""\"Download svtplay-dl...\" to download.");
        } else {
            *s += tr("Please click \"svtplay-dl\" and select svtplay-dl.");
        }

#endif
        QStringList *infoList = new QStringList;
        *infoList << tr("About svtplay-dl");
        *infoList << QString("wordwrap");
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
        mDownloadListDialog->initiate(infoList, s, true);
        return;
    }

    QString *currentVersion =  new QString(getSvtplaydlVersion(&svtplaydl));

    if(!currentVersion->isEmpty()) {
        *s += QStringLiteral("<b>svtplay-dl ") + tr("version ") + *currentVersion + QStringLiteral("</b><br><br>");
        auto *CommProcess = new QProcess(this);
        CommProcess->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess->start(svtplaydl, QStringList() << QStringLiteral("--help"));
        CommProcess->waitForFinished(-1);
        QTextStream stream(CommProcess->readAllStandardOutput());
        QString line;
        line.clear();
        int i = 0;

        while(!stream.atEnd()) {
            line = stream.readLine();
            line = line.simplified();

            if(i != 0) {
                *s += QStringLiteral("<br>") + line;
            } else {
                *s += line;
            }

            i++;
        }
    }

    QStringList *infoList = new QStringList;
    *infoList << tr("About ") + QDir::toNativeSeparators(svtplaydl);
    *infoList << QStringLiteral("wordwrap");
    // *infoList << QString("PRUT");
    DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
    connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
    mDownloadListDialog->initiate(infoList, s, true);
    delete currentVersion;
}

// END void Newprg::aboutSvtplayDl()
//
bool Newprg::fileExists(QString & path)
{
    QFileInfo check_file(path);

    // check if file exists and if yes: Is it really a file and no directory?
    // and is it executable?
    if(check_file.exists() && check_file.isFile() && check_file.isExecutable()) {
        return true;
    } else {
        return false;
    }
}

QString Newprg::getSvtplaydlVersion(QString * svtplaydl)
{
    auto *CommProcess = new QProcess(nullptr);
    CommProcess->setProcessChannelMode(QProcess::MergedChannels);
    CommProcess->start(*svtplaydl, QStringList() << QStringLiteral("--version"));
    CommProcess->waitForFinished(-1);
    QString line;
    QTextStream stream(CommProcess->readAllStandardOutput());

    while(!stream.atEnd()) {
        line = stream.readLine();
    }

    line = line.mid(11);
    return line;
    // CommProcess->deleteLater();
}

QString Newprg::selectFfmpeg()
{
#ifdef Q_OS_WIN // QT5
    QString EXECUTE =
        QCoreApplication::applicationDirPath() + QStringLiteral("/ffmpeg.exe");
#endif
#ifdef Q_OS_LINUX
    QString EXECUTE =
        QCoreApplication::applicationDirPath() + QStringLiteral("/ffmpeg");
#endif

    if(fileExists(EXECUTE)) {
        return EXECUTE;
    } else {
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        QString senv = env.value(QStringLiteral("PATH"));
#ifdef Q_OS_LINUX
        QStringList slenv = senv.split(QStringLiteral(":"));
#endif
#ifdef Q_OS_WIN
        QStringList slenv = senv.split(QStringLiteral(";"));
#endif

        for(auto &&s : slenv) {
#ifdef Q_OS_LINUX
            s.append(QStringLiteral("/ffmpeg"));
#endif
#ifdef Q_OS_WIN
            s.append(QStringLiteral("/ffmpeg.exe"));
#endif

            if(fileExists(s)) {
                return s;
            }
        }
    }

    return QStringLiteral("NOTFIND");
}
