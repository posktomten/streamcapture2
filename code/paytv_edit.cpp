// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include <QMessageBox>
#include <QSettings>
#include <QInputDialog>
void Newprg::editOrDelete(const QString &provider)
{
    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Question);
    msgBox.setWindowTitle(tr("Manage Login details for ") + provider);
    msgBox.setText(tr("Edit, rename or delete\n") + provider + "?");
    QPushButton *btnEdit = msgBox.addButton(tr("Edit"), QMessageBox::ActionRole);
    QPushButton *btnRename = msgBox.addButton(tr("Rename"), QMessageBox::ActionRole);
    QPushButton *btnDelete = msgBox.addButton(tr("Delete"), QMessageBox::ActionRole);
    QPushButton *btnCancel = msgBox.addButton(tr("Cancel"), QMessageBox::ActionRole);
    msgBox.setDefaultButton(btnEdit);
    msgBox.exec();

    if(msgBox.clickedButton() == btnEdit) {
        editprovider(provider);
    } else if(msgBox.clickedButton() == btnRename) {
        renameProvider(provider);
    } else if(msgBox.clickedButton() == btnDelete) {
        deleteProvider(provider);
    } else if(msgBox.clickedButton() == btnCancel) {
        return;
    }
}

void Newprg::deleteProvider(const QString &provider)
{
    ui->menuPayTV->clear();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Provider");
    QStringList keys = settings.childGroups();
    settings.endGroup();
    int find = keys.indexOf(provider);
    keys.removeAt(find);
    QIcon icon(":/images/new.png");
    QAction *actionCreateNew = new QAction(icon, tr("Create New"), nullptr);
    ui->menuPayTV->addAction(actionCreateNew);
    connect(actionCreateNew, &QAction::triggered, this, [this]() {
        newSupplier();
    });

    // cppcheck
    // QAction *actionPayTV;
    keys.sort(Qt::CaseInsensitive);
    ui->comboPayTV->clear();
    ui->comboPayTV->addItem("- " + tr("No Password"));
    /*nnnnn*/
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

    for(const QString &p : std::as_const(keys)) {
        QAction *actionPayTV = new QAction(p, nullptr);
        ui->menuPayTV->addAction(actionPayTV);
        ui->comboPayTV->addItem(p);
        connect(actionPayTV, &QAction::triggered, this,
        [this, actionPayTV]() {
            editOrDelete(actionPayTV->text());
        });
    }

#else

    for(const QString &p : qAsConst(keys)) {
        // cppcheck
        // actionPayTV = new QAction(p, nullptr);
        QAction *actionPayTV = new QAction(p, nullptr);
        ui->menuPayTV->addAction(actionPayTV);
        ui->comboPayTV->addItem(p);
        connect(actionPayTV, &QAction::triggered,
        [this, actionPayTV]() {
            editOrDelete(actionPayTV->text());
        });
    }

#endif
    /*nnnnn*/
    // for(const QString &p : keys) {
    //     // cppcheck
    //     // actionPayTV = new QAction(p, nullptr);
    //     QAction *actionPayTV = new QAction(p, nullptr);
    //     ui->menuPayTV->addAction(actionPayTV);
    //     ui->comboPayTV->addItem(p);
    //     connect(actionPayTV, &QAction::triggered,
    //     [this, actionPayTV]() {
    //         editOrDelete(actionPayTV->text());
    //     });
    // }
    settings.beginGroup("Provider");
    settings.remove(provider);
    settings.endGroup();
}

void Newprg::editprovider(QString provider)
{
    secretpassword = "";
    provider = provider.trimmed();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Provider");
    const QString username =
        settings.value(provider + "/username", "").toString();
    const QString password =
        settings.value(provider + "/password", "").toString();
    settings.endGroup();
    bool b;

    do {
        b = testinputusername_edit(username, provider);
    } while(!b);

    do {
        b = testinputpassword_edit(password, provider);
    } while(!b);
}

// private:
bool Newprg::testinputusername_edit(const QString &username,
                                    const QString &provider)
{
    bool ok;
    QInputDialog inputdialog;
    inputdialog.setWindowFlags(inputdialog.windowFlags() & ~Qt::WindowContextHelpButtonHint);
    inputdialog.setOkButtonText(tr("Ok"));
    inputdialog.setCancelButtonText(tr("Cancel"));
    inputdialog.setTextValue(username);
    inputdialog.setWindowTitle(tr("Enter your username"));
    inputdialog.setInputMode(QInputDialog::TextInput);
    inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
    ok = inputdialog.exec();
    QString newusername = inputdialog.textValue();

    if((newusername.indexOf(' ') >= 0) || (newusername.isEmpty())) {
        return false;
    }

    if(ok) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        // settings.setIniCodec("UTF-8");
        settings.beginGroup("Provider");
        settings.setValue(provider + "/username", newusername);
        settings.endGroup();
        return true;
    }

    return true;
}

// private:
bool Newprg::testinputpassword_edit(const QString &password,
                                    const QString &provider)
{
    bool ok;
    QInputDialog inputdialog;
    inputdialog.setWindowFlags(inputdialog.windowFlags() & ~Qt::WindowContextHelpButtonHint);
    inputdialog.setOkButtonText(tr("Ok"));
    inputdialog.setCancelButtonText(tr("Cancel"));
    inputdialog.setTextValue(password);
    inputdialog.setTextEchoMode(QLineEdit::Password);
    inputdialog.setWindowTitle(tr("Enter your password"));
    inputdialog.setInputMode(QInputDialog::TextInput);
    inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
    ok = inputdialog.exec();
    QString newpassword = inputdialog.textValue();

    if(newpassword.indexOf(' ') >= 0) {
        return false;
    }

    if(ok) {
        QMessageBox *msgBox = new QMessageBox(this);
        msgBox->setIcon(QMessageBox::Question);
        QPushButton  *yesButton = new QPushButton(tr("Yes"), msgBox);
        QPushButton  *noButton = new QPushButton(tr("Cancel"), msgBox);
        msgBox->setDefaultButton(noButton);
        msgBox->addButton(yesButton, QMessageBox::AcceptRole);
        msgBox->addButton(noButton, QMessageBox::RejectRole);
        msgBox->setDefaultButton(noButton);
        msgBox->setWindowTitle(tr("Save password?"));
        msgBox->setText(tr("Do you want to save the password? (unsafe)?"));
        msgBox->exec();
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Provider");

        if(msgBox->clickedButton() == yesButton)  {
            settings.setValue(provider + "/password", newpassword);
            secretpassword = newpassword;
        } else {
            settings.setValue(provider + "/password", "");
            secretpassword = newpassword;
        }

        settings.endGroup();
        return true;
        settings.endGroup();
        return true;
    }

    return true;
}

bool Newprg::renameProvider(const QString &provider)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Provider");
    const QString username =
        settings.value(provider + "/username", "").toString();
    const QString password =
        settings.value(provider + "/password", "").toString();
    settings.endGroup();
    bool ok;
    QInputDialog inputdialog;
    inputdialog.setWindowFlags(inputdialog.windowFlags() & ~Qt::WindowContextHelpButtonHint);
    inputdialog.setOkButtonText(tr("Ok"));
    inputdialog.setCancelButtonText(tr("Cancel"));
    inputdialog.setWindowTitle(tr("Streaming service"));
    inputdialog.setInputMode(QInputDialog::TextInput);
    inputdialog.setLabelText(tr("Enter the name of your streaming service."));
    inputdialog.setTextValue(provider);
    ok = inputdialog.exec();
    QString newprovider = inputdialog.textValue();
    deleteProvider(provider);
    QAction *actionPayTV;
    actionPayTV = new QAction(newprovider);
    ui->menuPayTV->addAction(actionPayTV);
    ui->comboPayTV->addItem(newprovider);
    connect(actionPayTV, &QAction::triggered, this,
    [actionPayTV, this]() {
        editOrDelete(actionPayTV->text());
    });

    settings.beginGroup("Provider");
    settings.setValue(newprovider + "/username", username);
    settings.setValue(newprovider + "/password", password);
    settings.endGroup();
    return ok;
}
