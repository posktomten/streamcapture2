// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>


#include "newprg.h"
#include <QSettings>
#include <QMessageBox>
#include <QFileInfo>
void Newprg::fusionDarkStyle()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Style");
    const QString currentstyle = settings.value("currentstyle", "none").toString();
    settings.endGroup();

    if(currentstyle == "fusionDarkStyle") {
        return;
    }

    settings.beginGroup("Style");
    settings.setValue("currentstyle", "fusionDarkStyle");
    settings.endGroup();
    QMessageBox *msgBox = new QMessageBox(this);
    QIcon iconYes(QPixmap(":/images/restart.png"));
    QIcon iconLater(QPixmap(":/images/later.png"));
    QIcon iconReject(QPixmap(":/images/application-exit.png"));
    QPushButton  *yesButton = new QPushButton(iconYes, tr("Restart Now"), msgBox);
    yesButton->setFixedSize(QSize(150, 40));
    QPushButton  *laterButton = new QPushButton(iconLater, tr("Change style on next start"), msgBox);
    laterButton->setFixedSize(QSize(350, 40));
    QPushButton  *rejectButton = new QPushButton(iconReject, tr("Cancel"), msgBox);
    rejectButton->setFixedSize(QSize(150, 40));
    msgBox->addButton(yesButton, QMessageBox::YesRole);
    msgBox->addButton(laterButton, QMessageBox::YesRole);
    msgBox->addButton(rejectButton, QMessageBox::RejectRole);
    msgBox->setDefaultButton(laterButton);
    msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox->setText(tr("The program needs to be restarted to switch to Dark Fusion style."));
    msgBox->exec();
    /***/

    if(msgBox->clickedButton() == yesButton) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Style");
        settings.setValue("currentstyle", "fusionDarkStyle");
        settings.endGroup();
        settings.sync();
        const QString EXECUTE =
            QCoreApplication::applicationDirPath() + "/" +
            QFileInfo(QCoreApplication::applicationFilePath()).fileName();
        // QProcess p;
        // p.setProgram(EXECUTE);
        // p.startDetached();
        QProcess::startDetached(EXECUTE);
        close();
    } else if(msgBox->clickedButton() == laterButton) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Style");
        settings.setValue("currentstyle", "fusionDarkStyle");
        settings.endGroup();
    } else {
        delete msgBox;
        return;
    }
}

void Newprg::fusionStyle()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Style");
    const QString currentstyle = settings.value("currentstyle", "none").toString();
    settings.endGroup();

    if(currentstyle == "fusionStyle") {
        return;
    }

    settings.beginGroup("Style");
    settings.setValue("currentstyle", "fusionStyle");
    settings.endGroup();
    QMessageBox *msgBox = new QMessageBox(this);
    QIcon iconYes(QPixmap(":/images/restart.png"));
    QIcon iconLater(QPixmap(":/images/later.png"));
    QIcon iconReject(QPixmap(":/images/application-exit.png"));
    QPushButton  *yesButton = new QPushButton(iconYes, tr("Restart Now"), msgBox);
    yesButton->setFixedSize(QSize(150, 40));
    QPushButton  *laterButton = new QPushButton(iconLater, tr("Change style on next start"), msgBox);
    laterButton->setFixedSize(QSize(350, 40));
    QPushButton  *rejectButton = new QPushButton(iconReject, tr("Cancel"), msgBox);
    rejectButton->setFixedSize(QSize(150, 40));
    msgBox->addButton(yesButton, QMessageBox::YesRole);
    msgBox->addButton(laterButton, QMessageBox::YesRole);
    msgBox->addButton(rejectButton, QMessageBox::RejectRole);
    msgBox->setDefaultButton(laterButton);
    msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox->setText(tr("The program needs to be restarted to switch to Fusion style."));
    msgBox->exec();
    /***/

    if(msgBox->clickedButton() == yesButton) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Style");
        settings.setValue("currentstyle", "fusionStyle");
        settings.endGroup();
        settings.sync();
        const QString EXECUTE =
            QCoreApplication::applicationDirPath() + "/" +
            QFileInfo(QCoreApplication::applicationFilePath()).fileName();
        // QProcess p;
        // p.setProgram(EXECUTE);
        // p.startDetached();
        QProcess::startDetached(EXECUTE);
        close();
    } else if(msgBox->clickedButton() == laterButton) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Style");
        settings.setValue("currentstyle", "fusionStyle");
        settings.endGroup();
    } else {
        delete msgBox;
        return;
    }
}

void Newprg::defaultStyle()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Style");
    const QString currentstyle = settings.value("currentstyle", "none").toString();
    settings.endGroup();

    if(currentstyle == "defaultStyle") {
        return;
    }

    settings.beginGroup("Style");
    settings.setValue("currentstyle", "defaultStyle");
    settings.endGroup();
    QMessageBox *msgBox = new QMessageBox(this);
    QIcon iconYes(QPixmap(":/images/restart.png"));
    QIcon iconLater(QPixmap(":/images/later.png"));
    QIcon iconReject(QPixmap(":/images/application-exit.png"));
    QPushButton  *yesButton = new QPushButton(iconYes, tr("Restart Now"), msgBox);
    yesButton->setFixedSize(QSize(150, 40));
    QPushButton  *laterButton = new QPushButton(iconLater, tr("Change style on next start"), msgBox);
    laterButton->setFixedSize(QSize(350, 40));
    QPushButton  *rejectButton = new QPushButton(iconReject, tr("Cancel"), msgBox);
    rejectButton->setFixedSize(QSize(150, 40));
    msgBox->addButton(yesButton, QMessageBox::YesRole);
    msgBox->addButton(laterButton, QMessageBox::YesRole);
    msgBox->addButton(rejectButton, QMessageBox::RejectRole);
    msgBox->setDefaultButton(laterButton);
    msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
    msgBox->setText(tr("The program needs to be restarted to switch to Default style."));
    msgBox->exec();
    /***/

    if(msgBox->clickedButton() == yesButton) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Style");
        settings.setValue("currentstyle", "defaultStyle");
        settings.endGroup();
        settings.sync();
        const QString EXECUTE =
            QCoreApplication::applicationDirPath() + "/" +
            QFileInfo(QCoreApplication::applicationFilePath()).fileName();
        // QProcess p;
        // p.setProgram(EXECUTE);
        // p.startDetached();
        QProcess::startDetached(EXECUTE);
        close();
    } else if(msgBox->clickedButton() == laterButton) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Style");
        settings.setValue("currentstyle", "defaultStyle");
        settings.endGroup();
    } else {
        delete msgBox;
        return;
    }
}
