// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "zoom_disable.h"
#ifdef Q_OS_LINUX
#include "cli.h"
#endif
// #include "zoom_disable.h"
#include <QStyleFactory>
#include <QTranslator>
#include <QSettings>
#include <QFileInfo>
#include <QMessageBox>

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);
#endif
    QApplication app(argc, argv);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    /*** STYLE ***/
    settings.beginGroup("Style");
    const QString currentstyle = settings.value("currentstyle", "none").toString();
    settings.endGroup();

    if((currentstyle == "fusionStyle") || (currentstyle == "fusionDarkStyle")) {
        QApplication::setStyle(QStyleFactory::create("Fusion"));
    } else {
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
        QApplication::setStyle(QStyleFactory::create("windows11"));
#endif
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
        QApplication::setStyle(QStyleFactory::create("windowsvista"));
#endif
    }

#if defined (Q_OS_LINUX)
    QApplication::setStyle(QStyleFactory::create("Fusion"));
#endif
    /*** END STYLE ***/
    settings.beginGroup("Settings");
    const bool dontusenativedialogs = settings.value("dontusenativedialogs", false).toBool();
    settings.endGroup();
    QCoreApplication::setAttribute(Qt::AA_DontUseNativeDialogs, dontusenativedialogs);
#if defined(Q_OS_LINUX)
    QString str = QString::fromLocal8Bit(argv[1]);

    /*** CLI ***/
    if((argc > 1) && (str != "--gui")) {
        QStringList argumentlist;

        for(int i = 0; i < argc; i++) {
            argumentlist << QString::fromLocal8Bit(argv[i]);
        }

        Cli *cli = new Cli;
        cli->parseOptions(argumentlist);
        return app.exec();
    }

    QCoreApplication::setAttribute(Qt::AA_NativeWindows, true);
#endif
    const QString currentLocale(QLocale::system().name());
    settings.beginGroup("Language");
    const QString sprak = settings.value("language", currentLocale).toString();
    settings.endGroup();
    const QLocale locale(sprak);
    const QString qttranslationsPath(":/i18n/qttranslations");
    QTranslator qtTranslator;

    if(qtTranslator.load(locale, "qt", "_", qttranslationsPath)) {
        QApplication::installTranslator(&qtTranslator);
    }

    QTranslator qtBaseTranslator;

    if(qtBaseTranslator.load(locale, "qtbase", "_", qttranslationsPath)) {
        QApplication::installTranslator(&qtBaseTranslator);
    }

    if(currentstyle == "fusionDarkStyle") {
        QColor darkGray(53, 53, 53);
        QColor gray(128, 128, 128);
        QColor black(25, 25, 25);
        QColor blue(42, 130, 218);
        QPalette darkPalette;
        darkPalette.setColor(QPalette::Window, darkGray);
        darkPalette.setColor(QPalette::WindowText, Qt::white);
        darkPalette.setColor(QPalette::Base, black);
        darkPalette.setColor(QPalette::AlternateBase, darkGray);
        darkPalette.setColor(QPalette::ToolTipBase, blue);
        darkPalette.setColor(QPalette::ToolTipText, Qt::white);
        darkPalette.setColor(QPalette::Text, Qt::white);
        darkPalette.setColor(QPalette::Button, darkGray);
        darkPalette.setColor(QPalette::ButtonText, Qt::white);
        darkPalette.setColor(QPalette::Link, Qt::red);
        // darkPalette.setColor(QPalette::Link, Qt::red);
        darkPalette.setColor(QPalette::Highlight, blue);
        // darkPalette.setColor(QPalette::HighlightedText, Qt::black);
        darkPalette.setColor(QPalette::HighlightedText, Qt::yellow);
        darkPalette.setColor(QPalette::Active, QPalette::Button, gray.darker());
        darkPalette.setColor(QPalette::Disabled, QPalette::ButtonText, gray);
        darkPalette.setColor(QPalette::Disabled, QPalette::WindowText, gray);
        darkPalette.setColor(QPalette::Disabled, QPalette::Text, gray);
        darkPalette.setColor(QPalette::Disabled, QPalette::Light, darkGray);
        QApplication::setPalette(darkPalette);
    }

// #endif // Q_OS_LINUX
    /*   */
    QTranslator translator;
    settings.beginGroup("Language");
    const QString testlanguage = settings.value("testlanguage").toString();
    const QString language = settings.value("language", "").toString();
    settings.endGroup();
    QFileInfo fi(testlanguage);

    if((language.isEmpty()) && (fi.exists())) {
        if(translator.load(testlanguage)) {
            QApplication::installTranslator(&translator);
        }
    }
    // END Test translation
    else {
        // ELSE
        settings.beginGroup("Language");
        QString sp = settings.value("language", "").toString();
        settings.endGroup();
#ifdef Q_OS_LINUX
        const QString translationPath = ":/i18n/_complete_lin";
#endif
#ifdef Q_OS_WIN
        const QString translationPath = ":/i18n/_complete_win";
#endif
        // const QString translationPath = ":/i18n/_complete";

        if(sp == "") {
            sp = QLocale::system().name();

            if(translator.load(translationPath + "_" + sp + ".qm")) {
                settings.beginGroup("Language");
                settings.setValue("language", sp);
                settings.endGroup();
                /*  */
                /*  */
                QApplication::installTranslator(&translator);
            } else if(sp != "en_US") {
                QMessageBox::information(
                    nullptr, DISPLAY_NAME " " VERSION,
                    DISPLAY_NAME
                    " is unfortunately not yet translated into your language, the "
                    "program will start with English menus.");
                settings.beginGroup("Language");
                settings.setValue("language", "en_US");
                settings.endGroup();
            }
        } else {
            if(translator.load(translationPath + "_" + sp + ".qm")) {
                QApplication::installTranslator(&translator);
            }
        }

        // END ELSE Test translation
    }

    Newprg *mNewPrg = new Newprg;
    ZoomDisabler *zoomDisabler = new ZoomDisabler;
    zoomDisabler->setParent(nullptr);
    // zoomDisabler->deleteLater();
    QObject::connect(&app, &QCoreApplication::aboutToQuit, &app,
                     [mNewPrg]() -> void { mNewPrg->setEndConfig(); });
    mNewPrg->show();
    int r = QApplication::exec();
    return r;
}
