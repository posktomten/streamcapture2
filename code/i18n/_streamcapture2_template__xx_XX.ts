<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>Cli</name>
    <message>
        <location filename="../cli.cpp" line="151"/>
        <source>Compiler:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="158"/>
        <source>Compiler: Clang version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="166"/>
        <source>Verify authenticity.
SHA256 checksum is generated.
 &apos;sha256sum&apos; is required.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="169"/>
        <source>Displays the version history.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="172"/>
        <source>The new AppImage is created with zsync.
Only the changes are downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="174"/>
        <source>Check for updates.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="176"/>
        <source>Trying to read your operating system&apos;s GLIBC version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="179"/>
        <source>Reads the version number for FFmpeg included in the AppImage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="182"/>
        <source>Trying to read your operating system&apos;s FFmpeg version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="183"/>
        <source>Starts the GUI (Graphical User Interface).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="187"/>
        <source>Select language --lang en (English), --lang it (Italian) or --lang sv (Swedish)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="191"/>
        <source>Displays the license.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="192"/>
        <source>Reads the version number for openSSL included in the AppImage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="194"/>
        <source>Trying to read your operating system&apos;s openSSL version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="197"/>
        <source>Runs on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="197"/>
        <source>Development tool: Qt</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="197"/>
        <source>Compiled with: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="197"/>
        <source>Copyright (C) </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="197"/>
        <source>Website: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="198"/>
        <source>Swedish website: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="200"/>
        <source>Source code: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>E-mail: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>This program was created </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>Path: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source> is free software: you can redistribute it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>and/or modify it under the terms of the</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>GNU General Public License as published by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>the Free Software Foundation, version 3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>This program is distributed in the hope that it will be useful, </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>but WITHOUT ANY WARRANTY; without even the implied warranty of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="202"/>
        <source>See the GNU General Public License for more details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="205"/>
        <source>Help about the AppImage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="324"/>
        <location filename="../cli.cpp" line="347"/>
        <source>Please select &apos;--lang en&apos;, &apos;--lang it&apos; or &apos;--lang sv&apos;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="400"/>
        <source>openSSL is not found on your system.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="425"/>
        <source>FFmpeg not found. You can use FFmpeg which is included in the AppImage.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="427"/>
        <source>FFmpeg not found. Install FFmpeg or download an AppImage that includes FFmpeg.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="450"/>
        <source>GLIBC not found. GLIBC is required to boot Linux.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="544"/>
        <source>Click &apos;1&apos; to show more or &apos;Ctrl + C&apos; to exit. :&gt; </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="611"/>
        <source>Unable to load the language file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="615"/>
        <source>Expected checksum:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="616"/>
        <source>Calculated checksum:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="617"/>
        <source>The checksums are identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="617"/>
        <source> is genuine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="617"/>
        <source> The checksums are not identical. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="617"/>
        <source> is not genuine</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadListDialog</name>
    <message>
        <location filename="../downloadlistdialog.ui" line="20"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="23"/>
        <source>Please zoom using the keyboard. &quot;Ctrl + +&quot;, &quot;Ctrl + -&quot; or &quot;Ctrl + 0&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="71"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="88"/>
        <location filename="../downloadlistdialog.cpp" line="188"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="111"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.ui" line="128"/>
        <source>Remove invalid list items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="118"/>
        <location filename="../downloadlistdialog.cpp" line="131"/>
        <location filename="../downloadlistdialog.cpp" line="149"/>
        <source>An unexpected error has occurred.
The file with saved links to downloadable files cannot be found.
Check that the file has not been deleted or moved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="119"/>
        <location filename="../downloadlistdialog.cpp" line="132"/>
        <location filename="../downloadlistdialog.cpp" line="150"/>
        <location filename="../downloadlistdialog.cpp" line="223"/>
        <location filename="../downloadlistdialog.cpp" line="339"/>
        <location filename="../downloadlistdialog.cpp" line="378"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="171"/>
        <source>Edit the download list (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="171"/>
        <source> Video streams)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="189"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="190"/>
        <source>Save as text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="198"/>
        <location filename="../downloadlistdialog.cpp" line="201"/>
        <location filename="../downloadlistdialog.cpp" line="208"/>
        <source>Text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="198"/>
        <location filename="../downloadlistdialog.cpp" line="201"/>
        <source>Text file, optional extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="222"/>
        <source>Could not save the file.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="337"/>
        <location filename="../downloadlistdialog.cpp" line="376"/>
        <source>Unable to find file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog.cpp" line="338"/>
        <location filename="../downloadlistdialog.cpp" line="377"/>
        <source>Unable to find</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../newprg.cpp" line="1499"/>
        <location filename="../newprg.cpp" line="1524"/>
        <source>Enter your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1500"/>
        <location filename="../newprg.cpp" line="1525"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1571"/>
        <source>svtplay-dl crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1575"/>
        <source>Could not stop svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1581"/>
        <source>svtplay-dl stopped. Exit code </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1584"/>
        <source>Delete any files that may have already been downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1606"/>
        <source>Copy to: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1771"/>
        <source>Download to: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <location filename="../about.cpp" line="39"/>
        <source>Version history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="49"/>
        <source>License streamCapture2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="60"/>
        <source>License svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="72"/>
        <source>License FFmpeg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="85"/>
        <source>License 7zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="103"/>
        <source> could not be found. Please download a portable streamCapture2 where FFmpeg is included.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="106"/>
        <source> could not be found. Please go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install FFmpeg.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="108"/>
        <location filename="../about.cpp" line="112"/>
        <source>Or install </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="108"/>
        <location filename="../about.cpp" line="112"/>
        <source> in your system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="111"/>
        <source> could not be found. Please download an AppImage where FFmpeg is included.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="138"/>
        <location filename="../about.cpp" line="250"/>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="153"/>
        <source>svtplay-dl is not found in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="155"/>
        <location filename="../about.cpp" line="159"/>
        <source> You can download svtplay-dl from bin.ceicer.com. Select &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="158"/>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="162"/>
        <source>About svtplay-dl (In the system path)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="198"/>
        <source> svtplay-dl.exe  cannot be found. Go to &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="200"/>
        <location filename="../about.cpp" line="209"/>
        <source>Please click &quot;svtplay-dl&quot; and select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="207"/>
        <source> svtplay-dl cannot be found. Go to &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="214"/>
        <source>About svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../about.cpp" line="225"/>
        <source>version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="29"/>
        <source>You have downloaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="32"/>
        <source>Nothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="33"/>
        <source>To folder &quot;stable&quot;: Nothing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="35"/>
        <source>To folder &quot;stable&quot;: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="39"/>
        <source>To folder &quot;beta&quot;: Nothing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="41"/>
        <source>To folder &quot;beta&quot;: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="55"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="79"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="94"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="247"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="268"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="285"/>
        <location filename="../coppytodefaultlocation.cpp" line="39"/>
        <location filename="../download.cpp" line="32"/>
        <location filename="../download.cpp" line="83"/>
        <location filename="../download.cpp" line="204"/>
        <location filename="../download.cpp" line="237"/>
        <location filename="../download.cpp" line="253"/>
        <location filename="../download.cpp" line="273"/>
        <location filename="../download.cpp" line="285"/>
        <location filename="../download.cpp" line="346"/>
        <location filename="../download.cpp" line="520"/>
        <location filename="../downloadall.cpp" line="38"/>
        <location filename="../downloadall.cpp" line="51"/>
        <location filename="../downloadall.cpp" line="78"/>
        <location filename="../downloadall.cpp" line="142"/>
        <location filename="../downloadall.cpp" line="169"/>
        <location filename="../downloadall.cpp" line="181"/>
        <location filename="../downloadall.cpp" line="214"/>
        <location filename="../downloadall.cpp" line="253"/>
        <location filename="../downloadall.cpp" line="286"/>
        <location filename="../downloadall.cpp" line="295"/>
        <location filename="../downloadall.cpp" line="341"/>
        <location filename="../downloadall.cpp" line="387"/>
        <location filename="../downloadall.cpp" line="433"/>
        <location filename="../downloadall.cpp" line="531"/>
        <location filename="../downloadlistdialog_get.cpp" line="39"/>
        <location filename="../import_export.cpp" line="68"/>
        <location filename="../import_export.cpp" line="132"/>
        <location filename="../import_export.cpp" line="160"/>
        <location filename="../import_export.cpp" line="196"/>
        <location filename="../listallepisodes.cpp" line="42"/>
        <location filename="../listallepisodes.cpp" line="127"/>
        <location filename="../newprg.cpp" line="299"/>
        <location filename="../newprg.cpp" line="454"/>
        <location filename="../newprg.cpp" line="593"/>
        <location filename="../newprg.cpp" line="616"/>
        <location filename="../newprg.cpp" line="713"/>
        <location filename="../newprg.cpp" line="736"/>
        <location filename="../newprg.cpp" line="795"/>
        <location filename="../newprg.cpp" line="812"/>
        <location filename="../newprg.cpp" line="824"/>
        <location filename="../newprg.cpp" line="854"/>
        <location filename="../newprg.cpp" line="1164"/>
        <location filename="../newprg.cpp" line="1310"/>
        <location filename="../newprg.cpp" line="1322"/>
        <location filename="../newprg.cpp" line="1336"/>
        <location filename="../newprg.cpp" line="1650"/>
        <location filename="../newprg.cpp" line="1717"/>
        <location filename="../newprg.cpp" line="1820"/>
        <location filename="../newprg.cpp" line="1853"/>
        <location filename="../newprg.cpp" line="1879"/>
        <location filename="../newprg.cpp" line="1957"/>
        <location filename="../newprg.cpp" line="1985"/>
        <location filename="../newprg.cpp" line="2019"/>
        <location filename="../newprg.cpp" line="2057"/>
        <location filename="../newprg.cpp" line="2096"/>
        <location filename="../newprg.cpp" line="2120"/>
        <location filename="../newprg.cpp" line="2363"/>
        <location filename="../newprg.cpp" line="2487"/>
        <location filename="../nfo.cpp" line="85"/>
        <location filename="../nfo.cpp" line="110"/>
        <location filename="../nfo.cpp" line="125"/>
        <location filename="../nfo.cpp" line="204"/>
        <location filename="../paytv_create.cpp" line="66"/>
        <location filename="../paytv_create.cpp" line="102"/>
        <location filename="../paytv_create.cpp" line="137"/>
        <location filename="../paytv_edit.cpp" line="145"/>
        <location filename="../paytv_edit.cpp" line="178"/>
        <location filename="../paytv_edit.cpp" line="239"/>
        <location filename="../setgetconfig.cpp" line="140"/>
        <location filename="../setgetconfig.cpp" line="556"/>
        <location filename="../setgetconfig.cpp" line="569"/>
        <location filename="../setgetconfig.cpp" line="722"/>
        <location filename="../shortcuts.cpp" line="67"/>
        <location filename="../shortcuts.cpp" line="142"/>
        <location filename="../sok.cpp" line="96"/>
        <location filename="../sok.cpp" line="126"/>
        <location filename="../st_create.cpp" line="28"/>
        <location filename="../st_create.cpp" line="37"/>
        <location filename="../st_edit.cpp" line="126"/>
        <location filename="../st_edit.cpp" line="142"/>
        <location filename="../user_message.cpp" line="116"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="57"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl beta can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="81"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="249"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="270"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="287"/>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl stable can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="96"/>
        <source>An unexpected error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="96"/>
        <source>Information about svtplay-dl stable can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="96"/>
        <source>Check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="113"/>
        <source>You are using version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="117"/>
        <source>You are not using svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="122"/>
        <source>The latest svtplay-dl available for&lt;br&gt;download from bin.ceicer.com are</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="122"/>
        <source>Stable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="122"/>
        <source>Beta:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="122"/>
        <source>You have selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="130"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="388"/>
        <location filename="../download_runtime.cpp" line="28"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="132"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="390"/>
        <location filename="../download_runtime.cpp" line="29"/>
        <location filename="../download.cpp" line="347"/>
        <location filename="../download.cpp" line="519"/>
        <location filename="../downloadall.cpp" line="256"/>
        <location filename="../downloadall.cpp" line="388"/>
        <location filename="../downloadall.cpp" line="432"/>
        <location filename="../import_export.cpp" line="46"/>
        <location filename="../import_export.cpp" line="106"/>
        <location filename="../import_export.cpp" line="175"/>
        <location filename="../language.cpp" line="40"/>
        <location filename="../language.cpp" line="95"/>
        <location filename="../language.cpp" line="148"/>
        <location filename="../newprg.cpp" line="59"/>
        <location filename="../newprg.cpp" line="180"/>
        <location filename="../newprg.cpp" line="300"/>
        <location filename="../newprg.cpp" line="1627"/>
        <location filename="../newprg.cpp" line="1692"/>
        <location filename="../newprg.cpp" line="1916"/>
        <location filename="../newprg.cpp" line="2364"/>
        <location filename="../newprg.cpp" line="2435"/>
        <location filename="../paytv_create.cpp" line="67"/>
        <location filename="../paytv_create.cpp" line="103"/>
        <location filename="../paytv_create.cpp" line="138"/>
        <location filename="../paytv_create.cpp" line="155"/>
        <location filename="../paytv_edit.cpp" line="31"/>
        <location filename="../paytv_edit.cpp" line="146"/>
        <location filename="../paytv_edit.cpp" line="179"/>
        <location filename="../paytv_edit.cpp" line="196"/>
        <location filename="../paytv_edit.cpp" line="240"/>
        <location filename="../save.cpp" line="36"/>
        <location filename="../setgetconfig.cpp" line="151"/>
        <location filename="../st_create.cpp" line="29"/>
        <location filename="../st_create.cpp" line="38"/>
        <location filename="../st_edit.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="127"/>
        <location filename="../st_edit.cpp" line="143"/>
        <location filename="../style.cpp" line="46"/>
        <location filename="../style.cpp" line="107"/>
        <location filename="../style.cpp" line="168"/>
        <location filename="../test_translation.cpp" line="40"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="191"/>
        <source>&quot;Use svtplay-dl stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="193"/>
        <source>&quot;Use svtplay-dl stable&quot;, but it can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="212"/>
        <source>&quot;Use svtplay-dl beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="214"/>
        <source>&quot;Use svtplay-dl beta&quot;, but it can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="227"/>
        <source>&quot;Use the selected svtplay-dl&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="229"/>
        <source>&quot;Use the selected svtplay-dl&quot;, but it can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="233"/>
        <source>svtplay-dl is not found in the specified path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="333"/>
        <source>&lt;b&gt;Welcome to streamCapture2!&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="342"/>
        <source>&lt;b&gt;svtplay-dl stable&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="343"/>
        <source>Click &lt;i&gt;Download&lt;/i&gt; and &lt;i&gt;Latest stable svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="349"/>
        <source>&lt;b&gt;svtplay-dl beta&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="350"/>
        <source>Click &lt;i&gt;Download&lt;/i&gt; and &lt;i&gt;Latest svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="358"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="367"/>
        <source>&lt;b&gt;svtplay-dl stable is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="359"/>
        <location filename="../check_svtplaydl_for_updates.cpp" line="376"/>
        <source>&lt;b&gt;svtplay-dl beta is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="363"/>
        <source>Click &lt;i&gt;Download&lt;/i&gt;, then click&lt;br&gt;&lt;i&gt;Latest stable svtplay-dl </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="363"/>
        <source>&lt;/i&gt;and&lt;br&gt;&lt;i&gt;Latest svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="370"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Latest stable svtplay-dl&quot;&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../check_svtplaydl_for_updates.cpp" line="379"/>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Latest svtplay-dl beta&quot;&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="36"/>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="61"/>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="66"/>
        <location filename="../coppytodefaultlocation.cpp" line="86"/>
        <source>Copy succeeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../coppytodefaultlocation.cpp" line="68"/>
        <location filename="../coppytodefaultlocation.cpp" line="88"/>
        <source>Copy failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download_runtime.cpp" line="27"/>
        <source>Only if svtplay-dl does not work do you need to install
&quot;Microsoft Visual C++ Redistributable&quot;.
Download and double-click to install.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <location filename="../downloadall.cpp" line="141"/>
        <location filename="../listallepisodes.cpp" line="41"/>
        <source>svtplay-dl cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <location filename="../downloadall.cpp" line="141"/>
        <location filename="../listallepisodes.cpp" line="41"/>
        <location filename="../newprg.cpp" line="1819"/>
        <location filename="../newprg.cpp" line="1852"/>
        <location filename="../newprg.cpp" line="1878"/>
        <location filename="../newprg.cpp" line="1984"/>
        <location filename="../sok.cpp" line="95"/>
        <source>Or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="31"/>
        <location filename="../downloadall.cpp" line="141"/>
        <location filename="../listallepisodes.cpp" line="41"/>
        <location filename="../newprg.cpp" line="1878"/>
        <location filename="../setgetconfig.cpp" line="621"/>
        <location filename="../sok.cpp" line="95"/>
        <source>Please click on &quot;svtplay-dl&quot; and select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="82"/>
        <location filename="../downloadall.cpp" line="530"/>
        <location filename="../listallepisodes.cpp" line="126"/>
        <location filename="../sok.cpp" line="125"/>
        <source>You need to copy the TV4 token from your browser and save it in streamCapture2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="103"/>
        <source>The request is processed...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="103"/>
        <source>Preparing to download...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="120"/>
        <source>Download streaming media to folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="202"/>
        <location filename="../download.cpp" line="283"/>
        <location filename="../downloadall.cpp" line="179"/>
        <location filename="../downloadall.cpp" line="212"/>
        <location filename="../downloadall.cpp" line="294"/>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="226"/>
        <source>The video stream is saved in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="235"/>
        <location filename="../downloadall.cpp" line="167"/>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="251"/>
        <source>You do not have the right to save to the default folder.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="271"/>
        <source>The folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="292"/>
        <location filename="../downloadall.cpp" line="300"/>
        <source>Selected folder to copy to is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="348"/>
        <location filename="../downloadall.cpp" line="389"/>
        <location filename="../listallepisodes.cpp" line="163"/>
        <location filename="../paytv_create.cpp" line="139"/>
        <location filename="../paytv_edit.cpp" line="182"/>
        <location filename="../sok.cpp" line="195"/>
        <source>Enter your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="351"/>
        <location filename="../downloadall.cpp" line="392"/>
        <location filename="../paytv_create.cpp" line="106"/>
        <location filename="../paytv_create.cpp" line="142"/>
        <location filename="../paytv_edit.cpp" line="150"/>
        <location filename="../paytv_edit.cpp" line="184"/>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="373"/>
        <source>Starts downloading: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="379"/>
        <location filename="../downloadall.cpp" line="636"/>
        <source>Unable to find any streams with the selected video resolution.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="407"/>
        <location filename="../downloadall.cpp" line="656"/>
        <source>Merge audio and video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="411"/>
        <location filename="../downloadall.cpp" line="660"/>
        <source>Removing old files, if there are any...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="432"/>
        <location filename="../download.cpp" line="435"/>
        <source>The download failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="441"/>
        <location filename="../download.cpp" line="448"/>
        <location filename="../downloadall.cpp" line="564"/>
        <location filename="../downloadall.cpp" line="571"/>
        <source>Download succeeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="485"/>
        <location filename="../download.cpp" line="490"/>
        <source>Download completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="500"/>
        <location filename="../download.cpp" line="504"/>
        <source>The download failed </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="521"/>
        <location filename="../downloadall.cpp" line="434"/>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="525"/>
        <location filename="../downloadall.cpp" line="438"/>
        <source>Select file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="545"/>
        <source>No folder is selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="573"/>
        <location filename="../download.cpp" line="574"/>
        <location filename="../listallepisodes.cpp" line="64"/>
        <location filename="../listallepisodes.cpp" line="65"/>
        <source>Searching...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../download.cpp" line="575"/>
        <location filename="../listallepisodes.cpp" line="66"/>
        <source>The request is processed...
Starting search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="37"/>
        <location filename="../newprg.cpp" line="209"/>
        <location filename="../newprg.cpp" line="592"/>
        <location filename="../newprg.cpp" line="712"/>
        <location filename="../newprg.cpp" line="779"/>
        <location filename="../newprg.cpp" line="794"/>
        <source>There is no file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="37"/>
        <location filename="../newprg.cpp" line="592"/>
        <location filename="../newprg.cpp" line="712"/>
        <location filename="../newprg.cpp" line="794"/>
        <source>Select &quot;Download List&quot; and &quot;New download List...&quot;.
Or select &quot;Download List&quot; and &quot;Import a download List...&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="50"/>
        <source>The list of downloadable files is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="77"/>
        <source>The list of downloadable video streams is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="99"/>
        <source>Invalid link found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="99"/>
        <source>Invalid links found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="125"/>
        <source>Invalid text string found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="125"/>
        <source>Invalid text strings found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="190"/>
        <location filename="../newprg.cpp" line="1693"/>
        <source>Download streaming media to directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="261"/>
        <source>You have chosen to download more than one file with the same name. In order not to overwrite files, folders will be created for each file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="284"/>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="324"/>
        <source>The video streams are saved in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="340"/>
        <source>An unexpected error has occurred.
The file with saved links to downloadable video streams is incorrect.
Check that all links are correct.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="517"/>
        <source>Preparing to download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="593"/>
        <location filename="../downloadall.cpp" line="597"/>
        <source>The download failed.
The error may be due to your &apos;method&apos;, &apos;quality&apos; and/or &apos;deviation&apos; selections not working.
It usually works best if you let svtplay-dl choose automatically.
If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="593"/>
        <location filename="../downloadall.cpp" line="597"/>
        <source>
You tried: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadall.cpp" line="614"/>
        <source>streamCapture2 is done with the task.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog_get.cpp" line="38"/>
        <location filename="../import_export.cpp" line="67"/>
        <location filename="../import_export.cpp" line="195"/>
        <source> is not allowed to save </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../downloadlistdialog_get.cpp" line="38"/>
        <location filename="../import_export.cpp" line="67"/>
        <location filename="../import_export.cpp" line="195"/>
        <source> Check your file permissions or choose another location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="45"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="47"/>
        <source>New downloadlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="48"/>
        <location filename="../import_export.cpp" line="108"/>
        <location filename="../import_export.cpp" line="177"/>
        <source>Download list files (*.lst)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="105"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="107"/>
        <source>Import a download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="131"/>
        <source> is not allowed to import </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="131"/>
        <source> Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="159"/>
        <source>In order to export a Download list file, you must first create or import such a file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="174"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../import_export.cpp" line="176"/>
        <source>Export </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="36"/>
        <location filename="../language.cpp" line="91"/>
        <location filename="../language.cpp" line="144"/>
        <location filename="../newprg.cpp" line="55"/>
        <location filename="../style.cpp" line="42"/>
        <location filename="../style.cpp" line="103"/>
        <location filename="../style.cpp" line="164"/>
        <source>Restart Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="38"/>
        <location filename="../language.cpp" line="93"/>
        <location filename="../language.cpp" line="146"/>
        <source>Change language on next start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="47"/>
        <source>The program needs to be restarted to switch to English.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="102"/>
        <source>The program needs to be restarted to switch to Swedish.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../language.cpp" line="155"/>
        <source>The program needs to be restarted to switch to Italian.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="80"/>
        <location filename="../nfo.cpp" line="26"/>
        <location filename="../nfo.cpp" line="68"/>
        <location filename="../nfo.cpp" line="99"/>
        <location filename="../sok.cpp" line="160"/>
        <source>The search field is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="90"/>
        <location filename="../sok.cpp" line="169"/>
        <source>Invalid link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="164"/>
        <location filename="../sok.cpp" line="196"/>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="209"/>
        <location filename="../listallepisodes.cpp" line="217"/>
        <location filename="../listallepisodes.cpp" line="326"/>
        <location filename="../sok.cpp" line="426"/>
        <source>ERROR: Can&apos;t find any videos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="226"/>
        <location filename="../listallepisodes.cpp" line="234"/>
        <location filename="../listallepisodes.cpp" line="250"/>
        <location filename="../sok.cpp" line="343"/>
        <location filename="../sok.cpp" line="357"/>
        <location filename="../sok.cpp" line="363"/>
        <location filename="../sok.cpp" line="371"/>
        <source>ERROR: No videos found. Can&apos;t find video id for the video.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="242"/>
        <location filename="../sok.cpp" line="440"/>
        <source>WARNING: Use program page instead of the clip / video page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="259"/>
        <source>ERROR: No videos found. Can&apos;t find video id.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="267"/>
        <location filename="../sok.cpp" line="377"/>
        <source>ERROR: No videos found. Can&apos;t decode api request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="275"/>
        <location filename="../sok.cpp" line="383"/>
        <source>ERROR: No videos found. Use the video page not the series page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="283"/>
        <location filename="../sok.cpp" line="391"/>
        <source>ERROR: No videos found. Can&apos;t find any videos. Is it removed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="291"/>
        <location filename="../sok.cpp" line="395"/>
        <source>ERROR: No videos found. Can&apos;t find the video file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="300"/>
        <location filename="../sok.cpp" line="404"/>
        <source>ERROR: Click on &quot;Tools&quot;, &quot;Show more&quot; and run again to get more information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="309"/>
        <location filename="../sok.cpp" line="412"/>
        <source>ERROR: If the error persists, you can report it at https://github.com/spaam/svtplay-dl/issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="318"/>
        <location filename="../sok.cpp" line="420"/>
        <source>ERROR: Include URL and error message in problem description.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="334"/>
        <location filename="../sok.cpp" line="434"/>
        <source>WARNING: --all-episodes not implemented for this service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="344"/>
        <location filename="../sok.cpp" line="231"/>
        <source> crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../listallepisodes.cpp" line="346"/>
        <location filename="../sok.cpp" line="233"/>
        <source>Can not find any video streams, please check the address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="57"/>
        <source>Change settings on next start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="66"/>
        <source>The program must be restarted for the new settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="124"/>
        <source>Whether notifications are displayed depends on the settings in your operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="179"/>
        <location filename="../setgetconfig.cpp" line="150"/>
        <location filename="../test_translation.cpp" line="39"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="181"/>
        <source>Open downloaded file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="184"/>
        <source>Video files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="207"/>
        <location filename="../newprg.cpp" line="777"/>
        <source>File: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="236"/>
        <source>If the download does not work</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="301"/>
        <source>Enter TV4 token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="304"/>
        <source>You must paste the exact TV4 token.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="400"/>
        <source>Edit streamCapture2 settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="425"/>
        <source>Edit download svtplay-dl settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="453"/>
        <source>svtplay-dl is not found in the system path.
You can download any svtplay-dl of your choice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="536"/>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="539"/>
        <source>bold and italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="542"/>
        <source>bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="544"/>
        <source>italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="548"/>
        <source>Current font:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="549"/>
        <source>size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="606"/>
        <location filename="../newprg.cpp" line="726"/>
        <source>Cannot find the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="606"/>
        <location filename="../newprg.cpp" line="726"/>
        <location filename="../newprg.cpp" line="807"/>
        <source>with download links listed.
Check if the file has been removed or moved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="608"/>
        <location filename="../newprg.cpp" line="728"/>
        <source>Cannot open the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="608"/>
        <location filename="../newprg.cpp" line="728"/>
        <location filename="../newprg.cpp" line="819"/>
        <source>with download links listed.
Check if the file permissions have changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="671"/>
        <source>Invalid list item found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="671"/>
        <source>Invalid list items found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="672"/>
        <source> Incorrect link found. Please click &quot;Download List&quot;, &quot;Edit the download List...&quot; to remove incorrect link.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="672"/>
        <source> Incorrect links found.  Please click &quot;Download List&quot;, &quot;Edit the download List...&quot; to remove incorrect links.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="690"/>
        <location filename="../newprg.cpp" line="758"/>
        <source>Video stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="690"/>
        <location filename="../newprg.cpp" line="758"/>
        <source>Video streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="691"/>
        <source>View download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="735"/>
        <source>Unable to open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="735"/>
        <source>with download links listed.
Check if the file has been deleted
or if the file permissions have changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="759"/>
        <source>Edit the download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="807"/>
        <location filename="../newprg.cpp" line="819"/>
        <source>Cannot delete the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="834"/>
        <location filename="../newprg.cpp" line="2434"/>
        <location filename="../paytv_create.cpp" line="154"/>
        <location filename="../paytv_edit.cpp" line="195"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="836"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="841"/>
        <source>The file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="841"/>
        <source>will be deleted and can not be restored.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="853"/>
        <source>Can&apos;t delete </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="968"/>
        <source>Please click on &quot;Tools&quot; and &quot;Update&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="973"/>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="976"/>
        <location filename="../setgetconfig.cpp" line="369"/>
        <source>Download a new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1018"/>
        <source>A graphical shell for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1020"/>
        <source> and </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1023"/>
        <source> streamCapture2 handles downloads of video streams.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1026"/>
        <source>Many thanks to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1026"/>
        <source> for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1061"/>
        <source>svtplay-dl is in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1066"/>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1072"/>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1076"/>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1086"/>
        <source>Path to svtplay-dl: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1090"/>
        <source>Path to svtplay-dl.exe: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1097"/>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1101"/>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1116"/>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1119"/>
        <source>Path to ffmpeg.exe: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1122"/>
        <source>Path to ffmpeg: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1163"/>
        <location filename="../newprg.cpp" line="1309"/>
        <source>To add downloads to the download list,
you need to create a list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1163"/>
        <location filename="../newprg.cpp" line="1309"/>
        <source>&quot;Downlod List&quot; and &quot;New download List...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1163"/>
        <location filename="../newprg.cpp" line="1309"/>
        <source> or import a list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1163"/>
        <location filename="../newprg.cpp" line="1309"/>
        <source>&quot;Downlod List&quot; and &quot;Import a download List...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1321"/>
        <source>There is no search to add.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1335"/>
        <source>Invalid link. Unable to add to download list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1626"/>
        <location filename="../newprg.cpp" line="1691"/>
        <location filename="../newprg.cpp" line="1915"/>
        <location filename="../save.cpp" line="35"/>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1628"/>
        <source>Copy streaming media to directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1649"/>
        <location filename="../newprg.cpp" line="1714"/>
        <source>You do not have the right to save to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1649"/>
        <source>and it cannot be used as the default folder to copy to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1714"/>
        <source>and it cannot be used as the default folder for downloads.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1819"/>
        <source>svtplay-dl stable cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1819"/>
        <location filename="../newprg.cpp" line="1852"/>
        <source>Please go to &quot;svtplay-dl&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1852"/>
        <source>svtplay-dl beta cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1878"/>
        <source>svtplay-dl cannot be found in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1917"/>
        <location filename="../newprg.cpp" line="1933"/>
        <source>Select svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1933"/>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1952"/>
        <source>svtplay-dl is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1955"/>
        <source>svtplay-dl.exe is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1984"/>
        <source>The selected svtplay-dl cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1984"/>
        <source>Please click &quot;svtplay-dl&quot; and &quot;Select svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="1984"/>
        <source>to select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2018"/>
        <source>This is a BETA version. This AppImage can&apos;t be updated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2018"/>
        <source>You can find more versions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2018"/>
        <source>here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2052"/>
        <source>Maintenance Tool cannot be found.
Only if you install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2054"/>
        <source>is it possible to update and uninstall the program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2093"/>
        <location filename="../newprg.cpp" line="2117"/>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2201"/>
        <source>Best quality is selected automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2202"/>
        <source>Best method is selected automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2208"/>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2268"/>
        <source>Can not find any video streams, please check the address.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2297"/>
        <source>The search is complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2300"/>
        <source>The search failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2308"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2329"/>
        <source>Click to copy to the search box.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2353"/>
        <source>The number of previous searches to be saved...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2355"/>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2371"/>
        <source>The number of searches to be saved: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2381"/>
        <source>Edit saved searches...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2384"/>
        <source>Click to edit all saved searches.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2389"/>
        <source>Edit saved searches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2397"/>
        <source>Remove all saved searches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2400"/>
        <source>Click to delete all saved searches.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2441"/>
        <source>All your saved settings will be deleted.
All lists of files to download will disappear.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2485"/>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2499"/>
        <source>was normally terminated. Exit code = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.cpp" line="2503"/>
        <source>crashed. Exit code = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="35"/>
        <source>Invalid link. No NFO information could be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="73"/>
        <source>NFO files contain release information about the media.
No NFO file was found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="167"/>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="171"/>
        <source>Episode title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="175"/>
        <source>Season</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="179"/>
        <source>Episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="183"/>
        <source>Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="189"/>
        <source>Published</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="203"/>
        <source>An unexpected error occurred while downloading the NFO file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../nfo.cpp" line="235"/>
        <source>NFO Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="68"/>
        <location filename="../paytv_edit.cpp" line="241"/>
        <location filename="../st_create.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="128"/>
        <source>Streaming service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="70"/>
        <location filename="../paytv_edit.cpp" line="243"/>
        <location filename="../st_create.cpp" line="32"/>
        <location filename="../st_edit.cpp" line="131"/>
        <source>Enter the name of your streaming service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="104"/>
        <location filename="../paytv_edit.cpp" line="148"/>
        <source>Enter your username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="160"/>
        <location filename="../paytv_edit.cpp" line="201"/>
        <source>Save password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_create.cpp" line="161"/>
        <location filename="../paytv_edit.cpp" line="202"/>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="26"/>
        <source>Manage Login details for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="27"/>
        <source>Edit, rename or delete
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="28"/>
        <location filename="../st_edit.cpp" line="30"/>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="29"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="30"/>
        <location filename="../st_edit.cpp" line="31"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="58"/>
        <source>Create New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../paytv_edit.cpp" line="68"/>
        <location filename="../setgetconfig.cpp" line="491"/>
        <source>No Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="29"/>
        <source>The mission failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qsystemtrayicon.cpp" line="32"/>
        <source>Mission accomplished!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="37"/>
        <source>Save a screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../screenshot.cpp" line="39"/>
        <source>Images (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="99"/>
        <source>Open the folder where streamCapture2 is located</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="103"/>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="107"/>
        <source>Read message from the developer (if there is any)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="112"/>
        <source>Force update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="139"/>
        <source>streamCapture2 is not allowed to open </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="139"/>
        <source> Use the file manager instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="153"/>
        <source>The folder where streamCapture2 is located</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="204"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="205"/>
        <source>Update this AppImage to the latest version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="361"/>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="366"/>
        <source>Please click on &quot;Tools&quot; and &quot;Maintenance Tool...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="419"/>
        <source>Up and running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="484"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="485"/>
        <source>Less then 720p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="486"/>
        <source>Less than or equal to 720p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="487"/>
        <source>Less than or equal to 1080p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="488"/>
        <source>More then 1080p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="493"/>
        <location filename="../sok.cpp" line="332"/>
        <source>Deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="555"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download an AppImage that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="564"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download a portable streamCapture2 that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or download and place ffmpeg.exe in the same folder as streamapture2.exe.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="567"/>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="620"/>
        <source> cannot be found or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../setgetconfig.cpp" line="720"/>
        <source>Could not save a file to store Recent Search list.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="66"/>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="117"/>
        <location filename="../shortcuts.cpp" line="119"/>
        <source>Download video streams.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../shortcuts.cpp" line="141"/>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="95"/>
        <source>svtplay-dl cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="105"/>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="106"/>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="240"/>
        <source>Unable to log in. Forgot your username and password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="247"/>
        <source>ERROR: No videos found. You need a token to access the website. see https://svtplay-dl.se/tv4play/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="254"/>
        <source>ERROR: That site is not supported. Please visit https://github.com/spaam/svtplay-dl/issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="261"/>
        <source>ERROR: No videos found. This mode is not supported anymore. Need the url with the video.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="268"/>
        <source>ERROR: No videos found. Can&apos;t find any videos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="275"/>
        <source>ERROR: No videos found. We can&apos;t download DRM protected content from this site.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="282"/>
        <source>ERROR: No videos found. Wrong url, need to be video url.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="351"/>
        <source>ERROR: No videos found. Can&apos;t find video info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="448"/>
        <source>ERROR: No videos found. Media doesn&apos;t have any associated videos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="457"/>
        <source>ERROR: No videos found. Can&apos;t find audio info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../sok.cpp" line="465"/>
        <source>ERROR: No videos found. Cant find video id.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="39"/>
        <location filename="../st_edit.cpp" line="144"/>
        <source>Enter &apos;st&apos; cookie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_create.cpp" line="41"/>
        <location filename="../st_edit.cpp" line="147"/>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="25"/>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="27"/>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="28"/>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="29"/>
        <source>Do not use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="202"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../st_edit.cpp" line="203"/>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../style.cpp" line="44"/>
        <location filename="../style.cpp" line="105"/>
        <location filename="../style.cpp" line="166"/>
        <source>Change style on next start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../style.cpp" line="53"/>
        <source>The program needs to be restarted to switch to Dark Fusion style.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../style.cpp" line="114"/>
        <source>The program needs to be restarted to switch to Fusion style.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../style.cpp" line="175"/>
        <source>The program needs to be restarted to switch to Default style.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="41"/>
        <source>Open your language file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../test_translation.cpp" line="44"/>
        <source>Compiled language file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_message.cpp" line="67"/>
        <location filename="../user_message.cpp" line="128"/>
        <source>Message from the developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../user_message.cpp" line="115"/>
        <source>There is no message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../zoom.cpp" line="31"/>
        <source>The font size changes to the selected font size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../cli.cpp" line="280"/>
        <source>An unexpected error occurred during the update process.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="282"/>
        <source>Preparing to download...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="302"/>
        <source>Current version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../cli.cpp" line="302"/>
        <source>Latest version:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <location filename="../newprg.ui" line="35"/>
        <source>TEST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="74"/>
        <source>Paste the link to the page where the video is displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="77"/>
        <location filename="../newprg.ui" line="1137"/>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="100"/>
        <source>https://</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="126"/>
        <location filename="../newprg.ui" line="1128"/>
        <source>Search for video streams.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="129"/>
        <location filename="../newprg.ui" line="1125"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="164"/>
        <source>Download the file you just searched for.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="170"/>
        <location filename="../newprg.ui" line="1152"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="202"/>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="208"/>
        <source>Add to Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="243"/>
        <source>Download all files you added to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="249"/>
        <location filename="../newprg.ui" line="1308"/>
        <source>Download all on the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="266"/>
        <source>If Dolby Vision 4K video streams are not found, the download will probably fail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="272"/>
        <location filename="../newprg.ui" line="1782"/>
        <source>Dolby Vision 4K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="378"/>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="381"/>
        <source>Quality (Bitrate)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="457"/>
        <source>Media streaming communications protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="472"/>
        <source>Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="527"/>
        <source>Select quality on the video you download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="536"/>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="572"/>
        <source>Select quality on the video you download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="612"/>
        <source>Video resolution: 480p=640x480, 720p=1280x720, 1080p=1920x1080</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="652"/>
        <source>Allows given quality to differ by an amount. 300 usually works well. (Bit rate +/- 300).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="695"/>
        <source>Select provider. If yoy need a password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="729"/>
        <location filename="../newprg.ui" line="1273"/>
        <location filename="../newprg.ui" line="1366"/>
        <source>If no saved password is found, click here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="732"/>
        <location filename="../newprg.ui" line="1270"/>
        <location filename="../newprg.ui" line="1363"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="814"/>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="828"/>
        <source>&amp;Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="872"/>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="876"/>
        <source>Check for updates at program start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="887"/>
        <source>Edit settings (Advanced)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="926"/>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="946"/>
        <source>&amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="951"/>
        <source>&amp;Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="964"/>
        <source>L&amp;ogin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="972"/>
        <source>&amp;All Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="980"/>
        <source>&apos;st&apos; &amp;cookies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="985"/>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="997"/>
        <source>TV&amp;4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1005"/>
        <source>&amp;svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1018"/>
        <location filename="../newprg.ui" line="1021"/>
        <source>The download may fail if no subtitles are found. You can try different menu options.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1024"/>
        <source>S&amp;ubtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1056"/>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1065"/>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1074"/>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1083"/>
        <source>Check for updates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1092"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1095"/>
        <source>Exits the program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1098"/>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1107"/>
        <source>About svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1116"/>
        <source>About FFmpeg...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1140"/>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1155"/>
        <source>Download the stream you just searched for.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1164"/>
        <source>License streamCapture2...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1173"/>
        <source>License svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1182"/>
        <source>License FFmpeg...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1187"/>
        <source>Recent files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1199"/>
        <source>Help...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1211"/>
        <source>View the download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1214"/>
        <source>Look at the list of all the streams to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1223"/>
        <source>Delete the download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1226"/>
        <source>All saved streams in the download list are deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1231"/>
        <source>Delete download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1240"/>
        <source>Italian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1249"/>
        <source>Version history...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1258"/>
        <source>Create new user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1261"/>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1281"/>
        <source>Explain what is going on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1293"/>
        <source>Add to download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1296"/>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1311"/>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1319"/>
        <source>Create folder &quot;method_quality_amount_resolution&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1322"/>
        <source>Create folder &quot;method_quality_amount&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1325"/>
        <source>Automatically creates a folder for each downloaded video stream. If you use &quot;Direct download of all...&quot; no folders are ever created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1334"/>
        <source>Edit the download List (Advanced) ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1337"/>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1345"/>
        <source>Show more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1348"/>
        <source>View more information from svtplay-dl. Appears in purple text color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1371"/>
        <source>Uninstall streamCapture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1374"/>
        <source>Uninstall and remove all components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1382"/>
        <source>Direct Download of all Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1385"/>
        <source>Trying to immediately download all episodes. Unable to create folders or select quality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1399"/>
        <source>Download after Date...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1408"/>
        <source>Stop all downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1411"/>
        <source>Trying to stop svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1419"/>
        <source>List all Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1422"/>
        <source>Looking for video streams for all episodes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1427"/>
        <location filename="../newprg.ui" line="1539"/>
        <source>Delete all settings and Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1430"/>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1438"/>
        <source>Copy to Selected Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1441"/>
        <source>Direct copy to the default copy location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1453"/>
        <source>Select Copy Location...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1456"/>
        <source>Save the location where the finished video file is copied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1465"/>
        <source>Select Default Download Location...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1468"/>
        <source>Save the location for direct download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1476"/>
        <source>Download to Default Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1479"/>
        <source>Direct download to the default location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1491"/>
        <source>Add all Episodes to Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1494"/>
        <source>Adds all episodes to the download list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1503"/>
        <source>Select font...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1512"/>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1524"/>
        <source>Maintenance Tool...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1527"/>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1542"/>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1550"/>
        <source>Don&apos;t show notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1553"/>
        <source>Do not show notifications when the download is complete.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1565"/>
        <source>Create a shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1576"/>
        <source>Desktop Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1579"/>
        <source>Create shortcut to streamCapture2 on desktop.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1590"/>
        <source>Applications menu Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1593"/>
        <source>Create shortcut to streamCapture2 in the operating system&apos;s program menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1598"/>
        <source>Load external language file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1601"/>
        <source>Useful when testing your own translation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1609"/>
        <source>Set new &apos;st&apos; cookie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1618"/>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1621"/>
        <source>Increase the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1624"/>
        <source>Ctrl++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1629"/>
        <source>Zoom Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1632"/>
        <source>Ctrl+0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1641"/>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1644"/>
        <source>Decrease the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1647"/>
        <source>Ctrl+-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1656"/>
        <source>Check for the latest svtplay-dl from bin.ceicer.com...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1665"/>
        <location filename="../newprg.ui" line="1668"/>
        <location filename="../newprg.ui" line="1671"/>
        <source>Download Microsoft runtime (required for svtplay-dl)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1674"/>
        <source>Download the runtime file from bin.ceicer.com.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1686"/>
        <source>License 7-Zip...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1697"/>
        <source>Check streamCapture2 for updates at start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1705"/>
        <source>Check for new versions of svtplay-dl at start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1714"/>
        <source>streamCapture2 settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1723"/>
        <source>download svtplay-dl settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1732"/>
        <source>NFO info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1735"/>
        <source>NFO files contain media release information. Available at svtplay.se, among other places.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1743"/>
        <source>Select file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1746"/>
        <source>You choose the name of the downloaded video file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1755"/>
        <source>About Qt...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1764"/>
        <source>Set TV4 Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1769"/>
        <source>How-to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1774"/>
        <source>How-to Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1785"/>
        <source>If Dolby Vision 4K video streams are not found, the download will fail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1796"/>
        <source>Download subtitles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1799"/>
        <location filename="../newprg.ui" line="1973"/>
        <source>The subtitle is saved in a text file (*.srt).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1807"/>
        <source>Download subtitles and try to merge with the video file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1810"/>
        <location filename="../newprg.ui" line="1962"/>
        <source>If embedding in the video file does not work, try using the text file with the subtitle.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1821"/>
        <location filename="../newprg.ui" line="1951"/>
        <source>No subtitles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1830"/>
        <source>If the download does not work...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1838"/>
        <source>Light Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1849"/>
        <source>Dark Fusion Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1861"/>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1872"/>
        <source>Use svtplay-dl from the system path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1875"/>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1886"/>
        <source>Use svtplay-dl stable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1889"/>
        <location filename="../newprg.ui" line="1900"/>
        <source>Note that the latest stable version may be newer than the latest beta version. Check version by clicking &quot;Help&quot; -&gt; &quot;About svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1897"/>
        <source>Use svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1909"/>
        <source>Select svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1912"/>
        <source>Select svtplay-dl that you have in your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1920"/>
        <source>Use the selected svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1923"/>
        <source>Use svtplay-dl that you selected in your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1932"/>
        <source>Download svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1935"/>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1943"/>
        <location filename="../newprg.ui" line="1981"/>
        <source>Download all subtitles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1959"/>
        <source>Merge subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1970"/>
        <source>Download subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1989"/>
        <source>Download the subtitles in their native format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="1997"/>
        <source>Don&apos;t Use Native Dialogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="2000"/>
        <source>Do not use the operating system&apos;s file dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="2008"/>
        <source>System Default Theme (Light or Dark)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="2016"/>
        <source>Fusion Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="2024"/>
        <source>Default Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="2033"/>
        <source>Export the download List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="2042"/>
        <source>Import a download List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../newprg.ui" line="2051"/>
        <source>New download List...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
