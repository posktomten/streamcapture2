<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>QMessageBox</name>
    <message>
        <source>&lt;h3&gt;About Qt&lt;/h3&gt;&lt;p&gt;This program uses Qt version %1.&lt;/p&gt;</source>
        <translation>&lt;h3&gt;Om Qt&lt;/h3&gt;&lt;p&gt;Detta program använder Qt version %1.&lt;/p&gt;</translation>
    </message>
    <message>
        <source>&lt;p&gt;Qt is a C++ toolkit for cross-platform application development.&lt;/p&gt;&lt;p&gt;Qt provides single-source portability across all major desktop operating systems. It is also available for embedded Linux and other embedded and mobile operating systems.&lt;/p&gt;&lt;p&gt;Qt is available under multiple licensing options designed to accommodate the needs of our various users.&lt;/p&gt;&lt;p&gt;Qt licensed under our commercial license agreement is appropriate for development of proprietary/commercial software where you do not want to share any source code with third parties or otherwise cannot comply with the terms of GNU (L)GPL.&lt;/p&gt;&lt;p&gt;Qt licensed under GNU (L)GPL is appropriate for the development of Qt&amp;nbsp;applications provided you can comply with the terms and conditions of the respective licenses.&lt;/p&gt;&lt;p&gt;Please see &lt;a href=&quot;http://%2/&quot;&gt;%2&lt;/a&gt; for an overview of Qt licensing.&lt;/p&gt;&lt;p&gt;Copyright (C) %1 The Qt Company Ltd and other contributors.&lt;/p&gt;&lt;p&gt;Qt and the Qt logo are trademarks of The Qt Company Ltd.&lt;/p&gt;&lt;p&gt;Qt is The Qt Company Ltd product developed as an open source project. See &lt;a href=&quot;http://%3/&quot;&gt;%3&lt;/a&gt; for more information.&lt;/p&gt;</source>
        <translation>&lt;p&gt;Qt är en C++-verktygslåda för plattformsoberoende applikationsutveckling.&lt;/p&gt;&lt;p&gt;Qt tillhandahåller portabilitet med alla större operativsystem. Qt är också tillgänglig för inbäddad Linux och andra inbäddade och mobila operativsystem.&lt;/p&gt;&lt;p&gt;Qt är tillgänglig under flera licensalternativ utformade för att tillgodose behoven hos våra olika användare.&lt;/p&gt;&lt;p&gt;Qt licensierad under våra kommersiella licensavtal är lämpligt för utveckling av proprietär/kommersiell programvara där du inte vill dela någon källkod eller inte kan följa villkoren för GNU (L)GPL.&lt;/p&gt;&lt;p&gt;Qt licensierad under GNU (L)GPL är lämplig för utveckling av Qt&amp;nbsp;applikationer förutsatt att du kan följa villkoren för respektive licenser.&lt;/p&gt;&lt;p&gt;Se &lt;a href=&quot;http://%2/&quot;&gt; %2&lt;/a&gt; för en översikt över Qt-licenser.&lt;/p&gt;&lt;p&gt;Copyright (C) %1 The Qt Company Ltd och andra bidragsgivare.&lt;/p&gt;&lt;p&gt;Qt och Qt-logotypen är varumärken som tillhör The Qt Company Ltd.&lt;/p&gt;&lt;p&gt;Qt är Qt Company Ltd-produkten utvecklad som ett projekt med öppen källkod. Se &lt;a href=&quot;http://%3/&quot;&gt;%3&lt;/a&gt; för mer information.&lt;/p&gt;</translation>
    </message>
</context>
</TS>
