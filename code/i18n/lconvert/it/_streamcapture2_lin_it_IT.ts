<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="it_IT">
<context>
    <name>Cli</name>
    <message>
        <source>Compiler:</source>
        <translation>Compilatore:</translation>
    </message>
    <message>
        <source>Verify authenticity.
SHA256 checksum is generated.
 &apos;sha256sum&apos; is required.</source>
        <translation>Verifica l&apos;autenticità.
Verrà generato il checksum SHA256.
 &apos;sha256sum&apos; è obbligatorio.</translation>
    </message>
    <message>
        <source>Reads the version number for openSSL included in the AppImage.</source>
        <translation>Lettura numero versione openSSL incluso nell&apos;AppImage.</translation>
    </message>
    <message>
        <source>The new AppImage is created with zsync.
Only the changes are downloaded.</source>
        <translation>La nuova AppImage verrà creata con zsync.
Vengono scaricate solo le modifiche.</translation>
    </message>
    <message>
        <source>Compiler: Clang version </source>
        <translation>Compilatore: versione Clang </translation>
    </message>
    <message>
        <source>Reads the version number for FFmpeg included in the AppImage.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trying to read your operating system&apos;s FFmpeg version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starts the GUI (Graphical User Interface).</source>
        <translation>Avvia GUI (Graphical User Interface).</translation>
    </message>
    <message>
        <source>Select language --lang en (English), --lang it (Italian) or --lang sv (Swedish)</source>
        <translation>Seleziona la lingua --lang en (inglese), --lang it (italiano) o --lang sv (svedese)</translation>
    </message>
    <message>
        <source>Runs on: </source>
        <translation>Esegui il: </translation>
    </message>
    <message>
        <source>Development tool: Qt</source>
        <translation>Strumento sviuluppo: Qt</translation>
    </message>
    <message>
        <source>Compiled with: </source>
        <translation>Compilato con: </translation>
    </message>
    <message>
        <source>Copyright (C) </source>
        <translation>Copyright (C) </translation>
    </message>
    <message>
        <source>Website: </source>
        <translation>Sito web: </translation>
    </message>
    <message>
        <source>Swedish website: </source>
        <translation>Sito web svedese: </translation>
    </message>
    <message>
        <source>GNU General Public License as published by</source>
        <translation>GNU General Public License come pubblicato dalla</translation>
    </message>
    <message>
        <source>the Free Software Foundation, version 3.</source>
        <translation>Free Software Foundation, versione 3.</translation>
    </message>
    <message>
        <source>Unable to load the language file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Expected checksum:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The checksums are identical.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is genuine.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> The checksums are not identical. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is not genuine</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Calculated checksum:</source>
        <translation>Checksum calcolata:</translation>
    </message>
    <message>
        <source>Please select &apos;--lang en&apos;, &apos;--lang it&apos; or &apos;--lang sv&apos;
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>openSSL is not found on your system.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FFmpeg not found. You can use FFmpeg which is included in the AppImage.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>FFmpeg not found. Install FFmpeg or download an AppImage that includes FFmpeg.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>GLIBC not found. GLIBC is required to boot Linux.
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click &apos;1&apos; to show more or &apos;Ctrl + C&apos; to exit. :&gt; </source>
        <translation>Fai clic su &quot;1&quot; per visualizzare più info o su premi &quot;Ctrl + C&quot; per uscire. :&gt; </translation>
    </message>
    <message>
        <source>Source code: </source>
        <translation>Codice sorgente: </translation>
    </message>
    <message>
        <source>E-mail: </source>
        <translation>Email: </translation>
    </message>
    <message>
        <source>This program was created </source>
        <translation>Questo programma è stato creato </translation>
    </message>
    <message>
        <source>Path: </source>
        <translation>Percorso: </translation>
    </message>
    <message>
        <source>Help about the AppImage.</source>
        <translation>Guida relativa AppImage.</translation>
    </message>
    <message>
        <source>Displays the version history.</source>
        <translation>Visualizza cronologia versioni.</translation>
    </message>
    <message>
        <source>Trying to read your operating system&apos;s openSSL version.</source>
        <translation>Lettura versione openSSL sistema operativo.</translation>
    </message>
    <message>
        <source>Trying to read your operating system&apos;s GLIBC version.</source>
        <translation>Lettura versione GLIBC sistema operativo.</translation>
    </message>
    <message>
        <source>Check for updates.</source>
        <translation>Controllo  aggiornamenti.</translation>
    </message>
    <message>
        <source>Displays the license.</source>
        <translation>Visualizza la licenza.</translation>
    </message>
    <message>
        <source> is free software: you can redistribute it</source>
        <translation> è un software libero: puoi ridistribuirlo</translation>
    </message>
    <message>
        <source>and/or modify it under the terms of the</source>
        <translation>e/o modificarlo secondo i termini della</translation>
    </message>
    <message>
        <source>This program is distributed in the hope that it will be useful, </source>
        <translation>Questo programma è distribuito nella speranza che possa essere utile, </translation>
    </message>
    <message>
        <source>but WITHOUT ANY WARRANTY; without even the implied warranty of </source>
        <translation>ma SENZA ALCUNA GARANZIA; senza nemmeno la garanzia implicita di </translation>
    </message>
    <message>
        <source>MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>COMMERCIABILITÀ o IDONEITÀ PER UNO SCOPO PARTICOLARE.</translation>
    </message>
    <message>
        <source>See the GNU General Public License for more details.</source>
        <translation>Per maggiori info, vedi la GNU General Public License.</translation>
    </message>
</context>
<context>
    <name>DownloadListDialog</name>
    <message>
        <source>Dialog</source>
        <translation>Finestra</translation>
    </message>
    <message>
        <source>Please zoom using the keyboard. &quot;Ctrl + +&quot;, &quot;Ctrl + -&quot; or &quot;Ctrl + 0&quot;.</source>
        <translation>Effettua lo zoom usando i tasti &quot;Ctrl + +&quot;, &quot;Ctrl + -&quot; o &quot;Ctrl + 0&quot;.</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation>Salva come...</translation>
    </message>
    <message>
        <source>Remove invalid list items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unexpected error has occurred.
The file with saved links to downloadable files cannot be found.
Check that the file has not been deleted or moved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Video streams)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit the download list (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Save as text file</source>
        <translation>Salva come file testo</translation>
    </message>
    <message>
        <source>Text file</source>
        <translation>File testo</translation>
    </message>
    <message>
        <source>Text file, optional extension</source>
        <translation>File testo, estensione facoltativa</translation>
    </message>
    <message>
        <source>Could not save the file.
Check your file permissions.</source>
        <translation>Impossibile salvare il file.
Verifica i permessi del file.</translation>
    </message>
    <message>
        <source>Unable to find file</source>
        <translation>Impossibile trovare il file</translation>
    </message>
    <message>
        <source>Unable to find</source>
        <translation>Impossibile trovare</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation>OK</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Enter your password</source>
        <translation>Inserisci password</translation>
    </message>
    <message>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Gli spazi non sono consentiti. Usa solo i caratteri consentiti dal provider.
La password non verrà salvata.</translation>
    </message>
    <message>
        <source>svtplay-dl crashed.</source>
        <translation>svtplay-dl è crashato.</translation>
    </message>
    <message>
        <source>Could not stop svtplay-dl.</source>
        <translation>Impossibile fermare svtplay-dl.</translation>
    </message>
    <message>
        <source>svtplay-dl stopped. Exit code </source>
        <translation>svtplay-dl fermato.
Codice uscita </translation>
    </message>
    <message>
        <source>Delete any files that may have already been downloaded.</source>
        <translation>Elimina tutti i file che potresti aver già scaricato.</translation>
    </message>
    <message>
        <source>Copy to: </source>
        <translation>Copia in: </translation>
    </message>
    <message>
        <source>Download to: </source>
        <translation>Download in: </translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation>Non hai selezionato alcun percorso dove copiare i file multimediali.
Prima di procedere seleziona un percorso.</translation>
    </message>
    <message>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation>Esiste già un file con lo stesso nome.
Il file non verrà copiato.</translation>
    </message>
    <message>
        <source>Copy succeeded</source>
        <translation>Copia completata</translation>
    </message>
    <message>
        <source>Copy failed</source>
        <translation>Copia fallita</translation>
    </message>
    <message>
        <source> cannot be found or is not an executable program.</source>
        <translation> non trovato o non è un programam eseguibile.</translation>
    </message>
    <message>
        <source>The request is processed...</source>
        <translation>Elaborazione richiesta...</translation>
    </message>
    <message>
        <source>Preparing to download...</source>
        <translation>Preparazione download...</translation>
    </message>
    <message>
        <source>The request is processed...
Preparing to download...</source>
        <translation>La richiesta è stata elaborata - preparazione download...</translation>
    </message>
    <message>
        <source>Download streaming media to folder</source>
        <translation>Download media streaming nella cartella</translation>
    </message>
    <message>
        <source>The video stream is saved in </source>
        <translation>Lo stream video è stato salvato in </translation>
    </message>
    <message>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Impossibile trovare la cartella predefinita per il download degli stream video.
Il download è stato annullato.</translation>
    </message>
    <message>
        <source>You do not have the right to save to the default folder.
Download is interrupted.</source>
        <translation>Non hai i diritti per salvare nella cartella predefinita.
Il download è stato interrotto.</translation>
    </message>
    <message>
        <source>The folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation>Impossibile trovare la cartella per il download dei flussi video.
Il download è interrotto.</translation>
    </message>
    <message>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation>Non hai diritti sufficienti per salvare in questa cartella.
Download annullato.</translation>
    </message>
    <message>
        <source>Less then 720p</source>
        <translation>Inferiore a 720p</translation>
    </message>
    <message>
        <source>Less than or equal to 720p</source>
        <translation>Inferiore o uguale a 720p</translation>
    </message>
    <message>
        <source>Less than or equal to 1080p</source>
        <translation>Inferiore o uguale a 1080p</translation>
    </message>
    <message>
        <source>You need to copy the TV4 token from your browser and save it in streamCapture2.</source>
        <translation>Devi copiare il token TV4 dal browser e salvarlo in streamCapture2.</translation>
    </message>
    <message>
        <source>Selected folder to copy to is </source>
        <translation>La cartella selezionata in cui copiare è </translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>svtplay-dl cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please click on &quot;svtplay-dl&quot; and select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter your password</source>
        <translation>Inserisci la password</translation>
    </message>
    <message>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation>Gli spazi non sono consentiti.
Usa solo caratteri approvati dal provider.</translation>
    </message>
    <message>
        <source>Starts downloading: </source>
        <translation>Avvio download: </translation>
    </message>
    <message>
        <source>Unable to find any streams with the selected video resolution.</source>
        <translation>Impossibile trovare stream con la risoluzione video selezionata.</translation>
    </message>
    <message>
        <source>Merge audio and video...</source>
        <translation>Unisci audio e video...</translation>
    </message>
    <message>
        <source>Removing old files, if there are any...</source>
        <translation>Rimozione vecchi file se presenti...</translation>
    </message>
    <message>
        <source>Download succeeded</source>
        <translation>Download completato</translation>
    </message>
    <message>
        <source>The download failed </source>
        <translation>Download fallito </translation>
    </message>
    <message>
        <source>Download completed</source>
        <translation>Download completato</translation>
    </message>
    <message>
        <source>The download failed.</source>
        <translation>Download fallito.</translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation>Salva come...</translation>
    </message>
    <message>
        <source>Select file name</source>
        <translation>Seleziona nome file</translation>
    </message>
    <message>
        <source>No folder is selected</source>
        <translation>Nessuna cartella selezionata</translation>
    </message>
    <message>
        <source>Searching...</source>
        <translation>Ricerca...</translation>
    </message>
    <message>
        <source>The request is processed...
Starting search...</source>
        <translation>Richiesta elaborata.
Avvio ricerca...</translation>
    </message>
    <message>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation>Cartella predefinita per copia stream video non trovata.
Download annullato.</translation>
    </message>
    <message>
        <source>You have chosen to download more than one file with the same name. In order not to overwrite files, folders will be created for each file.</source>
        <translation>Hai scelto di scaricare più di un file con lo stesso nome.
Per non sovrascrivere i file, verranno create delle cartelle per ogni file.</translation>
    </message>
    <message>
        <source>There is no file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select &quot;Download List&quot; and &quot;New download List...&quot;.
Or select &quot;Download List&quot; and &quot;Import a download List...&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The list of downloadable files is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The list of downloadable video streams is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid link found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid text string found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid text strings found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>
You tried: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The download failed.
The error may be due to your &apos;method&apos;, &apos;quality&apos; and/or &apos;deviation&apos; selections not working.
It usually works best if you let svtplay-dl choose automatically.
If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>streamCapture2 is done with the task.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid links found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The video streams are saved in</source>
        <translation>I flussi video saranno salvati in</translation>
    </message>
    <message>
        <source>An unexpected error has occurred.
The file with saved links to downloadable video streams is incorrect.
Check that all links are correct.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preparing to download</source>
        <translation>Pereparazione al download</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation>Sì</translation>
    </message>
    <message>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <source>Download anyway</source>
        <translation>Conferma download</translation>
    </message>
    <message>
        <source>You have chosen to create folders for each file and have the files copied.
Unfortunately, this is not possible as streamCapture2 does not know the file names.</source>
        <translation>Hai scelto di creare cartelle per ogni file e di copiare i file.
Questo non è possibile poiché streamCapture2 non conosce i nomi dei file.</translation>
    </message>
    <message>
        <source>Once svtplay-dl has started downloading all episodes, streamCapture2 no longer has control.
If you want to cancel, you may need to log out or restart your computer.
You can try to cancel using the command
&quot;sudo killall python3&quot;

Do you want to start the download?</source>
        <translation>Una volta che svtplay-dl ha iniziato a scaricare tutti gli episodi, streamCapture2 non ha più il controllo.
Se vuoi annullare il download, potrebbe essere necessario disconnettersi o riavviare il computer.
Puoi provare ad annullare il download usando il comando
&quot;sudo killall python3&quot;

Vuoi avviare il download?</translation>
    </message>
    <message>
        <source>Download all episodes to folder</source>
        <translation>Download di tutti gli episodi nella cartella</translation>
    </message>
    <message>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation>Non sono ammessi spazi.
Usa solo i caratteri approvati dal provider.
La password non verrà salvata.</translation>
    </message>
    <message>
        <source>Starts downloading all episodes: </source>
        <translation>Avvio download di tutti gli episdi: </translation>
    </message>
    <message>
        <source>The download failed. Did you forget to enter username and password?</source>
        <translation>Download fallito.
Hai dimenticato di inserire nome utente e password?</translation>
    </message>
    <message>
        <source>Episode</source>
        <translation>Episodio</translation>
    </message>
    <message>
        <source>of</source>
        <translation>di</translation>
    </message>
    <message>
        <source>The media files (and if you have selected the subtitles) have been downloaded.</source>
        <translation>Download file multimediali (e se li hai selezionati i dei sottotitoli) completato.</translation>
    </message>
    <message>
        <source>Restart Now</source>
        <translation>Riavvia ora</translation>
    </message>
    <message>
        <source>Change language on next start</source>
        <translation>Cambia lingua al prossimo avvio</translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to English.</source>
        <translation>Per passare alla GUI in Inglese il programma deve essere riavviato.</translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Swedish.</source>
        <translation>Per passare alla GUI in Svedese il programma deve essere riavviato.</translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Italian.</source>
        <translation>Per passare alla GUI in Italiano il programma deve essere riavviato.</translation>
    </message>
    <message>
        <source>The search field is empty!</source>
        <translation>Il campo ricerca è vuoto!</translation>
    </message>
    <message>
        <source> crashed.</source>
        <translation> crashato.</translation>
    </message>
    <message>
        <source>Can not find any video streams, please check the address.</source>
        <translation>Impossibile trovare uno stream video.
Verifica l&apos;indirizzo.</translation>
    </message>
    <message>
        <source>normal</source>
        <translation>normale</translation>
    </message>
    <message>
        <source>bold and italic</source>
        <translation>grassetto e corsivo</translation>
    </message>
    <message>
        <source>bold</source>
        <translation>grasetto</translation>
    </message>
    <message>
        <source>italic</source>
        <translation>corsivo</translation>
    </message>
    <message>
        <source>Current font:</source>
        <translation>Font attuale:</translation>
    </message>
    <message>
        <source>size:</source>
        <translation>dimensione:</translation>
    </message>
    <message>
        <source>View download list</source>
        <translation>Visualizza elenco download</translation>
    </message>
    <message>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation>Per aggiornare seleziona &quot;Strumenti&quot; -&gt; &quot;Aggiorna&quot;.</translation>
    </message>
    <message>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation>Selezionare &quot;Strumenti&quot;, &quot;Strumento manutenzione&quot; e &quot;Aggiorna componenti&quot;.</translation>
    </message>
    <message>
        <source>Download a new</source>
        <translation>Scarica un nuovo</translation>
    </message>
    <message>
        <source>svtplay-dl is in the system path.</source>
        <translation>svtplay-dl è nel percorso di sistema.</translation>
    </message>
    <message>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation>svtplay-dl.exe è nel percorso di sistema.</translation>
    </message>
    <message>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation>ERRORE! svtplay-dl non è nel percorso di sistema.</translation>
    </message>
    <message>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation>ERRORE! svtplay-dl.exe non è nel percorso di sistema.</translation>
    </message>
    <message>
        <source>Path to svtplay-dl: </source>
        <translation>Percorso svtplay.dl: </translation>
    </message>
    <message>
        <source>Path to svtplay-dl.exe: </source>
        <translation>Percorso svtplay-dl.exe: </translation>
    </message>
    <message>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation>ERRORE! svtplay-dl non trovato.</translation>
    </message>
    <message>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation>ERRORE! svtplay-dl.exe non trovato.</translation>
    </message>
    <message>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation>ERRORE! ffmpeg.exe non trovato.</translation>
    </message>
    <message>
        <source>Invalid link. Unable to add to download list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose</source>
        <translation>Scegli</translation>
    </message>
    <message>
        <source>Copy streaming media to directory</source>
        <translation>Copia media streaming nella cartella</translation>
    </message>
    <message>
        <source>svtplay-dl stable cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl beta cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl cannot be found in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The selected svtplay-dl cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please click &quot;svtplay-dl&quot; and &quot;Select svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>was normally terminated. Exit code = </source>
        <translation>era terminato normalmente.
Codice di uscita = </translation>
    </message>
    <message>
        <source>crashed. Exit code = </source>
        <translation>crashato.
Codice di uscita = </translation>
    </message>
    <message>
        <source>Download streaming media to directory</source>
        <translation>Download file media nella cartella</translation>
    </message>
    <message>
        <source>svtplay-dl is not found in the system path.
You can download any svtplay-dl of your choice.</source>
        <translation>svtplay-dl non è stato trovato nel percorso di sistema.
Puoi scaricare qualsiasi svtplay-dl a tua scelta.</translation>
    </message>
    <message>
        <source>Cannot find the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>with download links listed.
Check if the file has been removed or moved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot open the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>with download links listed.
Check if the file permissions have changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid list item found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid list items found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>with download links listed.
Check if the file has been deleted
or if the file permissions have changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit the download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot delete the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>will be deleted and can not be restored.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t delete </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A graphical shell for </source>
        <translation>streamCapture2 è una shell grafica per </translation>
    </message>
    <message>
        <source> and </source>
        <translation> e </translation>
    </message>
    <message>
        <source>Many thanks to </source>
        <translation>Molte grazie a </translation>
    </message>
    <message>
        <source> for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation> per la traduzioen italiana.
E per molte buone idee per rendere il programma migliore.</translation>
    </message>
    <message>
        <source>To add downloads to the download list,
you need to create a list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Downlod List&quot; and &quot;New download List...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> or import a list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Downlod List&quot; and &quot;Import a download List...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no search to add.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please go to &quot;svtplay-dl&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select svtplay-dl</source>
        <translation>Seelziona svtplay-dl</translation>
    </message>
    <message>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation>*.exe (svtplay-dl.exe)</translation>
    </message>
    <message>
        <source>svtplay-dl is not an executable program.</source>
        <translation>svtplay-dl non è un programma eseguibile.</translation>
    </message>
    <message>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation>Impossibile creare il collegamento sul desktop.
Verifica i tuoi permessi sui file.</translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <source>Can not find any video streams, please check the address.

</source>
        <translation>Impossibile trovare uno stream video.
Verifica l&apos;indirizzo.

</translation>
    </message>
    <message>
        <source>The search is complete</source>
        <translation>Ricerca completata</translation>
    </message>
    <message>
        <source>The search failed</source>
        <translation>Ricerca fallita</translation>
    </message>
    <message>
        <source>Click to copy to the search box.</source>
        <translation>Fai clic per copiare nel riquadro ricerca.</translation>
    </message>
    <message>
        <source>The number of previous searches to be saved...</source>
        <translation>Imposta numero precedenti ricerche salvate...</translation>
    </message>
    <message>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation>Specifica quante ricerche precedenti vuoi salvare.
Se il numero di ricerche supera il numero specificato, la ricerca più vecchia verrà eliminata.</translation>
    </message>
    <message>
        <source>The number of searches to be saved: </source>
        <translation>Numero precedenti ricerche da salvare: </translation>
    </message>
    <message>
        <source>Edit saved searches...</source>
        <translation>Modifica ricerche salvate...</translation>
    </message>
    <message>
        <source>Edit saved searches</source>
        <translation>Modifica ricerche salvate</translation>
    </message>
    <message>
        <source>Click to edit all saved searches.</source>
        <translation>Fai clic per modificare le ricerche salvate.</translation>
    </message>
    <message>
        <source>Change settings on next start</source>
        <translation>Modifica le impostazioni al prossimo avvio</translation>
    </message>
    <message>
        <source>The program must be restarted for the new settings to take effect.</source>
        <translation>Affinché le nuove impostazioni abbiano effettoil programma deve essere riavviato.</translation>
    </message>
    <message>
        <source>Whether notifications are displayed depends on the settings in your operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open downloaded file</source>
        <translation>Apri file scaricato</translation>
    </message>
    <message>
        <source>Video files</source>
        <translation>File video</translation>
    </message>
    <message>
        <source>File: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If the download does not work</source>
        <translation>Se il download non funziona</translation>
    </message>
    <message>
        <source>Enter TV4 token</source>
        <translation>Inserisci il token TV4</translation>
    </message>
    <message>
        <source>You must paste the exact TV4 token.</source>
        <translation>Devi incollare l&apos;esatto token TV4.</translation>
    </message>
    <message>
        <source>Edit streamCapture2 settings</source>
        <translation>Modifica impostazioni streamCapture2</translation>
    </message>
    <message>
        <source>Edit download svtplay-dl settings</source>
        <translation>Modifica impostazioni download svtplay-dl</translation>
    </message>
    <message>
        <source> Incorrect link found. Please click &quot;Download List&quot;, &quot;Edit the download List...&quot; to remove incorrect link.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Incorrect links found.  Please click &quot;Download List&quot;, &quot;Edit the download List...&quot; to remove incorrect links.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video stream</source>
        <translation>Stream video</translation>
    </message>
    <message>
        <source>Video streams</source>
        <translation>Stream video</translation>
    </message>
    <message>
        <source>Please click on &quot;Tools&quot; and &quot;Update&quot;</source>
        <translation>Seleziona &quot;Strumenti e &quot;Aggiorna&quot;</translation>
    </message>
    <message>
        <source> streamCapture2 handles downloads of video streams.</source>
        <translation> stremcapture2 gestisce i download degli stream video.</translation>
    </message>
    <message>
        <source>Path to ffmpeg.exe: </source>
        <translation>Percorso FFmpeg: </translation>
    </message>
    <message>
        <source>Path to ffmpeg: </source>
        <translation>Percorso FFmpeg: </translation>
    </message>
    <message>
        <source>You do not have the right to save to</source>
        <translation>Non hai diritti sufficienti per salvare in</translation>
    </message>
    <message>
        <source>and it cannot be used as the default folder to copy to.</source>
        <translation>e non può essere usata come cartella predefinita in cui copiare.</translation>
    </message>
    <message>
        <source>and it cannot be used as the default folder for downloads.</source>
        <translation>e non può essere usata come cartella predefinita per i download.</translation>
    </message>
    <message>
        <source>svtplay-dl.exe is not an executable program.</source>
        <translation>svtplay-dl.exe non è un programma eseguibile.</translation>
    </message>
    <message>
        <source>here</source>
        <translation>qui</translation>
    </message>
    <message>
        <source>You can find more versions</source>
        <translation>Puoi trovare altre versioni</translation>
    </message>
    <message>
        <source>This is a BETA version. This AppImage can&apos;t be updated.</source>
        <translation>Questa è una versione beta.
Questa AppImage non può essere aggiornata.</translation>
    </message>
    <message>
        <source>Maintenance Tool cannot be found.
Only if you install</source>
        <translation>Impossibile trovare lo strumento manutenzione.
Solo se installi</translation>
    </message>
    <message>
        <source>is it possible to update and uninstall the program.</source>
        <translation>è possibile aggiornare e disinstallare il programma.</translation>
    </message>
    <message>
        <source>Best quality is selected automatically</source>
        <translation>Verrà selezionata automaticamente la migliore qualità</translation>
    </message>
    <message>
        <source>Best method is selected automatically</source>
        <translation>Verrà selezionato automaticamente il metodo migliore</translation>
    </message>
    <message>
        <source>Select</source>
        <translation>Seleziona</translation>
    </message>
    <message>
        <source>Remove all saved searches</source>
        <translation>Rimuovi tutte le ricerche salvate</translation>
    </message>
    <message>
        <source>Click to delete all saved searches.</source>
        <translation>Fai clc per eliminare tuitte le ricerche salvate.</translation>
    </message>
    <message>
        <source>All your saved settings will be deleted.
All lists of files to download will disappear.
Do you want to continue?</source>
        <translation>Tutte le impostazioni salvate verranno eliminate.
Tutti gli elenchi di file da scaricare verranno eliminati.
Vuoi continuare?</translation>
    </message>
    <message>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation>Impossibile eliminare i file di configurazione.
Controlla i permessi dei file.</translation>
    </message>
    <message>
        <source>Streaming service</source>
        <translation>Servizio streaming</translation>
    </message>
    <message>
        <source>Enter the name of your streaming service.</source>
        <translation>Inserisci il nome del servizio streaming.</translation>
    </message>
    <message>
        <source>Enter your username</source>
        <translation>Inserisci nome utente</translation>
    </message>
    <message>
        <source>Save password?</source>
        <translation>Vuoi salvare la password?</translation>
    </message>
    <message>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation>Vuoi salvare la password (non sicuro)?</translation>
    </message>
    <message>
        <source>Manage Login details for </source>
        <translation>Gestione accessi per </translation>
    </message>
    <message>
        <source>Edit, rename or delete
</source>
        <translation>Modifica, rinomina o elimina
</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>Modifica</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Rinomina</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <source>Create New</source>
        <translation>Crea nuovo</translation>
    </message>
    <message>
        <source>No Password</source>
        <translation>Nessuna password</translation>
    </message>
    <message>
        <source>The mission failed!</source>
        <translation>Missione fallita!</translation>
    </message>
    <message>
        <source>Mission accomplished!</source>
        <translation>Missione compiuta!</translation>
    </message>
    <message>
        <source>Open the folder where streamCapture2 is located</source>
        <translation>Apri la cartella in cui si trova streamCapture2</translation>
    </message>
    <message>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation>Cattura una schermata (ritardo 5 secondi)</translation>
    </message>
    <message>
        <source>Read message from the developer (if there is any)</source>
        <translation>Leggi il messaggio (se presente) dello sviluppatore</translation>
    </message>
    <message>
        <source>Force update</source>
        <translation>Forza aggiornamento</translation>
    </message>
    <message>
        <source>streamCapture2 is not allowed to open </source>
        <translation>streamCapture2 non è autoizzato ad aprire </translation>
    </message>
    <message>
        <source> Use the file manager instead.</source>
        <translation> Usa invece il file manager.</translation>
    </message>
    <message>
        <source>Save a screenshot</source>
        <translation>Salva schermata</translation>
    </message>
    <message>
        <source>Images (*.png)</source>
        <translation>Immagini (*.png)</translation>
    </message>
    <message>
        <source>The folder where streamCapture2 is located</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update</source>
        <translation>Aggiorna</translation>
    </message>
    <message>
        <source>Update this AppImage to the latest version</source>
        <translation>Aggiorna questa applicazione alla versione più recente</translation>
    </message>
    <message>
        <source>Please click on &quot;Tools&quot; and &quot;Maintenance Tool...&quot;</source>
        <translation>Selezionare &quot;Strumenti&quot; e &quot;Aggiorna componenti...&quot;</translation>
    </message>
    <message>
        <source>Up and running</source>
        <translation>Attiva ed in esecuzione</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Risoluzione</translation>
    </message>
    <message>
        <source>More then 1080p</source>
        <translation>Superiore a 1080p</translation>
    </message>
    <message>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download an AppImage that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;Impossibile trovare FFmpeg o non è un programma eseguibile.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Scarica un&apos;AppImage che contiene FFmpeg.&lt;br&gt;&lt;br&gt;Oppure installa FFmpeg nel percorso di sistema.</translation>
    </message>
    <message>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download a portable streamCapture2 that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or download and place ffmpeg.exe in the same folder as streamapture2.exe.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;Impossibile trovare FFmpeg o non è un programma eseguibile.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Scarica streamCapture2 portatile che contiene FFmpeg.&lt;br&gt;&lt;br&gt;Oppure scarica e salva ffmpeg.exe nella stessa cartella di streamapture2.exe.&lt;br&gt;&lt;br&gt;Oppure installa FFmpeg nel percorso di sistema.</translation>
    </message>
    <message>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation>&lt;b&gt;Impossibile trovare FFmpeg o non è un programma eseguibile.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Per installare ffmpeg seleziona &quot;Strumenti&quot;, &quot;Strumento manutenzione&quot;.&lt;br&gt;&lt;br&gt;Oppure installa FFmpeg nel percorso di sistema .</translation>
    </message>
    <message>
        <source>Could not save a file to store Recent Search list.
Check your file permissions.</source>
        <translation>Impossibile salvare un file per memorizzare l&apos;elenco delle ricerche recenti.
Controlla i permessi dei file.</translation>
    </message>
    <message>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation>Errore! Impossibile creare il collegamento in
&quot;~ / .local / share / applications&quot;
Controlla i tuoi permessi sui file.</translation>
    </message>
    <message>
        <source>Download video streams.</source>
        <translation>Download stream video.</translation>
    </message>
    <message>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation>Errore! Impossibile creare il collegamento.
Verifica i tuoi permessi sui file.</translation>
    </message>
    <message>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation>L&apos;informazione da svtplay-dl può o non può contenere:</translation>
    </message>
    <message>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation>Metodo, qualità, codec, risoluzione, lingua e ruolo</translation>
    </message>
    <message>
        <source>Unable to log in. Forgot your username and password?</source>
        <translation>Impossibile accedere.
Hai dimenticato il nome utente e la password?</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find any videos.</source>
        <translation>ERRORE: nessun video trovato. Impossibile trovare un video.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Wrong url, need to be video url.</source>
        <translation>ERRORE: nessun video trovato. URL errata, deve essere l&apos;URL del video.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find video info.</source>
        <translation>ERRORE: nessun video trovato.
Impossibile trovare informazioni sul video.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Media doesn&apos;t have any associated videos.</source>
        <translation>ERRORE: nessun video trovato.
I media non hanno video associati.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find audio info.</source>
        <translation>ERRORE: nessun video trovato.
Impossibile trovare informazioni sull&apos;audio.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Cant find video id.</source>
        <translation>ERRORE: nessun video trovato.
Impossibile trovare l&apos;ID del video.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. You need a token to access the website. see https://svtplay-dl.se/tv4play/</source>
        <translation>ERRORE: nessun video trovato.
Per accedere al sito web hai bisogno di un token.
Per maggiori dettagli vedi https://svtplay-dl.se/tv4play/</translation>
    </message>
    <message>
        <source>svtplay-dl cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: That site is not supported. Please visit https://github.com/spaam/svtplay-dl/issues</source>
        <translation>ERRORE: quel sito non è supportato.
Per maggiori dettagli visita https://github.com/spaam/svtplay-dl/issues</translation>
    </message>
    <message>
        <source>ERROR: No videos found. This mode is not supported anymore. Need the url with the video.</source>
        <translation>ERRORE: nessun video trovato.
Questa modalità non è più supportata.
Hai bisogno dell&apos;URL con il video.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. We can&apos;t download DRM protected content from this site.</source>
        <translation>ERRORE: nessun video trovato.
Non è possibile scaricare da questo sito contenuti protetti da DRM.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find video id for the video.</source>
        <translation>ERRORE: nessun video trovato.
Impossibile trovare l&apos;ID video per il video.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find video id.</source>
        <translation>ERRORE: nessun video trovato.
Impossibile trovare l&apos;ID del video.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Use the video page not the series page.</source>
        <translation>ERRORE: nessun video trovato.
Usa la pagina del video e non la pagina della serie.</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find any videos. Is it removed?</source>
        <translation>ERRORE: nessun video trovato.
Impossibile trovare il video. È stato rimosso?</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find the video file.</source>
        <translation>ERRORE: nessun video trovato.
Impossibile trovare il file video.</translation>
    </message>
    <message>
        <source>WARNING: --all-episodes not implemented for this service.</source>
        <translation>ATTENZIONE: --all-episodes non implementato per questo servizio.</translation>
    </message>
    <message>
        <source>WARNING: Use program page instead of the clip / video page.</source>
        <translation>ATTENZIONE: usa la pagina del programma invece della pagina clip/video.</translation>
    </message>
    <message>
        <source>ERROR: Can&apos;t find any videos.</source>
        <translation>ERRORE: impossibile trovare il video.</translation>
    </message>
    <message>
        <source>Deviation</source>
        <translation>Deviazione</translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t decode api request.</source>
        <translation>ERRORE: nessun video trovato. Impossibile decodificare la richiesta API.</translation>
    </message>
    <message>
        <source>Enter &apos;st&apos; cookie</source>
        <translation>Inserisci cookie &apos;st&apos;</translation>
    </message>
    <message>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation>Inserisci il cookie &quot;st&quot; che il provider di streaming video ha salvato nel browser.</translation>
    </message>
    <message>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation>Gestione cookie &apos;st&apos; per </translation>
    </message>
    <message>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation>Usa, non usare, modifica o elimina
cookie &apos;st&apos; per
</translation>
    </message>
    <message>
        <source>Use</source>
        <translation>Usa</translation>
    </message>
    <message>
        <source>Do not use</source>
        <translation>Non usare</translation>
    </message>
    <message>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Imposta nuovo cookie &apos;st&apos;</translation>
    </message>
    <message>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation>Incolla e salva il cookie &apos;st&apos; che il provider dello streaming ha scaricato nel browser.</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>Apri</translation>
    </message>
    <message>
        <source>Open your language file</source>
        <translation>Apri il file lingua</translation>
    </message>
    <message>
        <source>Compiled language file</source>
        <translation>Compila file lingua</translation>
    </message>
    <message>
        <source>The font size changes to the selected font size</source>
        <translation>Dimensione tipo di carattere modificata come da dimensione selezionata</translation>
    </message>
    <message>
        <source>Stable:</source>
        <translation>Stabile:</translation>
    </message>
    <message>
        <source>You have downloaded</source>
        <translation>Hai scaricato</translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation>Niente</translation>
    </message>
    <message>
        <source>To folder &quot;stable&quot;: Nothing.</source>
        <translation>Nella cartella &quot;stable&quot;: niente.</translation>
    </message>
    <message>
        <source>To folder &quot;stable&quot;: </source>
        <translation>Nella cartella &quot;stable&quot;: </translation>
    </message>
    <message>
        <source>To folder &quot;beta&quot;: Nothing.</source>
        <translation>Nella cartella &quot;beta&quot;: niente.</translation>
    </message>
    <message>
        <source>To folder &quot;beta&quot;: </source>
        <translation>Nella cartella &quot;beta&quot;: </translation>
    </message>
    <message>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl beta can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Si è verificato un errore imprevisto.&lt;br&gt;Informazioni : impossibile trovare svtplay-dl beta .&lt;br&gt;Verifica la tua connessione Internet.</translation>
    </message>
    <message>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl stable can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation>Si è verificato un errore imprevisto.&lt;br&gt;Impossibile trovare  svtplay-dl stable.&lt;br&gt;Controlla la tua connessione Internet.</translation>
    </message>
    <message>
        <source>An unexpected error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Information about svtplay-dl stable can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You are not using svtplay-dl.</source>
        <translation>Non stai usando svtplay-dl.</translation>
    </message>
    <message>
        <source>Beta:</source>
        <translation>Beta:</translation>
    </message>
    <message>
        <source>The latest svtplay-dl available for&lt;br&gt;download from bin.ceicer.com are</source>
        <translation>La versione aggiornata di svtplay-dl disponibile&lt;br&gt;per il download da bin.ceicer.com è</translation>
    </message>
    <message>
        <source>&quot;Use svtplay-dl beta&quot;, but it can not be found.</source>
        <translation>&quot;Usa svtplay-dl beta&quot;, ma non è stata trovata.</translation>
    </message>
    <message>
        <source>&quot;Use the selected svtplay-dl&quot;</source>
        <translation>&quot;Usa svtplay-dl selezionato&quot;</translation>
    </message>
    <message>
        <source>&lt;b&gt;Welcome to streamCapture2!&lt;/b&gt;</source>
        <translation>&lt;b&gt;Benvenuto in streamCapture2!&lt;/b&gt;</translation>
    </message>
    <message>
        <source>&lt;b&gt;svtplay-dl stable&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>Impossibile trovare &lt;b&gt;svtplay-dl stabile&lt;/b&gt;.&lt;br&gt;Hai inserito un percorso errato o non è stato specificato alcun percorso.&lt;br&gt;Oppure i file sono stati spostati o eliminati.</translation>
    </message>
    <message>
        <source>Click &lt;i&gt;Download&lt;/i&gt; and &lt;i&gt;Latest stable svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click &lt;i&gt;Download&lt;/i&gt; and &lt;i&gt;Latest svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click &lt;i&gt;Download&lt;/i&gt;, then click&lt;br&gt;&lt;i&gt;Latest stable svtplay-dl </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;/i&gt;and&lt;br&gt;&lt;i&gt;Latest svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Latest stable svtplay-dl&quot;&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Latest svtplay-dl beta&quot;&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;svtplay-dl beta&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation>Impossibile trovare &lt;b&gt;svtplay-dl beta&lt;/b&gt;.&lt;br&gt;Hai inserito un percorso errato o non è stato specificato alcun percorso.&lt;br&gt;Oppure i file sono stati spostati o eliminati.</translation>
    </message>
    <message>
        <source>You are using version:</source>
        <translation>Versione in uso:</translation>
    </message>
    <message>
        <source>&quot;Use svtplay-dl stable&quot;</source>
        <translation>&quot;Use svtplay-dl stabile&quot;</translation>
    </message>
    <message>
        <source>&quot;Use svtplay-dl stable&quot;, but it can not be found.</source>
        <translation>&quot;Usa svtplay-dl stabile&quot;, ma non è stata trovata.</translation>
    </message>
    <message>
        <source>&quot;Use svtplay-dl beta&quot;</source>
        <translation>&quot;Usa svtplay-dl beta&quot;</translation>
    </message>
    <message>
        <source>&lt;b&gt;svtplay-dl stable is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;È disponibile per il download svtplay-dl stabile.&lt;/b&gt;&lt;br&gt;Versione: </translation>
    </message>
    <message>
        <source>&lt;b&gt;svtplay-dl beta is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation>&lt;b&gt;È disponibile per il download svtplay-dl beta.&lt;/b&gt;&lt;br&gt;Versione: </translation>
    </message>
    <message>
        <source>You have selected</source>
        <translation>Hai selezionato</translation>
    </message>
    <message>
        <source>svtplay-dl is not found in the specified path.</source>
        <translation>svtplay-dl non è stato trovato nel percorso specificato.</translation>
    </message>
    <message>
        <source>&quot;Use the selected svtplay-dl&quot;, but it can not be found.</source>
        <translation>&quot;Usa svtplay-dl selezionato&quot;, ma il file non è stato trovato.</translation>
    </message>
    <message>
        <source>Only if svtplay-dl does not work do you need to install
&quot;Microsoft Visual C++ Redistributable&quot;.
Download and double-click to install.</source>
        <translation>Solo se svtplay-dl non funziona è necessario installare
&quot;Microsoft Visual C++ ridistribuibile&quot;.
Scaricalo e fai doppio clic per installare.</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <source>Invalid link. No NFO information could be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>NFO files contain release information about the media.
No NFO file was found.</source>
        <translation>I file NFO contengono informazioni sulla versione del media.
Nessun file NFO trovato.</translation>
    </message>
    <message>
        <source>Title</source>
        <translation>Titolo</translation>
    </message>
    <message>
        <source>Episode title</source>
        <translation>Titolo episodio</translation>
    </message>
    <message>
        <source>Season</source>
        <translation>Stagione</translation>
    </message>
    <message>
        <source>Plot</source>
        <translation>Contenuto</translation>
    </message>
    <message>
        <source>Published</source>
        <translation>Pubblicato</translation>
    </message>
    <message>
        <source>An unexpected error occurred while downloading the NFO file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>NFO Info</source>
        <translation>Info NFO</translation>
    </message>
    <message>
        <source>To the website</source>
        <translation>Al sito web</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>Disinstalla</translation>
    </message>
    <message>
        <source>Download the new version</source>
        <translation>Download nuova versione</translation>
    </message>
    <message>
        <source>Download the latest version</source>
        <translation>Download versione più recente</translation>
    </message>
    <message>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation>Si è verificato un errore imprevisto.&lt;br&gt;</translation>
    </message>
    <message>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation>&lt;br&gt;non è stato trovato o non è un programma eseguibile.</translation>
    </message>
    <message>
        <source>Version history</source>
        <translation>Cronologia versioni</translation>
    </message>
    <message>
        <source>License streamCapture2</source>
        <translation>Licenza streamCapture2</translation>
    </message>
    <message>
        <source>License svtplay-dl</source>
        <translation>Licenza svtplay-dl</translation>
    </message>
    <message>
        <source>License FFmpeg</source>
        <translation>Licenza FFmpeg</translation>
    </message>
    <message>
        <source>License 7zip</source>
        <translation>Licenza 7zip</translation>
    </message>
    <message>
        <source> could not be found. Please download a portable streamCapture2 where FFmpeg is included.</source>
        <translation> non è stato trovato.
Scarica streamCapture2 portatile in cui è incluso FFmpeg.</translation>
    </message>
    <message>
        <source> could not be found. Please go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install FFmpeg.</source>
        <translation> non è stato trovato.
Per installare FFmpeg seleziona &quot;Strumenti&quot;, &quot;Strumento manutenzione&quot;.</translation>
    </message>
    <message>
        <source>Or install </source>
        <translation>Oppure installa </translation>
    </message>
    <message>
        <source> in your system.</source>
        <translation> nel sistema.</translation>
    </message>
    <message>
        <source> could not be found. Please download an AppImage where FFmpeg is included.</source>
        <translation> non è stato trovato.
Scarica un&amp;apos;AppImage in cui è incluso FFmpeg.</translation>
    </message>
    <message>
        <source>svtplay-dl is not found in the system path.</source>
        <translation>svtplay-dl non trovato nel path di sistema.</translation>
    </message>
    <message>
        <source> svtplay-dl.exe  cannot be found. Go to &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation>svtplay-dl.exe non trovato nel percorso di sistema.</translation>
    </message>
    <message>
        <source>About svtplay-dl (In the system path)</source>
        <translation>Info su svtplay-dl (nel percorso di sistema)</translation>
    </message>
    <message>
        <source>About </source>
        <translation>Info su </translation>
    </message>
    <message>
        <source> You can download svtplay-dl from bin.ceicer.com. Select &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please click &quot;svtplay-dl&quot; and select svtplay-dl.</source>
        <translation>Fai clic su &quot;svtplay-dl&quot; e seleziona svtplay-dl.</translation>
    </message>
    <message>
        <source> svtplay-dl cannot be found. Go to &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>version </source>
        <translation>ersione </translation>
    </message>
    <message>
        <source>Message from the developer</source>
        <translation>Messaggio dallo sviluppatore</translation>
    </message>
    <message>
        <source>There is no message.</source>
        <translation>Non c&apos;è nessun messaggio.</translation>
    </message>
    <message>
        <source>ERROR: Click on &quot;Tools&quot;, &quot;Show more&quot; and run again to get more information.</source>
        <translation>ERRORE: per ottenere maggiori informazioni seleziona &apos;Strumenti&apos;, &apos;Visualizza altro&apos; ed esegui nuovamente.</translation>
    </message>
    <message>
        <source>ERROR: If the error persists, you can report it at https://github.com/spaam/svtplay-dl/issues</source>
        <translation>ERRORE: Se l&apos;errore persiste, puoi segnalarlo su https://github.com/spaam/svtplay-dl/issues</translation>
    </message>
    <message>
        <source>ERROR: Include URL and error message in problem description.</source>
        <translation>ERRORE: nella descrizione del problema includi l&apos;URL e il messaggio di errore.</translation>
    </message>
    <message>
        <source>Change style on next start</source>
        <translation>Modifica lo stile al prossimo avvio</translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Dark Fusion style.</source>
        <translation>Per passare allo stile Fusion scuro il programma deve essere riavviato.</translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Fusion style.</source>
        <translation>Per passare allo stile Fusion il programma deve essere riavviato.</translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Default style.</source>
        <translation>Per passare allo stile predefinito scuro il programma deve essere riavviato.</translation>
    </message>
    <message>
        <source> is not allowed to save </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Check your file permissions or choose another location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <source>New downloadlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download list files (*.lst)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import a download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is not allowed to import </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>In order to export a Download list file, you must first create or import such a file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>An unexpected error occurred during the update process.</source>
        <translation>Durante il processo di aggiornamento si è verificato un errore imprevisto.</translation>
    </message>
    <message>
        <source>Preparing to download...</source>
        <translation>Preparazione download...</translation>
    </message>
    <message>
        <source>Current version:</source>
        <translation>Versione attuale:</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation>Versione aggironata:</translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <source>TEST</source>
        <translation>TEST</translation>
    </message>
    <message>
        <source>Paste the link to the page where the video is displayed</source>
        <translation>Incolla il collegamento alla pagina dove viene visualizzato il video</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>Incolla</translation>
    </message>
    <message>
        <source>Search for video streams.</source>
        <translation>Cerca stream video.</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation>Aggiungi video attuale all&apos;elenco di file che verranno scaricati.</translation>
    </message>
    <message>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation>Il numero di bit trasmessi o elaborati per unità di tempo.
Numeri più alti indicano una qualità migliore ma creano file più grandi. .</translation>
    </message>
    <message>
        <source>Quality (Bitrate)</source>
        <translation>Qualità (bitrate)</translation>
    </message>
    <message>
        <source>Media streaming communications protocol.</source>
        <translation>Protocollo comunicazione streaming media.</translation>
    </message>
    <message>
        <source>Method</source>
        <translation>Metodo</translation>
    </message>
    <message>
        <source>Download the file you just searched for.</source>
        <translation>Scarica il file che hai appena cercato.</translation>
    </message>
    <message>
        <source>Download all files you added to the list.</source>
        <translation>Scarica tutti i file aggiunti all&apos;elenco.</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <source>Select quality on the video you download</source>
        <translation>Seleziona la qualità del video da scaricare</translation>
    </message>
    <message>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation>Qualità (bitrate) e metodo.
Un bitrate più alto offre una qualità migliore ma crea un file più grande.</translation>
    </message>
    <message>
        <source>Select quality on the video you download.</source>
        <translation>Seleziona la qualità del video da scaricare.</translation>
    </message>
    <message>
        <source>Select provider. If yoy need a password.</source>
        <translation>Seleziona provider.
Se hai bisogno di una password..</translation>
    </message>
    <message>
        <source>If no saved password is found, click here.</source>
        <translation>Se non viene trovata alcuna password salvata, fai clic qui.</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>Password</translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <source>&amp;Language</source>
        <translation>&amp;Lingua</translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation>S&amp;trumenti</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>&amp;Guida</translation>
    </message>
    <message>
        <source>&amp;Recent</source>
        <translation>&amp;Recenti</translation>
    </message>
    <message>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation>Svedese</translation>
    </message>
    <message>
        <source>Check for updates at program start</source>
        <translation>Controlla aggiornamenti ad avvio programma</translation>
    </message>
    <message>
        <source>The download may fail if no subtitles are found. You can try different menu options.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About...</source>
        <translation>Info su streamCapture2...</translation>
    </message>
    <message>
        <source>Check for updates...</source>
        <translation>Controllo aggiornamenti...</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Esci</translation>
    </message>
    <message>
        <source>Exits the program.</source>
        <translation>Esci dal programma.</translation>
    </message>
    <message>
        <source>F4</source>
        <translation>F4</translation>
    </message>
    <message>
        <source>About svtplay-dl...</source>
        <translation>Info su svtplay-dl...</translation>
    </message>
    <message>
        <source>About FFmpeg...</source>
        <translation>Info su FFmpeg...</translation>
    </message>
    <message>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation>Incolla il collegamento alla pagina dove viene visualizzato il video.</translation>
    </message>
    <message>
        <source>Download the stream you just searched for.</source>
        <translation>Scarica lo stream che hai appena cercato.</translation>
    </message>
    <message>
        <source>License streamCapture2...</source>
        <translation>Licenza streamCapture2...</translation>
    </message>
    <message>
        <source>License svtplay-dl...</source>
        <translation>Licenza svtplay-dl...</translation>
    </message>
    <message>
        <source>License FFmpeg...</source>
        <translation>Licenza FFmpeg...</translation>
    </message>
    <message>
        <source>Recent files</source>
        <translation>File recenti</translation>
    </message>
    <message>
        <source>Help...</source>
        <translation>Guida in linea...</translation>
    </message>
    <message>
        <source>View the download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Look at the list of all the streams to download.</source>
        <translation>Visualizza l&apos;elenco di tutti gli stream da scaricare.</translation>
    </message>
    <message>
        <source>Delete the download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete download list</source>
        <translation>Elimina elenco download</translation>
    </message>
    <message>
        <source>All saved streams in the download list are deleted.</source>
        <translation>Tutti gli stream salvati nell&apos;elenco download sono stati eliminati.</translation>
    </message>
    <message>
        <source>Allows given quality to differ by an amount. 300 usually works well. (Bit rate +/- 300).</source>
        <translation>Consenti ad una determinata qualità di differire di un valore.
300 di solito funziona bene. (Bitrate +/- 300).</translation>
    </message>
    <message>
        <source>&amp;Download List</source>
        <translation>Elenco &amp;download</translation>
    </message>
    <message>
        <source>L&amp;ogin</source>
        <translation>&amp;Accedi</translation>
    </message>
    <message>
        <source>&amp;All Episodes</source>
        <translation>Tutti &amp;gli episodi</translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <source>Version history...</source>
        <translation>Cronologia versioni...</translation>
    </message>
    <message>
        <source>Create new user</source>
        <translation>Crea nuovo utente</translation>
    </message>
    <message>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation>Salva il nome di un provider di streaming video, il nome utente e, se lo desideri, la password.</translation>
    </message>
    <message>
        <source>Explain what is going on</source>
        <translation>Descrivi cosa sta succedendo</translation>
    </message>
    <message>
        <source>Add to Download List</source>
        <translation>Aggiungi ad elenco download</translation>
    </message>
    <message>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <translation>Aggiungi il video attuale all&apos;elenco degli stream che verranno scaricati.</translation>
    </message>
    <message>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <translation>Scarica tutti gli stream nell&apos;elenco.
Se si tratta dello stesso stream video in qualità diverse, verranno engono create automaticamente per ogni flusso video.</translation>
    </message>
    <message>
        <source>Create folder &quot;method_quality_amount&quot;</source>
        <translation>Crea cartella &quot;metodo_qualità_quantità&quot;</translation>
    </message>
    <message>
        <source>Looking for video streams for all episodes.</source>
        <translation>Ricerca stream video per tutti gli episodi.</translation>
    </message>
    <message>
        <source>Direct copy to the default copy location.</source>
        <translation>Copia diretta nel percorso copia predefinito.</translation>
    </message>
    <message>
        <source>Adds all episodes to the download list.</source>
        <translation>Aggiungi tutti gli episodi all&apos;elenco download.</translation>
    </message>
    <message>
        <source>Use svtplay-dl beta</source>
        <translation>Usa svtplay-dl beta</translation>
    </message>
    <message>
        <source>Use svtplay-dl stable</source>
        <translation>Usa svtplay-dl stabile</translation>
    </message>
    <message>
        <source>Select svtplay-dl that you have in your computer.</source>
        <translation>Seleziona svtplay-dl che hai nel computer.</translation>
    </message>
    <message>
        <source>Use svtplay-dl that you selected in your computer.</source>
        <translation>Usa svtplay-dl che hai selezionato nel computer.</translation>
    </message>
    <message>
        <source>Useful when testing your own translation.</source>
        <translation>Utile quando si testa una traduzione.</translation>
    </message>
    <message>
        <source>Edit settings (Advanced)</source>
        <translation>Modifica impostazioni (avanzate)</translation>
    </message>
    <message>
        <source>Video resolution: 480p=640x480, 720p=1280x720, 1080p=1920x1080</source>
        <translation>Risoluzione video: 480p=640x480, 720p=1280x720, 1080p=1920x1080</translation>
    </message>
    <message>
        <source>Automatically creates a folder for each downloaded video stream. If you use &quot;Direct download of all...&quot; no folders are ever created.</source>
        <translation>Crea automaticamente una cartella per ogni flusso video scaricato.
Se usi &quot;Download diretto di tutti...&quot; non verranno mai create le cartelle.</translation>
    </message>
    <message>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation>Cambia metodo o qualità.
Rimuovi un download dall&apos;elenco.
NOTA! Se cambi in modo errato, non funzionerà.</translation>
    </message>
    <message>
        <source>Show more</source>
        <translation>Visualizza altro</translation>
    </message>
    <message>
        <source>Uninstall streamCapture</source>
        <translation>Disinstalla streamCapture2</translation>
    </message>
    <message>
        <source>Uninstall and remove all components</source>
        <translation>Disinstalla e rimuovi tutti i componenti</translation>
    </message>
    <message>
        <source>Download after Date...</source>
        <translation>Download dopo una data...</translation>
    </message>
    <message>
        <source>Stop all downloads</source>
        <translation>Stop di tutti i download</translation>
    </message>
    <message>
        <source>Trying to stop svtplay-dl.</source>
        <translation>Prova a fermare svtplay-dl.</translation>
    </message>
    <message>
        <source>Delete all settings and Exit</source>
        <translation>Elimina tutte le impostazioni ed esci</translation>
    </message>
    <message>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation>Tutte le ricerche salvate e l&apos;elenco degli stream da scaricare verranno eliminati.</translation>
    </message>
    <message>
        <source>Copy to Selected Location</source>
        <translation>Copia nel percorso selezionato</translation>
    </message>
    <message>
        <source>Select Copy Location...</source>
        <translation>Seleziona percorso in cui copiare...</translation>
    </message>
    <message>
        <source>Select Default Download Location...</source>
        <translation>Seleziona percorso predefinito download...</translation>
    </message>
    <message>
        <source>Save the location for direct download.</source>
        <translation>Salva impostazione percorso download diretto.</translation>
    </message>
    <message>
        <source>Download to Default Location</source>
        <translation>Download nel percorso predefinito</translation>
    </message>
    <message>
        <source>Direct download to the default location.</source>
        <translation>Download diretto nel percorso predefinito.</translation>
    </message>
    <message>
        <source>Direct Download of all Episodes</source>
        <translation>Download diretto di tutti gli episodi</translation>
    </message>
    <message>
        <source>https://</source>
        <translation>https://</translation>
    </message>
    <message>
        <source>Download all on the list</source>
        <translation>Scarica tutti i file nell’elenco</translation>
    </message>
    <message>
        <source>If Dolby Vision 4K video streams are not found, the download will fail.</source>
        <translation>Se i flussi video Dolby Vision 4K non vengono trovati, il download fallirà.</translation>
    </message>
    <message>
        <source>Dolby Vision 4K</source>
        <translation>Dolby Vision 4K</translation>
    </message>
    <message>
        <source>If Dolby Vision 4K video streams are not found, the download will probably fail.</source>
        <translation>Se non vengono rilevati i flussi video Dolby Vision 4K, probabilmente il download non riuscirà.</translation>
    </message>
    <message>
        <source>TV&amp;4</source>
        <translation></translation>
    </message>
    <message>
        <source>Create folder &quot;method_quality_amount_resolution&quot;</source>
        <translation>Crea cartella &quot;metodo_qualità_quantità_risoluzione&quot;</translation>
    </message>
    <message>
        <source>View more information from svtplay-dl. Appears in purple text color.</source>
        <translation>Visualizza maggiori informazioni con svtplay-dl.
Appare con testo in color viola.</translation>
    </message>
    <message>
        <source>Trying to immediately download all episodes. Unable to create folders or select quality.</source>
        <translation>Cerca di scaricare immediatamente tutti gli episodi.
Impossibile creare cartelle o selezionare la qualità.</translation>
    </message>
    <message>
        <source>List all Episodes</source>
        <translation>Elenco di tutti gli episodi</translation>
    </message>
    <message>
        <source>Add all Episodes to Download List</source>
        <translation>Aggiungi tutti gli episodi all&apos;elenco download</translation>
    </message>
    <message>
        <source>Select font...</source>
        <translation>Seleziona font...</translation>
    </message>
    <message>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation>Visita forum svtplay-dl...</translation>
    </message>
    <message>
        <source>Maintenance Tool...</source>
        <translation>Strumento manutenzione...</translation>
    </message>
    <message>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation>Avvia lo strumento di manutenzione per aggiornamenti o disinstallazione.</translation>
    </message>
    <message>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation>Tutte le ricerche salvate, l&apos;elenco download e le impostazioni verranno eliminate.</translation>
    </message>
    <message>
        <source>License 7-Zip...</source>
        <translation>Licenza 7-Zip...</translation>
    </message>
    <message>
        <source>Dark Fusion Style</source>
        <translation>Stile Fusion scuro</translation>
    </message>
    <message>
        <source>Note that the latest stable version may be newer than the latest beta version. Check version by clicking &quot;Help&quot; -&gt; &quot;About svtplay-dl...&quot;</source>
        <translation>Nota che la versione stabile più recente potrebbe essere più recente dell&apos;ultima versione beta.
Controlla la versione facendo clic su &quot;Aiuto&quot; -&gt; &quot;Info su svtplay-dl...&quot;</translation>
    </message>
    <message>
        <source>Use svtplay-dl from the system path</source>
        <translation>Usa svtplay-dl dal percorso di sistema</translation>
    </message>
    <message>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation>Usa (se disponibile) svtplay-dl nel percorso di sistema.</translation>
    </message>
    <message>
        <source>Select svtplay-dl...</source>
        <translation>Seleziona svtplay-dl...</translation>
    </message>
    <message>
        <source>Use the selected svtplay-dl</source>
        <translation>Usa svtplay-dl selezionato</translation>
    </message>
    <message>
        <source>Do not show notifications when the download is complete.</source>
        <translation>Non visualizzare le notifiche a download completato.</translation>
    </message>
    <message>
        <source>Create a shortcut</source>
        <translation>Crea un collegamento</translation>
    </message>
    <message>
        <source>Desktop Shortcut</source>
        <translation>Collegamento sul desktop</translation>
    </message>
    <message>
        <source>Applications menu Shortcut</source>
        <translation>Collegamentto menu applicazioni</translation>
    </message>
    <message>
        <source>Load external language file...</source>
        <translation>Carica file esterno lingua...</translation>
    </message>
    <message>
        <source>Set new &apos;st&apos; cookie</source>
        <translation>Imposta nuovo cookie &apos;st&apos;</translation>
    </message>
    <message>
        <source>Download svtplay-dl...</source>
        <translation>Download svtplay-dl...</translation>
    </message>
    <message>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation>Scarica e decomprime svtplay-dl da bin.ceicer.com</translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation>Zoom +</translation>
    </message>
    <message>
        <source>Increase the font size.</source>
        <translation>Aumenta dimensione tipo di carattere.</translation>
    </message>
    <message>
        <source>Ctrl++</source>
        <translation>Ctrl++</translation>
    </message>
    <message>
        <source>Ctrl+0</source>
        <translation>Ctrl+0</translation>
    </message>
    <message>
        <source>Ctrl+-</source>
        <translation>Ctrl+-</translation>
    </message>
    <message>
        <source>Download subtitles</source>
        <translation>Download sottotitoli</translation>
    </message>
    <message>
        <source>Download subtitles and try to merge with the video file</source>
        <translation>Download sottotitoli e unione con il file video</translation>
    </message>
    <message>
        <source>If the download does not work...</source>
        <translation>Se il download non funziona...</translation>
    </message>
    <message>
        <source>Open...</source>
        <translation>Apri...</translation>
    </message>
    <message>
        <source>The subtitle is saved in a text file (*.srt).</source>
        <translation>I sottotitoli verranno salvati in un file sottotitoli (*.srt).</translation>
    </message>
    <message>
        <source>If embedding in the video file does not work, try using the text file with the subtitle.</source>
        <translation>Se l&apos;incorporamento dei sottotitoli nel file video non funziona, prova ad usare il file dei sottotitoli.</translation>
    </message>
    <message>
        <source>No subtitles</source>
        <translation>Nessun sottotitolo</translation>
    </message>
    <message>
        <source>Zoom Default</source>
        <translation>Zoom predefinito</translation>
    </message>
    <message>
        <source>&apos;st&apos; &amp;cookies</source>
        <translation>&apos;st&apos; &amp;cookies</translation>
    </message>
    <message>
        <source>&amp;svtplay-dl</source>
        <translation>&amp;svtplay-dl</translation>
    </message>
    <message>
        <source>S&amp;ubtitle</source>
        <translation>S&amp;ottotitoli</translation>
    </message>
    <message>
        <source>Add to download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit the download List... (Advanced)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save the location where the finished video file is copied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Don&apos;t show notifications</source>
        <translation>Non visualizzare notifiche</translation>
    </message>
    <message>
        <source>Create shortcut to streamCapture2 on desktop.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create shortcut to streamCapture2 in the operating system&apos;s program menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation>Zoom -</translation>
    </message>
    <message>
        <source>Decrease the font size.</source>
        <translation>Diminuisci dimensione tipo di carattere.</translation>
    </message>
    <message>
        <source>Check for the latest svtplay-dl from bin.ceicer.com...</source>
        <translation>Controllo versione aggiornata svtplay-dl in ceicer.com...</translation>
    </message>
    <message>
        <source>Download Microsoft runtime (required for svtplay-dl)...</source>
        <translation>Scarica Microsoft Runtime (richiesto per svtplay-dl)...</translation>
    </message>
    <message>
        <source>Download the runtime file from bin.ceicer.com.</source>
        <translation>Scarica il file runtime da bin.ceicer.com.</translation>
    </message>
    <message>
        <source>Check streamCapture2 for updates at start</source>
        <translation>Controlla aggiornamenti streamCapture2 ad avvio programma</translation>
    </message>
    <message>
        <source>Check for new versions of svtplay-dl at start</source>
        <translation>Controlla aggiornamenti svtplay-dl ad avvio programma</translation>
    </message>
    <message>
        <source>streamCapture2 settings</source>
        <translation>impostazioni streamCapture2</translation>
    </message>
    <message>
        <source>download svtplay-dl settings</source>
        <translation>scarica impostazioni svtplay-dl</translation>
    </message>
    <message>
        <source>NFO info</source>
        <translation>Info NFO</translation>
    </message>
    <message>
        <source>NFO files contain media release information. Available at svtplay.se, among other places.</source>
        <translation>I file NFO contengono informazioni sulla versione dei media.
Disponibile, tra gli altri, su svtplay.</translation>
    </message>
    <message>
        <source>Select file name</source>
        <translation>Seleziona nome file</translation>
    </message>
    <message>
        <source>You choose the name of the downloaded video file.</source>
        <translation>Scegli il nome per il file video scaricato.</translation>
    </message>
    <message>
        <source>About Qt...</source>
        <translation>Info su Qt...</translation>
    </message>
    <message>
        <source>Set TV4 Token</source>
        <translation>Imposta token TV4</translation>
    </message>
    <message>
        <source>How-to</source>
        <translation>Come fare</translation>
    </message>
    <message>
        <source>How-to Video</source>
        <translation>Video come fare</translation>
    </message>
    <message>
        <source>Don&apos;t Use Native Dialogs</source>
        <translation>Non usare finestre di dialogo native</translation>
    </message>
    <message>
        <source>Fusion Style</source>
        <translation>Stile Fusion</translation>
    </message>
    <message>
        <source>Default Style</source>
        <translation>Stile predefinito</translation>
    </message>
    <message>
        <source>Export the download List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import a download List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New download List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download all subtitles</source>
        <translation>Scarica tutti i sottotitoli</translation>
    </message>
    <message>
        <source>Merge subtitle</source>
        <translation>Unisci sottotitoli</translation>
    </message>
    <message>
        <source>Download subtitle</source>
        <translation>Download sottotitoli</translation>
    </message>
    <message>
        <source>Download the subtitles in their native format</source>
        <translation>Download sottotitoli in formato nativo</translation>
    </message>
    <message>
        <source>Do not use the operating system&apos;s file dialog.</source>
        <translation>Non usare finestra di dialogo file del sistema operativo.</translation>
    </message>
    <message>
        <source>System Default Theme (Light or Dark)</source>
        <translation>Tema predefinito sistema (chiaro o scuro)</translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation>Tema chiaro</translation>
    </message>
</context>
</TS>
