
//  ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "ui_newprg.h"
#include <QSettings>
#ifdef Q_OS_LINUX // Linux
#include "updatedialog.h"
#include "update.h"
#endif
#include "checkupdate.h"
#include "about.h"
#include <QFileInfo>
#include <QFileDialog>
#include <QTimer>
#include <QDir>
#include <QStandardPaths>
#include <QCloseEvent>
void Newprg::setStartConfig()
{
#ifdef OFFLINE_INSTALLER
    const QString tmppath = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    QFile fil(tmppath + "/" FILENAME);

    if(fil.exists()) {
        fil.remove();
    }

#endif
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    QSettings settings2(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                        LIBRARY_NAME);
    settings.beginGroup("Settings");
    QString svtplaydlversion = settings.value("svtplaydlversion").toString();

    if(svtplaydlversion == "system") {
        svtplaydl = "svtplay-dl";
    }

    const QStringList childKeys = settings.childKeys();
    bool try4k = settings.value("try4k", false).toBool();
    settings.endGroup();
    settings.beginGroup("Style");
    const QString currentstyle = settings.value("currentstyle", "none").toString();
    settings.endGroup();

    if(currentstyle == "fusionDarkStyle") {
        ui->actionDarkFusionStyle->setChecked(true);
    }

    if(currentstyle == "fusionStyle") {
        ui->actionFusionStyle->setChecked(true);
    }

    if((currentstyle == "defaultStyle") || (currentstyle == "none")) {
        ui->actionDefaultStyle->setChecked(true);
    }

    if(childKeys.contains("readyupdate")) {
        settings.remove("readyupdate");
    }

    if(try4k) {
        ui->action4k->setChecked(true);
        ui->chb4k->setChecked(true);
    } else {
        ui->action4k->setChecked(false);
        ui->chb4k->setChecked(false);
    }

    deleteSettings = false;
#ifdef Q_OS_WIN
    ui->actionLicense7zip->setVisible(true);
#endif
#ifndef FFMPEG
    ui->actionLicenseFfmpeg->setDisabled(true);
    ui->actionLicenseFfmpeg->setVisible(false);
#endif
//#ifdef PORTABLE
    // CONTEXT MENU
    ui->statusBar->setContextMenuPolicy(Qt::CustomContextMenu);
    QObject::connect(ui->statusBar, &QWidget::customContextMenuRequested, this, [this]() {
        QAction *pathToExecutable = new QAction;
        QIcon icon(QStringLiteral(":/images/appicon.png"));
        pathToExecutable->setIcon(icon);
        pathToExecutable->setText(tr("Open the folder where streamCapture2 is located"));
        QAction *screenShot = new QAction;
        QIcon icon2(QStringLiteral(":/images/screenshot.png"));
        screenShot->setIcon(icon2);
        screenShot->setText(tr("Take a screenshot (5 seconds delay)"));
        QAction *userMess = new QAction;
        QIcon icon3(QStringLiteral(":/images/message.png"));
        userMess->setIcon(icon3);
        userMess->setText(tr("Read message from the developer (if there is any)"));
#ifdef Q_OS_LINUX
        QAction *forceUpdate = new QAction;
        QIcon icon4(QStringLiteral(":/images/update.png"));
        forceUpdate->setIcon(icon4);
        forceUpdate->setText(tr("Force update"));
        // QAction *rebuildIcon = new QAction;
        // QIcon icon5(QStringLiteral(":/images/appicon.png"));
        // rebuildIcon->setIcon(icon5);
        // rebuildIcon->setText(tr("Rebuild missing icon."));
#endif
        QMenu *contextMenu = new QMenu;
        contextMenu->addAction(pathToExecutable);
        contextMenu->addAction(screenShot);
        contextMenu->addAction(userMess);
#ifdef Q_OS_LINUX
#ifndef EXPERIMENTAL
        contextMenu->addAction(forceUpdate);
        // contextMenu->addAction(rebuildIcon);
#endif
#endif
#ifdef Q_OS_LINUX
        // QObject::connect(rebuildIcon, &QAction::triggered, this, &Newprg::firstRun);
#endif
        QObject::connect(pathToExecutable, &QAction::triggered, this, [this]() {
            /***/
            QFileInfo fi(QApplication::applicationDirPath());

            if(!fi.isReadable()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("streamCapture2 is not allowed to open ") + "\n\"" + QDir::toNativeSeparators(QApplication::applicationDirPath()) + "\"" + '\n' + tr(" Use the file manager instead."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                return;
            }

            /***/
            QFileDialog filedialog(this);
            int x = this->x();
            int y = this->y();
            filedialog.setDirectory(QApplication::applicationDirPath());
            filedialog.setLabelText(QFileDialog::Accept, tr("Open"));
            filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
            filedialog.setOption(QFileDialog::ReadOnly, true);
            filedialog.setWindowTitle(tr("The folder where streamCapture2 is located"));
            filedialog.setGeometry(x + 50, y + 50, 900, 550);
            filedialog.setFileMode(QFileDialog::AnyFile);//Folder name

            if(!filedialog.exec()) {
                return;
            }
        });

        QObject::connect(userMess, &QAction::triggered, this, [this]() {
            userMessage();
        });

#ifdef Q_OS_LINUX
#ifndef EXPERIMENTAL
        QObject::connect(forceUpdate, &QAction::triggered, this, [this]() {
            QPixmap *pixmap = new QPixmap(QStringLiteral(":/images/appicon.png"));
            Update *update = new Update;
            connect(update, &Update::isUpdated, this, &Newprg::isUpdated);
            // QDialog class
            updatedialog = new UpdateDialog;
            updatedialog->viewUpdate(pixmap);
            updatedialog->show();
            update->doUpdate(ARG1, ARG2, DISPLAY_NAME, updatedialog, pixmap);
        });

#endif
#endif
        QObject::connect(screenShot, &QAction::triggered, this, [this]() {
            QTimer::singleShot(5000, this, &Newprg::takeAScreenshot);
        });

        contextMenu->exec(QCursor::pos());
        contextMenu->clear();
        contextMenu->deleteLater();
    });

//#endif
    this->setAcceptDrops(true);
#ifdef Q_OS_WINDOWS
    windowicon = new QIcon(QStringLiteral(":/images/icon.ico"));
#endif
#ifdef Q_OS_LINUX
    windowicon = new QIcon(QStringLiteral(":/images/appicon.png"));
#endif
    this->setWindowIcon(*windowicon);
    QApplication::setWindowIcon(*windowicon);
    this->setWindowTitle(DISPLAY_NAME " " DISPLAY_VERSION);
    ui->teOut->setReadOnly(true);
    testTranslation();
#ifdef Q_OS_LINUX
    ui->actionMaintenanceTool->setText(tr("Update"));
    ui->actionMaintenanceTool->setStatusTip(tr("Update this AppImage to the latest version"));
    QIcon updateIcon(QStringLiteral(":/images/update.png"));
    ui->actionMaintenanceTool->setIcon(updateIcon);
    settings.beginGroup("Update");
    ui->actionMaintenanceTool->setEnabled(settings.value("readyupdate", false).toBool());
    settings.endGroup();
#endif
    settings.beginGroup("Settings");
    ui->actionDesktopShortcut->setChecked(settings.value("desktopshortcut", false).toBool());
    ui->actionApplicationsMenuShortcut->setChecked(settings.value("applicationsmenushortcut", false).toBool());
    ui->actionDontUseNativeDialogs->setChecked(settings.value("dontusenativedialogs", false).toBool());
    settings.endGroup();
#ifdef Q_OS_WINDOWS
    QIcon maintenancetoolIcon(":/images/maintenancetool.png");
    ui->actionMaintenanceTool->setIcon(maintenancetoolIcon);
    ui->actionDownloadMicrosoftRuntime->setVisible(true);
#endif
    QFont systemfont = QApplication::font();
    settings.beginGroup("Font");
    QString family = settings.value("family", systemfont).toString();
    int size = settings.value("size", systemfont.pointSize()).toInt();
    bool bold = settings.value("bold", false).toBool();
    bool italic = settings.value("italic", false).toBool();
    bool boldItalic = settings.value("boldItalic", false).toBool();
    settings.endGroup();
    QFont f2(family, size);

    if(boldItalic) {
        f2.setBold(true);
        f2.setItalic(true);
    } else {
        if(bold) {
            f2.setBold(true);
        } else if(italic) {
            f2.setItalic(true);
        } else {
            f2.setBold(false);
            f2.setItalic(false);
        }
    }

    ui->teOut->setFont(f2);
#ifdef Q_OS_LINUX
    ui->actionStopAllDownloads->setVisible(false);
#endif
    processpid = 0;
    avbrutet = false;
    /* */
    QFileInfo fi(settings.fileName());
    QString settings_location = fi.canonicalPath();
    settings.beginGroup("Settings");
    int ma = settings.value("maxantal", 10).toInt();
    bool createfolder = settings.value("createfolder", "false").toBool();
    bool showmore = settings.value("showmore", "false").toBool();
    bool selectfilename = settings.value("selectfilename", "false").toBool();
    // QString svtplaydlversion =
    //     settings.value("svtplaydlversion").toString();
    bool no_notifications = settings.value("no_notifications", "true").toBool();
    settings.endGroup();
    settings.beginGroup("Path");
    bool copytodefaultlocation =
        settings.value("copytodefaultlocation", "false").toBool();
    bool downloadtodefaultlocation =
        settings.value("downloadtodefaultlocation", "false").toBool();
    QString downloadlistfile = settings.value("downloadlistfile").toString();
    settings.endGroup();
    fi.setFile(downloadlistfile);

    if(fi.isFile() && fi.size() > 0) {
        ui->actionDownloadAll->setEnabled(true);
        ui->pbDownloadAll->setEnabled(true);
    } else {
        ui->actionDownloadAll->setEnabled(false);
        ui->pbDownloadAll->setEnabled(false);
    }

    if(createfolder) {
        ui->actionCreateFolder->setChecked(true);
    } else {
        ui->actionCreateFolder->setChecked(false);
    }

    if(showmore) {
        ui->actionShowMore->setChecked(true);
    } else {
        ui->actionShowMore->setChecked(false);
    }

    if(selectfilename) {
        ui->actionSelectFileName->setChecked(true);
    } else {
        ui->actionSelectFileName->setChecked(false);
    }

    if(copytodefaultlocation) {
        ui->actionCopyToDefaultLocation->setChecked(true);
    } else {
        ui->actionCopyToDefaultLocation->setChecked(false);
    }

    if(downloadtodefaultlocation) {
        ui->actionDownloadToDefaultLocation->setChecked(true);
    } else {
        ui->actionDownloadToDefaultLocation->setChecked(false);
    }

    if(no_notifications) {
        ui->actionNotifications->setChecked(true);
    } else {
        ui->actionNotifications->setChecked(false);
    }

    /* svtplay-dl */
    if(svtplaydlversion == "stable") {
        // ui->actionSvtplayDlBleedingEdge->setChecked(false);
        // ui->actionSvtplayDlSystem->setChecked(false);
        // ui->actionSvtPlayDlManuallySelected->setChecked(false);
        ui->actionSvtplayDlStable->setChecked(true);
        /* SYSTEM */
    } else if(svtplaydlversion == "system") {
        // ui->actionSvtplayDlStable->setChecked(false);
        // ui->actionSvtplayDlBleedingEdge->setChecked(false);
        // ui->actionSvtPlayDlManuallySelected->setChecked(false);
        ui->actionSvtplayDlSystem->setChecked(true);
    }
    /* SYSTEM ENDS */
    /* MANUALLY */
    else if(svtplaydlversion == "manually") {
// cppcheck
        settings.beginGroup("Path");
        svtplaydl = settings.value("svtplayPath").toString();
        settings.endGroup();
        // ui->actionSvtplayDlStable->setChecked(false);
        // ui->actionSvtplayDlBleedingEdge->setChecked(false);
        // ui->actionSvtplayDlSystem->setChecked(false);
        ui->actionSvtPlayDlManuallySelected->setChecked(true);
    }
    /* MANUALLY ENDS */
    else if(svtplaydlversion == "bleedingedge") {
        // ui->actionSvtplayDlStable->setChecked(false);
        // ui->actionSvtplayDlSystem->setChecked(false);
        // ui->actionSvtPlayDlManuallySelected->setChecked(false);
        ui->actionSvtplayDlBleedingEdge->setChecked(true);
    }

    /* svtplay-dl ENDS */
    MAX_RECENT = ma;
    settings.beginGroup("Update");
    const bool checkonstart = settings.value("checkonstart", true).toBool();
    const bool checksvtplaydlonstart = settings.value("checksvtplaydlonstart", true).toBool();
    ui->actionCheckSvtplayDlForUpdatesAtStart->setChecked(checksvtplaydlonstart);
    settings.endGroup();

    if(checkonstart) {
#ifdef Q_OS_LINUX
        QString *updateinstructions =
            new QString(tr("Select \"Tools\", \"Update\" to update."));
#endif
#ifdef Q_OS_WIN
#ifndef PORTABLE
        QString *updateinstructions = new QString(
            tr("Please click on \"Tools\" and \"Maintenance Tool...\""));
#else
        QString *updateinstructions = new QString(
            tr("Download a new") + " <a href=\"" DOWNLOAD_PATH "\"> portable</a>");
#endif
#endif
        ui->actionCheckOnStart->setChecked(true);
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Language");
        QString language = settings.value("language", "en_US").toString();
        settings.endGroup();

        if(language == "sv_SE") {
            language = VERSION_PATH_SV;
        } else {
            language = VERSION_PATH;
        }

        auto *cfu = new CheckUpdate;
        cfu->checkOnStart(DISPLAY_NAME, VERSION, language, *updateinstructions, windowicon);
#ifdef Q_OS_WIN
        connect(cfu, &CheckUpdate::foundUpdate, this, [](bool doupdate) {
#endif
#ifdef Q_OS_LINUX
            connect(cfu, &CheckUpdate::foundUpdate, this, [this](bool doupdate) {
#endif

                if(doupdate) {
                    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                                       EXECUTABLE_NAME);
                    // settings.setIniCodec("UTF-8");
                    settings.beginGroup("Update");
                    settings.setValue("readyupdate", true);
                    settings.endGroup();
#ifdef Q_OS_LINUX // Linux
                    ui->actionMaintenanceTool->setEnabled(true);
#endif
                } else {
                    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                                       EXECUTABLE_NAME);
                    // settings.setIniCodec("UTF-8");
                    settings.beginGroup("Update");
                    settings.setValue("readyupdate", false);
                    settings.endGroup();
#ifdef Q_OS_LINUX // Linux
                    ui->actionMaintenanceTool->setDisabled(true);
#endif
                }
            });
        } else {
            ui->actionCheckOnStart->setChecked(false);
        }
        ui->statusBar->showMessage(tr("Up and running"));
        settings.beginGroup("Settings");
                const QString sub = settings.value("sub", "nosubtitels").toString();
                settings.endGroup();

        if(sub == "subtitle") {
        ui->actionSubtitle->setChecked(true);
        } else if(sub == "allsubtitels") {
        ui->actionAllSubtitles->setChecked(true);
        } else if(sub == "mergesubtitels") {
        ui->actionMergeSubtitle->setChecked(true);
        } else if(sub == "nosubtitels") {
        ui->actionNoSubtitles->setChecked(true);
        } else if(sub == "rawsubtitels") {
        ui->actionDownloadRaw->setChecked(true);
        }
        const QString RECENT_FILE_FILE = settings_location + "/recentfiles.list";
                                         QFile file(RECENT_FILE_FILE);

        if(file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QTextStream in(&file);

            while(!in.atEnd()) {
                recentFiles.append(in.readLine());
            }
        }
        file.close();
            /*  */
            // const QString DOWNLOADLIST = settings_location + "/downloads.lst";
            // QFile file2(DOWNLOADLIST);
            // if(file2.open(QIODevice::ReadOnly | QIODevice::Text)) {
            // QTextStream in(&file2);
            //     while(!in.atEnd()) {
            //         downloadList.append(in.readLine());
            //     }
            //     if(!downloadList.empty()) {
            //         ui->pbDownloadAll->setEnabled(true);
            //         ui->actionDownloadAll->setEnabled(true);
            //     }
            // }
            // file2.close();
//        QPoint punkt = settings.value("pos", QPoint(200, 200)).toPoint();
//                       this->move(settings.value("pos", QPoint(200, 200)).toPoint());
//                       QSize storlek = settings.value("size", QSize(700, 300)).toSize();
//                       this->resize(settings.value("size", QSize(700, 300)).toSize());
//                       int widd = 0, hojd = 0;
//        foreach(QScreen *screen, QGuiApplication::screens()) {
//        widd = screen->size().width();
//            hojd = screen->size().height();
//            if((storlek.width() >= widd) || (storlek.height() >= hojd)) {
//                this->resize(widd, hojd);
//                this->setWindowState(Qt::WindowMaximized);
//            }
//            if((punkt.x() > widd - 100) || (punkt.y() > hojd - 100) || punkt.x() < 0 ||
//                    punkt.y() < 0) {
//                this->move(200, 200);
//                break;
//            }
//        }
            settings.beginGroup(QStringLiteral("MainWindow"));
            this->restoreGeometry(settings.value("savegeometry").toByteArray());
            this->restoreState(settings.value("savestate").toByteArray());
            settings.endGroup();
// Provider
            settings.beginGroup("Provider");
            ui->comboResolution->addItem("- " + tr("Resolution"));
            ui->comboResolution->addItem(tr("Less then 720p"));
            ui->comboResolution->addItem(tr("Less than or equal to 720p"));
            ui->comboResolution->addItem(tr("Less than or equal to 1080p"));
            ui->comboResolution->addItem(tr("More then 1080p"));
            QStringList keys = settings.childGroups();
            settings.endGroup();
            ui->comboPayTV->addItem("- " + tr("No Password"));
            keys.sort(Qt::CaseInsensitive);
            ui->comboAmount->addItem("- " + tr("Deviation"));
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

        for(const QString &p1 : std::as_const(keys)) {
        QAction *actionPayTV;
        actionPayTV = new QAction(p1, nullptr);
            ui->comboPayTV->addItem(p1);
            ui->menuPayTV->addAction(actionPayTV);
            connect(actionPayTV, &QAction::triggered, this,
            [this, actionPayTV]() {
                const QString temp = actionPayTV->text();
                editOrDelete(temp);
            });
        }
#else

        for(const QString &p1 : qAsConst(keys)) {
        QAction *actionPayTV;
        actionPayTV = new QAction(p1, nullptr);
            ui->comboPayTV->addItem(p1);
            ui->menuPayTV->addAction(actionPayTV);
            connect(actionPayTV, &QAction::triggered,
            [this, actionPayTV]() {
                const QString temp = actionPayTV->text();
                editOrDelete(temp);
            });
        }
#endif
// st
        actionSt();
//    settings.beginGroup(QStringLiteral(u"Stcookies"));
//    QString currentst = settings.value(QStringLiteral(u"currentst"), "").toString();
//    keys = settings.childGroups();
//    settings.endGroup();
//    keys.sort(Qt::CaseInsensitive);
//    settings.beginGroup(QStringLiteral(u"Stcookies"));
//    for(const QString &p : qAsConst(keys)) {
//        QAction *actionSt;
//        actionSt = new QAction(p, nullptr);
//        ui->menuStCookies->addAction(actionSt);
//        actionSt->setCheckable(true);
//        if((!currentst.isEmpty()) && (currentst == settings.value(p + QStringLiteral(u"/st")))) {
//            actionSt->setChecked(true);
//        }
//        connect(actionSt, &QAction::triggered,
//        [this, actionSt]() {
//            editOrDeleteSt(actionSt);
//        });
//    }
//    settings.endGroup();
        ui->pbPassword->setDisabled(true);
        ui->actionPassword->setDisabled(true);
#ifdef Q_OS_WIN
        ui->actionMaintenanceTool->setVisible(true);
        ui->actionMaintenanceTool->setEnabled(true);
#endif

        if(selectFfmpeg() == "NOTFIND") {
#ifdef Q_OS_LINUX
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("<b>FFmpeg cannot be found or is not an executable program.</b><br><br>Download an AppImage that contains FFmpeg.<br><br>Or install FFmpeg in the system path."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
#endif
#ifdef Q_OS_WIN
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
#ifdef PORTABLE
            msgBox.setText(tr("<b>FFmpeg cannot be found or is not an executable program.</b><br><br>Download a portable streamCapture2 that contains FFmpeg.<br><br>Or download and place ffmpeg.exe in the same folder as streamapture2.exe.<br><br>Or install FFmpeg in the system path."));
#endif
#ifndef PORTABLE
            msgBox.setText(tr("<b>FFmpeg cannot be found or is not an executable program.</b><br><br>Go to \"Tools\", \"Maintenance Tool\" to install.<br><br>Or install FFmpeg in the system path."));
#endif
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
#endif
        }

        // Find svtplay-dl
        if(ui->actionSvtplayDlSystem->isChecked()) {
#ifdef Q_OS_WINDOWS
        svtplaydl = "svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
        svtplaydl = "svtplay-dl";
#endif
    } else if(ui->actionSvtPlayDlManuallySelected->isChecked()) {
        settings.beginGroup("Path");
            QString svtplaydlpath = settings.value("svtplaydlpath", "NOTFIND").toString();
            settings.endGroup();
            svtplaydl = svtplaydlpath;
        } else if(ui->actionSvtplayDlStable->isChecked()) {
        settings2.beginGroup("Path");
            QString stablepath = settings2.value("stablepath").toString();
            settings2.endGroup();
#ifdef Q_OS_WINDOWS
            svtplaydl = stablepath + "/stable/svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
            svtplaydl = stablepath + "/stable/svtplay-dl";
#endif
        } else if(ui->actionSvtplayDlBleedingEdge->isChecked()) {
        settings2.beginGroup("Path");
            QString betapath = settings2.value("betapath").toString();
            settings2.endGroup();
#ifdef Q_OS_WINDOWS
            svtplaydl = betapath + "/beta/svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
            svtplaydl = betapath + "/beta/svtplay-dl";
#endif
        } else {
            settings2.beginGroup("Path");
            QString stablepath = settings2.value("stablepath", "svtplay-dl").toString();
            settings2.endGroup();
#ifdef Q_OS_WINDOWS
            svtplaydl = stablepath + "/stable/svtplay-dl.exe";
#endif
#ifdef Q_OS_LINUX
            svtplaydl = stablepath + "/stable/svtplay-dl";
#endif

            if(!QFile::exists(svtplaydl)) {
                ui->teOut->setText(svtplaydl +
                                   tr(" cannot be found or is not an executable program."));
                ui->teOut->append(tr("Please click on \"svtplay-dl\" and select svtplay-dl."));
            }
        }
        // END find svtplay-dl
        // END startconfig
        userMessageStart();
    }

    QString Newprg::findSvtplayDl() {
        QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
        QString senv = env.value("PATH");
#ifdef Q_OS_LINUX
        QStringList slenv = senv.split(":");
#endif
#ifdef Q_OS_WIN
        QStringList slenv = senv.split(";");
#endif
        /*nnnnn*/
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

        for(QString s : std::as_const(slenv)) {
#else

        for(QString s : qAsConst(slenv)) {
#endif
            /*nnnnn*/
            // for(QString s : slenv) {
#ifdef Q_OS_LINUX
            s.append(QDir::toNativeSeparators("/svtplay-dl"));
#endif
#ifdef Q_OS_WIN
            s.append(QDir::toNativeSeparators("/svtplay-dl.exe"));
#endif

            if(fileExists(s)) {
                return s;
            }
        }

        return "NOTFIND";
    }

    void Newprg::closeEvent(QCloseEvent * event) {
        event->accept();
    }

    void Newprg::setEndConfig() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        QFileInfo fi(settings.fileName());
        QString settings_location = fi.canonicalPath();
        settings.beginGroup("Settings");
        settings.setValue("maxantal", MAX_RECENT);
        settings.endGroup();
        settings.beginGroup("Settings");
        settings.setValue("desktopshortcut", ui->actionDesktopShortcut->isChecked());
        settings.setValue("applicationsmenushortcut", ui->actionApplicationsMenuShortcut->isChecked());

        if(ui->actionSubtitle->isChecked()) {
            settings.setValue("sub", "subtitle");
        } else if(ui->actionAllSubtitles->isChecked()) {
            settings.setValue("sub", "allsubtitels");
        } else if(ui->actionMergeSubtitle->isChecked()) {
            settings.setValue("sub", "mergesubtitels");
        } else if(ui->actionNoSubtitles->isChecked()) {
            settings.setValue("sub", "nosubtitels");
        } else if(ui->actionDownloadRaw->isChecked()) {
            settings.setValue("sub", "rawsubtitels");
        }

        if(ui->actionSvtplayDlStable->isChecked()) {
            settings.setValue("svtplaydlversion", "stable");
        } else if(ui->actionSvtplayDlSystem->isChecked()) {
            settings.setValue("svtplaydlversion", "system");
        } else if(ui->actionSvtPlayDlManuallySelected->isChecked()) {
            settings.setValue("svtplaydlversion", "manually");
        } else if(ui->actionSvtplayDlBleedingEdge->isChecked()) {
            settings.setValue("svtplaydlversion", "bleedingedge");
        }

        if(ui->actionNotifications->isChecked()) {
            settings.setValue("no_notifications", "true");
        } else {
            settings.setValue("no_notifications", "false");
        }

        settings.endGroup();
        settings.beginGroup("MainWindow");
        settings.setValue("savegeometry", this->saveGeometry());
        settings.setValue("savestate", this->saveState());
        settings.endGroup();
        recentFiles.removeDuplicates();
        const QString RECENT_FILE_FILE = settings_location + "/recentfiles.list";
        QFile file(RECENT_FILE_FILE);

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("Could not save a file to store Recent Search "
                              "list.\nCheck your file permissions."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
        }

        if(!recentFiles.empty()) {
            recentFiles.removeDuplicates();

            for(int i = 0; i < recentFiles.size(); ++i) {
                QTextStream out(&file);
                out << recentFiles.at(i) << '\n';
            }
        }

        file.close();
    }
