#  ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#
#    		streamCapture2
#    		(C) Copyright  2016 - 2024 Ingemar Ceicer
#    		https://gitlab.com/posktomten/streamcapture2/
#    		programming@ceicer.com
#
#   This program is free software: you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation, either version 3 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
# ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#-------------------------------------------------
#
# Project created by QtCreator 2017-05-07T13:14:14
#
#-------------------------------------------------

QT += core gui network xml

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = streamcapture2
TEMPLATE = app
#CONFIG += optimize_full
#CONFIG += static
#CONFIG += precompile_header

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS
# DEFINES += QT_DISABLE_DEPRECATED_BEFORE
# DEFINES += QT_NO_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

# CONFIG += C++17
#CONFIG += /opt/Qt5.15.0_systemfreetype_static/bin/lrelease embed_translations
#CONFIG += C:\Qt\Qt\5.15.0\mingw81_32\bin\lrelease embed_translations

#CONFIG += /opt/Qt5.15.0_systemfreetype_static/bin/lrelease embed_translation#s
# CONFIG += lrelease embed_translations

# Input

#CONFIG += embed_manifest_exe
#contains(QMAKE_HOST.version_string, x86_64):{
#    message("Host is 64bit")

#}

#OPENSSL_LIBS="-lssl -lcrypto"
unix{

HEADERS += \
           cli.h \# unix
           downloadlistdialog.h \
           newprg.h \
           zoom_disable.h


FORMS +=   \
           downloadlistdialog.ui \
           newprg.ui

SOURCES += \
    about.cpp \
    check_svtplaydl_for_updates.cpp \
    cli.cpp  \ # unix
    coppytodefaultlocation.cpp \
    download_runtime.cpp \
    download.cpp \
    downloadall.cpp \
    downloadallepisodes.cpp \
    downloadfromceicer.cpp \
    downloadlistdialog_get.cpp \
    downloadlistdialog_zoom.cpp \
    downloadlistdialog.cpp \
    drag_and_drop.cpp \
    firstrun.cpp \
    import_export.cpp \
    language.cpp \
    listallepisodes.cpp \
    main.cpp \
    newprg.cpp \
    nfo.cpp \
    paytv_create.cpp \
    paytv_edit.cpp \
    qsystemtrayicon.cpp \
    save.cpp \
    screenshot.cpp \
    setgetconfig.cpp \
    shortcuts.cpp \
    sok.cpp \
    st_create.cpp \
    st_edit.cpp \
    style.cpp \
    test_translation.cpp \
    user_message.cpp \
    zoom.cpp

RESOURCES += myres_lin.qrc \
              resource_selectfont_lin.qrc




} # END unix

win32{

HEADERS += \
           downloadlistdialog.h \
           newprg.h \
           zoom_disable.h


FORMS +=   \
           downloadlistdialog.ui \
           newprg.ui

SOURCES += \
    about.cpp \
    check_svtplaydl_for_updates.cpp \
    coppytodefaultlocation.cpp \
    download_runtime.cpp \
    download.cpp \
    downloadall.cpp \
    downloadallepisodes.cpp \
    downloadfromceicer.cpp \
    downloadlistdialog_get.cpp \
    downloadlistdialog_zoom.cpp \
    downloadlistdialog.cpp \
    drag_and_drop.cpp \
    firstrun.cpp \
    import_export.cpp \
    language.cpp \
    listallepisodes.cpp \
    main.cpp \
    newprg.cpp \
    nfo.cpp \
    offline_installer.cpp \ # win32
    paytv_create.cpp \
    paytv_edit.cpp \
    qsystemtrayicon.cpp \
    save.cpp \
    screenshot.cpp \
    setgetconfig.cpp \
    shortcuts.cpp \
    sok.cpp \
    st_create.cpp \
    st_edit.cpp \
    style.cpp \
    test_translation.cpp \
    user_message.cpp \
    zoom.cpp


RESOURCES += myres_win.qrc \
              resource_selectfont_win.qrc

} # END win32



RC_FILE = myapp.rc

unix{

TRANSLATIONS += i18n/_streamcapture2_template_lin_xx_XX.ts \
                i18n/_streamcapture2_lin_sv_SE.ts \
                i18n/_streamcapture2_lin_it_IT.ts
} # END unix

win32{

TRANSLATIONS += i18n/_streamcapture2_template_win_xx_XX.ts \
                i18n/_streamcapture2_win_sv_SE.ts \
                i18n/_streamcapture2_win_it_IT.ts
} # END win32

CODECFORSRC = UTF-8
#UI_DIR = ../code
win32:RC_ICONS += images/icon.ico

DEPENDPATH += "../include"
INCLUDEPATH += "../include"


DESTDIR="../build-executable5"

equals(QT_PATCH_VERSION, 2) {

contains(QMAKE_CC, gcc){

unix: CONFIG  (release, debug|release): LIBS += -L../lib_Qt5.15.2/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib_Qt5.15.2/ -lupdateappimaged # Debug

win32: CONFIG (release, debug|release): LIBS += -L../lib_Qt5.15.2/ -ldownload_install # Release
else: win32: CONFIG (debug, debug|release): LIBS += -L../lib_Qt5.15.2/ -ldownload_installd # Debug

CONFIG (release, debug|release): LIBS += -L../lib_Qt5.15.2/ -lcheckupdate  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib_Qt5.15.2/ -lcheckupdated # Debug
CONFIG (release, debug|release): LIBS += -L../lib_Qt5.15.2/ -lselectfont  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib_Qt5.15.2/ -lselectfontd # Debug
CONFIG (release, debug|release): LIBS += -L../lib_Qt5.15.2/ -ldownloadunpack # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib_Qt5.15.2/ -ldownloadunpackd # Debug

unix: CONFIG (release, debug|release): LIBS += -L../lib_Qt5.15.2/ -lcheckupdcli # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib_Qt5.15.2/ -lcheckupdclid # Debug


CONFIG (release, debug|release): LIBS += -L../lib_Qt5.15.2/ -labout # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib_Qt5.15.2/ -laboutd # Debug

}


  contains(QMAKE_CC, cl){

        unix: CONFIG  (release, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -lupdateappimage # Release
        else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -lupdateappimaged # Debug

        win32: CONFIG (release, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -ldownload_install # Release
        else: win32: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -ldownload_installd # Debug

        CONFIG (release, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -lcheckupdate  # Release
        else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -lcheckupdated # Debug
        CONFIG (release, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -lselectfont  # Release
        else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -lselectfontd # Debug
        CONFIG (release, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -ldownloadunpack # Release
        else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -ldownloadunpackd # Debug

        CONFIG (release, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -labout # Release
        else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc_Qt5.15.2/ -laboutd # Debug


  }





}
greaterThan(QT_PATCH_VERSION, 2) {

# GCC


message("Qt5.15.16 32-bit")

contains(QT_ARCH, i386) {

contains(QMAKE_CC, gcc){

      #  unix: CONFIG  (release, debug|release): LIBS += -L../lib5_32/ -lcrypto # Release
      #  unix: CONFIG  (release, debug|release): LIBS += -L../lib5_32/ -lssl # Release

      #  WIN32: CONFIG  (release, debug|release): LIBS += -L../lib5_32/ -lcrypto # Release
      #  WIN32: CONFIG  (release, debug|release): LIBS += -L../lib5_32/ -lssl # Release

        unix: CONFIG  (release, debug|release): LIBS += -L../lib5_32/ -lupdateappimage # Release
        else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lupdateappimaged # Debug

        win32: CONFIG (release, debug|release): LIBS += -L../lib5_32/ -ldownload_install # Release
        #else: win32: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -ldownload_installd # Debug

        CONFIG (release, debug|release): LIBS += -L../lib5_32/ -lcheckupdate  # Release
        #else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lcheckupdated # Debug
        CONFIG (release, debug|release): LIBS += -L../lib5_32/ -lselectfont  # Release
        #else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lselectfontd # Debug
        CONFIG (release, debug|release): LIBS += -L../lib5_32/ -ldownloadunpack # Release
        #else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -ldownloadunpackd # Debug

        CONFIG (release, debug|release): LIBS += -L../lib5_32/ -labout # Release
        #else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -laboutd # Debug
        
        unix:CONFIG (release, debug|release): LIBS += -L../lib5_32/ -lcheckupdcli # Release
        #else: CONFIG (debug, debug|release): LIBS += -L../lib5_32/ -lcheckupdclid # Debug
}
		
		    # Visual C++
  contains(QMAKE_CC, cl){




                CONFIG (release, debug|release): LIBS += -L../../lib5_32_msvc/ -lcheckupdate  # Release
                #else: CONFIG (debug, debug|release): LIBS += -L../lib5_32_msvc/ -lcheckupdated # Debug
                CONFIG (release, debug|release): LIBS += -L../../lib5_32_msvc/ -lselectfont  # Release
                #else: CONFIG (debug, debug|release): LIBS += -L../../lib5_32_msvcc/ -lselectfontd # Debug
                CONFIG (release, debug|release): LIBS += -L../../lib5_32_msvc/ -ldownloadunpack # Release
                #else: CONFIG (debug, debug|release): LIBS += -L../../lib5_32_msvc/ -ldownloadunpackd # Debug

                CONFIG (release, debug|release): LIBS += -L../lib5_32_msvc/ -labout # Release
                #else: CONFIG (debug, debug|release): LIBS += -L../../lib5_32_msvc/ -laboutd # Debug
				
				CONFIG (release, debug|release): LIBS += -L../../lib5_32_msvc/ -ldownload_install # Release
                #else: CONFIG (debug, debug|release): LIBS += -L../../lib5_32_msvc/ -ldownload_installd # Debug


}
}


contains(QT_ARCH, x86_64) {

contains(QMAKE_CC, gcc){



#unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lcrypto # Release
#unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lssl # Release

unix: CONFIG (release, debug|release): LIBS += -L../lib5/ -lcheckupdcli # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcheckupdclid # Debug

#WIN32: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lcrypto # Release
#WIN32: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lssl # Release

unix: CONFIG  (release, debug|release): LIBS += -L../lib5/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lupdateappimaged # Debug

win32: CONFIG (release, debug|release): LIBS += -L../lib5/ -ldownload_install # Release
#else: win32: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ldownload_installd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5/ -lcheckupdate  # Release
#else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lcheckupdated # Debug
CONFIG (release, debug|release): LIBS += -L../lib5/ -lselectfont  # Release
#else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -lselectfontd # Debug
CONFIG (release, debug|release): LIBS += -L../lib5/ -ldownloadunpack # Release
#else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -ldownloadunpackd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5/ -labout # Release
#else: CONFIG (debug, debug|release): LIBS += -L../lib5/ -laboutd # Debug
}
contains(QMAKE_CC, clang){




unix: CONFIG (release, debug|release): LIBS += -L../lib5_clang/ -lcheckupdcli # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5_clang/ -lcheckupdclid # Debug

#WIN32: CONFIG  (release, debug|release): LIBS += -L../lib5_clang/ -lcrypto # Release
#WIN32: CONFIG  (release, debug|release): LIBS += -L../lib5_clang/ -lssl # Release

unix: CONFIG  (release, debug|release): LIBS += -L../lib5_clang/ -lupdateappimage # Release
else: unix: CONFIG (debug, debug|release): LIBS += -L../lib5_clang/ -lupdateappimaged # Debug

win32: CONFIG (release, debug|release): LIBS += -L../lib5_clang/ -ldownload_install # Release
else: win32: CONFIG (debug, debug|release): LIBS += -L../lib5_clang/ -ldownload_installd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5_clang/ -lcheckupdate  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_clang/ -lcheckupdated # Debug
CONFIG (release, debug|release): LIBS += -L../lib5_clang/ -lselectfont  # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_clang/ -lselectfontd # Debug
CONFIG (release, debug|release): LIBS += -L../lib5/ -ldownloadunpack # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5_clang/ -ldownloadunpackd # Debug

CONFIG (release, debug|release): LIBS += -L../lib5_clang/ -labout # Release
else: CONFIG (debug, debug|release): LIBS += -L../lib5__clang/ -laboutd # Debug





}

    # Visual C++
  contains(QMAKE_CC, cl){
message("MSVC 64-bit")

                CONFIG (release, debug|release): LIBS += -L../../lib5_msvc/ -ldownload_install # Release
                # else: CONFIG (debug, debug|release): LIBS += -L../../lib5_msvc/ -ldownload_installd # Debug

                CONFIG (release, debug|release): LIBS += -L../../lib5_msvc/ -lcheckupdate  # Release
                # else: CONFIG (debug, debug|release): LIBS += -L../lib5_msvc/ -lcheckupdated # Debug
                CONFIG (release, debug|release): LIBS += -L../../lib5_msvc/ -lselectfont  # Release
                # else: CONFIG (debug, debug|release): LIBS += -L../../lib5_msvc/ -lselectfontd # Debug
                CONFIG (release, debug|release): LIBS += -L../../lib5_msvc/ -ldownloadunpack # Release
                # else: CONFIG (debug, debug|release): LIBS += -L../../lib5_msvc/ -ldownloadunpackd # Debug

                CONFIG (release, debug|release): LIBS += -L../lib5_msvc/ -labout # Release
                # else: CONFIG (debug, debug|release): LIBS += -L../../lib5_msvc/ -laboutd # Debug


}
}
}

message (----------------------------------------)
message (OS: $$QMAKE_HOST.os)
#message (Arch: $$QMAKE_HOST.arch)
#message (Cpu count: $$QMAKE_HOST.cpu_count)
#message (Name: $$QMAKE_HOST.name)
#message (Version: $$QMAKE_HOST.version)
#message (Version string: $$QMAKE_HOST.version_string)
compiler=$$basename(QMAKESPEC)
message(compiler: $$compiler)
message (QMAKE_QMAKE: $$QMAKE_QMAKE)
#message (QT_VERSION: $$QT_VERSION)
message(_PRO_FILE_PWD_: $$_PRO_FILE_PWD_)
message(TEMPLATE: $$TEMPLATE)
message(TARGET: $$TARGET)
message(DESTDIR: $$DESTDIR)
#message(HEADERS $$HEADERS)
#message(SOURCES: $$SOURCES)
#message(INCLUDEPATH $$INCLUDEPATH)
message(LIBS: $$LIBS)

