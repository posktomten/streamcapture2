// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

//          CLI
//          Copyright (C) 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten/template
//          programming@ceicer.com

// A template program with frequently used functions.
// Change language, change style, show "About" dialog.
// View the license, check ether updates.
// The program can update itself.
// Works with Qt5 and Qt6.
// With Linux and with Windows operating systems.
// Works with 32-bit and with 64-bit operating systems.

// CLI is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, version 3.
// And NO LATER versions are allowed.
// GPL Version 3.0 only.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.

// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>




// EDIT
#include "newprg.h"

#include "cli.h"
#include "checkupdcli.h"
#include <QCommandLineParser>
#include <QProcess>
#include <QSettings>
#include <QApplication>
#include <QDir>
#include <QTextStream>
#include <QDate>
#include <QObject>
#include <QNetworkReply>
#include <QTranslator>
#include <QKeyEvent>
#include <QtGlobal>
#include <iostream>

Cli::Cli(QObject *parent)
    : QObject{parent}
{}

// Necessary definitions
// template and streamcapture2
// #define LANGUAGE_NAME "_complete_lin"
// streamcapture2
#define LANGUAGE_NAME "_complete"
/*

DISPLAY_NAME
EXECUTABLE_NAME
COPYRIGHT_YEAR
BUILD_DATE_TIME
COMPILEDON
COPYRIGHT
DISPLAY_NAME
VERSION
WEBSITE
EMAIL
VERSION_PATH_SV
VERSION_PATH
LICENSE
ARG1
ARG2
*/
void  Cli::parseOptions(QStringList argumentlist)
{
#if defined(Q_OS_LINUX)
    /*** TEST TRANSLATION **/
    QString currentLocale(QLocale::system().name());
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    QTranslator translator;
    settings.beginGroup("Language");
    QString testlanguage = settings.value("testlanguage").toString();
    QString currentlanguage = settings.value("language", currentLocale).toString();
    settings.endGroup();
    QFileInfo fi(testlanguage);

    if((currentlanguage.isEmpty()) && (fi.exists())) {
        if(translator.load(testlanguage)) {
            QApplication::installTranslator(&translator);
        }
    }
    /*** END TEST TRANSLATION ***/
    else {
        // ELSE
        settings.beginGroup("Language");
        QString sp = settings.value("language", "").toString();
        settings.endGroup();
        const QString translationPath = ":/i18n/" LANGUAGE_NAME;

        if(sp == "") {
            sp = QLocale::system().name();

            if(translator.load(translationPath + "_" + sp + ".qm")) {
                settings.beginGroup("Language");
                settings.setValue("language", sp);
                settings.endGroup();
                /*  */
                /*  */
                QApplication::installTranslator(&translator);
            } else if(sp != "en_US") {
                QTextStream(stdout) << DISPLAY_NAME << QStringLiteral(" is unfortunately not yet translated into your language, the program will start with English menus.");
                settings.beginGroup("Language");
                settings.setValue("language", "en_US");
                settings.endGroup();
            }
        } else {
            if(sp != "en_US") {
                if(translator.load(translationPath + "_" + sp + ".qm")) {
                    if(!QApplication::installTranslator(&translator)) {
                        QTextStream(stdout) << QStringLiteral("Unable to install language file, continues in English.");
                    } else {
                    }
                } else {
                    QTextStream(stdout) << QStringLiteral("Unable to load language file, continues in English.");
                }
            }
        }

        // END ELSE Test translation
    }

    // QDate currentda = QDate::currentDate();
    // QString currents = currentda.toString(QStringLiteral("yyyy"));
    QString *copyright_year = new QString;

    if(COPYRIGHT_YEAR == QString::number(QDate::currentDate().year())) {
        *copyright_year = QString::number(QDate::currentDate().year());
    } else {
        *copyright_year = COPYRIGHT_YEAR " - " + QString::number(QDate::currentDate().year());
    }

    QString builddate = QStringLiteral(BUILD_DATE_TIME);
    builddate = builddate.simplified();
    QCommandLineParser parser;
#if defined (__GNUC__)
    QString version = (QString(tr("Compiler:") + " %1 %2.%3.%4")
                       .arg("GCC")
                       .arg(__GNUC__)
                       .arg(__GNUC_MINOR__)
                       .arg(__GNUC_PATCHLEVEL__));
#endif
#ifdef __clang__
    version = (QString(tr("Compiler: Clang version ") + " %1.%2.%3")
               .arg(__clang_major__)
               .arg(__clang_minor__)
               .arg(__clang_patchlevel__));
#endif
    QCoreApplication::setApplicationName(QStringLiteral(DISPLAY_NAME));
    QCoreApplication::setApplicationVersion(QStringLiteral(VERSION));
    QTextStream(stdout) << '\n';
    QCommandLineOption getAuthenticity(QStringList() << QStringLiteral("a") << QStringLiteral("authenticity"), tr("Verify authenticity.\nSHA256 checksum is generated.\n \'sha256sum\' is required."));
    /***/
    /***/
    QCommandLineOption changelogOption(QStringList() << QStringLiteral("c") << QStringLiteral("changelog"), tr("Displays the version history."));
    /***/
    // QCommandLineOption downloadOption(QStringList() << QStringLiteral("d") << QStringLiteral("download"), tr("The new AppImage has been created using zsync.\nOnly the changes are downloaded."));
    QCommandLineOption updateOption(QStringList() << QStringLiteral("u") << QStringLiteral("update"), tr("The new AppImage is created with zsync.\nOnly the changes are downloaded."));
    /***/
    QCommandLineOption checkupdateOption(QStringList() << QStringLiteral("cu") << QStringLiteral("checkforupdates"), tr("Check for updates."));
    /***/
    QCommandLineOption getGLIBC(QStringList() << QStringLiteral("g") << QStringLiteral("glibc"), tr("Trying to read your operating system's GLIBC version."));
    /**/
#ifdef FFMPEG
    QCommandLineOption getFFmpeg(QStringList() << QStringLiteral("f") << QStringLiteral("ffmpeg"), tr("Reads the version number for FFmpeg included in the AppImage."));
#endif
    /**/
    QCommandLineOption getFFmpegSystem(QStringList() << QStringLiteral("fs") << QStringLiteral("ffmpeg-system"), tr("Trying to read your operating system's FFmpeg version."));
    QCommandLineOption startGui(QStringList() << QStringLiteral("gui"), tr("Starts the GUI (Graphical User Interface)."));
    // parser.addOption(startGui);
    /***/
    // QCommandLineOption LanguageOption(QStringList() << QStringLiteral("lang"), tr("Select language --lang en (English) or --lang sv (Swedish)"));
    QCommandLineOption LanguageOption(QStringList() << QStringLiteral("la") << QStringLiteral("lang"), tr("Select language --lang en (English), --lang it (Italian) or --lang sv (Swedish)"));
    /***/
    QString location = "\"" + QDir::toNativeSeparators(QApplication::applicationDirPath()) + QStringLiteral("\"");
    // parser.setOptionsAfterPositionalArgumentsMode(QCommandLineParser::ParseAsOptions);
    QCommandLineOption licenseOption(QStringList() << "l" << "license", tr("Displays the license."));
    QCommandLineOption getOpenSSL(QStringList() << QStringLiteral("o") << QStringLiteral("openssl"), tr("Reads the version number for openSSL included in the AppImage."));
    /***/
    QCommandLineOption getOpenSSLsystem(QStringList() << QStringLiteral("os") << QStringLiteral("openssl-system"), tr("Trying to read your operating system's openSSL version."));
    /***/
    /***/
    parser.setApplicationDescription(tr("Runs on: ") + QSysInfo::kernelType() + QStringLiteral(" ") + QSysInfo::prettyProductName() + QStringLiteral(" ") + QSysInfo::currentCpuArchitecture() + '\n' + tr("Development tool: Qt") + QT_VERSION_STR + '\n' + version + '\n' + tr("Compiled with: ") + COMPILEDON + '\n' + tr("Copyright (C) ") + *copyright_year + QStringLiteral(" ") + QStringLiteral(COPYRIGHT) + '\n' + tr("Website: ") + QStringLiteral(WEBSITE) + '\n'
                                     + tr("Swedish website: ")
                                     + QStringLiteral(SWEDISH_WEBSITE) + '\n'
                                     + tr("Source code: ")
                                     + QStringLiteral(SOURCECODE) + '\n'
                                     + tr("E-mail: ") + QStringLiteral(EMAIL) + '\n' + tr("This program was created ") + builddate + '\n' + tr("Path: ") + location + QStringLiteral("\n\n") + QStringLiteral(DISPLAY_NAME) + tr(" is free software: you can redistribute it") + '\n' + tr("and/or modify it under the terms of the") + '\n' + tr("GNU General Public License as published by") + '\n' + tr("the Free Software Foundation, version 3.") + '\n' + tr("This program is distributed in the hope that it will be useful, ") + '\n' + tr("but WITHOUT ANY WARRANTY; without even the implied warranty of ") + '\n' + tr("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.") + '\n' + tr("See the GNU General Public License for more details."));
    /**/
    parser.addHelpOption();
    QCommandLineOption AppImageOption(QStringList() << QStringLiteral("appimage-help"), tr("Help about the AppImage."));
    parser.addOption(AppImageOption);
    parser.addOption(getAuthenticity);
    parser.addOption(changelogOption);
    parser.addOption(checkupdateOption);
#ifdef FFMPEG
    parser.addOption(getFFmpeg);
#endif
    parser.addOption(getFFmpegSystem);
    parser.addOption(getGLIBC);
    parser.addOption(startGui);
    parser.addOption(licenseOption);
    parser.addOption(LanguageOption);
    parser.addOption(getOpenSSL);
    parser.addOption(getOpenSSLsystem);
    parser.addOption(updateOption);
    /***/
    /***/
    parser.addVersionOption();
    /***/
    parser.process(*qApp);
    /***/
    bool authenticity = parser.isSet(getAuthenticity);
    bool uppdate = parser.isSet(updateOption);
    bool checkupdate = parser.isSet(checkupdateOption);
    bool language = parser.isSet(LanguageOption);
    bool license = parser.isSet(licenseOption);
    bool changelog = parser.isSet(changelogOption);
    bool GLIBC = parser.isSet(getGLIBC);
    bool openSSL = parser.isSet(getOpenSSL);
    bool openSSLsystem = parser.isSet(getOpenSSLsystem);
    bool ffmpegSystem = parser.isSet(getFFmpegSystem);
#ifdef FFMPEG
    bool ffmpeg = parser.isSet(getFFmpeg);
#endif

    // AUTHENTICITY  //
    if(authenticity) {
        QString appimagepath = qgetenv("APPIMAGE"); // The path to AppImage
        QString appimagefollder(QFileInfo(appimagepath).absolutePath()); // The folder where AppImage is located
        QProcess *process  = new QProcess(nullptr);  // A QProcess is created
        process->setWorkingDirectory(appimagefollder); // The folder where AppImage is located
        QStringList ARG;
        ARG << appimagepath;
        process->start("sha256sum", ARG);
        QString in;

        while(process->waitForReadyRead()) {
            while(process->canReadLine()) {
                in = process->readAllStandardOutput();
            }
        }

        process->deleteLater();
        QString left = in.left(64);
        ;
        globalhashsum = left;
        /***/
        mLibcheckupdcli = new Libcheckupdcli;
        QObject::connect(mLibcheckupdcli, &Libcheckupdcli::sendVersion, this, &Cli::getCHECKSUM);
        mLibcheckupdcli->getVersionRequest(CHECKSUM);
        // END AUTHENTICITY  //
        // UPDATE  //
    } else if(uppdate) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup(QStringLiteral("Language"));
        QString language = settings.value(QStringLiteral("language"), currentLocale).toString();
        settings.endGroup();
        const QString EXECUTABLE = QCoreApplication::applicationDirPath() + QStringLiteral("/downloadandremove");
        QStringList ARG;
        QProcess *process  = new QProcess(nullptr);
        ARG << ARG1 << ARG2 << language << VERSION;

        if(!process->startDetached(EXECUTABLE, ARG)) {
            QTextStream(stdout) << QObject::tr("An unexpected error occurred during the update process.");
        } else {
            QTextStream(stdout) << QObject::tr("Preparing to download...") + '\n';
        }

        exit(0);
    }
    /***/
    else if(checkupdate) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup(QStringLiteral("Language"));
        QString language = settings.value(QStringLiteral("language"), currentLocale).toString();
        settings.endGroup();
        QString address;

        if(language == "sv_SE") {
            address = QStringLiteral(VERSION_PATH_SV);
        } else {
            address = QStringLiteral(VERSION_PATH);
        }

        QTextStream(stdout) << QObject::tr("Current version:") << " " << VERSION << '\n' << QObject::tr("Latest version:") << " ";
        mLibcheckupdcli = new Libcheckupdcli;
        connect(mLibcheckupdcli, &Libcheckupdcli::sendVersion, this, &Cli::getVersion);
        mLibcheckupdcli->getVersionRequest(address);
    }
    /***/
    else if(language) {
        if(argumentlist.size() != 3) {
            QProcess *process = new QProcess(nullptr);
            process->deleteLater();
            process->setProcessChannelMode(QProcess::MergedChannels);
            QStringList ARG;
            ARG << QStringLiteral("-version");
            process->start("ffmpeg", ARG);
            QString in;

            while(process->waitForReadyRead()) {
                while(process->canReadLine()) {
                    in +=  process->readAllStandardOutput();
                }
            }

            QTextStream(stdout) << tr("Please select \'--lang en\', '--lang it' or \'--lang sv\'\n");
            exit(0);
        }

        if(argumentlist[2] == "en") {
            settings.beginGroup(QStringLiteral("Language"));
            settings.setValue(QStringLiteral("language"), QStringLiteral("en_US"));
            settings.endGroup();
            settings.sync();
            exit(0);
        } else if(argumentlist[2] == QStringLiteral("sv")) {
            settings.beginGroup("Language");
            settings.setValue(QStringLiteral("language"), QStringLiteral("sv_SE"));
            settings.endGroup();
            settings.sync();
            exit(0);
        } else if(argumentlist[2] == QStringLiteral("it")) {
            settings.beginGroup("Language");
            settings.setValue(QStringLiteral("language"), QStringLiteral("it_IT"));
            settings.endGroup();
            settings.sync();
            exit(0);
        } else {
            QTextStream(stdout) << tr("Please select \'--lang en\', '--lang it' or \'--lang sv\'\n");
            exit(0);
        }

        /***/
#ifdef FFMPEG
    } else if(ffmpeg) {
        QProcess *process  = new QProcess(nullptr);
        QStringList ARG;
        ARG << "-version";
        QString EXECUTE =
            QCoreApplication::applicationDirPath() + QStringLiteral("/ffmpeg");
        process->start(EXECUTE, ARG);
#if (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
        QObject::connect(process, &QProcess::finished, [process, this]() {
            QTextStream stream;
            QTextStream(stdout) << (process->readAllStandardOutput());
            exit(0);        // QTextStream(stdout) << (process->readAllStandardError() + '\n');
            /***/
        });

#endif
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
        QObject::connect(process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
        [process] {
            QTextStream stream;
            QTextStream(stdout) << (process->readAllStandardOutput());

            exit(0);
        });
#endif
        QObject::connect(process, &QProcess::errorOccurred, [process]() {
            QTextStream stream;
            QTextStream(stdout) << (process->readAllStandardError() + '\n');
        });

#endif // FFMPEG
    } else if(openSSLsystem) {
        QProcess *process = new QProcess(nullptr);
        process->deleteLater();
        process->setProcessChannelMode(QProcess::MergedChannels);
        QStringList ARG;
        ARG << "version" <<  "-a";
        process->start("openssl", ARG);
        QString in;

        while(process->waitForReadyRead()) {
            while(process->canReadLine()) {
                in +=  process->readAllStandardOutput();
            }
        }

        if(in.isEmpty()) {
            QTextStream(stdout) <<  tr("openSSL is not found on your system.\n");
        }  else {
            QTextStream(stdout) << in;
        }

        process->deleteLater();
        exit(0);
        /***/
    } else if(ffmpegSystem) {
        QProcess *process = new QProcess(nullptr);
        process->deleteLater();
        process->setProcessChannelMode(QProcess::MergedChannels);
        QStringList ARG;
        ARG << QStringLiteral("-version");
        process->start("ffmpeg", ARG);
        QString in;

        while(process->waitForReadyRead()) {
            while(process->canReadLine()) {
                in +=  process->readAllStandardOutput();
            }
        }

        if(in.isEmpty()) {
#ifdef FFMPEG
            QTextStream(stdout) <<  tr("FFmpeg not found. You can use FFmpeg which is included in the AppImage.\n");
#else
            QTextStream(stdout) <<  tr("FFmpeg not found. Install FFmpeg or download an AppImage that includes FFmpeg.\n");
#endif
        }  else {
            QTextStream(stdout) << in;
        }

        exit(0);
    } else if(GLIBC) {
        QProcess *process = new QProcess(nullptr);
        process->deleteLater();
        process->setProcessChannelMode(QProcess::MergedChannels);
        QStringList ARG;
        ARG << QStringLiteral("--version");
        process->start("ldd", ARG);
        QString in;

        while(process->waitForReadyRead()) {
            while(process->canReadLine()) {
                in +=  process->readAllStandardOutput();
            }
        }

        if(in.isEmpty()) {
            QTextStream(stdout) <<  tr("GLIBC not found. GLIBC is required to boot Linux.\n");
        }  else {
            QTextStream(stdout) << in;
        }

        exit(0);
    }  else if(openSSL) {
        QProcess *process  = new QProcess(nullptr);
        QStringList ARG;
        ARG << "version" <<  "-a";
        QString EXECUTE =
            QCoreApplication::applicationDirPath() + QStringLiteral("/openssl");
        process->start(EXECUTE, ARG);
#if (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
        QObject::connect(process, &QProcess::finished, [process, this]() {
            QTextStream stream;
            QTextStream(stdout) << (process->readAllStandardOutput());
            exit(0);
        });

#endif
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
        QObject::connect(process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
        [process] {
            QTextStream stream;
            QTextStream(stdout) << (process->readAllStandardOutput());

            exit(0);
        });
#endif
        QObject::connect(process, &QProcess::errorOccurred, [process]() {
            QTextStream stream;
            QTextStream(stdout) << (process->readAllStandardError() + '\n');
            // emit completed(false);
        });
    } else if(openSSLsystem) {
        QProcess *process = new QProcess(nullptr);
        process->deleteLater();
        process->setProcessChannelMode(QProcess::MergedChannels);
        QStringList ARG;
        ARG << "version";
        process->startDetached(QStringLiteral("openssl"), ARG);
        exit(0);
#if (QT_VERSION > QT_VERSION_CHECK(6, 0, 0))
        QObject::connect(process, &QProcess::finished, [process]() {
            QTextStream(stdout) << (process->readAllStandardOutput() + '\n');
        });

#else
        QObject::connect(process, static_cast<void(QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
        [process] {
            QTextStream(stdout) << (process->readAllStandardOutput());

        });
#endif
        process->deleteLater();
    } else if(license) {
        QFile fil(QStringLiteral(":/txt/LICENSE"));
        QString *intext = new QString;
        intext->clear();

        if(fil.open(QFile::ReadOnly | QFile::Text)) {
            QTextStream in(&fil);
            intext->append(in.readAll());
        }

        QTextStream(stdout) << *intext;
        exit(0);
    } else if(changelog) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup(QStringLiteral("Language"));
        QString language = settings.value(QStringLiteral("language"), currentLocale).toString();
        settings.endGroup();
        QString readchengelog;

        if(language == QStringLiteral("sv_SE")) {
            readchengelog = QStringLiteral(":/txt/CHANGELOG_sv_SE");
        } else {
            readchengelog = QStringLiteral(":/txt/CHANGELOG_en_US");
        }

        QFile fil(readchengelog);
        QTextStream in(&fil);
        int lines = 0;

        if(fil.open(QFile::ReadOnly | QFile::Text)) {
            while(!in.atEnd()) {
                QString line = in.readLine();
                lines++;
                QTextStream(stdout) << line << '\n';
                std::string in;

                if(lines > 30) {
                    QTextStream(stdout) << '\n' << tr("Click '1' to show more or 'Ctrl + C' to exit. :> ");
                    std::cin >> in;

                    if(in == "1") {
                        lines = 0;
                    } else {
                        exit(0);
                    }
                }
            }

            exit(0);
        }
    }

#endif
}

bool Cli::fileExists(QString & path)
{
    QFileInfo check_file(path);

    // check if file exists and if yes: Is it really a file and no directory?
    // and is it executable?
    if(check_file.exists() && check_file.isFile() && check_file.isExecutable()) {
        return true;
    } else {
        return false;
    }
}

void Cli::doupdate(bool update)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);

    if(update) {
        settings.beginGroup(QStringLiteral("Update"));
        settings.setValue(QStringLiteral("readyupdate"), true);
        settings.endGroup();
    } else {
        settings.beginGroup(QStringLiteral("Update"));
        settings.setValue(QStringLiteral("readyupdate"), false);
        settings.endGroup();
    }
}

void Cli::getVersion(QString version)
{
    QTextStream(stdout)  << version;
    exit(0);
}

// public slots:
void Cli::getCHECKSUM(QString checksum)
{
    QString hash = checksum.left(64);
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Language"));
    QString lang = settings.value(QStringLiteral("language")).toString();
    settings.endGroup();
    QTranslator *translator = new QTranslator;

    if(translator->load(":/i18n/_complete_lin_" + lang + ".qm")) {
        if(QCoreApplication::installTranslator(translator)) {
        } else {
            QTextStream(stdout) << tr("Unable to load the language file.");
        }
    }

    QString expectedLabel = tr("Expected checksum:");
    QString calculatedLabel = tr("Calculated checksum:");
    QString lika = (hash == globalhashsum ? tr("The checksums are identical.") + "\'"  DISPLAY_NAME  "\'" + tr(" is genuine.") + '\n' : tr(" The checksums are not identical. ") + "\'"  DISPLAY_NAME  "\'" + tr(" is not genuine") + '\n');
    // Bestäm en bredd för etiketterna (t.ex. 25 tecken) och skapa formaterade strängar.
    int labelWidth = 28;
    QTextStream(stdout)
            << QString("%1 %2 (SHA256)\n")
            .arg(expectedLabel, labelWidth)
            .arg(hash)
            << QString("%1 %2 (SHA256)\n")
            .arg(calculatedLabel, labelWidth)
            .arg(globalhashsum)
            << QString("%3")
            .arg(lika, 0);
    exit(0);
}
