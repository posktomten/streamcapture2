// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "ui_newprg.h"
#include <QTimer>
#include <QMessageBox>
#include <QSettings>
#include <QInputDialog>
void Newprg::listAllEpisodes()
{
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, [this]() {
        if(ui->progressBar->value() >= 100) {
            ui->progressBar->setValue(0);;
        }

        ui->progressBar->setValue(ui->progressBar->value() + 1);
    });

    timer->start(100);

    if(!fileExists(svtplaydl)) {
        timer->stop();
        ui->progressBar->setValue(0);;
        QMessageBox msgBox1;
        msgBox1.setIcon(QMessageBox::Critical);
        msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox1.setText(tr("svtplay-dl cannot be found.") + '\n' + tr("Or is not an executable program.") + '\n' + tr("Please click on \"svtplay-dl\" and select svtplay-dl."));
        msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox1.exec();
        svtplaydl = "NOTFIND";
        ui->actionAddAllEpisodesToDownloadList->setEnabled(false);
        return;
    }

    QStringList ARG;
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Stcookies"));
    QString stcookie = settings.value(QStringLiteral("stcookie"), "").toString();
    settings.endGroup();

    if(!stcookie.isEmpty()) {
        ARG << QStringLiteral("--cookies")  << QStringLiteral("st=") + stcookie;
    }

    ui->pbAdd->setDisabled(true);
    ui->actionAdd->setDisabled(true);
    // ui->actionAddAllEpisodesToDownloadList->setEnabled(true);
    ui->pbDownloadAll->setDisabled(true);
    ui->comboBox->clear();
    ui->leQuality->setText(tr("Searching..."));
    ui->leMethod->setText(tr("Searching..."));
    ui->teOut->setText(tr("The request is processed...\nStarting search..."));
    ui->pbSok->setEnabled(false);
    ui->actionSearch->setEnabled(false);
    // ui->actionListAllEpisodes->setEnabled(false);
    QTimer::singleShot(2000, this, [this] {
        ui->pbSok->setEnabled(true);
        ui->actionSearch->setEnabled(true);
        // ui->actionListAllEpisodes->setEnabled(true);
    });
    ui->teOut->setReadOnly(true);
    QString sok = ui->leSok->text();
    sok = sok.simplified();

    if(sok.isEmpty()) {
        ui->teOut->setText(tr("The search field is empty!"));
        ui->leQuality->setText("");
        ui->leMethod->setText("");
        ui->actionDownload->setEnabled(false);
        ui->pbDownload->setEnabled(false);
        ui->actionAddAllEpisodesToDownloadList->setEnabled(false);
        timer->stop();
        ui->progressBar->setValue(0);
        return;
    } else if((sok.left(7) != QStringLiteral("http://")) && (sok.left(8) != QStringLiteral("https://"))) {
        ui->teOut->setText(tr("Invalid link"));
        ui->actionDownload->setEnabled(false);
        ui->pbDownload->setEnabled(false);
        ui->actionAddAllEpisodesToDownloadList->setEnabled(false);
        ui->leQuality->setText("");
        ui->leMethod->setText("");
        timer->stop();
        ui->progressBar->setValue(0);
    } else {
        address = ui->leSok->text().trimmed();
        int questionmark = address.indexOf('?');
        address = address.left(questionmark);

        // Hit

        if(address.contains("https://www.tv4play.se")) {
            // tv4token
            /*
            QMessageBox msgBox1;
            msgBox1.setIcon(QMessageBox::Critical);
            msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox1.setText(tr("Unfortunately, \"List all Episodes\" from Swedish TV4 do not work."));
            msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox1.exec();
            return;
            */
            settings.beginGroup(QStringLiteral("Stcookies"));
            QString tv4token = settings.value(QStringLiteral("tv4token"), "").toString();
            settings.endGroup();

            if(!tv4token.isEmpty()) {
                ARG << QStringLiteral("--token") << tv4token;
            } else {
                QMessageBox msgBox1;
                msgBox1.setIcon(QMessageBox::Critical);
                msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox1.setText(tr("You need to copy the TV4 token from your browser and save it in streamCapture2."));
                msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox1.exec();
                timer->stop();
                ui->progressBar->setValue(0);;
                ui->actionAddAllEpisodesToDownloadList->setEnabled(false);
                return;
            }
        }

        /*   */
        settings.beginGroup(QStringLiteral("Settings"));
        bool try4k = settings.value("try4k", false).toBool();
        settings.endGroup();

        if(try4k) {
            ARG << QStringLiteral("--format") << QStringLiteral("dvhevc-51,dvhevc,hevc-51,hevc");
        }

        /*   */

        if(ui->comboPayTV->currentIndex() == 0) {  // No Password
            ARG << QStringLiteral("-A")
                << QStringLiteral("--get-only-episode-url") << address;
        } else { // Password
            QString provider = ui->comboPayTV->currentText();
            settings.beginGroup("Provider");
            QString username = settings.value(provider + QStringLiteral("/username")).toString();
            QString password = settings.value(provider + QStringLiteral("/password")).toString();
            settings.endGroup();

            if(password == "") {
                password = secretpassword;

                if(password == "") {
                    bool ok;
                    QString newpassword = QInputDialog::getText(
                                              nullptr, provider + " " + tr("Enter your password"),
                                              tr("Spaces are not allowed. Use only the characters your "
                                                 "streaming provider approves.\nThe Password will not be "
                                                 "saved."),
                                              QLineEdit::Password, "", &ok);

                    if(newpassword.indexOf(' ') >= 0) {
                        timer->stop();
                        ui->progressBar->setValue(0);;
                        ui->actionAddAllEpisodesToDownloadList->setEnabled(false);
                        return;
                    }

                    if(ok) {
                        secretpassword = newpassword;
                        password = newpassword;
                    }
                }
            }

// tesstsearch
//            QString test = doTestSearch(address, username, password);
//            if(test != "") {
//                ui->teOut->setText(test);
//                return;
//            }
            ARG << QStringLiteral(u"-A")
                << "--get-only-episode-url"
                << "--username=" + username << "--password=" + password << address;
        }

        CommProcess2 = new QProcess(nullptr);
        CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
        CommProcess2->start(svtplaydl, ARG);
        // processpid = CommProcess2->processId();
        connect(CommProcess2, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
                this, &Newprg::onCommProcessExit_sok);
        connect(CommProcess2, &QProcess::readyReadStandardOutput, this, [this]() {
            QString result(CommProcess2->readAllStandardOutput());
            QString translate;

            if(result.contains(QStringLiteral("ERROR:"))) {
                ui->actionAddAllEpisodesToDownloadList->setEnabled(false);
            }

            if(result.contains(QStringLiteral("ERROR: Can't find any videos"))) {
                translate = tr("ERROR: Can't find any videos.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("ERROR: Cant find any videos"))) {
                translate = tr("ERROR: Can't find any videos.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            /*       FROM sok.cpp     */
            if(result.contains(QStringLiteral("ERROR: No videos found. Cant find video id for the video"))) {
                translate  = tr("ERROR: No videos found. Can't find video id for the video.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. Can't find video id for the video"))) {
                translate  = tr("ERROR: No videos found. Can't find video id for the video.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("WARNING: Use program page instead of the clip / video page"))) {
                translate = tr("WARNING: Use program page instead of the clip / video page.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. Can't find video id for the video"))) {
                translate = tr("ERROR: No videos found. Can't find video id for the video.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
                ;
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. Can't find video id"))) {
                translate  = tr("ERROR: No videos found. Can't find video id.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. Can't decode api request:"))) {
                translate = tr("ERROR: No videos found. Can't decode api request.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. Use the video page not the series page"))) {
                translate = tr("ERROR: No videos found. Use the video page not the series page.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. Can't find any videos. Is it removed?"))) {
                translate = tr("ERROR: No videos found. Can't find any videos. Is it removed?");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("ERROR: No videos found. Can't find the video file"))) {
                translate = tr("ERROR: No videos found. Can't find the video file.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            // newerror
            if(result.contains(QStringLiteral("ERROR: Run again and add --verbose as an argument, to get more information"))) {
                translate = tr("ERROR: Click on \"Tools\", \"Show more\" and run again to get more information.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            // newerror
            if(result.contains(QStringLiteral("ERROR: If the error persists, you can report it at https://github.com/spaam/svtplay-dl/issues"))) {
                translate = tr("ERROR: If the error persists, you can report it at https://github.com/spaam/svtplay-dl/issues");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            // newerror
            if(result.contains(QStringLiteral("ERROR: Include the URL used, the stack trace and the output of svtplay-dl --version in the issue"))) {
                translate = tr("ERROR: Include URL and error message in problem description.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("ERROR: Can't find any videos."))) {
                translate = tr("ERROR: Can't find any videos.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            if(result.contains(QStringLiteral("WARNING: --all-episodes not implemented for this service"))) {
                translate = tr("WARNING: --all-episodes not implemented for this service.");
                translate = "<br>" + translate;
                ui->teOut->append(translate);
                timer->stop();
                ui->progressBar->setValue(0);
            }

            /*       *********************     */

            if(result.mid(0, 25) == QStringLiteral("ERROR: svtplay-dl crashed")) {
                ui->teOut->append(svtplaydl + tr(" crashed."));
                ui->teOut->append(
                    tr("Can not find any video streams, please check the address."));
                ui->actionAddAllEpisodesToDownloadList->setEnabled(false);
                return;
            }

            if(result.mid(0, 34) == QStringLiteral("Traceback (most recent call last):")) {
                return;
            }

            if(result.mid(0, 6) != QStringLiteral("ERROR:")) {
                result = result.simplified();
                result.replace("Url: ", "");
                QStringList list = result.split(QStringLiteral("INFO:"), Qt::SkipEmptyParts);
                list.removeDuplicates();
                /*nnnnn*/
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

                for(const QString &temp : std::as_const(list)) {
                    ui->teOut->append(temp.trimmed());
                }

#else

                for(const QString &temp : qAsConst(list)) {
                    ui->teOut->append(temp.trimmed());
                }

#endif
                /*nnnnn*/
                // for(const QString &temp : list) {
                //     ui->teOut->append(temp.trimmed());
                // }
            } else {
                ui->teOut->append(result);
                // timer->stop();
                // ui->progressBar->setValue(0);;
            }

            // timer->stop();
            // ui->progressBar->setValue(0);;
        });
    }

    timer->stop();
    ui->progressBar->setValue(0);;
    ui->actionAddAllEpisodesToDownloadList->setEnabled(true);
}
