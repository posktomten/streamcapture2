// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#include "newprg.h"
#include <QStandardPaths>
#include <QSettings>
#include <QFileInfo>
#include <QFileDialog>
#include <QMessageBox>
void Newprg::_newDownloadList()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Path"));
    QString downloadlistfile = settings.value(QStringLiteral("downloadlistfile")).toString();
    settings.endGroup();
    QString downloadlistpath;
    QString settingsfile = settings.fileName();
    QFileInfo fi(settingsfile);
    QString settingspath = fi.absolutePath();

    if(downloadlistfile.isEmpty()) {
        downloadlistpath = settingspath;
    } else {
        downloadlistpath = fi.absolutePath();
    }

    QFileDialog filedialog(nullptr);
    filedialog.setAcceptMode(QFileDialog::AcceptSave);
    int x = this->x();
    int y = this->y();
    filedialog.setGeometry(x + 50, y + 50, 900, 550);
    filedialog.setDirectory(downloadlistpath);
    filedialog.setLabelText(QFileDialog::Accept, tr("Save"));
    filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
    filedialog.setWindowTitle(tr("New downloadlist"));
    filedialog.setNameFilter(tr("Download list files (*.lst)"));

    if(filedialog.exec() == QDialog::Rejected) {
        return;
    }

    QStringList downloadlistfiles = filedialog.selectedFiles();
    downloadlistfile = downloadlistfiles.at(0);

    if(downloadlistfile.right(4) != ".lst") {
        downloadlistfile.append(".lst");
    }

    QFile file(downloadlistfile);

    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(DISPLAY_NAME + tr(" is not allowed to save ") + "\n\"" + downloadlistfile + "\"" + '\n' + tr(" Check your file permissions or choose another location."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

    file.close();
    settings.beginGroup(QStringLiteral("Path"));
    settings.setValue(QStringLiteral("downloadlistfile"), downloadlistfile);
    settings.endGroup();
}



void Newprg::_importDownloadList()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Path");
    QString downloadlistfile = settings.value(QStringLiteral("downloadlistfile")).toString();
    settings.endGroup();
    QString downloadlistpath;
    QString settingsfile = settings.fileName();
    QFileInfo fi(settingsfile);
    QString settingspath = fi.absolutePath();

    if(downloadlistfile.isEmpty()) {
        downloadlistpath = settingspath;
    } else {
        downloadlistpath = fi.absolutePath();
    }

    QFileDialog filedialog(nullptr);
    filedialog.setAcceptMode(QFileDialog::AcceptOpen);
    int x = this->x();
    int y = this->y();
    filedialog.setGeometry(x + 50, y + 50, 900, 550);
    filedialog.setDirectory(downloadlistpath);
    filedialog.setLabelText(QFileDialog::Accept, tr("Import"));
    filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
    filedialog.setWindowTitle(tr("Import a download list"));
    filedialog.setNameFilter(tr("Download list files (*.lst)"));

    if(filedialog.exec() == QDialog::Rejected) {
        return;
    }

    QString secondary = filedialog.selectedFiles().at(0);
    fi.setFile(secondary);

    if(fi.size() > 0) {
        ui->actionDownloadAll->setEnabled(true);
        ui->pbDownloadAll->setEnabled(true);
    } else {
        ui->actionDownloadAll->setEnabled(false);
        ui->pbDownloadAll->setEnabled(false);
    }

    QFile file(secondary);

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(DISPLAY_NAME + tr(" is not allowed to import ") + "\n\"" + QDir::toNativeSeparators(secondary) + "\"" + '\n' + tr(" Check your file permissions."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

    file.close();
    settings.beginGroup("Path");
    settings.setValue("downloadlistfile", secondary);
    settings.endGroup();
}

void Newprg::_exportDownloadList()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Path");
    QString downloadlistfile = settings.value(QStringLiteral("downloadlistfile")).toString();
    settings.endGroup();
    QString downloadlistpath;
    QString settingsfile = settings.fileName();
    QFileInfo fi(settingsfile);
    QFileInfo fi2(downloadlistfile);

    if(!fi2.exists()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("In order to export a Download list file, you must first create or import such a file."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    } else {
        downloadlistpath = fi.absolutePath();
    }

    QFileDialog filedialog(nullptr);
    filedialog.setOptions(QFileDialog::DontUseNativeDialog);
    filedialog.setAcceptMode(QFileDialog::AcceptSave);
    int x = this->x();
    int y = this->y();
    filedialog.setGeometry(x + 50, y + 50, 900, 550);
    filedialog.setDirectory(downloadlistpath);
    filedialog.setLabelText(QFileDialog::Accept, tr("Export"));
    filedialog.setLabelText(QFileDialog::Reject, tr("Cancel"));
    filedialog.setWindowTitle(tr("Export ") + "\"" + QDir::toNativeSeparators(downloadlistfile) + "\"");
    filedialog.setNameFilter(tr("Download list files (*.lst)"));

    if(filedialog.exec() == QDialog::Rejected) {
        return;
    }

    QFile primary(downloadlistfile);
    QString secondary = filedialog.selectedFiles().at(0);

    if(secondary.right(5) != ".lst") {
        secondary.append(".lst");
    }

    if(primary.exists() && primary.copy(secondary)) {
    } else {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(DISPLAY_NAME + tr(" is not allowed to save ") + "\n\"" + QDir::toNativeSeparators(downloadlistfile) + "\"" + '\n' + tr(" Check your file permissions or choose another location."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }
}
