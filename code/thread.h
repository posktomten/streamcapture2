//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          Program name
//          Copyright (C) 2024 Ingemar Ceicer
//          https://gitlab.com/posktomten
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef THREAD_H
#define THREAD_H

#include "downloadunpack.h"
#include <QObject>
#include <QThread>
class MyThread : public QThread
{
    Q_OBJECT
public:

private:
    void run() override
    {
        DownloadUnpack *du = new DownloadUnpack;
        QThread *thread = new QThread(0);
        du->moveToThread(thread);
        // du->show();
    }

public:
    void doRun()
    {
        run();
    }

signals:

};

#endif // THREAD_H
