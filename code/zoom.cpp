// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include <QSettings>
void Newprg::zoom()
{
    connect(ui->actionDefault, &QAction::triggered, this, &Newprg::zoomDefault);
    connect(ui->actionOut, &QAction::triggered,  this, &Newprg::zoomMinus);
    connect(ui->actionIn, &QAction::triggered, this, &Newprg::zoomPlus);
    connect(ui->teOut, &QTextEdit::textChanged, this, &Newprg::textBrowserTeOutTextChanged);
    connect(ui->actionDefault, &QAction::hovered, this, [this]() {
        QFont f = ui->teOut->currentFont();
        int size = f.pointSize();
        ui->actionDefault->setStatusTip(tr("The font size changes to the selected font size") + " (" + QString::number(size) + ").");
    });
}

void Newprg::textBrowserTeOutTextChanged()
{
    QTextCursor cursor = ui->teOut->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->teOut->setTextCursor(cursor);
}

void Newprg::zoomDefault()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    QFont applicationfont = QApplication::font();
    int default_pointsize = applicationfont.pointSize();
    settings.beginGroup("Font");
    // QFont FONT;
    int pointsize = settings.value("size", default_pointsize).toInt();
    settings.endGroup();
    QFont f = ui->teOut->font();
    f.setPointSize(pointsize);
    ui->teOut->setFont(f);
    QTextCursor cursor = ui->teOut->textCursor();
    ui->teOut->selectAll();
    ui->teOut->setFontPointSize(pointsize);
    ui->teOut->setTextCursor(cursor);
    // ui->teOut->zoomOut();
}

void Newprg::zoomMinus()
{
    QFont f = ui->teOut->font();
    int pointsize = f.pointSize();

    if(pointsize > 5) {
        QTextCursor cursor = ui->teOut->textCursor();
        ui->teOut->selectAll();
        ui->teOut->setFontPointSize(pointsize - 1);
        ui->teOut->setTextCursor(cursor);
        ui->teOut->zoomOut(1);
    }
}

void Newprg::zoomPlus()
{
    QFont f = ui->teOut->font();
    int pointsize = f.pointSize();

    if(pointsize < 34) {
        QTextCursor cursor = ui->teOut->textCursor();
        ui->teOut->selectAll();
        ui->teOut->setFontPointSize(pointsize  + 1);
        ui->teOut->setTextCursor(cursor);
        ui->teOut->zoomIn(1);
    }
}

// void Newprg::wheelEvent(QWheelEvent *ev)
// {
//     if(QApplication::queryKeyboardModifiers() == Qt::ControlModifier) {
//         QPoint numDegrees = ev->angleDelta();
//         int direction = numDegrees.y();

//         if(direction < 0) {
//             zoomMinus();
//         } else if(direction > 0) {
//             zoomPlus();
//         }

//         ev->accept();
//     }
// }
