// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
void Newprg::dragEnterEvent(QDragEnterEvent *ev)
{
    ev->accept();
}

void Newprg::dropEvent(QDropEvent *ev)
{
    QList<QUrl> urls = ev->mimeData()->urls();
    QUrl url(urls.at(0));

    if(url.isValid()) {
        QString urlstring = url.toString();

        if((urlstring.left(7) != QStringLiteral("http://")) && (urlstring.left(8) != QStringLiteral("https://"))) {
            return;
        }

        ui->leSok->setText(urlstring);
    }
}
