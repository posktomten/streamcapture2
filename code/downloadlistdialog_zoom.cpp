//gitlab.com/posktomten/appimagehelper/-/wikis/DOWNLOADS
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2 (DownloadListDialog)
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "downloadlistdialog.h"
#include "ui_downloadlistdialog.h"
#include "newprg.h"
#include <QSettings>

void DownloadListDialog::textBrowsertextEditTextChanged()
{
    QTextCursor cursor = ui->textEdit->textCursor();
    cursor.movePosition(QTextCursor::End);
    ui->textEdit->setTextCursor(cursor);
}

void DownloadListDialog::zoomDefault()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    QFont applicationfont = QApplication::font();
    int default_pointsize = applicationfont.pointSize();
    settings.beginGroup(QStringLiteral("Font"));
    // QFont FONT;
    int pointsize = settings.value(QStringLiteral("size"), default_pointsize).toInt();
    settings.endGroup();
    QFont f = ui->textEdit->font();
    f.setPointSize(pointsize);
    ui->textEdit->setFont(f);
    QTextCursor cursor = ui->textEdit->textCursor();
    ui->textEdit->selectAll();
    ui->textEdit->setFontPointSize(pointsize);
    ui->textEdit->setTextCursor(cursor);
    // ui->teOut->zoomOut();
}

void DownloadListDialog::zoomMinus()
{
    QFont f = ui->textEdit->font();
    int pointsize = f.pointSize();

    if(pointsize > 5) {
        QTextCursor cursor = ui->textEdit->textCursor();
        ui->textEdit->selectAll();
        ui->textEdit->setFontPointSize(pointsize - 1);
        ui->textEdit->setTextCursor(cursor);
        ui->textEdit->zoomOut(1);
    }
}

void DownloadListDialog::zoomPlus()
{
    QFont f = ui->textEdit->font();
    int pointsize = f.pointSize();

    if(pointsize < 34) {
        QTextCursor cursor = ui->textEdit->textCursor();
        ui->textEdit->selectAll();
        ui->textEdit->setFontPointSize(pointsize  + 1);
        ui->textEdit->setTextCursor(cursor);
        ui->textEdit->zoomIn(1);
    }
}
