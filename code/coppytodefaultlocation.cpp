// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include <QSettings>
#include <QMessageBox>
#include <QFile>
#include <QDir>
void Newprg::copyToDefaultLocation(QString filnamn,
                                   const QString *folderaddress, const QString *copyto)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    // settings.setIniCodec("UTF-8");
    settings.beginGroup("Path");
//    QString savepath = settings.value("savepath").toString();
    QString copypath = settings.value("copypath").toString();
    settings.endGroup();

    if(copypath.isEmpty()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("You have not selected any place to copy the media files.\nPlease select a location before proceeding."));
//        msgBox.addButton(QMessageBox::Ok);
//        msgBox.setButtonText(QMessageBox::Ok, tr("Ok"));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

    QString old = *folderaddress + "/" + filnamn;  // Direct Download
    QString ny;

    if(copyto->isEmpty()) {
        ny = copypath + "/" + filnamn;
    } else {
        ny = copypath  + *copyto + "/" + filnamn;
    }

    QFile f(ny);
    QDir dir(copypath + *copyto);

    if(!dir.exists()) {
        dir.mkpath(".");
    }

    if(f.exists()) {
        ui->teOut->append(tr("A file with the same name already exists. The file will not be copied.") + " (" + filnamn + ")");
        return;
    }

    if(QFile::copy(old, ny)) {
        ui->teOut->append(tr("Copy succeeded") + " \"" + filnamn + "\"");
    } else {
        ui->teOut->append(tr("Copy failed") + " \"" + filnamn + "\"");
    }

    int langd = old.size();
    QString undertexten = old.replace(langd - 3, 3, "srt");
    /* */
    int langd2 = filnamn.size();
    QString undertextfilen = filnamn.replace(langd2 - 3, 3, "srt");

    if(QFile::exists(undertexten)) {
        langd = ny.size();
        QString ny_undertexten = ny.replace(langd - 3, 3, "srt");

        if(QFile::exists(ny_undertexten)) {
            QFile::remove(ny_undertexten);
        }

        if(QFile::copy(undertexten, ny_undertexten)) {
            ui->teOut->append(tr("Copy succeeded") + " \"" + undertextfilen + "\"");
        } else {
            ui->teOut->append(tr("Copy failed") + " \"" + undertextfilen + "\"");
        }
    }
}


