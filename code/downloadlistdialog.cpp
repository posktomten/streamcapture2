
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2 (DownloadListDialog)
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "downloadlistdialog.h"
#include "ui_downloadlistdialog.h"
#include "zoom_disable.h"
#include <QTextStream>
#include <QMessageBox>
#include <QFileDialog>
#include <QFontDatabase>
#include <QSettings>
#include <QStandardPaths>
#include <QRegularExpression>
#include <QCloseEvent>
#include <QShortcut>
DownloadListDialog::DownloadListDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DownloadListDialog)
{
    ui->setupUi(this);
    ui->textEdit->setStyleSheet("QTextEdit { padding-left:4; padding-right:4}");
    ui->pbRemoveInvaliLinks->setVisible(false);
    QShortcut *zoomD = new QShortcut((QKeySequence(Qt::CTRL | Qt::Key_0)),
                                     this);
    connect(zoomD, &QShortcut::activated, this, &DownloadListDialog::zoomDefault);
    ///
    QShortcut *zoomM = new QShortcut((QKeySequence(Qt::CTRL | Qt::Key_Minus)),
                                     this);
    connect(zoomM, &QShortcut::activated, this, &DownloadListDialog::zoomMinus);
    ///
    QShortcut *zoomP = new QShortcut((QKeySequence(Qt::CTRL | Qt::Key_Plus)),
                                     this);
    connect(zoomP, &QShortcut::activated, this, &DownloadListDialog::zoomPlus);
    ///
#ifdef Q_OS_WINDOWS
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    this->setWindowFlags(this->windowFlags() & ~Qt::WindowContextHelpButtonHint);
#endif
#endif
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Downloadlistdialog");
    restoreGeometry(settings.value("geometry").toByteArray());
    settings.endGroup();
// change textedit palette
    /* Font */
#if defined(Q_OS_WINDOWS)
    int id = QFontDatabase::addApplicationFont(":/fonts/segoeui.ttf");
#endif
#if defined(Q_OS_LINUX)
    int id = QFontDatabase::addApplicationFont(":/fonts/Ubuntu-M.ttf");
#endif
    QString _family = QFontDatabase::applicationFontFamilies(id).at(0);
    QFont systemfont(_family, FONTSIZE);
    QGuiApplication::setFont(systemfont);
    settings.beginGroup("Font");
    QFont font = settings.value(QStringLiteral("currentfont"), QFont()).value<QFont>();
    settings.endGroup();
    QTextCursor cursor = ui->textEdit->textCursor();
    ui->textEdit->selectAll();
    ui->textEdit->setTextCursor(cursor);
    /** REMOVE INVALD LINKS **/
    connect(ui->pbRemoveInvaliLinks, &QPushButton::clicked, this, [this]() {
        QString *data =  new QString(ui->textEdit->toPlainText());
        QStringList contents = data->split('\n');
        contents.removeAll(QString(""));
        static const QRegularExpression pattern(QStringLiteral("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)"));
        QStringList matchings;

        foreach(QString s, contents) {
            bool is_match = true;
            QRegularExpressionMatch match = pattern.match(s);

            if(!match.hasMatch()) {
                is_match = false;
            }

            int hitta = s.indexOf(",");
            QString is = s.mid(hitta);
            hitta = is.count(QLatin1Char(','));

            if((is_match) && (hitta == 7)) {
                matchings += (s + '\n');
            } else {
                continue;
            }
        }

        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Path");
        QString downloadlistfile = settings.value(QStringLiteral("downloadlistfile")).toString();
        settings.endGroup();
        QFileInfo fi(downloadlistfile);

        if(!fi.exists()) {
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Critical);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("An unexpected error has occurred.\nThe file with saved links to downloadable files cannot be found.\nCheck that the file has not been deleted or moved."));
            msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox->exec();
            return;
        } else {
            QFile file(downloadlistfile);
            QTextStream infile(&file);

            /***/
            if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("An unexpected error has occurred.\nThe file with saved links to downloadable files cannot be found.\nCheck that the file has not been deleted or moved."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                return;
            } else {
                foreach(QString s, matchings) {
                    infile << s;
                }

                file.close();
                ui->textEdit->clear();
                // QByteArray line;
                QStringList contents;

                if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Critical);
                    msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                    msgBox.setText(tr("An unexpected error has occurred.\nThe file with saved links to downloadable files cannot be found.\nCheck that the file has not been deleted or moved."));
                    msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                    msgBox.exec();
                } else {
                    while(!file.atEnd()) {
                        QByteArray line = file.readLine();
                        contents << line;
                    }
                }

                file.close();
                contents.removeAll(QString(""));

                foreach(QString s, contents) {
                    s.remove('\n');
                    ui->textEdit->append(s);
                }
            }

            /***/
        }

        this->setWindowTitle(tr("Edit the download list (") + QString::number(matchings.count()) + tr(" Video streams)"));
    });

    connect(ui->pbSave, &QPushButton::clicked, this, [this]() {
        QString *data =  new QString(ui->textEdit->toPlainText());
        emit sendContent(data);
    });

    connect(ui->pbSaveAs, &QPushButton::clicked, this, [ this ]() {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                           EXECUTABLE_NAME);
        settings.beginGroup("Downloadlistdialog");
        QString copyfileto = settings.value("copyfileto", QStandardPaths::writableLocation(QStandardPaths::HomeLocation)).toString();
        settings.endGroup();
        int x = this->x();
        int y = this->y();
        QFileDialog *dialog = new QFileDialog(this);
        dialog->setLabelText(QFileDialog::Accept, tr("Save"));
        dialog->setLabelText(QFileDialog::Reject, tr("Cancel"));
        dialog->setWindowTitle(tr("Save as text file"));
        dialog->setGeometry(x + 50, y + 50, 900, 550);
        dialog->setViewMode(QFileDialog::Detail);
        dialog->setFileMode(QFileDialog::AnyFile);
        dialog->setDirectory(copyfileto);
        dialog->setAcceptMode(QFileDialog::AcceptSave);
        QStringList filters;
#if defined(Q_OS_LINUX)
        filters << tr("Text file") + " (*.txt)" << tr("Text file, optional extension") + " (*)";
        dialog->setNameFilters(filters);
#elif defined(Q_OS_WINDOWS)
        filters << tr("Text file") + " (*.txt)" << tr("Text file, optional extension") + " (*.*)";
        dialog->setNameFilters(filters);
#endif

        if(dialog->exec() == 1) {
#if defined(Q_OS_LINUX)

            if(dialog->selectedNameFilter() == tr("Text file") + " (*.txt)") {
                dialog->setDefaultSuffix(".txt");
            }

#endif
            QString newfile = dialog->selectedFiles().at(0);
            QFileInfo fi(newfile);
            QString path = fi.absolutePath();
            QFileInfo fi1(path);

            if(!fi1.isWritable()) {
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Critical);
                msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox.setText(tr("Could not save the file.\nCheck your file permissions."));
                msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox.exec();
                return;
            }

            QFile file_newfile(newfile);
            QString text = ui->textEdit->toPlainText();
            static QRegularExpression qre("[\r\n]");
            static QStringList list = text.split(qre, Qt::KeepEmptyParts);
            file_newfile.open(QIODevice::WriteOnly | QIODevice::Text);
            QTextStream outStream(&file_newfile);
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

            for(const QString &s : std::as_const(list)) {
                outStream << s + '\n';
            }

#else

            for(const QString &s : qAsConst(list)) {
                outStream << s + '\n';
            }

#endif
            file_newfile.close();
            fi.setFile(file_newfile);
            copyfileto = fi.absolutePath();
            settings.beginGroup(QStringLiteral("Downloadlistdialog"));
            settings.setValue(QStringLiteral("copyfileto"), copyfileto);
            settings.endGroup();
        }
    });

    connect(ui->pbClose, &QPushButton::clicked, this, [this]() {
        close();
    });

    // QFont f2(family, size);
    // ui->textEdit->setFont(font);
    ui->textEdit->setFont(font);
}

void DownloadListDialog::initiate(const QStringList * instructions, const QString * content, bool readonly, bool removeinvalidlinks)
{
    if(removeinvalidlinks) {
        ui->pbRemoveInvaliLinks->setVisible(true);
    }

    initiate(instructions, content, readonly);
}

// 1pac 1
void DownloadListDialog::initiate(const QStringList * instructions, const QString * content, bool readonly)
{
    this->setWindowTitle(instructions->at(0));
    ui->textEdit->clear();

    if(readonly) {
        ui->textEdit->setReadOnly(true);
        ui->pbSave->setDisabled(true);
        ui->pbSave->hide();
    }

    QStringList list = content->split('\n');

    foreach(QString s, list) {
        ui->textEdit->append(s);
    }

    if(instructions->size() == 3) {
        // foreach(QString s, list) {
        ui->textEdit->append(instructions->at(2));
    }

    ui->textEdit->moveCursor(QTextCursor::Start);
    this->show();
}

void DownloadListDialog::openFile(const QStringList * instructions, const QString *filename, bool readonly)
{
    this->setWindowTitle(instructions->at(0));
    ui->textEdit->clear();
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup("Font");
    QFont font = settings.value(QStringLiteral("currentfont"), QFont()).value<QFont>();
    settings.endGroup();
    ui->textEdit->setFont(font);

    if(readonly) {
        ui->textEdit->setReadOnly(true);
        ui->pbSave->setDisabled(true);
        ui->pbSave->hide();
    }

    // 3 argument
    //**************************************************************************
    if(instructions->length() == 3) {
        if(instructions->at(1) == QString(QStringLiteral("wordwrap"))) {
            ui->textEdit->setWordWrapMode(QTextOption::WordWrap);
        } else {
            ui->textEdit->setWordWrapMode(QTextOption::WrapAnywhere);
        }

        QTextDocument *tdoc = new QTextDocument;
        tdoc->setDefaultStyleSheet(instructions->at(2));
        QFile file(*filename);
        QString content;

        if(!file.exists()) {
            int hittat = filename->lastIndexOf('/');
            QString filnamnet = filename->mid(hittat + 1);
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setWindowTitle(tr("Unable to find file"));
            msgBox.setText(tr("Unable to find") + " \"" + filnamnet + "\"");
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
        } else {
            file.open(QIODevice::ReadOnly);

            while(!file.atEnd()) {
                content += file.readLine();
            }

            file.close();
        }

        tdoc->setHtml(QStringLiteral("<html><head><title>Important</title></head><body>") + content + QStringLiteral("</body></html>"));
        ui->textEdit->setDocument(tdoc);
        textBrowsertextEditTextChanged();
        this->show();
        ui->textEdit->moveCursor(QTextCursor::Start);
        zoomDefault();
        return;
        // 3 argument end
        //******************************************************************
        // 2 argument
    } else if(instructions->length() == 2) {
        if(instructions->at(1) == QStringLiteral("wordwrap")) {
            ui->textEdit->setWordWrapMode(QTextOption::WordWrap);
        } else {
            ui->textEdit->setWordWrapMode(QTextOption::WrapAnywhere);
        }

        QFile file(*filename);
        QString content;

        if(!file.exists()) {
            int hittat = filename->lastIndexOf('/');
            QString filnamnet = filename->mid(hittat + 1);
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setWindowTitle(tr("Unable to find file"));
            msgBox.setText(tr("Unable to find") + " \"" + filnamnet + "\"");
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
        } else {
            file.open(QIODevice::ReadOnly);

            while(!file.atEnd()) {
                content += file.readLine();
            }

            file.close();
        }

        ui->textEdit->append(content);
        ui->textEdit->moveCursor(QTextCursor::Start);
        this->show();
        // 2 argument end
    } else {
        return;
    }
}

void DownloadListDialog::closeEvent(QCloseEvent * e)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Downloadlistdialog"));
    settings.setValue(QStringLiteral("geometry"), saveGeometry());
    settings.endGroup();
    e->accept();
}

// public slots
void DownloadListDialog::getNewFont(QFont font)
{
    ui->textEdit->setFont(font);
}

DownloadListDialog::~DownloadListDialog()
{
    delete ui;
}
