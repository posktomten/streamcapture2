// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef NEWPRG_H
#define NEWPRG_H
#include <QWidget>

// EDIT All
#define VERSION "3.0.1"
// #define BETA
#define FFMPEG

#ifdef BETA
#define DISPLAY_VERSION VERSION " BETA"
#else
#define DISPLAY_VERSION VERSION
#endif

#if defined(Q_OS_LINUX)

//#define EXPERIMENTAL

#elif defined(Q_OS_WINDOWS)
//EDIT Windows
// #define PORTABLE
// #define OFFLINE_INSTALLER

#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
#define PYTHON38
#endif
#endif // Q_OS

// END EDIT


#define CHANGELOG_NAME "CHANGELOG"
#define CHANGELOG_NAME_SV "CHANGELOG_sv_SE"

#define MANUAL "https://gitlab.com/posktomten/streamcapture2/-/wikis/Manual"
#define EXECUTABLE_NAME "streamcapture2"
#define DISPLAY_NAME "streamCapture2"
#define EXECUTABLE_DISPLAY_NAME DISPLAY_NAME
#define LIBRARY_NAME "downloadunpack"
#define TV4_HOWTO "https://svtplay-dl.se/tv4play/"
#define TV4_HOWTO_VIDEO "https://bin.ceicer.com/" EXECUTABLE_NAME "/video/tv4token.webm"
#define TV4_HOWTO_VIDEO_SV "https://bin.ceicer.com/" EXECUTABLE_NAME "/video/tv4token_sv.webm"
#define COPYRIGHT "Ingemar Ceicer"
#define EMAIL "programming@ceicer.com"
#define COPYRIGHT_YEAR "2016"
#define LICENSE "GNU General Public License v3.0"
#define LICENSE_LINK "https://gitlab.com/posktomten/" EXECUTABLE_NAME "/-/blob/Qt6/LICENSE"
#define CHANGELOG "https://gitlab.com/posktomten/" EXECUTABLE_NAME "/-/blob/Qt6/CHANGELOG"
#define SOURCECODE "https://gitlab.com/posktomten/" EXECUTABLE_NAME
#define WEBSITE "https://gitlab.com/posktomten/" EXECUTABLE_NAME "/-/wikis/home"
#define SWEDISH_WEBSITE "https://bin.ceicer.com/" EXECUTABLE_NAME "/index.php"
#define SVTPLAYDL_ISSUES "https://github.com/spaam/svtplay-dl/issues"
#define DOWNLOAD_PATH                                                          \
    "https://gitlab.com/posktomten/" EXECUTABLE_NAME "/-/wikis/Home"
// #define MANUAL_PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/" EXECUTABLE_NAME "_help/help-current/index_"
#define MANUAL_PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/help-current/index_"
#define BUILD_DATE_TIME __DATE__ " " __TIME__
//#define START_YEAR "2016"
#define CURRENT_YEAR __DATE__
#define LICENCE_VERSION "3"
#define PROGRAMMER_EMAIL "programming@ceicer.com"
#define PROGRAMMER_NAME "Ingemar Ceicer"
#define PROGRAMMER_PHONE "+46706361747"
#define APPLICATION_HOMEPAGE                                                   \
    "https://gitlab.com/posktomten/" EXECUTABLE_NAME "/wikis/Home"
#define APPLICATION_HOMEPAGE_ENG                                               \
    "https://gitlab.com/posktomten/" EXECUTABLE_NAME "/wikis/Home"
#define SOURCEKODE "https://gitlab.com/posktomten/" EXECUTABLE_NAME
#define WIKI "https://gitlab.com/posktomten/" EXECUTABLE_NAME "/wikis/Home"
#define FFMPEG_HOMEPAGE "https://www.ffmpeg.org/"
#define SVTPLAYDL_HOMEPAGE "https://svtplay-dl.se/"
#define ITALIAN_TRANSLATER "bovirus"
#if defined(Q_OS_WINDOWS)
#if defined(Q_PROCESSOR_X86_64) // Windows 64 bit
#define VC_REDIST "https://bin.ceicer.com/vc_redist/vc_redist.x64.exe"
#elif defined(Q_PROCESSOR_X86_32) // 32 bit
#define VC_REDIST "https://bin.ceicer.com/vc_redist/vc_redist.x86.exe"
#endif
#endif
#if defined(EXPERIMENTAL)
#define EXPERIMENTAL_VERSION_LINK "\"https://bin.ceicer.com/" EXECUTABLE_NAME "/bin\""
#endif // EXPERIMENTAL
#define SIZE_OF_RECENT 50


#if defined(Q_OS_LINUX) // LINUX

#define FONTSIZE 10


#define APPIMAGE // For "Create Icon"changelog
#define VERSION_PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_linux.txt"
#define USER_MESSAGE "https://bin.ceicer.com" EXECUTABLE_NAME "/user_linux.txt"

#define VERSION_PATH_SV "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_linux_sv.txt"
#define USER_MESSAGE_SV "https://bin.ceicer.com/" EXECUTABLE_NAME "/user_linux_sv.txt"

#define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Linux/version_stable.txt"
#define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Linux/version.txt"

#if defined(Q_PROCESSOR_X86_64) // 64 bit

#if (__GLIBC_MINOR__ == 27)
#define COMPILEDON "Ubuntu 18.04.6 LTS 64-bit, GLIBC 2.27"

#if defined(FFMPEG)
#define ARG1 EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/BETA/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
#endif // BETA

#elif !defined(FFMPEG)
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#endif // BETA
#endif // FFMPEG

#endif // __GLIBC_MINOR__ == 27

#if (__GLIBC_MINOR__ == 31)
#define COMPILEDON "Lubuntu 20.04.6 LTS 64-bit, GLIBC 2.31"
#if defined(FFMPEG)
#define ARG1 EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/BETA/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.31/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.31/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
#endif // BETA
#elif !defined(FFMPEG)
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.31/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.31/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#endif // BETA


#endif // FFMPEG
#endif // __GLIBC_MINOR__ == 31

#if  (__GLIBC_MINOR__ == 35)
#define COMPILEDON "Ubuntu 22.04.5 LTS 64-bit, GLIBC 2.35"
#if defined(FFMPEG)
#define ARG1 EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/BETA/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.35/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.35/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
#endif // BETA

#elif !defined(FFMPEG)

#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.35/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.35/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#endif // BETA
#endif // FFMPEG

#endif // __GLIBC_MINOR__ == 35

#if  (__GLIBC_MINOR__ == 39)
#define COMPILEDON "Ubuntu 24.04.1 LTS, GLIBC 2.39"

#if defined(FFMPEG)
#define ARG1 EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/BETA/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.39/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.39/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
#endif // BETA
#elif !defined(FFMPEG)
#define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
#ifdef BETA
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.39/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.39/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
#endif // BETA
#endif // FFMPEG

#endif // __GLIBC_MINOR__ == 39

#elif defined(Q_PROCESSOR_X86_32) // 32 bit

#define COMPILEDON "Ubuntu 18.04.6 LTS 32-bit, GLIBC 2.27"
#if defined(FFMPEG)
#define ARG1 EXECUTABLE_NAME "-ffmpeg-i386.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/BETA/" EXECUTABLE_NAME "-ffmpeg-i386.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-i386.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-ffmpeg-i386.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-i386.AppImage.txt"
#endif // BETA

#elif !defined(FFMPEG)
#define ARG1 EXECUTABLE_NAME "-i386.AppImage"
#if defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/BETA/" EXECUTABLE_NAME "-i386.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/BETA/.invisible_checksum_" EXECUTABLE_NAME "-i386.AppImage.txt"
#elif !defined(BETA)
#define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-i386.AppImage.zsync"
#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/.invisible_checksum_" EXECUTABLE_NAME "-i386.AppImage.txt"
#endif // BETA
#endif

#endif // Q_PROCESSOR_

#elif defined(Q_OS_WINDOWS) // WINDOWS

#define FONTSIZE 10
#if defined(OFFLINE_INSTALLER)
#if defined(PYTHON38)
#define PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/windows/obsolete/"
#if defined(Q_PROCESSOR_X86_64) // Windows 64 bit
#define FILENAME DISPLAY_NAME "_setup.exe"
#elif defined (Q_PROCESSOR_X86_32) // Windows 32 bit
#define FILENAME DISPLAY_NAME "_32-bit_setup.exe"
#endif

#elif !defined(PYTHON38)

#define PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/windows/"
#define FILENAME DISPLAY_NAME "_setup.exe"
#endif // PYTHON38
#endif // OFFLINE_INSTALLER

#define VERSION_PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_windows.txt"
#define USER_MESSAGE "https://bin.ceicer.com/" EXECUTABLE_NAME "/user_windows.txt"

#define VERSION_PATH_SV "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_windows_sv.txt"
#define USER_MESSAGE_SV "https://bin.ceicer.com/" EXECUTABLE_NAME "/user_windows_sv.txt"

#if defined(Q_PROCESSOR_X86_64) // Windows 64 bit
#if defined(PYTHON38)
#define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Windows/64bit_python3.8/version_stable.txt"
#define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Windows/64bit_python3.8/version.txt"
#elif !defined(PYTHON38)
// #define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Windows/64bit/version_stable.txt"
// #define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Windows/64bit/version.txt"
#define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Windows/64bit/version_stable.txt"
#define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Windows/64bit/version.txt"
#endif


#define COMPILEDON "Microsoft Windows 11 Pro Version: 24H2<br>OS build: 26100.2033<br>System type: 64-bit operating system, x64-based processor"

#elif defined (Q_PROCESSOR_X86_32) // Windows 32 bit
#define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Windows/32bit/version_stable.txt"
#define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Windows/32bit/version.txt"
#define COMPILEDON "Microsoft Windows 11 Pro Version: 24H2<br>OS build: 26100.2894<br>System type: 64-bit operating system, x64-based processor"

#endif // Q_PROCESSOR

#endif // Q_OS

#include <QMainWindow>

// #include <QtWidgets>
#include <QProcess>

#ifdef Q_OS_LINUX // Linux
#include "updatedialog.h"
#endif
#include "ui_newprg.h"

namespace Ui
{
class newprg;
}

class Newprg : public QMainWindow
{
    Q_OBJECT

public:
    explicit Newprg(QWidget *parent = nullptr);
    QString getSvtplaydlVersion(QString *);
    void setEndConfig();

protected:
    void dropEvent(QDropEvent *ev)  override;
    void dragEnterEvent(QDragEnterEvent *ev)  override;

private slots:

    void aboutSvtplayDl();

    void aboutFfmpeg();

    void english();
    void swedish();
    void italian();
    void initSok();
    void sok();
    void download();
    void license();
    // void importantFinished(bool finished);
#ifdef FFMPEG
    void licenseFfmpeg();
#endif
#ifdef  Q_OS_WINDOWS
    void license7zip();
#endif
    void licenseSvtplayDl();
    void versionHistory();
    void slotRecent();
    void downloadAll();
    void onCommProcessStart();
    void onCommProcessExit_sok(int, QProcess::ExitStatus);
    void comboBoxChanged();
    // Pay TV
    void newSupplier();
    void editOrDelete(const QString &);
    void listAllEpisodes();
    void downloadFromCeicer();
    void takeAScreenshot();
    void _newDownloadList();
    void _importDownloadList();
    void _exportDownloadList();
private:

    Ui::newprg *ui;
    void closeEvent(QCloseEvent *event) override;

    void setStartConfig();

    QStringList recentFiles;
    // QStringList downloadList;
    int MAX_RECENT{};
    QString address{};
    QString save(QString windowTitle);
    void statusExit(QProcess::ExitStatus, int);
    // Pay TV
    bool testinputprovider(QString &, bool &);
    bool testinputusername(const QString &);
    bool testinputpassword(const QString &);
    bool testinputusername_edit(const QString &, const QString &);
    bool testinputpassword_edit(const QString &, const QString &);
    void editprovider(QString);
    void deleteProvider(const QString &);
    bool renameProvider(const QString &provider);
    QString secretpassword;

    void downloadAllEpisodes();
    void deleteAllSettings();

    QProcess *CommProcess2{};
    int antalnedladdade{};
    qint64 processpid{};
    bool avbrutet{};
    bool deleteSettings{};

    void copyToDefaultLocation(QString, const QString *, const QString *copyto);

    bool fileExists(QString &path);
    QString selectFfmpeg();

    QString svtplaydl;

    QString findSvtplayDl();

    void zoom();
#ifdef Q_OS_WIN
    void downloadRuntime();
#endif
#ifdef Q_OS_LINUX
    void firstRun();
    // void detectTheme();
#endif
    void chortcutdesktop(int state);
    void chortcutapplications(int state);
    bool makeDesktopFile(QString path);
    bool chortcutExists(QString path);
    // Unused c/c++ function
    // bool chortcutIsExecutable(QString path);

    void showNotification(QString notifikation, int ikonType);
#ifdef Q_OS_LINUX // Linux
    UpdateDialog *updatedialog;


#endif

    void testTranslation();
    // st cookie
    void createSt();
    void editSt(QAction *provider, bool iscurrentst);
    void editOrDeleteSt(QAction *provider);
    void actionSt();

    QStringList checkSvtplaydlForUpdates();
    void checkSvtplaydlForUpdatesHelp();
    QString hittaSvtplaydl();
    void checkSvtplaydlAtStart();
    QString getSvtplaydlVersion(QString path);

    void nfo();
    QTimer *timer;

#ifdef OFFLINE_INSTALLER
    void offlineInstaller();
#endif


    /*** STYLE ***/
    // QActionGroup *actionGroupStyle;
#if defined (Q_OS_WINDOWS)
    // bool theStyle(QString *style);
#endif

    void fusionDarkStyle();
    void fusionStyle();
    void defaultStyle();

    /*** END STYLE ***/

    QIcon *windowicon;

public slots:
    void setFontValue(QFont font);
    void selectFontIsClosing(bool is_closing);

    // Zoom
    void textBrowserTeOutTextChanged();
    void zoomPlus();
    void zoomMinus();
    void zoomDefault();
    // DownloadListDialog
    void saveDownloadList(QString *content);


    void getSettings(QString *content);
    void getSettingsDownloadunpack(QString *content);
    void getRecentSearches(QString *content);

#ifdef Q_OS_LINUX // Linux
    //    Receives a Boolean value from the "Update" class.
    //    True if the update was successful and false if the update failed
    void isUpdated(bool uppdated);

#endif
    void userMessage();
    void userMessageStart();


signals:
    void sendNewFont(const QFont &font);

};

#ifdef Q_OS_WIN
extern Q_CORE_EXPORT int qt_ntfs_permission_lookup;

#endif
#endif // NEWPRG_H
