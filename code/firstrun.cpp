// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "ui_newprg.h"
#include <QSettings>
#include <QFontInfo>
#include <QDir>
#include <QFile>
#include <QFileDevice>

#ifdef Q_OS_LINUX
void Newprg::firstRun()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Created"));
    settings.setValue(QStringLiteral("created"), true);
    settings.endGroup();
    settings.sync();
    QString inifile = settings.fileName();
    QFileInfo fi(inifile);
    QString iconpath = fi.absolutePath();
    QDir pathDir(iconpath);

    if(pathDir.exists()) {
        iconpath.append("/streamcapture2.png");
        const QString pngpath = QCoreApplication::applicationDirPath() + QStringLiteral("/streamcapture2.png");

        if(!QFile::exists(iconpath)) {
            if(QFile::copy(pngpath, iconpath)) {
                QFile file(iconpath);
                file.setPermissions(QFileDevice::ReadUser | QFileDevice::WriteUser | QFileDevice::ReadOther);
            }
        }
    }
}

#endif

