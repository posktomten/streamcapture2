// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2 (ZoomDisabler)
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3
//   as published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#ifndef ZOOM_DISABLE_H
#define ZOOM_DISABLE_H



#include <QWheelEvent>
#include <QApplication>
class ZoomDisabler : public QObject
{
    Q_OBJECT

public:
    ZoomDisabler(QObject* parent = nullptr) : QObject(parent)
    {
        // Anslut eventfiltret till applikationsinstansen
        qApp->installEventFilter(this);
    }

    // Eventfiltermetod som interceperar musevent
    bool eventFilter(QObject* obj, QEvent* event) override
    {
        if(event->type() == QEvent::Wheel) {
            // QWheelEvent* wheelEvent = static_cast<QWheelEvent*>(event);

            // Kontrollera om Ctrl-tangenten är nedtryckt
            if(QApplication::keyboardModifiers() & Qt::ControlModifier) {
                // Förhindra zoom genom att konsumera händelsen
                return true;
            }
        }

        // Fortsätt att behandla händelsen normalt
        return QObject::eventFilter(obj, event);
    }
};

#endif // ZOOM_DISABLE_H
