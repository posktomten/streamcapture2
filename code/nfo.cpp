// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "downloadlistdialog.h"
#include <QTimer>
#include <QStandardPaths>
#include <QMessageBox>
#include <QFile>
#include <QXmlStreamReader>
void Newprg::nfo()
{
    if(ui->leSok->text().isEmpty()) {
        ui->teOut->setText(tr("The search field is empty!"));
        return;
    }

    static QRegularExpression pattern(QStringLiteral("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)"));
    {
        QRegularExpressionMatch match = pattern.match(ui->leSok->text());

        if(!match.hasMatch()) {
            ui->teOut->setText(tr("Invalid link. No NFO information could be found."));
            return;
        }
    }

    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, [this]() {
        if(ui->progressBar->value() >= 100) {
            ui->progressBar->setValue(0);
        }

        ui->progressBar->setValue(ui->progressBar->value() + 1);
    });

    timer->start(100);
    QString tmppath = QStandardPaths::writableLocation(QStandardPaths::TempLocation);
    QStringList ARG;
    ARG << "--nfo" << "--force-nfo" << "-o" << tmppath;
    QString adress = ui->leSok->text();
    adress = adress.trimmed();
    ARG << adress;
    QProcess *CommProcess = new QProcess(nullptr);
    CommProcess->setProcessChannelMode(QProcess::MergedChannels);
    CommProcess->start(svtplaydl, ARG);
    processpid = CommProcess->processId();
    QString *result = new QString;
    QStringList *list = new QStringList;
    connect(CommProcess, QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished), this,
    [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
        *result = CommProcess->readAllStandardOutput();
        QString failure;

        if(ui->leSok->text().isEmpty()) {
            failure = tr("The search field is empty!");
            ui->progressBar->setValue(0);
            timer->stop();
            return;
        } else {
            failure = tr("NFO files contain release information about the media.\nNo NFO file was found.");
            ui->progressBar->setValue(0);
            timer->stop();
        }

        if(result->left(6) == QString("ERROR:")) {
            ui->progressBar->setValue(0);
            timer->stop();
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(failure);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

        if(exitCode != 0 || exitStatus != QProcess::NormalExit) {
            ui->progressBar->setValue(0);
            timer->stop();
            // QMessageBox msgBox;
            // msgBox.setIcon(QMessageBox::Critical);
            // msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            // msgBox.setText(failure);
            // msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            // msgBox.exec();
            ui->teOut->setText(tr("The search field is empty!"));
            return;
        }

        if(result->isEmpty()) {
            ui->progressBar->setValue(0);
            timer->stop();
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(failure);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

//        *list = result->split('\n');
        *list = result->split("INFO:");

        if(list->at(1) == QString("")) {
            ui->progressBar->setValue(0);
            timer->stop();
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(failure);
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            return;
        }

        list->removeDuplicates();
        QString smalFile, bigFile;

        for(auto &&s :  *list) {
            if(s.contains(QStringLiteral("NFO show:"))) {
                smalFile = s;
            }

            if(s.contains(QStringLiteral("NFO episode:"))) {
                bigFile = s;
            }
        }

        int slash = smalFile.indexOf(QStringLiteral("INFO: NFO show:"));
        smalFile = smalFile.mid(slash + 12);
        smalFile.replace("\\", "/");
        bigFile = bigFile.mid(slash + 15);
        bigFile.replace("\\", "/");
#if defined(Q_OS_LINUX)
        smalFile.remove('\n');
        bigFile.remove('\n');
#endif
#if defined(Q_OS_WINDOWS)
        smalFile.remove(QStringLiteral("\r\n"));
        bigFile.remove(QStringLiteral("\r\n"));
#endif
        QString path = bigFile;
        QFile file(path);
        file.open(QIODevice::ReadOnly);
        QXmlStreamReader reader(&file);
        QStringList s;

        while(reader.readNextStartElement()) {
            // svtplay.se
            if(reader.name() == QString(QStringLiteral("episodedetails"))) {
                while(reader.readNextStartElement()) {
                    if(reader.name() == QStringLiteral("showtitle")) {
                        s << QStringLiteral("<b>") + tr("Title") + QStringLiteral("</b><br>") + reader.readElementText();
                    }

                    if(reader.name() == QStringLiteral("title")) {
                        s << QStringLiteral("<br><b>") + tr("Episode title") + QStringLiteral("</b><br>") + reader.readElementText();
                    }

                    if(reader.name() == QStringLiteral("season")) {
                        s << QStringLiteral("<br><b>") + tr("Season") + QStringLiteral("</b><br>") + reader.readElementText();
                    }

                    if(reader.name() == QStringLiteral("episode")) {
                        s << QStringLiteral("<br><b>") + tr("Episode") + QStringLiteral("</b><br>") + reader.readElementText();
                    }

                    if(reader.name() == QStringLiteral("plot")) {
                        s << QStringLiteral("<br><b>") + tr("Plot") + QStringLiteral("</b><br>") + reader.readElementText();
                    }

                    if(reader.name() == QStringLiteral("aired")) {
                        QString tmp = reader.readElementText();
                        tmp.replace("T", " ");
                        s << QStringLiteral("<br><b>") + tr("Published") + QStringLiteral("</b><br>") + tmp;
                    }
                }
            }
        }

        file.close();
        file.remove(); // bigFile
        file.setFileName(smalFile);

        if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("An unexpected error occurred while downloading the NFO file."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->progressBar->setValue(0);
            timer->stop();
            return;
        }

        QTextStream out(&file);
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)
        out.setEncoding(QStringConverter::Utf8); // Qt6
#else
        out.setCodec("UTF-8");  // Qt5
#endif
        // for(const QString &tmp : s) {
        //     out << tmp << "<br>";
        // }
#if QT_VERSION  > QT_VERSION_CHECK(6, 0, 0)

        for(const QString &tmp : std::as_const(s)) {
            out << tmp << "<br>";
        }

#else

        for(const QString &tmp : qAsConst(s)) {
            out << tmp << "<br>";
        }

#endif
        file.close();
        QStringList *infoList = new QStringList;
        *infoList << (QString(tr("NFO Info")));
        *infoList << (QString("wordwrap"));
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
//        mDownloadListDialog->openFile(infoList, &path, true);
        mDownloadListDialog->openFile(infoList, &smalFile, true);
        file.remove(); // smalFile
        timer->stop();
        ui->progressBar->setValue(0);
    });
}



