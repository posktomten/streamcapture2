// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include "downloadlistdialog.h"
#include <QtGlobal>
#include <QSettings>
#include <QFileInfo>
#include <QMessageBox>
#include <QDir>
#include <QInputDialog>
// DOWNLOAD ALL FROM DOWNLOAD LIST
void Newprg::downloadAll()
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Path"));
    QString downloadlistfile = settings.value(QStringLiteral("downloadlistfile")).toString();
    settings.endGroup();
    QFileInfo fi(downloadlistfile);

    if(!fi.isFile()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("There is no file.") + '\n' + tr("Select \"Download List\" and \"New download List...\".\nOr select \"Download List\" and \"Import a download List...\"."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        settings.beginGroup(QStringLiteral("Path"));
        settings.setValue(QStringLiteral("downloadlistfile"), "");
        settings.endGroup();
        return;
    }

    if(fi.size() <= 0) {
        QMessageBox msgBox(this);
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("The list of downloadable files is empty."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

    QFile file(downloadlistfile);
    QStringList downloadList;
    QString list;

    if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        return;
    } else {
        QByteArray readall;
        readall = file.readAll();
        list = QString(readall);
        downloadList = list.split('\n');
        file.close();
    }

    downloadList.removeDuplicates();
    downloadList.removeAll(QString(""));

    if(downloadList.size() == 0) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("The list of downloadable video streams is empty."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

    antalnedladdade = downloadList.size();
    static QRegularExpression pattern(QStringLiteral("((http|https)://)(www.)?[a-zA-Z0-9@:%._\\+~#?&//=]{2,256}\\.[a-z]{2,6}\\b([-a-zA-Z0-9@:%._\\+~#?&//=]*)"));
    QString *nomatch = new QString;
    int amount = 0;

    foreach(QString s, downloadList) {
        QRegularExpressionMatch match = pattern.match(s);

        if(!match.hasMatch()) {
            amount++;
            nomatch->append(s + '\n');
        }
    }

    if(!nomatch->isEmpty()) {
        QStringList *infoList = new QStringList;
        const QString amountofstreams = (amount == 1) ? tr("Invalid link found.") : tr("Invalid links found.");
        *infoList  << QString::number(amount) + " " + amountofstreams;
        *infoList  << downloadlistfile;
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
        connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::saveDownloadList);
        mDownloadListDialog->initiate(infoList, nomatch, true);
        return;
    }

    amount = 0;

    foreach(QString s, downloadList) {
        static QRegularExpressionMatch match = pattern.match(s);
        int hitta = s.indexOf(",");
        QString is = s.mid(hitta);
        hitta = is.count(QLatin1Char(','));

        if(hitta != 7) {
            amount++;
            nomatch->append(s + '\n');
        }
    }

    if(!nomatch->isEmpty()) {
        QStringList *infoList = new QStringList;
        const QString amountofstreams = (amount == 1) ? tr("Invalid text string found.") : tr("Invalid text strings found.");
        *infoList  << QString::number(amount) + " " + amountofstreams;
        *infoList  << downloadlistfile;
        DownloadListDialog *mDownloadListDialog = new DownloadListDialog;
        connect(this, &Newprg::sendNewFont, mDownloadListDialog, &DownloadListDialog::getNewFont);
        connect(mDownloadListDialog, &DownloadListDialog::sendContent, this, &Newprg::saveDownloadList);
        mDownloadListDialog->initiate(infoList, nomatch, true);
        return;
    }

    // QString errorlinks;

    if(!fileExists(svtplaydl)) {
        QMessageBox msgBox1;
        msgBox1.setIcon(QMessageBox::Critical);
        msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox1.setText(tr("svtplay-dl cannot be found.") + '\n' + tr("Or is not an executable program.") + '\n' + tr("Please click on \"svtplay-dl\" and select svtplay-dl."));
        msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox1.exec();
        return;
    }

    QStringList ARG;
    avbrutet = false;
    QString save_all_path;
    settings.beginGroup("Path");
    QString savepath =
        settings.value("savepath").toString();
    QString copypath = settings.value("copypath").toString();
    settings.endGroup();
    settings.beginGroup("Settings");
    bool selectfilename = settings.value("selectfilename", "false").toBool();
    settings.endGroup();

    if(ui->actionDownloadToDefaultLocation->isChecked()) {
        /* */
        const QFileInfo downloadlocation(QDir::toNativeSeparators(savepath));

        if(!downloadlocation.exists()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("The default folder for downloading video streams cannot be "
                              "found.\nDownload is interrupted."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
            return;
        }

        if(!downloadlocation.isWritable()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("You do not have the right to save to the "
                              "folder.\nDownload is interrupted."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
            return;
        }

        /* */
        save_all_path = savepath;
    } else {
        save_all_path = save(tr("Download streaming media to directory"));
    }

    if(save_all_path == QStringLiteral("nothing")) {
        ui->teOut->clear();
        return;
    }

#ifdef Q_OS_WIN
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
// NTFS
    qt_ntfs_permission_lookup++; // turn checking on
#else
    qEnableNtfsPermissionChecks();  // turn checking on
#endif
#endif
    fi.setFile(save_all_path);

    if(!fi.isWritable()) {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(tr("You do not have the right to save to the "
                          "folder.\nDownload is interrupted."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        ui->teOut->clear();
        return;
#ifdef Q_OS_WIN
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
        // NTFS
        qt_ntfs_permission_lookup--; // turn checking off
#else
        qDisableNtfsPermissionChecks();  // turn checking off
#endif
#endif
        return;
    }

#ifdef Q_OS_WIN
#if QT_VERSION < QT_VERSION_CHECK(6, 6, 0)
// NTFS
    qt_ntfs_permission_lookup--; // turn checking off
#else
    qDisableNtfsPermissionChecks();  // turn checking off
#endif
#endif
// Om lika filnamn
    QStringList temp;
    bool ischecked = ui->actionCreateFolder->isChecked();

    if(!ischecked) {
        for(int i = 0; i < downloadList.size(); i++) {
            QStringList list = downloadList.at(i).split(",");
            temp << list.at(0);
        }

        int lika = temp.removeDuplicates();

        if(lika > 0) {
            /**/
            QMessageBox *msgBox = new QMessageBox(this);
            msgBox->setIcon(QMessageBox::Information);
            QPushButton  *okButton = new QPushButton(tr("Ok"), msgBox);
            // yesButton->setFixedSize(QSize(150, 40));
            msgBox->addButton(okButton, QMessageBox::YesRole);
            QPushButton  *cancelButton = new QPushButton(tr("Cancel"), msgBox);
            msgBox->setDefaultButton(cancelButton);
            // cancelButton->setFixedSize(QSize(150, 40));
            msgBox->addButton(cancelButton, QMessageBox::RejectRole);
            msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox->setText(tr("You have chosen to download more than one file with the same name. "
                               "In order not to overwrite files, folders will be created for each file."));
            msgBox->exec();
            /**/

            if(msgBox->clickedButton() == cancelButton)  {
                return;
            }

            ui->actionCreateFolder->setChecked(true);
            ui->actionCreateFolder->setDisabled(true);;
        }
    }

    QString kopierastill;

    if(ui->actionCopyToDefaultLocation->isChecked()) {
        const QFileInfo copypathlocation(copypath);

        if(!copypathlocation.exists()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("The default folder for copying video streams "
                              "cannot be found.\nDownload is interrupted."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
            return;
        } else if(!copypathlocation.isWritable()) {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Critical);
            msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
            msgBox.setText(tr("You do not have the right to save to the folder.\nDownload is interrupted."));
            msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
            msgBox.exec();
            ui->teOut->clear();
            return;
        } else {
            kopierastill = tr("Selected folder to copy to is ") + QDir::toNativeSeparators("\"" + copypath) + "\"";
        }
    }

    if(save_all_path != "nothing") {
        ui->teOut->setReadOnly(true);
//        QStringList allDownloads;
        QVector<QString> allDownloads;
#if QT_VERSION  > QT_VERSION_CHECK(6, 0, 0)

        for(const QString &listitems : std::as_const(downloadList)) {
            int hittat = listitems.indexOf(',', 0);
            allDownloads.append(listitems.left(hittat));
        }

#else

        for(const QString &listitems : qAsConst(downloadList)) {
            int hittat = listitems.indexOf(',', 0);
            allDownloads.append(listitems.left(hittat));
        }

#endif
        ui->teOut->clear();
        ui->teOut->setText(tr("The video streams are saved in") + " \"" +
                           QDir::toNativeSeparators(save_all_path) + "\"");
        QString preferred, quality, provider, sub, folderaddress, stcookie, amount, resolution;
        QString address;
        QString save_all_path2;

// loop
        for(int antal = 0; antal < downloadList.size(); antal++) {
            ARG << "--force";
            ARG << "--verbose";
            QStringList list = downloadList.at(antal).split(",");

            if(list.size() != 8) {
                QMessageBox *msgBox = new QMessageBox(this);
                msgBox->setIcon(QMessageBox::Critical);
                msgBox->setWindowTitle(DISPLAY_NAME " " VERSION);
                msgBox->setText(tr("An unexpected error has occurred.\nThe file with saved links to downloadable video streams is incorrect.\nCheck that all links are correct."));
                msgBox->addButton(tr("Ok"), QMessageBox::AcceptRole);
                msgBox->exec();
                // msgBox->deleteLater();
                return;
            }

            address = list.at(0);
            preferred = list.at(1);
            preferred = preferred.trimmed();
            preferred = preferred.toLower();
            quality = list.at(2);
            quality = quality.trimmed();
            provider = list.at(3);
            sub = list.at(4);
            stcookie = list.at(5);
            amount = list.at(6);
            resolution = list.at(7);
            resolution = resolution.trimmed();
            folderaddress = address;
            QString resolutionToARG;

            if(resolution == QStringLiteral("less_than_720p")) {
                resolutionToARG = QStringLiteral("<720");
            } else if(resolution == QStringLiteral("less_or_equal_720p")) {
                resolutionToARG = QStringLiteral("<=720");
            } else if(resolution == QStringLiteral("less_or_equal_1080p")) {
                resolutionToARG = QStringLiteral("<=1080");
            } else if(resolution == QStringLiteral("more_than_1080p")) {
                resolutionToARG = QStringLiteral("<=1080");
            } else {
                resolutionToARG = QStringLiteral("unknown");
            }

            QString username, password;

            if(provider != "npassword") {
// cppcheck
                settings.beginGroup(QStringLiteral("Provider"));
                username = settings.value(provider + QStringLiteral("/username")).toString();
                password = settings.value(provider + QStringLiteral("/password")).toString();
                settings.endGroup();
                {
                    if(password == "") {
                        bool ok;
                        QInputDialog inputdialog;
                        inputdialog.setWindowFlags(inputdialog.windowFlags() & ~Qt::WindowContextHelpButtonHint);
                        inputdialog.setOkButtonText(tr("Ok"));
                        inputdialog.setCancelButtonText(tr("Cancel"));
                        inputdialog.setWindowTitle(tr("Enter your password"));
                        inputdialog.setInputMode(QInputDialog::TextInput);
                        inputdialog.setTextEchoMode(QLineEdit::Password);
                        inputdialog.setLabelText(tr("Spaces are not allowed. Use only the characters\nyour streaming provider approves."));
                        ok = inputdialog.exec();
                        QString newpassword = inputdialog.textValue();

                        if(newpassword.indexOf(' ') >= 0) {
                            return;
                        }

                        if(ok) {
                            secretpassword = newpassword;
                            password = newpassword;
                        } else {
                            return;
                        }
                    }
                }
            }

            CommProcess2 = new QProcess(this);
            QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
            const QString path =
                QDir::toNativeSeparators(QCoreApplication::applicationDirPath());
            const QString oldpath = env.value("PATH");
#ifdef Q_OS_LINUX
            env.insert("PATH", path + ":" + oldpath); // Add an environment variable
#endif
#ifdef Q_OS_WIN                                 // QT5
            env.insert("PATH", path + ";" + oldpath); // Add an environment variable
#endif
            CommProcess2->setProcessEnvironment(env);
            CommProcess2->setProcessChannelMode(QProcess::MergedChannels);
            /* MAKE FOLDER */
            int hittat = folderaddress.lastIndexOf('/');
            folderaddress = folderaddress.mid(hittat);

            if(selectfilename) {
                // SELECT FILE NAME
                QInputDialog selectFilename;
                selectFilename.setWindowFlags(selectFilename.windowFlags() & ~Qt::WindowContextHelpButtonHint);
                selectFilename.setSizeGripEnabled(true);
                selectFilename.setCancelButtonText(tr("Cancel"));
                selectFilename.setOkButtonText(tr("Ok"));
                selectFilename.setWindowTitle(tr("Save as..."));
                bool ok;
                QString filename = folderaddress.remove("/");
                selectFilename.setInputMode(QInputDialog::TextInput);
                selectFilename.setLabelText(tr("Select file name"));
                selectFilename.setTextValue(filename);
                selectFilename.setModal(true);
                ok = selectFilename.exec();

                if(ok) {
                    folderaddress = "/" + selectFilename.textValue().trimmed();
                }
            }

            QDir dir;
            QString ratt;

            if(ui->actionCreateFolder->isChecked()) {
                dir.setPath(save_all_path + folderaddress + "/" + preferred + "_" + quality + "_" + amount + '_' + resolution);

                if(!dir.exists()) {
                    dir.mkpath(".");
                }

                save_all_path2 = dir.path();
                ratt = save_all_path2;
            } else {
                dir.setPath(save_all_path);
                save_all_path2 = save_all_path;
                ratt = save_all_path2;
            }

            // ALLTID FOLDER
            /*     */

            if(!dir.exists()) {
                dir.mkpath(".");
            }

            /*    */

            if(ui->actionCopyToDefaultLocation->isChecked()) {
                ui->teOut->append(kopierastill);
            }

            CommProcess2->setWorkingDirectory(
                QDir::toNativeSeparators(save_all_path2));

            /* END MAKE FOLDER */

            if(stcookie != QStringLiteral("nst")) {
                ARG << QStringLiteral("--cookies")  << QStringLiteral("st=") + stcookie;
            }

            if(provider != QStringLiteral("npassword")) {
                ARG << QStringLiteral("--username") << username << QStringLiteral("--password") << password;
            }

            if(sub == QStringLiteral("ysub")) {  // Subtitles
                ARG << QStringLiteral("--subtitle");
            } else if(sub == QStringLiteral("asub")) { // All subtitles
                ARG << QStringLiteral("--all-subtitles");
            } else if(sub == QStringLiteral("ymsub")) { // Merge Subtitles
                ARG << QStringLiteral("--subtitle") << QStringLiteral("--merge-subtitle");
            } else if(ui->actionDownloadRaw->isChecked()) {
                ARG << QStringLiteral("--raw-subtitles");
            }

            if(preferred != QStringLiteral("unknown") && quality != QStringLiteral("unknown")) {
                if(amount != QStringLiteral("unknown")) {
                    ARG << QStringLiteral("--quality") << quality << QStringLiteral("--preferred") << preferred << QStringLiteral("--flexible-quality") << amount;
                } else {
                    ARG << QStringLiteral("--quality") << quality << QStringLiteral("--preferred") << preferred;
                }
            }

            if(resolution != QStringLiteral("unknown")) {
                QString tmp = resolution;
                tmp.chop(1);
                ARG << QStringLiteral("--resolution") << resolutionToARG;
            }

            ARG << address;
            ui->teOut->append(tr("Preparing to download") + " \"" + address + "\"");

            if(address.contains(QStringLiteral("https://www.tv4play.se"))) {
                settings.beginGroup(QStringLiteral("Stcookies"));
                QString tv4token = settings.value(QStringLiteral("tv4token"), "").toString();
                settings.endGroup();

                if(!tv4token.isEmpty()) {
                    ARG << QStringLiteral("--token") << tv4token;
                } else {
                    QMessageBox msgBox1;
                    msgBox1.setIcon(QMessageBox::Critical);
                    msgBox1.setWindowTitle(DISPLAY_NAME " " VERSION);
                    msgBox1.setText(tr("You need to copy the TV4 token from your browser and save it in streamCapture2."));
                    msgBox1.addButton(tr("Ok"), QMessageBox::AcceptRole);
                    msgBox1.exec();
                    return;
                }
            }

            settings.beginGroup(QStringLiteral("Settings"));
            bool try4k = settings.value(QStringLiteral("try4k"), false).toBool();
            settings.endGroup();

            if(try4k) {
                ARG << QStringLiteral("--format") << QStringLiteral("dvhevc-51,dvhevc,hevc-51,hevc");
            }

#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
            connect(CommProcess2,
                    QOverload<int, QProcess::ExitStatus>::of(&QProcess::finished),
            [ = ](int exitCode, QProcess::ExitStatus exitStatus) {
#else
            QObject::connect(CommProcess2, &QProcess::finished, this, [ = ](int exitCode, QProcess::ExitStatus exitStatus)  {
#endif
                statusExit(exitStatus, exitCode);
                QString filnamn = folderaddress.mid(1);
                QString fil;

                if(ui->actionCreateFolder->isChecked()) {
                    fil = ratt + folderaddress + QStringLiteral(".mp4");
                } else {
                    fil = ratt + "/" + filnamn + QStringLiteral(".mp4");
                }

                if(!avbrutet) {
                    if(QFile::exists(fil)) {
                        ui->teOut->append(tr("Download succeeded") + " \"" +  filnamn + QStringLiteral(".mp4") + " \"" + preferred + ", " + quality + ", " + amount + ", " + resolution + "\"");
                        ui->progressBar->setValue(0);
                        int langd = fil.size();
                        QString path_undertextfilen =
                            fil.replace(langd - 3, 3, QStringLiteral("srt"));

                        if(QFile::exists(path_undertextfilen)) {
                            ui->teOut->append(tr("Download succeeded") + " \"" + filnamn + QStringLiteral(".srt"));
                            ui->progressBar->setValue(0);
                        }

// COPY
                        QString *copyto = new QString;

                        if(ui->actionCopyToDefaultLocation->isChecked()) {
                            if(ui->actionCreateFolder->isChecked()) {
                                *copyto = folderaddress + "/" + preferred + '_' + quality + '_' + amount + '_' + resolution;
                                QString *hela = new QString;
                                *hela = savepath + *copyto;
                                copyToDefaultLocation(filnamn + QStringLiteral(".mp4"), hela, copyto);
                            } else {
                                *copyto = "";
                                copyToDefaultLocation(filnamn + QStringLiteral(".mp4"), &savepath, copyto);
                            }
                        }

                        delete copyto;
                        antalnedladdade--;
                    }  else {
                        ui->teOut->append(tr("The download failed.\nThe error may be due to your 'method', 'quality' and/or 'deviation' selections not working.\nIt usually works best if you let svtplay-dl choose automatically.\nIf a username and password or 'st' cookie is required, you must enter these.") +  tr("\nYou tried: ") +  filnamn + ".mp4" + " (" + preferred + ", " + quality + ", " + amount + "," + resolution + ")");

                        if(!ui->actionNotifications->isChecked()) {
                            // showNotification
                            showNotification(tr("The download failed.\nThe error may be due to your 'method', 'quality' and/or 'deviation' selections not working.\nIt usually works best if you let svtplay-dl choose automatically.\nIf a username and password or 'st' cookie is required, you must enter these.") + tr("\nYou tried: ") +  filnamn + ".mp4" + " (" + preferred + ", " + quality + ", " + amount + ", " + resolution + ")", 3);
                        }
                    }

                    avbrutet = false;
                }

                antalnedladdade--;

                if(antalnedladdade == 0) {
                    // ui->teOut->append(tr("streamCapture2 is done with the task."));
                    ui->actionCreateFolder->setChecked(ischecked);
                    ui->actionCreateFolder->setEnabled(true);
                    ui->progressBar->setValue(0);

                    if(!ui->actionNotifications->isChecked()) {
                        // showNotification
                        showNotification(tr("streamCapture2 is done with the task."), 1);
                    }

                    processpid = 0;
                }

                return antal;
                /* ... */
            });

//            connect(CommProcess2, SIGNAL(started()), this,
//                    SLOT(onCommProcessStart()));
            connect(CommProcess2, &QProcess::started, this, &Newprg::onCommProcessStart);
            // HIT
            ARG << "--output" << ratt + folderaddress;
            CommProcess2->start(svtplaydl, ARG);
            ARG.clear();
            processpid = CommProcess2->processId();
            connect(CommProcess2, &QProcess::readyReadStandardOutput, this, [this]() {
                QString result(CommProcess2->readAllStandardOutput());

                if(result.trimmed().contains("Can't find any streams with that video resolution")) {
                    ui->teOut->append(tr("Unable to find any streams with the selected video resolution."));
                }

                if(ui->actionShowMore->isChecked()) {
                    ;
                    ui->teOut->append(result);

                    // progressbar
                    if(ui->progressBar->value() >= 100) {
                        ui->progressBar->setValue(0);
                    }

                    ui->progressBar->setValue(ui->progressBar->value() + 1);
                } else {
                    // progressbar
                    if(ui->progressBar->value() >= 100) {
                        ui->progressBar->setValue(0);
                    }

                    if(result.trimmed().contains("Merge audio and video")) {
                        ui->teOut->append(tr("Merge audio and video..."));
                    }

                    if(result.trimmed().contains("removing old files")) {
                        ui->teOut->append(tr("Removing old files, if there are any..."));
                    }

                    ui->progressBar->setValue(ui->progressBar->value() + 1);
                }
            });
        }

        // END LOOP
    }
}
