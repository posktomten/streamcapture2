// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//          streamCapture2
//          (C) Copyright  2016 - 2025 Ingemar Ceicer
//          https://gitlab.com/posktomten/streamcapture2/
//          programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License as published by
//   the Free Software Foundation, either version 3 of the License, or
//   (at your option) any later version.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//
// ><(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>

#include "newprg.h"
#include <QSettings>
#include <QFile>
#include <QMessageBox>
void Newprg::saveDownloadList(QString *content)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME,
                       EXECUTABLE_NAME);
    settings.beginGroup(QStringLiteral("Path"));
    QString downloadlistfile = settings.value(QStringLiteral("downloadlistfile")).toString();
    settings.endGroup();
    QFile file(downloadlistfile);

    if(file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        QTextStream out(&file);
        out << *content;
    } else {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Critical);
        msgBox.setWindowTitle(DISPLAY_NAME " " VERSION);
        msgBox.setText(DISPLAY_NAME + tr(" is not allowed to save ") + "\n\"" + downloadlistfile + "\"" + '\n' + tr(" Check your file permissions or choose another location."));
        msgBox.addButton(tr("Ok"), QMessageBox::AcceptRole);
        msgBox.exec();
        return;
    }

    file.close();
}



void Newprg::getSettings(QString *content)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, EXECUTABLE_NAME);
    QString inifile = settings.fileName();
#ifdef Q_OS_WINDOWS
//    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
#endif
#ifdef Q_OS_LINUX
    QStringList list = content->split('\n', Qt::SkipEmptyParts);
#endif
    QFile fil(inifile);

    if(fil.open(QIODevice::WriteOnly)) {
        QTextStream in(&fil);
        /*nnnn*/
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

        for(const QString &s : std::as_const(list)) {
#else

        for(const QString &s : qAsConst(list)) {
#endif
            /*nnnn*/
#ifdef Q_OS_LINUX
            in << s + '\n';
#endif
#ifdef Q_OS_WINDOWS
            in << s + "\r\n";
#endif
        }
    }

    fil.close();
}

void Newprg::getSettingsDownloadunpack(QString *content)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, DISPLAY_NAME, LIBRARY_NAME);
    QString inifile = settings.fileName();
#ifdef Q_OS_WINDOWS
//    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
#endif
#ifdef Q_OS_LINUX
    QStringList list = content->split('\n', Qt::SkipEmptyParts);
#endif
    QFile fil(inifile);

    if(fil.open(QIODevice::WriteOnly)) {
        QTextStream in(&fil);
        /*nnnnn*/
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

        for(const QString &s : std::as_const(list)) {
#else

        for(const QString &s : qAsConst(list)) {
#endif
#ifdef Q_OS_LINUX
            in << s + '\n';
#endif
#ifdef Q_OS_WINDOWS
            in << s + "\r\n";
#endif
        }
    }

    fil.close();
}

void Newprg::getRecentSearches(QString *content)
{
#ifdef Q_OS_WINDOWS
    QStringList list = content->split("\r\n", Qt::SkipEmptyParts);
#endif
#ifdef Q_OS_LINUX
    QStringList list = content->split('\n', Qt::SkipEmptyParts);
#endif
//    QFile fil(recentsearches);
    recentFiles.clear();
//    if(fil.open(QIODevice::WriteOnly)) {
//        QTextStream in(&fil);
#if QT_VERSION > QT_VERSION_CHECK(6, 0, 0)

    for(const QString &s : std::as_const(list)) {
        //            in << s + '\n';
        recentFiles << s;
    }

#else

    for(const QString &s : qAsConst(list)) {
        //            in << s + '\n';
        recentFiles << s;
    }

#endif
//        fil.close();
//    }
}
