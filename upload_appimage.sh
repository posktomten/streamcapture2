#!/bin/bash


# EDIT
QT5=5.15.16
QT6=6.8.2
EXECUTABLE=streamcapture2
SUPORTEDGLIBC=2.31
# STOP EDIT

    PS3=$(echo -e "${BLUE}Pleas Select Qt version ${NC}")
    stable_or_beta=("5.15.2" "${QT5}" "${QT6}" "Quit")
    select opt in "${stable_or_beta[@]}"; do

        case "$opt" in
            "5.15.2")
            QTVERSION="${EXECUTABLE} is built with Qt version 5.15.2"
            break
            ;;
        "${QT5}")
            QTVERSION="${EXECUTABLE} is built with Qt version ${QT5}"
            break
            ;;
        "${QT6}")
           QTVERSION="${EXECUTABLE} is built with Qt version ${QT6}"
            break
            ;;
        "Quit")
            echo "Goodbye!"
            exit 0
            break
            ;;

        esac
    done

###

RED='\033[0;31m'
BLUE='\033[0;34m'
NC='\033[0m' # No Color
BOLD=$(tput bold)
NORMAL=$(tput sgr0)
GLIBCVERSION=$(glibcversion)
READMORE=https://gitlab.com/posktomten/${EXECUTABLE}

if [ $(getconf LONG_BIT) = 64 ]; then
    GCC="gcc_64"
else
    GCC="gcc_32"
fi

function doZsynkMake() {

    PS3=$(echo -e "${BLUE}Pleas Select Stable or BETA ${NC}")
    stable_or_beta=("Stable" "BETA" "Quit")
    select opt in "${stable_or_beta[@]}"; do

        case "$opt" in
        "Stable")
            BIN_DIR="/bin.ceicer.com/public_html/${EXECUTABLE}/bin/linux/GLIBC${GLIBCVERSION}"
            ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}/"
            ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}"
            echo "${EXECUTABLE} AppImage will be uploaded to"
            echo $BIN_DIR
            break
            ;;
        "BETA")
            BIN_DIR="/bin.ceicer.com/public_html/${EXECUTABLE}/bin/linux/GLIBC${GLIBCVERSION}/BETA"
            ZSYNC_ADDRESS="http://bin.ceicer.com/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}/BETA/"
            ZSYNC_DIR="/bin.ceicer.com/public_html/zsync/${EXECUTABLE}/GLIBC${GLIBCVERSION}/BETA"
            echo "${EXECUTABLE} AppImage will be uploaded to"
            echo $BIN_DIR
            break
            ;;
        "Quit")
            echo "Goodbye!"
            exit 0
            break
            ;;

        esac
    done

    whitespace="     "
    date_now=$(date "+%F %H:%M:%S")

    for i in *.AppImage; do
        filelenght=$(("${#i}" + 65))


        streck=""

        while [[ ${#streck} -le ${filelenght} ]]; do
            streck="${streck}-"

        done

        echo "${streck}" >"$i-SHA256.txt"
        echo "SHA256 HASH SUM" >>"$i-SHA256.txt"
        sha256sum "$i" > ".invisible_checksum_$i.txt"
        sha256sum "$i" >>"$i-SHA256.txt"
        echo "${streck}" >>"$i-SHA256.txt"
        filesize=$(stat -c%s "$i")
        mb=$(echo "scale=2; $filesize/1048576" | bc)
        echo "Uploaded: $date_now" >>"$i-SHA256.txt"
        echo "Size:$whitespace$mb MB" >>"$i-SHA256.txt"
        echo "$i" requires GLIBC higher than or equal to "${GLIBCVERSION}" >> "$i-SHA256.txt"
        echo ${QTVERSION} >> "$i-SHA256.txt"
        echo "${streck}" >>"$i-SHA256.txt"
        echo "HOW TO CHECK THE HASH SUM" >>"$i-SHA256.txt"
        echo "${streck}" >>"$i-SHA256.txt"
        echo "LINUX" >>"$i-SHA256.txt"
        echo "\"sha256sum\" is included in most Linus distributions." >>"$i-SHA256.txt"
        echo "sha256sum $i" >>"$i-SHA256.txt"
        echo "${streck}" >>"$i-SHA256.txt"
        echo "WINDOWS" >>"$i-SHA256.txt"
        echo "\"certutil\" is included in Windows." >>"$i-SHA256.txt"
        echo "certutil -hashfile $i SHA256" >>"$i-SHA256.txt"
        echo "${streck}" >>"$i-SHA256.txt"
        echo "READ MORE" >>"$i-SHA256.txt"
        echo "$READMORE" >>"$i-SHA256.txt"
        echo "${streck}" >>"$i-SHA256.txt"

        echo
        echo " _   _                      _____               _ " >>"$i-SHA256.txt"
        echo "| | | |  __ _ __   __ ___  |  ___|_   _  _ __  | |" >>"$i-SHA256.txt"
        echo "| |_| | / _  |\ \ / // _ \ | |_  | | | ||  _ \ | |" >>"$i-SHA256.txt"
        echo "|  _  || (_| | \ V /|  __/ |  _| | |_| || | | ||_|" >>"$i-SHA256.txt"
        echo "|_| |_| \__,_|  \_/  \___| |_|    \__,_||_| |_|(_)" >>"$i-SHA256.txt"

    done

    echo "----------------------------------------------------------------"
    PS3=$(echo -e "${BLUE}Please enter your choice: ${NC}")
    options=("Upload AppImage" "Upload AppImage later" "Quit")
    select opt in "${options[@]}"; do

        case $opt in
        "Upload AppImage")
            echo "You chose $REPLY which is $opt"



            cp -f *.AppImage ${EXECUTABLE}-zsyncmake/
            cp -f *.AppImage-SHA256.txt ${EXECUTABLE}-zsyncmake/
            cp -f .invisible_checksum_* ${EXECUTABLE}-zsyncmake/
            cd ${EXECUTABLE}-zsyncmake

            for i in *.AppImage; do

                ./zsyncmake "$i" -u "${ZSYNC_ADDRESS}/${i}"

            done

            HOST="$(sed -n 1p ../../secret)"
            USER="$(sed -n 2p ../../secret)"
            PASSWORD="$(sed -n 3p ../../secret)"

            for fil in *.AppImage; do

                ftp -v -inv $HOST <<EOF
passive
user $USER $PASSWORD
cd "$BIN_DIR"
put $fil
cd "$ZSYNC_DIR"
put $fil
bye
EOF
            done

            for fil in *.zsync; do

                ftp -v -inv $HOST <<EOF
passive
user $USER $PASSWORD
cd "$ZSYNC_DIR"
put $fil
bye
EOF

            done

            for fil in .invisible_checksum*; do

                ftp -v -inv $HOST <<EOF
passive    
user $USER $PASSWORD
cd "$BIN_DIR"
put $fil
bye
EOF

            done


            for fil in *AppImage-SHA256.txt; do

                ftp -v -inv $HOST <<EOF
passive
user $USER $PASSWORD
cd "$BIN_DIR"
put $fil
bye
EOF

            done
            break
            ;;

        "Upload AppImage later")
            cp -f *.AppImage ${EXECUTABLE}-zsyncmake/
            cd ${EXECUTABLE}-zsyncmake

            for i in *.AppImage; do

                ./zsyncmake "$i" -u "${ZSYNC_ADDRESS}/$i"

            done
            break
            ;;

        "Quit")
            echo "Goodbye"
            exit 0
            ;;
        *)
            echo "invalid option $REPLY"
            ;;
        esac
    done

} #doZsynkMake



PS3=$(echo -e "${BLUE}Create *.zsync file and SHA256 sum: ${NC}")
select opt in "${options[@]}" "Create *.zsync fil and SHA256 sum" "Quit"; do
    case "$REPLY" in
    $((${#options[@]} + 1)))
        echo "Create *.zsync file and SHA256 sum"
        doZsynkMake
        break
        ;;
    $((${#options[@]} + 2)))
        echo "Goodbye!"
        exit 0
        break
        ;;

    esac
    [ $REPLY -gt $((${#options[@]} + 1)) -o $REPLY -lt 1 ] && echo "Invalid selection" || break
done

echo "You chose $opt which is $(pwd)/${options[(($REPLY - 1))]}"



