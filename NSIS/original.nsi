;Use MakeNSISW to compile this script
;For add files use "File <somefile>" in Section of this script
 
;-----------------------------------
;General
 
  ;Name and file
  Name "Advanced"
  OutFile "Advanced.exe"
 
  ;Use solid LZMA compression
  ;SetCompressor /SOLID lzma
 
  ;Change the default Modern UI icons
  ;!define MUI_ICON "${NSISDIR}\Contrib\Graphics\Icons\modern-install-blue.ico"
  ;!define MUI_UNICON "${NSISDIR}\Contrib\Graphics\Icons\classic-uninstall.ico"
 
  ;Default installation folder
  InstallDir "$PROGRAMFILES\$(^Name)"
 
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\$(^Name)" ""
 
  ;Define uninstaller name
  !define UninstName "Uninstall"
 
;--------------------------------
;Header Files
 
  !include "MUI2.nsh"
  ;!include "UnInst.nsh"
  ;!include "zipdll.nsh"
 
;--------------------------------
;Variables
 
  Var StartMenuFolder
 
;--------------------------------
;Interface Settings
 
  !define MUI_ABORTWARNING
  ;!define MUI_FINISHPAGE_NOAUTOCLOSE
 
  !define MUI_WELCOMEFINISHPAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Wizard\win.bmp"
  !define MUI_UNWELCOMEFINISHPAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Wizard\win.bmp"
 
  ;!define MUI_HEADERIMAGE
  ;!define MUI_HEADERIMAGE_RIGHT
  ;!define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\nsis.bmp"
 
  !define MUI_FINISHPAGE_RUN "$INSTDIR\Advanced.exe"
  ;!define MUI_FINISHPAGE_RUN_NOTCHECKED
 
  !define MUI_FINISHPAGE_SHOWREADME
  !define MUI_FINISHPAGE_SHOWREADME_TEXT "Create Desktop Shortcut"
  !define MUI_FINISHPAGE_SHOWREADME_FUNCTION CreateDesktopShortCut
  !define MUI_FINISHPAGE_SHOWREADME_NOTCHECKED
 
  !define MUI_FINISHPAGE_LINK "nsis.sourceforge.net"
  !define MUI_FINISHPAGE_LINK_LOCATION "http://nsis.sourceforge.net"
 
  InstType "Full"
  InstType "Minimal"
 
  ShowInstDetails show
  ;ShowUninstDetails show
 
;--------------------------------
;Language Selection Dialog Settings
 
  ;Remember the installer language and select it by default
  ;(should be set before installation page)
  !define MUI_LANGDLL_REGISTRY_ROOT "HKCU" 
  !define MUI_LANGDLL_REGISTRY_KEY "Software\$(^Name)" 
  !define MUI_LANGDLL_REGISTRY_VALUENAME "Installer Language"
 
  ;Always show the language selection dialog (override the stored value)
  !define MUI_LANGDLL_ALWAYSSHOW
 
  ;Don't filter languages according to their codepage
  ;!define MUI_LANGDLL_ALLLANGUAGES
 
;--------------------------------
;Macros
 
!macro MUI_FINISHPAGE_SHORTCUT
 
  !ifndef MUI_FINISHPAGE_NOREBOOTSUPPORT
    !define MUI_FINISHPAGE_NOREBOOTSUPPORT
    !ifdef MUI_FINISHPAGE_RUN
      !undef MUI_FINISHPAGE_RUN
    !endif
  !endif
  !define MUI_PAGE_CUSTOMFUNCTION_SHOW DisableCancelButton
  !insertmacro MUI_PAGE_FINISH
  !define MUI_PAGE_CUSTOMFUNCTION_SHOW DisableBackButton
 
  Function DisableCancelButton
 
    EnableWindow $mui.Button.Cancel 0
 
  FunctionEnd
 
  Function DisableBackButton
 
    EnableWindow $mui.Button.Back 0
 
  FunctionEnd
 
!macroend
 
!macro NextCD Label IDFile
 
  IfFileExists "${IDFile}" +7
  MessageBox MB_OK|MB_ICONINFORMATION "$(NextCD) ${Label}..."
 
  IfFileExists "${IDFile}" +5
  Sleep 1000
 
  StrCpy $0 "${IDFile}"
  MessageBox MB_RETRYCANCEL|MB_ICONEXCLAMATION "$(CDNotFound)" IDRETRY -3
  Quit
 
!macroend
 
!include "FileFunc.nsh"
 
!macro Extract7z Label Archive Part
 
  !insertmacro NextCD "${Label}" "${Archive}"
  ${GetFileName} "${Archive}" $0
  DetailPrint "$(Extract) $0... ${Part}"
  Nsis7z::ExtractWithDetails "${Archive}" "$(Extract) $0... %s"
 
!macroend
 
!macro SHORTCUTS Name File Icon
 
  !if "${Name}" == ""
    !undef Name
    !define Name "$(^Name)"
  !endif
  !ifdef UninstName
    StrCpy $1 "${UninstName}.exe"
  !else
    StrCpy $1 "Uninstall.exe"
  !endif
 
  ;Create uninstaller
  WriteUninstaller "$OUTDIR\$1"
 
  ;Use "All Users" shell folder
  SetShellVarContext all
 
  ;Get Start Menu Folder from registry if available
  !insertmacro MUI_STARTMENU_WRITE_BEGIN Application
 
  ;Create shortcuts
  CreateDirectory "$SMPROGRAMS\$StartMenuFolder"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\${Name}.lnk" "$OUTDIR\${File}" "" "${Icon}"
  CreateShortCut "$SMPROGRAMS\$StartMenuFolder\$(Uninstall).lnk" "$OUTDIR\$1"
 
  ;Store Start Menu Folder in registry
  !insertmacro MUI_STARTMENU_WRITE_END
 
  ;Add to control panel programs list
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayName" "${Name} (remove only)"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "DisplayIcon" "${Icon}"
  WriteRegStr HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\${Name}" "UninstallString" "$OUTDIR\$1"
 
!macroend
 
;--------------------------------
;Pages
 
  !insertmacro MUI_PAGE_WELCOME
 
  !insertmacro MUI_PAGE_LICENSE "${NSISDIR}\Docs\Modern UI\License.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
 
  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\$(^Name)"
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "Start Menu Folder"
 
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder
 
  !insertmacro MUI_PAGE_INSTFILES
 
  ;Create desktop shortcut before reboot
  ;!insertmacro MUI_FINISHPAGE_SHORTCUT
 
  !insertmacro MUI_PAGE_FINISH
 
  ;Uninstaller pages
  !insertmacro MUI_UNPAGE_WELCOME
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  !insertmacro MUI_UNPAGE_FINISH
 
;--------------------------------
;Language Include
 
  !insertmacro MUI_LANGUAGE "English"
  !insertmacro MUI_LANGUAGE "Russian"
  ;!insertmacro MUI_LANGUAGE "Korean"
 
;--------------------------------
;Reserve Files
 
  ;If you are using solid compression this will make installer start faster
  ;!insertmacro MUI_RESERVEFILE_LANGDLL
 
;--------------------------------
;Language Strings
 
  !define UNINST_LOCALIZE
 
  LangString UNINST_EXCLUDE_ERROR ${LANG_ENGLISH} "Error creating an exclusion list."
  LangString UNINST_EXCLUDE_ERROR ${LANG_RUSSIAN} "Ошибка при создании списка исключений."
  LangString UNINST_DATA_ERROR ${LANG_ENGLISH} "Error creating the uninstaller data: $\r$\nCannot find an exclusion list."
  LangString UNINST_DATA_ERROR ${LANG_RUSSIAN} "Ошибка при создании данных деинсталлятора: $\r$\nНе удается найти список исключений."
  LangString UNINST_DAT_NOT_FOUND ${LANG_ENGLISH} "$UNINST_DAT not found, unable to perform uninstall. Manually delete files."
  LangString UNINST_DAT_NOT_FOUND ${LANG_RUSSIAN} "$UNINST_DAT не найден, не удается выполнить удаление. Удалите файлы вручную."
  LangString UNINST_DAT_MISSING ${LANG_ENGLISH} "$UNINST_DAT is missing, some elements could not be removed. These can be removed manually."
  LangString UNINST_DAT_MISSING ${LANG_RUSSIAN} "$UNINST_DAT отсутствует, некоторые элементы не могут быть удалены. Они могут быть удалены вручную."
  LangString UNINST_DEL_FILE ${LANG_ENGLISH} "Delete File"
  LangString UNINST_DEL_FILE ${LANG_RUSSIAN} "Удалить файл"
 
  LangString Uninstall ${LANG_ENGLISH} "Uninstall"
  LangString Uninstall ${LANG_RUSSIAN} "Удалить"
  LangString NextCD ${LANG_ENGLISH} "Please insert the disk labeled"
  LangString NextCD ${LANG_RUSSIAN} "Пожалуйста, вставьте диск с меткой"
  LangString CDNotFound ${LANG_ENGLISH} "Can not find the file $0.$\r$\nPlease insert the correct disk."
  LangString CDNotFound ${LANG_RUSSIAN} "Не удается найти файл $0.$\r$\nПожалуйста, вставьте правильный диск."
  LangString Extract ${LANG_ENGLISH} "Decompression:"
  LangString Extract ${LANG_RUSSIAN} "Декомпрессия:"
  LangString REMOVEALL ${LANG_ENGLISH} "Remove all files and folders from '$INSTDIR'?"
  LangString REMOVEALL ${LANG_RUSSIAN} "Удалить все файлы и папки из '$INSTDIR'?"
  LangString UNCOMPLATE ${LANG_ENGLISH} "$(^Name) uninstall complete."
  LangString UNCOMPLATE ${LANG_RUSSIAN} "Деинсталляция $(^Name) завершена."
 
;--------------------------------
;Installer Sections
 
Section "Advanced" Sec1
 
  ;Set selected in "Full" and "Minimal" install types (see InstType) and 
  ;Make unavailable to change section state
  SectionIn 1 2 RO
 
  ;Set output path ($OUTDIR) and create it recursively if necessary
  SetOutPath "$INSTDIR"
 
  ;Create an exclusion list (UnInst.nsh)
  ;!insertmacro UNINSTALLER_DATA_BEGIN
 
  ;Add/Install Files
  File Advanced.nsi
  ;File /r App\*.*
 
  ;Use nsis7z plug-in (nsis7z.dll) to extract 7-zip archive
  ;(unicode version is supported in MakeNSIS v2.50 or later)
  ;for multi-volume distribution sort your files and archive them separately
  ;!insertmacro Extract7z "CD 1" "$EXEDIR\archive1.a2b" "(1/2)"
  ;!insertmacro Extract7z "CD 2" "$EXEDIR\archive2.a2b" "(2/2)"
 
  ;Use ZipDLL plug-in (ZipDLL.dll) to extract zip-archive
  ;!insertmacro NextCD "CD 3" "$EXEDIR\archive3.a2b"
  ;ZipDLL::extractall "$EXEDIR\archive3.a2b" "$OUTDIR"
 
  ;Localize ZipDLL and extract zip-archive (zipdll.nsh)
  ;!insertmacro ZIPDLL_EXTRACT "$EXEDIR\archive3.zip" "$OUTDIR" "<ALL>"
 
  ;Require additional disk space to extract the archives (size_kb)
  ;AddSize 1000000
 
  ;Store uninstaller data (UnInst.nsh)
  ;!insertmacro UNINSTALLER_DATA_END
 
  ;Get section name
  SectionGetText ${Sec1} $0
 
  ;Write uninstaller and create shortcuts
  !insertmacro SHORTCUTS "$0" "Advanced.exe" "${NSISDIR}\Contrib\Graphics\Icons\modern-install-blue.ico"
 
  ;Extract Temporary files
  ;SetOutPath "$PLUGINSDIR\Resource"
  ;File Tmp\*.*
 
  ;Execute commands
  ;ExecWait 'msiexec /i PhysX-9.13.0725-SystemSoftware.msi /qb'
  ;ExecWait 'vcredist_x86_5069.exe /q:a /c:"msiexec /i vcredist.msi /qb"'
 
  ;SetRebootFlag true
 
SectionEnd
 
;Another section in the group
SectionGroup /e "Components" SecGrp1
Section "Basic" Sec2
!insertmacro SHORTCUTS "" "Advanced.exe" "${NSISDIR}\Contrib\Graphics\Icons\modern-install-blue.ico"
  ;SetOutPath "$INSTDIR\2"
  ;File Basic.nsi
SectionEnd
SectionGroupEnd
 
;--------------------------------
;Descriptions
 
  ;Language strings
  LangString DESC_Sec1 ${LANG_ENGLISH} "Advanced Example Script (English language)."
  LangString DESC_Sec1 ${LANG_RUSSIAN} "Advanced Example Script (Russian language)."
 
  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${Sec1} $(DESC_Sec1)
    !insertmacro MUI_DESCRIPTION_TEXT ${Sec2} "Basic Example Script (All languages)."
  !insertmacro MUI_FUNCTION_DESCRIPTION_END
 
;--------------------------------
;Uninstaller Section
 
Section "Uninstall"
 
  ;Require confirmation to delete every other file except installed (UnInst.nsh)
  ;!define UNINST_INTERACTIVE
 
  ;Terminate uninstaller if the .dat file does not exist (UnInst.nsh)
  ;!define UNINST_TERMINATE
 
  ;Use generated header to delete files
  ;!include "UnFiles.nsh"
 
  ;Get installation folder from registry if available
  ;ReadRegStr $INSTDIR HKCU "Software\$(^Name)" ""
  ;StrCmp $INSTDIR "" 0 +2
  ;StrCpy $INSTDIR $OUTDIR
 
  ;Remove files with uninstaller data (UnInst.nsh)
  ;Note that, the $INSTDIR will be equal to uninstaller directory in this section
  ;StrCpy $INSTDIR "$PROGRAMFILES\$(^Name)"
  ;!insertmacro UNINST_DELETE "$INSTDIR" "${UninstName}"
  ;!insertmacro UNINST_DELETE "$INSTDIR\..\2" "${UninstName}"
 
  ;Delete files
  Delete "$INSTDIR\Advanced.nsi"
  Delete "$INSTDIR\${UninstName}.exe"
 
  ;Remove installation folder
  RMDir "$INSTDIR"
  ;IfErrors 0 +3
  ;MessageBox MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON1 "$(REMOVEALL)" /SD IDNO IDNO +2
  ;RMDir /r /REBOOTOK "$INSTDIR"
 
  ;Use "All Users" shell folder
  SetShellVarContext all
 
  ;Get Start Menu Folder
  !insertmacro MUI_STARTMENU_GETFOLDER Application $StartMenuFolder
 
  ;Delete shortcuts
  Delete "$SMPROGRAMS\$StartMenuFolder\$(^Name).lnk"
  Delete "$SMPROGRAMS\$StartMenuFolder\$(Uninstall).lnk"
  RMDir "$SMPROGRAMS\$StartMenuFolder"
  ;RMDir /r "$SMPROGRAMS\$StartMenuFolder"
 
  Delete "$DESKTOP\$(^Name).lnk"
 
  ;Remove from control panel programs list
  DeleteRegKey HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Advanced"
 
  ;Remove Start Menu Folder, language and installation folder from registry
  DeleteRegKey /ifempty HKCU "Software\$(^Name)"
 
SectionEnd
 
;--------------------------------
;Installer Functions
 
Function .onInit
 
  ;Display language selection dialog
  !insertmacro MUI_LANGDLL_DISPLAY
 
FunctionEnd
 
Function .onGUIEnd
 
  ;Store installation folder in registry
  WriteRegStr HKCU "Software\$(^Name)" "" $INSTDIR
 
  ;Escape from $PLUGINSDIR to completely remove it
  SetOutPath "$INSTDIR"
 
FunctionEnd
 
Function CreateDesktopShortCut
 
  CreateShortCut "$DESKTOP\$(^Name).lnk" "$INSTDIR\Advanced.exe" "" "${NSISDIR}\Contrib\Graphics\Icons\modern-install-blue.ico"
 
FunctionEnd
 
;--------------------------------
;Uninstaller Functions
 
Function un.onInit
 
  ;Get stored language preference
  !insertmacro MUI_UNGETLANGUAGE
 
FunctionEnd
 
Function un.onUninstSuccess
 
  MessageBox MB_OK|MB_ICONINFORMATION "$(UNCOMPLATE)" /SD IDOK
 
FunctionEnd