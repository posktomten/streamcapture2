set "QT=5.15.16"

set PATH=C:\Qt\%QT%\mingw81_64\bin;C:\Qt\Tools\mingw1120_64\bin;%PATH%
qmake -project ..\code\*.pro
qmake ..\code\*.pro
mingw32-make -j6
attrib +r *.bat
del /S /Q *
rd /S /Q release
rd /S /Q debug
attrib -r *.bat
cd ..\build-executable5
C:\Qt\%QT%\mingw81_64\bin\windeployqt.exe -no-translations streamcapture2.exe

