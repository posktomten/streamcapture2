#!/bin/bash

# Edit
QT6=6.8.1
QT5=5.15.16
EXECUTABLE=streamcapture2
EXTRA_LIB=./openssl

if [ $(getconf LONG_BIT) = 64 ]; then
    GCC="gcc_64"
    APPIMAGE="x86_64.AppImage"
    BIT="x86_64"
else
    GCC="gcc_32"
    APPIMAGE="i386.AppImage"
    BIT="i386"
fi

[ -e ${EXECUTABLE}-*.AppImage ] && rm ${EXECUTABLE}-*.AppImage
[ -e AppDir ] && rm -R AppDir


echo -----------------------------------------------------------
echo "Instructions for building an Appimage"
PS3='Please enter your choice: '
options=("Qt5.15.2" "Qt${QT5}" "Qt${QT6}" "Quit")

select opt in "${options[@]}"; do
    case $opt in
    "Qt${QT5}")
        export PATH=/opt/Qt/${QT5}/${GCC}/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/${QT5}/${GCC}/lib:${EXTRA_LIB}:$LD_LIBRARY_PATH
        break
        ;;
    "Qt5.15.2")
        EXTRA_LIB=./openssl1
        export PATH=/opt/Qt/5.15.2/${GCC}/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/5.15.2/${GCC}/lib:${EXTRA_LIB}:$LD_LIBRARY_PATH
        QT5=5.15.2
        break
        ;;
    "Qt${QT6}")
        export PATH=/opt/Qt/${QT6}/${GCC}/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/${QT6}/${GCC}/lib:${EXTRA_LIB}:$LD_LIBRARY_PATH
        break
        ;;
    "Quit")
        echo "Goodbye!"
        exit 0
        break

        ;;
    *) echo "Invalid option $REPLY"
    ;;
    esac
done

if [[ "${QT5}" == "5.15.2" ]]; then

linuxdeploy --appdir ./AppDir \
-e ./${EXECUTABLE} \
-d ./${EXECUTABLE}.desktop \
-e ./zsync \
-e ./downloadandremove \
-e ./download-svtplay-dl \
-e ./openssl1/openssl \
-l ./openssl1/libcrypto.so \
-l ./openssl1/libssl.so \
-l ./openssl1/libcrypto.so.1.1 \
-l ./openssl1/libssl.so.1.1 \
-i ./16x16/${EXECUTABLE}.png \
-i ./32x32/${EXECUTABLE}.png \
-i ./64x64/${EXECUTABLE}.png  \
-i ./128x128/${EXECUTABLE}.png \
-i ./256x256/${EXECUTABLE}.png \
-i ./512x512/${EXECUTABLE}.png \
-e ./ffmpeg/ffmpeg \
-l ./ffmpeg/libbsd.so.0 \
-l ./ffmpeg/liblzma.so.5 \
-l ./ffmpeg/libXau.so.6 \
-l ./ffmpeg/libxcb-shape.so.0 \
-l ./ffmpeg/libxcb-shm.so.0 \
-l ./ffmpeg/libxcb-xfixes.so.0 \
-l ./ffmpeg/libXdmcp.so.6 \
--plugin qt

else

linuxdeploy --appdir ./AppDir \
-e ./${EXECUTABLE} \
-d ./${EXECUTABLE}.desktop \
-e ./openssl/openssl \
-e ./zsync \
-e ./downloadandremove \
-e ./download-svtplay-dl-from-ceicer \
-l ./openssl/libssl.so \
-l ./openssl/libssl.so.3 \
-l ./openssl/libcrypto.so \
-l ./openssl/libcrypto.so.3 \
-i ./16x16/${EXECUTABLE}.png \
-i ./32x32/${EXECUTABLE}.png \
-i ./64x64/${EXECUTABLE}.png  \
-i ./128x128/${EXECUTABLE}.png \
-i ./256x256/${EXECUTABLE}.png \
-i ./512x512/${EXECUTABLE}.png \
-e ./ffmpeg/ffmpeg \
-l ./ffmpeg/libbsd.so.0 \
-l ./ffmpeg/liblzma.so.5 \
-l ./ffmpeg/libXau.so.6 \
-l ./ffmpeg/libxcb-shape.so.0 \
-l ./ffmpeg/libxcb-shm.so.0 \
-l ./ffmpeg/libxcb-xfixes.so.0 \
-l ./ffmpeg/libXdmcp.so.6 \
--plugin qt

fi




cp ./${EXECUTABLE}.png ./AppDir/usr/bin/
rm -R ./AppDir/usr/translations
ARCH=${BIT} appimagetool ./AppDir ${EXECUTABLE}-ffmpeg-${APPIMAGE}

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Copy" "Upload" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "Copy")
        echo "You chose choice $REPLY which is $opt"
        yes | cp -rf ${EXECUTABLE}-ffmpeg-${APPIMAGE} ../
        break
        ;;
    "Upload")
        echo "You chose choice $REPLY which is $opt"
        yes | cp -rf ${EXECUTABLE}-ffmpeg-${APPIMAGE} ../
        cd ..
        ./upload_appimage.sh
        break
        ;;
    "Quit")
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done
