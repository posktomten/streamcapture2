

"C:\Program Files (x86)\Windows Kits\10\bin\10.0.22621.0\x86\signtool.exe"

Qt 6.7.3 dell MinGW
SET PATH=C:\Qt\Tools\mingw1310_64\bin;C:\Qt\6.7.3\mingw_64\bin;%PATH%
C:\Qt\6.7.3\mingw_64\bin\windeployqt.exe streamcapture2.exe -no-translations -verbose 2

Qt 6.8.1 clang LLVM
SET PATH=C:\Qt\Tools\llvm-mingw1706_64\bin;C:\Qt\6.8.1\llvm-mingw_64\bin;%PATH%
C:\Qt\6.8.1\llvm-mingw_64\bin\windeployqt6.exe streamcapture2.exe -no-translations -verbose 2  
C:\Qt\6.8.1\llvm-mingw_64\bin\windeployqt6.exe download-svtplay-dl-from-ceicer.exe -no-translations -verbose 2  


Qt 6.8.2 dell msvc
SET PATH=C:\Qt\6.8.2\msvc2022_64\bin;%PATH%
C:\Qt\6.8.2\msvc2022_64\bin\windeployqt6.exe streamcapture2.exe -no-translations  -verbose 1 --compiler-runtime
C:\Qt\6.8.2\msvc2022_64\bin\windeployqt6.exe download-svtplay-dl.exe -no-translations  -verbose 1 --compiler-runtime


Qt 5.15.16 MinGW 64
SET PATH=C:\Qt\5.15.16\mingw81_64\bin;C:\Qt\Tools\mingw810_64\bin;%PATH%
C:\Qt\5.15.16\mingw81_64\bin\windeployqt.exe streamcapture2.exe -no-translations


Qt 5.15.14 MinGW 32 (Built on 64-bit)
SET PATH=C:\Qt\5.15.14\mingw810_32\bin;C:\Qt\Tools\mingw810_32\bin;%PATH%
C:\Qt\5.15.14\mingw810_32\bin\windeployqt.exe streamcapture2.exe -no-translations






Qt 5.15.15 msvc 64
SET PATH=C:\Qt\5.15.15\msvc2019_64\bin;%PATH%
C:\Qt\5.15.15\msvc2019_64\bin\windeployqt.exe streamcapture2.exe -no-translations

Qt 5.15.15 msvc 32 (Built on 64-bit)
SET PATH=C:\Qt\5.15.15\msvc2019_32\bin;%PATH%
C:\Qt\5.15.15\msvc2019_32\bin\windeployqt.exe streamcapture2.exe -no-translations




