-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 30, 2024 at 02:28 PM
-- Server version: 8.0.39-0ubuntu0.22.04.1
-- PHP Version: 8.1.2-1ubuntu2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_streamcapture2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_streamcapture2`
--

CREATE TABLE `tb_streamcapture2` (
  `_id` int UNSIGNED NOT NULL,
  `_Operating_system` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_bit` char(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Python` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Qt` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Compiler` char(28) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Compiler_version` char(12) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_GLIBC` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_streamCapture2` char(29) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_File_type` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_FFmpeg_included` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Download_Link` varchar(140) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_MD5_and_Size` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_comment` varchar(400) CHARACTER SET utf8mb3 COLLATE utf8mb3_swedish_ci NOT NULL,
  `_comment_sv` varchar(400) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `tb_streamcapture2`
--

INSERT INTO `tb_streamcapture2` (`_id`, `_Operating_system`, `_bit`, `_Python`, `_Qt`, `_Compiler`, `_Compiler_version`, `_GLIBC`, `_streamCapture2`, `_File_type`, `_FFmpeg_included`, `_Download_Link`, `_MD5_and_Size`, `_comment`, `_comment_sv`) VALUES
(4, 'Linux', '64', '3.8+', '5.15.15', ' GCC', ' 7.5.0', ' 2.27', '2.22.0', ' AppImage', 'None', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/streamcapture2-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/streamcapture2-x86_64.AppImage-SHA256.txt', 'For old 64-bit Linux computers still using an operating system from around 2018. FFmpeg is not included in the AppImage. You need to install FFmpeg in your system.', 'För äldre 64-bitars Linux-datorer som fortfarande använder ett operativsystem från omkring 2018. FFmpeg ingår inte i AppImagen. Du måste installera FFmpeg i ditt system.'),
(5, 'Linux', '64', '3.8+', '6.7.3', ' GCC', ' 9.4.0', ' 2.31', '2.22.0', ' AppImage', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.31/streamcapture2-ffmpeg-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.31/streamcapture2-ffmpeg-x86_64.AppImage-SHA256.txt', 'Suitable for most Linux users. If you have an operating system from 2020 or later, it should work fine. FFmpeg is included in the AppImage.', 'Passar de flesta Linuxanvändare. Om du har ett operativsystem från 2020 eller senare ska det fungera fint. FFmpeg är inkluderad i AppImagen.'),
(6, 'Linux', '64', '3.8+', '6.7.3', ' GCC', ' 9.4.0', ' 2.31', '2.22.0', ' AppImage', 'None', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.31/streamcapture2-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.31/streamcapture2-x86_64.AppImage-SHA256.txt', 'Suitable for most Linux users. If you have an operating system from 2020 or later, it should work fine. FFmpeg is not included in the AppImage. You need to install FFmpeg in your system.', 'Passar de flesta Linuxanvändare. Om du har ett operativsystem från 2020 eller senare ska det fungera fint. FFmpeg är inte inkluderad i AppImagen. Du måste installera FFmpeg i ditt system.'),
(7, 'Linux', '64', '3.8+', '6.7.3', ' GCC', ' 11.4.0', ' 2.35', '2.22.0', ' AppImage', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.35/streamcapture2-ffmpeg-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.35/streamcapture2-ffmpeg-x86_64.AppImage-SHA256.txt', 'If you have an operating system from 2022 or later, it should work fine. FFmpeg is included in the AppImage.', 'Om du har ett operativsystem från 2022 eller senare ska det fungera fint. FFmpeg är inkluderad i AppImagen.'),
(8, 'Linux', '64', '3.8+', '6.7.3', ' GCC', ' 11.4.0', ' 2.35', '2.22.0', ' AppImage', 'None', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.35/streamcapture2-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.35/streamcapture2-x86_64.AppImage-SHA256.txt', 'If you have an operating system from 2022 or later, it should work fine. FFmpeg is not included in the AppImage. You need to install FFmpeg in your system.', 'Om du har ett operativsystem från 2022 eller senare ska det fungera fint. FFmpeg är inte inkluderad i AppImagen. Du måste installera FFmpeg i ditt system.'),
(9, 'Windows 7 8.1 10 11', '32', '3.8', '5.15.15', ' MinGW GCC', ' 8.1.0', '', '2.22.0', 'Portable', '6.1.1', 'https://bin.ceicer.com/streamcapture2/bin/windows/obsolete/streamCapture2-ffmpeg_32-bit_portable.zip', 'https://bin.ceicer.com/streamcapture2/bin/windows/obsolete/streamCapture2-ffmpeg_32-bit_portable.zip-SHA256.txt', 'If you\'re still using Windows 7, 32-bit or 64-bit, it should work. Can also be used on newer Windows versions. FFmpeg is included. This is a Portable version. It can\'t be updated and it won\'t install. You only need to delete the streamCapture2 folder if you want to remove the program.', 'Om du fortfarande använder Windows 7 , 32- eller 64-bitar, ska det fungera. Går också att använda på nyare windowsversioner. FFmpeg är inkluderad. Detta är en Portable version. Den går inte att uppdatera och den installeras inte. Du behöver bara ta bort mappen med streamCapture2 om du vill ta bort programmet.'),
(10, 'Windows 10 11', '64', '3.10', '6.7.3', 'MinGW GCC', '13.1.0', '', '2.22.0', 'Portable', 'None', 'https://bin.ceicer.com/streamcapture2/bin/windows/streamCapture2_portable.zip', 'https://bin.ceicer.com/streamcapture2/bin/windows/streamCapture2_portable.zip-SHA256.txt', 'For Windows 10 and 11. FFmpeg is not included. You need to install FFmpeg in your system. This is a Portable version. It can\'t be updated and it won\'t install. You only need to delete the streamCapture2 folder if you want to remove the program.', 'För Windows 10 och 11. FFmpeg är inte inkluderad. Du måste installera FFmpeg i ditt system. Detta är en Portable version. Den går inte att uppdatera och den installeras inte. Du behöver bara ta bort mappen med streamCapture2 om du vill ta bort programmet.'),
(11, 'Windows 10 11', '64', '3.10', '6.7.3', 'MinGW GCC', '13.1.0', '', '2.22.0', 'Portable', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/windows/streamCapture2-ffmpeg_portable.zip', 'https://bin.ceicer.com/streamcapture2/bin/windows/streamCapture2-ffmpeg_portable.zip-SHA256.txt', 'For Windows 10 and 11. FFmpeg is included. This is a Portable version. It can\'t be updated and it won\'t install. You only need to delete the streamCapture2 folder if you want to remove the program.', 'För Windows 10 och 11. FFmpeg är inkluderad. Detta är en Portable version. Den går inte att uppdatera och den installeras inte. Du behöver bara ta bort mappen med streamCapture2 om du vill ta bort programmet.'),
(12, 'Windows 10 11', '64', '3.10', '6.7.3', 'MinGW GCC', '13.1.0', '', '2.22.0', 'Installer', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/windows/streamCapture2_installer.exe', 'https://bin.ceicer.com/streamcapture2/bin/windows/streamCapture2_installer.exe-SHA256.txt', 'For Windows 10 and 11. FFmpeg is included. This is an Online installer. You choose whether you want to download FFmpeg or install FFmpeg in another way. You can update and uninstall with the built-in Maintenance Tool.', 'För Windows 10 och 11. FFmpeg är inkluderad. Detta är en Online installer. Du väljer om du vill ladda ner FFmpeg eller installera FFmpeg på annat sätt. Uppdatering och avinstallation gör du med det inbygda Underhållsverktyget.'),
(13, 'Windows 10 11', '64', '3.10', '6.7.3', 'MinGW GCC', '13.1.0', '', '2.22.0', 'Setup', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/windows/streamCapture2_setup.exe', 'https://bin.ceicer.com/streamcapture2/bin/windows/streamCapture2_setup.exe-SHA256.txt', 'For Windows 10 and 11. FFmpeg is included. This is an Offline installer. You choose whether you want to install FFmpeg at the same time as streamCapture2 or install FFmpeg in another way. You can update and uninstall with the built-in Maintenance tool.', 'För Windows 10 och 11. FFmpeg är inkluderad. Detta är en Offline  installer. Du väljer om du vill installera  FFmpeg samtidigt med streamCapture2 eller installera FFmpeg på annat sätt. Uppdatering och avinstallation gör du med det inbygda Underhållsverktyget.'),
(14, 'Windows 7 8.1 10 11', '64', '3.8', '5.15.15', 'MinGW GCC', '8.1.0', '', '2.22.0', 'Portable', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/windows/obsolete/streamCapture2-ffmpeg_portable.zip', 'https://bin.ceicer.com/streamcapture2/bin/windows/obsolete/streamCapture2-ffmpeg_portable.zip-SHA256.txt', 'A 64-bit version that works on Windows 7 and 8.1. FFmpeg is included. This is a Portable version. It can\'t be updated and it won\'t install. You only need to delete the streamCapture2 folder if you want to remove the program.', 'En 64-bitarsversion som fungerar på Windows 7 och 8.1. FFmpeg ingår. Detta är en Portable version. Den kan inte uppdateras och den installeras inte. Du behöver bara ta bort mappen med streamCapture2 om du vill ta bort programmet.'),
(16, 'Visual C++ Redistributable', '64', '', '', '', '', '', '', 'Installer', '', 'https://bin.ceicer.com/vc_redist/VC_redist.x64.exe', 'https://bin.ceicer.com/vc_redist/VC_redist.x64.exe-SHA256.txt', 'Visual C++ Redistributable from Microsoft, may be needed if you use a 64-bit OS or 64-bit svtplay-dl. For Windows operating systems only. Download and double click to install.', 'Visual C++ Redistributable från Microsoft, kan behövas om du använder ett 64-bitars operativsystem eller 64-bitars svtplay-dl. Endast för Windows operativsystem. Ladda ner och dubbelklicka för att installera.'),
(18, 'Visual C++ Redistributable', '32', '', '', '', '', '', '', 'Installer', '', 'https://bin.ceicer.com/vc_redist/win7/vcredist_x86.exe', 'https://bin.ceicer.com/vc_redist/win7/vcredist_x86.exe-SHA256.txt', 'Visual C++ Redistributable from Microsoft, may be needed if you use a 32-bit OS or 32-bit svtplay-dl. For Windows operating systems only. Download and double click to install.', 'Visual C++ Redistributable från Microsoft, kan behövas om du använder ett 32-bitars operativsystem eller 32-bitars svtplay-dl. Endast för Windows operativsystem. Ladda ner och dubbelklicka för att installera.'),
(29, 'Linux', '64', '3.8+', '5.15.15', ' GCC', ' 7.5.0', ' 2.27', '2.22.0', ' AppImage', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/streamcapture2-ffmpeg-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/streamcapture2-ffmpeg-x86_64.AppImage-SHA256.txt', 'For old 64-bit Linux computers still using an operating system from around 2018. FFmpeg is included in the AppImage.', 'För äldre 64-bitars Linux-datorer som fortfarande använder ett operativsystem från omkring 2018. FFmpeg ingår i AppImagen.'),
(31, 'Windows 7 8.1 10 11', '64', '3.8', '5.15.15', 'MinGW GCC', '8.1.0', '', '2.22.0', 'Setup', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/windows/obsolete/streamCapture2_setup.exe', 'https://bin.ceicer.com/streamcapture2/bin/windows/obsolete/streamCapture2_setup.exe-SHA256.txt', 'A 64-bit version that works on Windows 7 and 8.1. This is an Offline installer. You choose whether you want to install FFmpeg at the same time as streamCapture2 or install FFmpeg in another way. You can update and uninstall with the built-in Maintenance tool.', 'En 64-bitarsversion som fungerar på Windows 7 och 8.1. Detta är en Offline-installer. Du väljer om du vill installera FFmpeg samtidigt som streamCapture2 eller installera FFmpeg på ett annat sätt. Du kan uppdatera och avinstallera med det inbyggda Underhållsverktyget.'),
(32, 'Linux', '32', '3.8+', '5.15.15', ' GCC', ' 7.5.0', ' 2.27', '2.22.0', ' AppImage', 'None', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/streamcapture2-ffmpeg-i386.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/streamcapture2-ffmpeg-i386.AppImage-SHA256.txt', 'For old 32-bit Linux computers still using an operating system from around 2018. FFmpeg is not included in the AppImage. You need to install FFmpeg in your system.', 'För äldre 32-bitars Linux-datorer som fortfarande använder ett operativsystem från omkring 2018. FFmpeg ingår inte i AppImagen. Du måste installera FFmpeg i ditt system.'),
(33, 'Linux', '32', '3.8+', '5.15.15', ' GCC', ' 7.5.0', ' 2.27', '2.22.0', ' AppImage', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/streamcapture2-ffmpeg-i386.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/streamcapture2-ffmpeg-i386.AppImage-SHA256.txt', 'For old 32-bit Linux computers still using an operating system from around 2018. FFmpeg is included in the AppImage.', 'För äldre 32-bitars Linux-datorer som fortfarande använder ett operativsystem från omkring 2018. FFmpeg ingår i AppImagen.'),
(34, 'Windows 7 8.1 10 11', '32', '3.8', '5.15.15', 'MinGW GCC', '8.1.0', '', '2.22.0', 'Setup', '6.1.1', 'https://bin.ceicer.com/streamcapture2/bin/windows/obsolete/streamCapture2_32-bit_setup.exe', 'https://bin.ceicer.com/streamcapture2/bin/windows/obsolete/streamCapture2_32-bit_setup.exe-SHA256.txt', 'A 32-bit version that works on Windows 7 and 8.1. This is an Offline installer. You choose whether you want to install FFmpeg at the same time as streamCapture2 or install FFmpeg in another way. You can update and uninstall with the built-in Maintenance tool.', 'En 32-bitarsversion som fungerar på Windows 7 och 8.1. Detta är en Offline-installer. Du väljer om du vill installera FFmpeg samtidigt som streamCapture2 eller installera FFmpeg på ett annat sätt. Du kan uppdatera och avinstallera med det inbyggda Underhållsverktyget.'),
(35, 'Linux', '64', '3.8+', '6.7.3', ' GCC', '13.2.0', ' 2.39', '2.22.0', ' AppImage', 'None', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.39/streamcapture2-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.39/streamcapture2-x86_64.AppImage-SHA256.txt', 'If you have an operating system from 2024-01-31 or later, it should work fine. FFmpeg is not included in the AppImage. You need to install FFmpeg in your system.', 'Om du har ett operativsystem från 2024-01-31 eller senare ska det fungera fint. FFmpeg är inte inkluderad i AppImagen. Du måste installera FFmpeg i ditt system.'),
(36, 'Linux', '64', '3.8+', '6.7.3', ' GCC', '13.2.0', ' 2.39', '2.22.0', ' AppImage', '7.0.2', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.39/streamcapture2-ffmpeg-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.39/streamcapture2-ffmpeg-x86_64.AppImage-SHA256.txt', 'If you have an operating system from 2024-01-31 or later, it should work fine. FFmpeg is included in the AppImage.', 'Om du har ett operativsystem från 2024-01-31 eller senare ska det fungera fint. FFmpeg är inkluderad i AppImagen.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_streamcapture2`
--
ALTER TABLE `tb_streamcapture2`
  ADD PRIMARY KEY (`_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_streamcapture2`
--
ALTER TABLE `tb_streamcapture2`
  MODIFY `_id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
