#!/bin/bash

# Edit
QT6=6.8.2
QT5=5.15.16
EXECUTABLE=streamcapture2
EXTRA_LIB=./openssl:./ffmpeg

[ -e ${EXECUTABLE}-*.AppImage ] && rm ${EXECUTABLE}-*.AppImage
[ -e AppDir ] && rm -R AppDir


echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Qt${QT5}" "Qt${QT6}" "Quit")

select opt in "${options[@]}"; do
    case $opt in
    "Qt${QT5}")
        export PATH=/opt/Qt/${QT5}/gcc_64/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/${QT5}/gcc_64/lib:${EXTRA_LIB}:$LD_LIBRARY_PATH
        break
        ;;
    "Qt${QT6}")
        export PATH=/opt/Qt/${QT6}/gcc_64/bin:$PATH
        export LD_LIBRARY_PATH=/opt/Qt/${QT6}/gcc_64/lib:${EXTRA_LIB}:$LD_LIBRARY_PATH
        break
        ;;
    "Quit")
        echo "Goodbye!"
        exit 0
        break
        ;;
    *) echo "Invalid option $REPLY"
    ;;
    esac
done


linuxdeploy --appdir AppDir \
-e ./${EXECUTABLE} \
-d ./${EXECUTABLE}.desktop \
-e ./openssl/openssl \
-e ./zsync \
-e ./downloadandremove \
-e ./ffmpeg/ffmpeg \
-e ./download-svtplay-dl \
-l ./ffmpeg/libbsd.so.0 \
-l ./ffmpeg/libmd.so.0 \
-l ./ffmpeg/libXau.so.6 \
-l ./ffmpeg/libxcb-shape.so.0  \
-l ./ffmpeg/libxcb-shm.so.0 \
-l ./ffmpeg/libxcb-xfixes.so.0 \
-l ./ffmpeg/libXdmcp.so.6 \
-l ./openssl/libssl.so \
-l ./openssl/libssl.so.3 \
-l ./openssl/libcrypto.so \
-l ./openssl/libcrypto.so.3 \
-i ./16x16/${EXECUTABLE}.png \
-i ./32x32/${EXECUTABLE}.png \
-i ./64x64/${EXECUTABLE}.png  \
-i ./128x128/${EXECUTABLE}.png \
-i ./256x256/${EXECUTABLE}.png \
-i ./512x512/${EXECUTABLE}.png \
--plugin qt

cp ./${EXECUTABLE}.png ./AppDir/usr/bin/
rm -R ./AppDir/usr/translations
appimagetool ./AppDir ${EXECUTABLE}-ffmpeg-x86_64.AppImage

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Copy" "Upload" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "Copy")
        echo "You chose choice $REPLY which is $opt"
        yes | cp -rf ${EXECUTABLE}-ffmpeg-x86_64.AppImage ../
        break
        ;;
    "Upload")
        echo "You chose choice $REPLY which is $opt"
        yes | cp -rf ${EXECUTABLE}-ffmpeg-x86_64.AppImage ../
        cd ..
        ./upload_appimage_ffmpeg.sh
        break
        ;;
    "Quit")
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done
