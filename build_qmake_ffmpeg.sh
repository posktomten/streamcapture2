#!/bin/bash

B=$(tput bold)
N=$(tput sgr0)

EXECUTABLE=streamcapture2
QT5=5.15.16
QT6=6.7.3

HERE=$(pwd)

if [ `getconf LONG_BIT` = 64 ]; then
	GCC="gcc_64"
else 
	GCC="gcc_32"
fi


#DELETE

files=(./${EXECUTABLE}-zsyncmake/${EXECUTABLE}-*)

if [[ -f "$files" ]]; then

for value in "${files[@]}"
do
  rm $value
  echo "$value deleted"
done

fi


files=(./build/)
if [ -d "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

files=(./${EXECUTABLE}-i386.AppImage)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi


files=(./${EXECUTABLE}-i386.AppImage-SHA256.txt-i386)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

files=(./${EXECUTABLE}-x86_64.AppImage)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi


files=(./${EXECUTABLE}-x86_64.AppImage-SHA256.txt)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

files=(./${EXECUTABLE}-zsyncmake/${EXECUTABLE}-*AppImage*)
if [ -f "$files" ]; then
for value in "${files[@]}"
do
  rm $value
  echo "$value deleted"
done
  rm -Rf $files
fi

files=(./linuxdeploy/${EXECUTABLE})
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

#FFMPEG




files=(./${EXECUTABLE}-ffmpeg-i386.AppImage)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi


files=(./${EXECUTABLE}-ffmpeg-i386.AppImage-SHA256.txt-i386)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi

files=(./${EXECUTABLE}-ffmpeg-x86_64.AppImage)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi


files=(./${EXECUTABLE}-ffmpeg-x86_64.AppImage-SHA256.txt)
if [ -f "$files" ]; then
  echo "$files deleted"
  rm -Rf $files
fi
#END DELETE



echo -----------------------------------------------------------
PS3='Please select the Qt version to use when compiling: '
options=("Qt5.15.2" "Qt${QT5}" "Qt${QT6}" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "Qt5.15.2")
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/5.15.2/${GCC}/bin/qmake"
        build_executable="build-executable5"
        export LD_LIBRARY_PATH=/opt/Qt/5.15.2/${GCC}/lib:$LD_LIBRARY_PATH
        export PATH=/opt/Qt/5.15.2/${GCC}bin:$PATH
        QT=5.15.2
        break
        ;;
    "Qt${QT5}")
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/${QT5}/${GCC}/bin/qmake"
        build_executable="build-executable5"
        export LD_LIBRARY_PATH=/opt/Qt/${QT5}/${GCC}/lib:$LD_LIBRARY_PATH
        export PATH=/opt/Qt/${QT5}/${GCC}/bin:$PATH
        QT=${QT5}
        break
        ;;
    "Qt${QT6}")
        echo "You chose choice $REPLY which is $opt"
        qmakePath="/opt/Qt/${QT6}/${GCC}/bin/qmake"
        build_executable="build-executable6"
        export LD_LIBRARY_PATH=/opt/Qt/${QT6}/${GCC}/lib:$LD_LIBRARY_PATH
        export PATH=/opt/Qt/${QT6}/${GCC}/bin:$PATH
        QT=${QT6}
        break
        ;;
    "Quit")
        g189
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done

echo -----------------------------------------------------------
PS3='Please enter your choice: '
options=("Debug" "Release" "Quit")
select opt in "${options[@]}"; do
    case $opt in
    "Debug")
        echo "You chose choice $REPLY which is $opt"
        mkdir build
        cd build
        $qmakePath -project ../code/*.pro
        if [ $(getconf LONG_BIT) -eq "64" ]; then
             echo -e "${B}64-bit${N}"
            $qmakePath ../code/*.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
         else
            echo -e "${B}32-bit${N}"
            $qmakePath ../code/*.pro -spec linux-g++ CONFIG+=debug CONFIG+=qml_debug && /usr/bin/make qmake_all
        fi
        /usr/bin/make -j$(nproc)
        /usr/bin/make clean -j$(nproc)
        cd ..
        rm -r build
        echo "Copying ${EXECUTABLE}  to linuxdeploy-ffmpeg"
        cp -f ${HERE}/$build_executable/${EXECUTABLE} linuxdeploy-ffmpeg/

        if [ "${QT}" == "${QT5}" ]; then
            cd linuxdeploy-ffmpeg
            ./instructions-lubuntu18.04.sh

        elif [ "${QT}" == "5.15.2" ]; then
            cd linuxdeploy-ffmpeg
           ./instructions-lubuntu18.04.sh

        else
           cd linuxdeploy-ffmpeg
            ./instructions-ffmpeg.sh

        fi

        break
        ;;
    "Release")
        echo "You chose choice $REPLY which is $opt"
        mkdir build
        cd build
        $qmakePath -project ../code/*.pro
        if [ $(getconf LONG_BIT) -eq "64" ]; then
            echo -e "${B}64-bit${N}"
            $qmakePath ../code/*.pro -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
        else
            echo -e "${B}32-bit${N}"
            $qmakePath ../code/*.pro -spec linux-g++ CONFIG+=qtquickcompiler && /usr/bin/make qmake_all
        fi

        /usr/bin/make -j$(nproc)
        /usr/bin/make clean -j$(nproc)
        cd ..
        echo "Copying ${EXECUTABLE}  to linuxdeploy-ffmpeg"
        cp -f ${HERE}/$build_executable/${EXECUTABLE} linuxdeploy-ffmpeg/

        if [ "${QT}" == "${QT5}" ]; then
            cd linuxdeploy-ffmpeg
            ./instructions-lubuntu18.04.sh

        elif [ "${QT}" == "5.15.2" ]; then
            cd linuxdeploy-ffmpeg
           ./instructions-lubuntu18.04.sh

        else
           cd linuxdeploy-ffmpeg
            ./instructions-ffmpeg.sh
        fi
        break
        ;;
    "Quit")
        break
        ;;
    *) echo "invalid option $REPLY" ;;
    esac
done




