-- phpMyAdmin SQL Dump
-- version 5.1.1deb5ubuntu1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 12, 2024 at 05:03 PM
-- Server version: 8.0.36-0ubuntu0.22.04.1
-- PHP Version: 8.1.2-1ubuntu2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_streamcapture2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_streamcapture2_beta`
--

CREATE TABLE `tb_streamcapture2_beta` (
  `_id` int UNSIGNED NOT NULL,
  `_Operating_system` varchar(30) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_bit` char(2) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Python` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Qt` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Compiler` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Compiler_version` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_GLIBC` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_streamCapture2` char(29) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_File_type` char(10) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_FFmpeg_included` char(5) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_Download_Link` varchar(140) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_MD5_and_Size` varchar(200) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `_comment` varchar(400) CHARACTER SET utf8mb3 COLLATE utf8mb3_swedish_ci NOT NULL,
  `_comment_sv` varchar(400) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;

--
-- Dumping data for table `tb_streamcapture2_beta`
--

INSERT INTO `tb_streamcapture2_beta` (`_id`, `_Operating_system`, `_bit`, `_Python`, `_Qt`, `_Compiler`, `_Compiler_version`, `_GLIBC`, `_streamCapture2`, `_File_type`, `_FFmpeg_included`, `_Download_Link`, `_MD5_and_Size`, `_comment`, `_comment_sv`) VALUES
(5, 'Linux', '64', '3.8+', '6.6.2', ' GCC', ' 9.4.0', ' 2.31', '3.17.10 BETA', ' AppImage', '6.1', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.31/BETA/streamcapture2-ffmpeg-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.31/BETA/streamcapture2-ffmpeg-x86_64.AppImage-MD5.txt', 'Suitable for most Linux users. If you have an operating system from 2020 or later, it should work fine. FFmpeg is included in the AppImage.', 'Passar de flesta Linuxanvändare. Om du har ett operativsystem från 2020 eller senare ska det fungera fint. FFmpeg är inkluderad i AppImagen.'),
(11, 'Windows 10 11', '64', '3.10', '6.6.2', ' MinGW GCC', ' 11.2.0', '', '3.17.10 BETA', 'Portable', '6.1', 'https://bin.ceicer.com/streamcapture2/bin/windows/BETA/streamCapture2-ffmpeg_portable.zip', 'https://bin.ceicer.com/streamcapture2/bin/windows/BETA/streamCapture2-ffmpeg_portable.zip-MD5.txt', 'For Windows 10 and 11. FFmpeg is included. This is a Portable version. It can\'t be updated and it won\'t install. You only need to delete the streamCapture2 folder if you want to remove the program.', 'För Windows 10 och 11. FFmpeg är inkluderad. Detta är en Portable version. Den går inte att uppdatera och den installeras inte. Du behöver bara ta bort mappen med streamCapture2 om du vill ta bort programmet.'),
(35, 'Linux', '64', '3.8+', '5.15.13', ' GCC', '7.5.0', ' 2.27', '3.17.10 BETA', ' AppImage', '6.1', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/BETA/streamcapture2-ffmpeg-x86_64.AppImage', 'https://bin.ceicer.com/streamcapture2/bin/linux/GLIBC2.27/BETA/streamcapture2-ffmpeg-x86_64.AppImage-MD5.txt', 'For old 64-bit Linux computers still using an operating system from around 2018. FFmpeg is included in the AppImage.', 'För äldre 64-bitars Linux-datorer som fortfarande använder ett operativsystem från omkring 2018. FFmpeg ingår i AppImagen.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_streamcapture2_beta`
--
ALTER TABLE `tb_streamcapture2_beta`
  ADD PRIMARY KEY (`_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_streamcapture2_beta`
--
ALTER TABLE `tb_streamcapture2_beta`
  MODIFY `_id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
