

SET "SOKVAG=C:\Qt\6.7.3\llvm-mingw_64\bin\"

set "LUPDATE_OPTIONS=-no-ui-lines -no-obsolete -locations none"

copy /y C:\Users\posktomten\PROGRAMMERING\streamcapture2\code\i18n\*.ts

%SOKVAG%lconvert %LUPDATE_OPTIONS% -i _streamcapture2_it_IT.ts _download_install_sv_SE.ts _about_it_IT.ts _libcheckupdate_it_IT.ts _libcreateshortcut_it_IT.ts _libcreateshortcut_sv_SE.ts -o _complete_it_IT.ts
%SOKVAG%lrelease _complete_it_IT.ts

%SOKVAG%lconvert %LUPDATE_OPTIONS% -i _streamcapture2_sv_SE.ts _download_install_it_IT.ts _about_sv_SE.ts _libcheckupdate_sv_SE.ts _libselectfont_it_IT.ts _libselectfont_sv_SE.ts -o _complete_sv_SE.ts
%SOKVAG%lrelease _complete_sv_SE.ts


%SOKVAG%lconvert %LUPDATE_OPTIONS% -i  _streamcapture2_template_xx_XX.ts _download_install_template_xx_XX.ts _about_template_xx_XX.ts _libcheckupdate_template_xx_XX.ts _libcreateshortcut_template_xx_XX.ts _libselectfont_template_xx_XX.ts -o _complete_template_xx_XX.ts




copy /y _complete_sv_SE.qm ..\code\i18n\

copy /y _complete_it_IT.qm ..\code\i18n\




lupdate -no-ui-lines -no-obsolete -locations none newprg.pro
