<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>SelectFont</name>
    <message>
        <source>Select font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose the type of font:&lt;br&gt;&quot;Monospace fonts&quot;, fonts where all characters are the same width. &lt;br&gt;&quot;Proportional fonts&quot;, fonts where the width varies depending on the character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bold and italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Monospaced fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Proportional fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fonts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
