<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>About</name>
    <message>
        <source>About </source>
        <translation>Info su </translation>
    </message>
    <message>
        <source>Copyright © </source>
        <translatorcomment>Copyright ©</translatorcomment>
        <translation>Copyright © </translation>
    </message>
    <message>
        <source>License: </source>
        <translation>Licenza: </translation>
    </message>
    <message>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation>viene distribuito nella speranza che possa essere utile, ma SENZA ALCUNA GARANZIA; senza nemmeno la garanzia implicita di COMMERCIABILITÀ o IDONEITÀ PER UNO SCOPO PARTICOLARE.</translation>
    </message>
    <message>
        <source>See the GNU General Public License version 3.0 for more details.</source>
        <translation>Per maggiori dettagli vedi la GNU General Public License versione 3.0.</translation>
    </message>
    <message>
        <source>Source code</source>
        <translation>Codice sorgente</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Sito web</translation>
    </message>
    <message>
        <source>Version history</source>
        <translation>Cronolgia versioni</translation>
    </message>
    <message>
        <source>Created: </source>
        <translation>Creato: </translation>
    </message>
    <message>
        <source>Compiled on: </source>
        <translation>Compilato: </translation>
    </message>
    <message>
        <source>Runs on: </source>
        <translation>Esegui il: </translation>
    </message>
    <message>
        <source> is in the folder: </source>
        <translation> è nellacartella: </translation>
    </message>
    <message>
        <source>Compiler:</source>
        <translation>Compilatore:</translation>
    </message>
    <message>
        <source>Compiler: LLVM/Clang/LLD based MinGW version </source>
        <translation>Compilatore: MinGW (GCC per Windows) versione </translation>
    </message>
    <message>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation>Compilatore: MinGW (GCC per Windows) versione </translation>
    </message>
    <message>
        <source>Programming language: C++</source>
        <translation>Linguaggio programmazione: C++</translation>
    </message>
    <message>
        <source>C++ version: C++20</source>
        <translation>Versione C++: 20</translation>
    </message>
    <message>
        <source>C++ version: C++17</source>
        <translation>Versione C++: 17</translation>
    </message>
    <message>
        <source>C++ version: C++14</source>
        <translation>Versione C++: 14</translation>
    </message>
    <message>
        <source>C++ version: C++11</source>
        <translation>Versione C++: 11</translation>
    </message>
    <message>
        <source>C++ version: Unknown</source>
        <translatorcomment>C++ version: Unknown</translatorcomment>
        <translation>Versione C++: sconosciuta</translation>
    </message>
    <message>
        <source>Application framework: Qt version </source>
        <translation>Framework applicazione: Qt versione </translation>
    </message>
    <message>
        <source>Processor architecture: 32-bit</source>
        <translation>Architettura processore: 32 bit</translation>
    </message>
    <message>
        <source>Processor architecture: 64-bit</source>
        <translation>Architettura processore: 64 bit</translation>
    </message>
    <message>
        <source>Compiler: Clang version </source>
        <translation>Compileatore: versione Clang </translation>
    </message>
</context>
</TS>
