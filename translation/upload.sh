#!/bin/bash

if [[ -x "/opt/Qt/6.8.1/gcc_64/bin/qmake" ]]
then

    SOKVAG="/opt/Qt/6.8.1/gcc_64/bin/"

    echo "Qt6.8.1"

elif [[ -x "/opt/Qt/Qt5.15.16/bin/qmake" ]]
then

    SOKVAG="/opt/Qt/Qt5.15.16/bin/"

    echo "Qt5.15.16"

else

	echo "Cannot find executable program files."
	exit 0

fi

date_now=$(date "+%FT%H-%M-%S")

mkdir -p all_translations-${date_now}



FILES=($(ls *.ts))
#echo ${FILES[*]}

for value in "${FILES[@]}"

do
   if [[ "${value}" != "_complete_sv_SE.ts" ]] && [[ "${value}" != "_complete_template_xx_XX.ts" ]] && [[ "${value}" != "_complete_it_IT.ts" ]] ; then

  cp $value all_translations-${date_now}

  fi

done

#cp *.ts all_translations-${date_now}/


mkdir -p all_translations-${date_now}/download-svtplay-dl-from-ceicer
cp ../../download-svtplay-dl-from-ceicer/code/i18n/*.ts all_translations-${date_now}/download-svtplay-dl-from-ceicer
mkdir -p all_translations-${date_now}/downloadandremove
cp ../../downloadandremove/code/i18n/*.ts all_translations-${date_now}/downloadandremove
tar -czvf all_translations-${date_now}.tar.gz all_translations-${date_now}
/usr/bin/zip all_translations-${date_now}.zip -r all_translations-${date_now}




HOST="`sed -n 1p ../../secret`"
USER="`sed -n 2p ../../secret`"
PASSWORD="`sed -n 3p ../../secret`"

fil=all_translations-${date_now}.tar.gz


ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "/bin.ceicer.com/public_html/streamcapture2/bin/all_translations"
put $fil
bye
EOF

fil=all_translations-${date_now}.zip

ftp -v -inv $HOST <<EOF
user $USER $PASSWORD
passive
cd "/bin.ceicer.com/public_html/streamcapture2/bin/all_translations"
put $fil
bye
EOF

#rm -R all_translations-linux-${date_now}
#rm all_translations-${date_now}.zip
