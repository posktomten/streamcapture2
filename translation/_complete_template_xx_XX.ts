<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1">
<context>
    <name>DownloadListDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please zoom using the keyboard. &quot;Ctrl + +&quot;, &quot;Ctrl + -&quot; or &quot;Ctrl + 0&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove invalid list items</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unexpected error has occurred.
The file with saved links to downloadable files cannot be found.
Check that the file has not been deleted or moved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit the download list (</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Video streams)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save as text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Text file, optional extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not save the file.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to find file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to find</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Enter your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not stop svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl stopped. Exit code </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete any files that may have already been downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy to: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Newprg</name>
    <message>
        <source>Version history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License streamCapture2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License FFmpeg</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License 7zip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> could not be found. Please download a portable streamCapture2 where FFmpeg is included.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> could not be found. Please go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install FFmpeg.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Or install </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> in your system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> could not be found. Please download an AppImage where FFmpeg is included.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl is not found in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> You can download svtplay-dl from bin.ceicer.com. Select &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl.exe is not found in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About svtplay-dl (In the system path)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> svtplay-dl.exe  cannot be found. Go to &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please click &quot;svtplay-dl&quot; and select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> svtplay-dl cannot be found. Go to &quot;svtplay-dl&quot;, &quot;Download svtplay-dl...&quot; to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have downloaded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Nothing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To folder &quot;stable&quot;: Nothing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To folder &quot;stable&quot;: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To folder &quot;beta&quot;: Nothing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To folder &quot;beta&quot;: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl beta can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unexpected error occurred.&lt;br&gt;Information about svtplay-dl stable can not be found.&lt;br&gt;Check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unexpected error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Information about svtplay-dl stable can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check your internet connection.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You are using version:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You are not using svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The latest svtplay-dl available for&lt;br&gt;download from bin.ceicer.com are</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Beta:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Use svtplay-dl stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Use svtplay-dl stable&quot;, but it can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Use svtplay-dl beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Use svtplay-dl beta&quot;, but it can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Use the selected svtplay-dl&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Use the selected svtplay-dl&quot;, but it can not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl is not found in the specified path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;Welcome to streamCapture2!&lt;/b&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;svtplay-dl stable&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click &lt;i&gt;Download&lt;/i&gt; and &lt;i&gt;Latest stable svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;svtplay-dl beta&lt;/b&gt; can not be found. You have entered an incorrect path or no path has been specified at all.&lt;br&gt;Or the files have been moved or deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click &lt;i&gt;Download&lt;/i&gt; and &lt;i&gt;Latest svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;svtplay-dl stable is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;svtplay-dl beta is available for download.&lt;/b&gt;&lt;br&gt;Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click &lt;i&gt;Download&lt;/i&gt;, then click&lt;br&gt;&lt;i&gt;Latest stable svtplay-dl </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;/i&gt;and&lt;br&gt;&lt;i&gt;Latest svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Latest stable svtplay-dl&quot;&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;i&gt;Click &quot;Download&quot; and &quot;Latest svtplay-dl beta&quot;&lt;/i&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have not selected any place to copy the media files.
Please select a location before proceeding.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A file with the same name already exists. The file will not be copied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy succeeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Only if svtplay-dl does not work do you need to install
&quot;Microsoft Visual C++ Redistributable&quot;.
Download and double-click to install.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please click on &quot;svtplay-dl&quot; and select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You need to copy the TV4 token from your browser and save it in streamCapture2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request is processed...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preparing to download...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download streaming media to folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You do not have the right to save to the folder.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The video stream is saved in </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The default folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You do not have the right to save to the default folder.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The folder for downloading video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Selected folder to copy to is </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spaces are not allowed. Use only the characters
your streaming provider approves.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starts downloading: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to find any streams with the selected video resolution.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Merge audio and video...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removing old files, if there are any...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The download failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download succeeded</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download completed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The download failed </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save as...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No folder is selected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Searching...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request is processed...
Starting search...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select &quot;Download List&quot; and &quot;New download List...&quot;.
Or select &quot;Download List&quot; and &quot;Import a download List...&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The list of downloadable files is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The list of downloadable video streams is empty.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid link found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid links found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid text string found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid text strings found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download streaming media to directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have chosen to download more than one file with the same name. In order not to overwrite files, folders will be created for each file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The default folder for copying video streams cannot be found.
Download is interrupted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The video streams are saved in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unexpected error has occurred.
The file with saved links to downloadable video streams is incorrect.
Check that all links are correct.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Preparing to download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The download failed.
The error may be due to your &apos;method&apos;, &apos;quality&apos; and/or &apos;deviation&apos; selections not working.
It usually works best if you let svtplay-dl choose automatically.
If a username and password or &apos;st&apos; cookie is required, you must enter these.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>
You tried: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>streamCapture2 is done with the task.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is not allowed to save </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Check your file permissions or choose another location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New downloadlist</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download list files (*.lst)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import a download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is not allowed to import </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>In order to export a Download list file, you must first create or import such a file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Restart Now</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change language on next start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to English.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Swedish.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Italian.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The search field is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Spaces are not allowed. Use only the characters your streaming provider approves.
The Password will not be saved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: Can&apos;t find any videos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find video id for the video.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>WARNING: Use program page instead of the clip / video page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find video id.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t decode api request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Use the video page not the series page.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find any videos. Is it removed?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find the video file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: Click on &quot;Tools&quot;, &quot;Show more&quot; and run again to get more information.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: If the error persists, you can report it at https://github.com/spaam/svtplay-dl/issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: Include URL and error message in problem description.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>WARNING: --all-episodes not implemented for this service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> crashed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not find any video streams, please check the address.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change settings on next start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The program must be restarted for the new settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Whether notifications are displayed depends on the settings in your operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open downloaded file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>File: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If the download does not work</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter TV4 token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You must paste the exact TV4 token.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit streamCapture2 settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit download svtplay-dl settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl is not found in the system path.
You can download any svtplay-dl of your choice.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bold and italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current font:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot find the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>with download links listed.
Check if the file has been removed or moved.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot open the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>with download links listed.
Check if the file permissions have changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid list item found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid list items found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Incorrect link found. Please click &quot;Download List&quot;, &quot;Edit the download List...&quot; to remove incorrect link.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Incorrect links found.  Please click &quot;Download List&quot;, &quot;Edit the download List...&quot; to remove incorrect links.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video stream</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video streams</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to open file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>with download links listed.
Check if the file has been deleted
or if the file permissions have changed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit the download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cannot delete the file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>will be deleted and can not be restored.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can&apos;t delete </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please click on &quot;Tools&quot; and &quot;Update&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select &quot;Tools&quot;, &quot;Maintenance Tool&quot; and &quot;Update components&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download a new</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A graphical shell for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> and </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> streamCapture2 handles downloads of video streams.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Many thanks to </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> for the Italian translation. And for many good ideas that have made the program better.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl is in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl.exe is in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR! svtplay-dl is not in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR! svtplay-dl.exe is not in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path to svtplay-dl: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path to svtplay-dl.exe: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR! svtplay-dl could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR! svtplay-dl.exe could not be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR! FFmpeg cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path to ffmpeg.exe: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Path to ffmpeg: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To add downloads to the download list,
you need to create a list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Downlod List&quot; and &quot;New download List...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> or import a list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;Downlod List&quot; and &quot;Import a download List...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no search to add.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid link. Unable to add to download list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy streaming media to directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You do not have the right to save to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>and it cannot be used as the default folder to copy to.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>and it cannot be used as the default folder for downloads.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl stable cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please go to &quot;svtplay-dl&quot; and &quot;Download svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl beta cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl cannot be found in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>*.exe (svtplay-dl.exe)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl.exe is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The selected svtplay-dl cannot be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please click &quot;svtplay-dl&quot; and &quot;Select svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to select svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This is a BETA version. This AppImage can&apos;t be updated.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You can find more versions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maintenance Tool cannot be found.
Only if you install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>is it possible to update and uninstall the program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to create desktop shortcut.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Best quality is selected automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Best method is selected automatically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Downloading...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not find any video streams, please check the address.

</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The search is complete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The search failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click to copy to the search box.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The number of previous searches to be saved...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Specify how many previous searches you want to save. If the number of searches exceeds the specified number, the oldest search is deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The number of searches to be saved: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit saved searches...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click to edit all saved searches.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit saved searches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Remove all saved searches</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Click to delete all saved searches.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All your saved settings will be deleted.
All lists of files to download will disappear.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>was normally terminated. Exit code = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>crashed. Exit code = </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invalid link. No NFO information could be found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>NFO files contain release information about the media.
No NFO file was found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Episode title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Season</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Episode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Plot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Published</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unexpected error occurred while downloading the NFO file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>NFO Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Streaming service</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter the name of your streaming service.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter your username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do you want to save the password? (unsafe)?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Manage Login details for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit, rename or delete
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The mission failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Mission accomplished!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save a screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open the folder where streamCapture2 is located</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Read message from the developer (if there is any)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Force update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>streamCapture2 is not allowed to open </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> Use the file manager instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The folder where streamCapture2 is located</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Update this AppImage to the latest version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select &quot;Tools&quot;, &quot;Update&quot; to update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please click on &quot;Tools&quot; and &quot;Maintenance Tool...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Up and running</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Less then 720p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Less than or equal to 720p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Less than or equal to 1080p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>More then 1080p</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Deviation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download an AppImage that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Download a portable streamCapture2 that contains FFmpeg.&lt;br&gt;&lt;br&gt;Or download and place ffmpeg.exe in the same folder as streamapture2.exe.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;b&gt;FFmpeg cannot be found or is not an executable program.&lt;/b&gt;&lt;br&gt;&lt;br&gt;Go to &quot;Tools&quot;, &quot;Maintenance Tool&quot; to install.&lt;br&gt;&lt;br&gt;Or install FFmpeg in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> cannot be found or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not save a file to store Recent Search list.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failure!
The shortcut could not be created in
&quot;~/.local/share/applications&quot;
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download video streams.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failure!
The shortcut could not be created.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl cannot be found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The information from svtplay-dl may or may not contain:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quality, Method, Codec, Resolution, Language and Role</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to log in. Forgot your username and password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. You need a token to access the website. see https://svtplay-dl.se/tv4play/</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: That site is not supported. Please visit https://github.com/spaam/svtplay-dl/issues</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. This mode is not supported anymore. Need the url with the video.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find any videos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. We can&apos;t download DRM protected content from this site.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Wrong url, need to be video url.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find video info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Media doesn&apos;t have any associated videos.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Can&apos;t find audio info.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ERROR: No videos found. Cant find video id.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter &apos;st&apos; cookie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Enter the &apos;st&apos; cookie that your video stream provider has saved in your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Manage &apos;st&apos; cookie for </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use, Do not use, Edit or Delete
&apos;st&apos; cookie for
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set new &apos;st&apos; cookie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste and save the &apos;st&apos; cookie that your streaming provider has downloaded to your browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change style on next start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Dark Fusion style.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Fusion style.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The program needs to be restarted to switch to Default style.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open your language file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiled language file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Message from the developer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is no message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The font size changes to the selected font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To the website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download the latest version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download the new version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unexpected error occurred.&lt;br&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;br&gt;can not be found or is not an executable program.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>newprg</name>
    <message>
        <source>TEST</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste the link to the page where the video is displayed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>https://</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search for video streams.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download the file you just searched for.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add current video to the list of files that will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add to Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download all files you added to the list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download all on the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If Dolby Vision 4K video streams are not found, the download will probably fail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dolby Vision 4K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The number of bits that are conveyed or processed per unit of time. Higher numbers give better quality and larger file..</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quality (Bitrate)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Media streaming communications protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select quality on the video you download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Quality (bitrate) and method. Higher bitrate gives better quality and larger file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select quality on the video you download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Video resolution: 480p=640x480, 720p=1280x720, 1080p=1920x1080</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Allows given quality to differ by an amount. 300 usually works well. (Bit rate +/- 300).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select provider. If yoy need a password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If no saved password is found, click here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check for updates at program start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit settings (Advanced)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Recent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>L&amp;ogin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;All Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&apos;st&apos; &amp;cookies</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TV&amp;4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The download may fail if no subtitles are found. You can try different menu options.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>S&amp;ubtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check for updates...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exits the program.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About FFmpeg...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paste the link to the page where the video is displayed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download the stream you just searched for.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License streamCapture2...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License FFmpeg...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Recent files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Help...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View the download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Look at the list of all the streams to download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete the download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All saved streams in the download list are deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete download list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version history...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create new user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save the name of a video stream provider, your username and, if you want, your password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Explain what is going on</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add to download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add current video to the list of streams that will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download all the streams in the list. If it is the same video stream in different qualities folders are automatically created for each video stream.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create folder &quot;method_quality_amount_resolution&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create folder &quot;method_quality_amount&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Automatically creates a folder for each downloaded video stream. If you use &quot;Direct download of all...&quot; no folders are ever created.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Edit the download List... (Advanced)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Change method or quality. Remove a download from the list. NOTE! If you change incorrectly, it will not work.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show more</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>View more information from svtplay-dl. Appears in purple text color.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uninstall streamCapture</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uninstall and remove all components</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Direct Download of all Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trying to immediately download all episodes. Unable to create folders or select quality.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download after Date...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Stop all downloads</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Trying to stop svtplay-dl.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>List all Episodes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Looking for video streams for all episodes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Delete all settings and Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All saved searches and the list of streams to be downloaded will be deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copy to Selected Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Direct copy to the default copy location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select Copy Location...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save the location where the finished video file is copied.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select Default Download Location...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save the location for direct download.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to Default Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Direct download to the default location.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add all Episodes to Download List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Adds all episodes to the download list.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select font...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Visit svtplay-dl forum for issues...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maintenance Tool...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starts the Maintenance Tool. To update or uninstall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All saved searches, download list and settings are deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Don&apos;t show notifications</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not show notifications when the download is complete.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create a shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Desktop Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create shortcut to streamCapture2 on desktop.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Applications menu Shortcut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Create shortcut to streamCapture2 in the operating system&apos;s program menu.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load external language file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Useful when testing your own translation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set new &apos;st&apos; cookie</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Increase the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Decrease the font size.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check for the latest svtplay-dl from bin.ceicer.com...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download Microsoft runtime (required for svtplay-dl)...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download the runtime file from bin.ceicer.com.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License 7-Zip...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check streamCapture2 for updates at start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check for new versions of svtplay-dl at start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>streamCapture2 settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>download svtplay-dl settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>NFO info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>NFO files contain media release information. Available at svtplay.se, among other places.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select file name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You choose the name of the downloaded video file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>About Qt...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set TV4 Token</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>How-to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>How-to Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If Dolby Vision 4K video streams are not found, the download will fail.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download subtitles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The subtitle is saved in a text file (*.srt).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download subtitles and try to merge with the video file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If embedding in the video file does not work, try using the text file with the subtitle.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No subtitles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>If the download does not work...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Light Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Dark Fusion Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use svtplay-dl from the system path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Uses (if available) svtplay-dl in the system path.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use svtplay-dl stable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Note that the latest stable version may be newer than the latest beta version. Check version by clicking &quot;Help&quot; -&gt; &quot;About svtplay-dl...&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select svtplay-dl that you have in your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use the selected svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use svtplay-dl that you selected in your computer.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download svtplay-dl...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download and decompress svtplay-dl from bin.ceicer.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download all subtitles</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Merge subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download subtitle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download the subtitles in their native format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Don&apos;t Use Native Dialogs</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not use the operating system&apos;s file dialog.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>System Default Theme (Light or Dark)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fusion Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Export the download List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Import a download List...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>New download List...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DownloadUnpack</name>
    <message>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>V&amp;isit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Tools</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose location for</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&quot;&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Try to decompress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>F4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>English</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Swedish</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Load external language file...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italian</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Binary files...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download beta...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download latest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom In</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Zoom Out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ctrl+-</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show all Toolbars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to directory &quot;beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to
directory &quot;beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download to
directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>stable...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>beta...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search stable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download
stable...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable to directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable to
directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download stable
to directory &quot;stable&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download beta to directory &quot;beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download beta to
directory &quot;beta&quot;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show Default Toolbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Is not writable. Cancels</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting to download...
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Check your file permissions and antivirus software.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not save to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>is downloaded to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Starting to decompress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>is decompressed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Could not delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Linux operating systems. It will not work with Windows operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning! The version of svtplay-dl that you downloaded is intended for Windows operating systems. It will not work with Linux operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download failed, please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Changed name from</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl beta&quot; to use this version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select &quot;svtplay-dl&quot;, &quot;Use svtplay-dl stable&quot; to use this version.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to decompress.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download svtplay-dl from bin.ceicer.com</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Select svtplay-dl and download to the &quot;stable&quot; folder and to the &quot;beta&quot; folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Direct download of stable to the &quot;stable&quot; folder and beta to the &quot;beta&quot; folder.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current location for stable:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Current location for beta:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Take a screenshot (5 seconds delay)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>For</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>will be downloaded to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;svtplay-dl&quot;, &quot;Use svtplay-dl stable&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The stable version of svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The beta version of svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>To use this svtplay-dl version, click
&quot;svtplay-dl&quot;, &quot;Use svtplay-dl beta&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Selected svtplay-dl</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>will be downloaded.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl stable version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>svtplay-dl beta</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <oldsource>Cancell</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save svtplay-dl in directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The font size changes to the selected font size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save a screenshot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Images (*.png)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Save </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> in directory</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <source>About </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copyright © </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>License: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>See the GNU General Public License version 3.0 for more details.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Source code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Version history</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Created: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiled on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Runs on: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is in the folder: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler: LLVM/Clang/LLD based MinGW version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler: MinGW (GCC for Windows) version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Programming language: C++</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++17</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++14</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: C++11</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>C++ version: Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Application framework: Qt version </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Processor architecture: 32-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Processor architecture: 64-bit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compiler: Clang version </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CheckUpdate</name>
    <message>
        <source>No Internet connection was found.
Please check your Internet settings and firewall.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> is newer than the latest official version. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>You have the latest version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>
There was an error when the version was checked.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updates:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is a new version of </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Latest version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote host name was not found (invalid hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the remote server timed out.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The operation was canceled via calls to abort() or close() before it was finished.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The SSL/TLS handshake failed and the encrypted channel could not be established. The sslErrors() signal should have been emitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network, however the system has initiated roaming to another access point. The request should be resubmitted and will be processed as soon as the connection is re-established.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection was broken due to disconnection from the network or failure to start the network.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The background request is not currently allowed due to platform policy.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>While following redirects, the maximum limit was reached. The limit is by default set to 50 or as set by QNetworkRequest::setMaxRedirectsAllowed().</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>While following redirects, the network access API detected a redirect from a encrypted protocol (https) to an unencrypted one (http).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown network-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the proxy server was refused (the proxy server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy server closed the connection prematurely, before the entire reply was received and processed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy host name was not found (invalid proxy hostname).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The connection to the proxy timed out or the proxy did not reply in time to the request sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The proxy requires authentication in order to honour the request but did not accept any credentials offered (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown proxy-related error was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The access to the remote content was denied (similar to HTTP error 403).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The operation requested on the remote content is not permitted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote content was not found at the server (similar to HTTP error 404).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server requires authentication to serve the content but the credentials provided were not accepted (if any).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request needed to be sent again, but this failed for example because the upload data could not be read a second time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The request could not be completed due to a conflict with the current state of the resource.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The requested resource is no longer available at the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unknown error related to the remote content was detected.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The Network Access API cannot honor the request because the protocol is not known.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The requested operation is invalid for this protocol.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>A breakdown in protocol was detected (parsing error, invalid or unexpected responses, etc.).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server encountered an unexpected condition which prevented it from fulfilling the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server does not support the functionality required to fulfill the request.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The server is unable to handle the request at this time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Server Error.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The remote server refused the connection (the server is not accepting requests).</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown Error.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Network error. Check your network connection. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Network error. Check your network connection.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Createshortcut</name>
    <message>
        <source>not found.&lt;br&gt;The shortcut will be created without an icon.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>not found.&lt;br&gt;The shortcut will not be created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The shortcut could not be created.&lt;br&gt;Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Warning!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All your saved settings will be deleted.
All shortcuts will be removed.
And the program will exit.
Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to delete your configuration files.
Check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source> can not be removed.&lt;br&gt;Pleas check your file permissions.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectFont</name>
    <message>
        <source>Select font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose the type of font:&lt;br&gt;&quot;Monospace fonts&quot;, fonts where all characters are the same width. &lt;br&gt;&quot;Proportional fonts&quot;, fonts where the width varies depending on the character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Default</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Bold and italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>All fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Monospaced fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Proportional fonts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>fonts</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>zsync cannot be found in path:
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to update.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An unexpected error occurred. Error message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>is updated.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UpdateDialog</name>
    <message>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updating...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Updating, please wait...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
