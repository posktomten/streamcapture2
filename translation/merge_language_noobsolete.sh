#!/bin/bash

QT5="5.15.16"
QT6="6.8.2"

#/opt/Qt/5.15.16/gcc_64/bin/lupdate-pro newprg.pro -no-obsolete -no-ui-lines -locations none


#C:\Qt\5.15.16\mingw81_64\bin\lupdate-pro newprg.pro -no-obsolete -no-ui-lines -locations none


HERE="$(pwd)"
LUPDATE_OPTIONS="-no-ui-lines -no-obsolete -locations none"
echo ${HERE}

# Find qmake, to also find lupdate, lconvert and lrelease

if [[ -x "/opt/Qt/${QT6}/gcc_64/bin/qmake" ]]

then

  SOKVAG="/opt/Qt/${QT6}/gcc_64/bin/"

elif [[ -x "/opt/Qt/${QT5}/gcc_64/bin/qmake" ]]


then

    SOKVAG="/opt/Qt/${QT5}/gcc_64/bin/"

else

	echo "Cannot find executable program files."
	exit 1

fi


${SOKVAG}lupdate -no-obsolete ../code/newprg.pro



cp -f ../code/i18n/_streamcapture2_sv_SE.ts ./
cp -f ../code/i18n/_streamcapture2_it_IT.ts ./
cp -f ../code/i18n/_streamcapture2_template_lin_xx_XX.ts  ./


#${SOKVAG}lupdate -no-ui-lines -no-obsolete -locations none _streamcapture2_it_IT.ts _libdownloadunpack_it_IT.ts _about_it_IT.ts _libcheckupdate_it_IT.ts _libcheckupdcli_it_IT.ts _libcreateshortcut_lin_it_IT.ts _libselectfont_it_IT.ts _libupdateappimage_it_IT.ts -o _complete_it_IT.ts


${SOKVAG}lconvert -i _streamcapture2_it_IT.ts -i _about_it_IT.ts -i _libcheckupdate_it_IT.ts -i _libcheckupdcli_it_IT.ts  -i _libselectfont_it_IT.ts -i _libupdateappimage_it_IT.ts -o _complete_it_IT.ts
${SOKVAG}lrelease _complete_it_IT.ts


${SOKVAG}lconvert -i _streamcapture2_sv_SE.ts -i _about_sv_SE.ts -i _libcheckupdate_sv_SE.ts -i _libcheckupdcli_sv_SE.ts -i _libselectfont_sv_SE.ts -i _libupdateappimage_sv_SE.ts -o _complete_sv_SE.ts

${SOKVAG}lrelease _complete_sv_SE.ts
${SOKVAG}lrelease _complete_it_IT.ts




#${SOKVAG}lconvert ${LUPDATE_OPTIONS} -i  _streamcapture2_template_xx_XX.ts  _libdownloadunpack_template_xx_XX.ts _about_template_xx_XX.ts _libcheckupdate_template_xx_XX.ts _libcheckupdcli_template_xx_XX.ts _libcreateshortcut_template_lin_xx_XX.ts _libselectfont_template_xx_XX.ts _libupdateappimage_template_xx_XX.ts -o _complete_template_xx_XX.ts



cp -f _complete_lin_sv_SE.qm ../code/i18n/

cp -f _complete_lin_it_IT.qm ../code/i18n/

#/usr/bin/zip _complete_template_xx_XX.ts.zip _complete_template_xx_XX.ts

