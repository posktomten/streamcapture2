<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="55"/>
        <source>zcync not found.</source>
        <translation>zcync non trovato.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="64"/>
        <source>An unexpected download error has occurred</source>
        <translation>Si è verificato un errore download imprevisto</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="86"/>
        <source>Downloading...</source>
        <translation>Download...</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="103"/>
        <source> cannot be found.</source>
        <translation> non trovato.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="107"/>
        <location filename="../main.cpp" line="120"/>
        <source>The old </source>
        <translation>Il vecchio </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="107"/>
        <source> has been removed.</source>
        <translation> è stato rimosso.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="113"/>
        <location filename="../main.cpp" line="117"/>
        <source>The new </source>
        <translation>Il nuovo </translation>
    </message>
    <message>
        <location filename="../main.cpp" line="113"/>
        <source> is executable.</source>
        <translation> è eseguibile.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="114"/>
        <source>The update is complete.</source>
        <translation>L&apos;aggiornamento è stato completato.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="117"/>
        <source> cannot be made executable.</source>
        <translation> non può essere reso eseguibile.</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="120"/>
        <source> cannot be removed.</source>
        <translation> non può essere rimosso.</translation>
    </message>
</context>
</TS>
