<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT">
<context>
    <name>QObject</name>
    <message>
        <source>Network error. Check your network connection. </source>
        <translation>Errore di rete. Controlla la connessione di rete. </translation>
    </message>
    <message>
        <source>Network error. Check your network connection.</source>
        <translation>Errore di rete. Controlla la connessione di rete.</translation>
    </message>
</context>
</TS>
