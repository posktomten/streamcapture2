<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>QObject</name>
    <message>
        <source>Network error. Check your network connection. </source>
        <translation>Nätverksfel. Kontrollera din nätverksanslutning. </translation>
    </message>
    <message>
        <source>Network error. Check your network connection.</source>
        <translation>Nätverksfel. Kontrollera din nätverksanslutning.</translation>
    </message>
</context>
</TS>
