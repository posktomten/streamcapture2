<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sv_SE">
<context>
    <name>SelectFont</name>
    <message>
        <source>Select font</source>
        <translation>Välj teckensnitt</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation>Fet</translation>
    </message>
    <message>
        <source>Choose the type of font:&lt;br&gt;&quot;Monospace fonts&quot;, fonts where all characters are the same width. &lt;br&gt;&quot;Proportional fonts&quot;, fonts where the width varies depending on the character.</source>
        <translation>Välj teckensnitt:&lt;br&gt;&quot;Teckensnitt med fast bredsteg&quot;, teckensnitt där alla tecken har samma bredd.&lt;br&gt;&quot;Proportionellt teckensnitt&quot;, teckensnitt som varierar i bredd beroende på tecken.</translation>
    </message>
    <message>
        <source>Size</source>
        <translation>Storlek</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Italic</source>
        <translation>Kursiv</translation>
    </message>
    <message>
        <source>Bold and italic</source>
        <translation>Fet och kursiv</translation>
    </message>
    <message>
        <source>Default</source>
        <translation>Standard</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>Avsluta</translation>
    </message>
    <message>
        <source>All fonts</source>
        <translation>Alla teckensnitt</translation>
    </message>
    <message>
        <source>Monospaced fonts</source>
        <translation>Teckensnitt med fast breddsteg</translation>
    </message>
    <message>
        <source>Proportional fonts</source>
        <translation>Proportionella teckensnitt</translation>
    </message>
    <message>
        <source>fonts</source>
        <translation>teckensnitt</translation>
    </message>
</context>
</TS>
