#!/bin/bash

BOLD=$(tput bold)
NORMAL=$(tput sgr0)

echo -e ${BOLD}Checking python3...${NORMAL}
test=$([[ "$(python3 --version)" =~ "Python 3" ]] && echo "Python 3 is installed")
n=${#test}
if [ $n -eq 0 ]; then
    echo installing Python 3...
    sudo apt install python3
else
    test=$(python3 --version)
    echo $test is already installed
fi

echo
echo -e ${BOLD}Checking pip3...${NORMAL}
test=$(pip3 --version)
n=${#test}
if [ $n -eq 0 ]; then
    echo installing pip3...
    sudo apt install python3-pip
else
    echo $test is already installed
fi

echo
echo -e ${BOLD}Checking cryptography...${NORMAL}
test=$(pip3 freeze | grep -w cryptography)
n=${#test}

if [ $n -eq 0 ]; then
    echo installing cryptography...
    pip3 install cryptography
else
    echo cryptography is already installed
fi

echo
echo -e ${BOLD}Checking requests...${NORMAL}
test=$(pip freeze | grep -w requests)
n=${#test}

if [ $n -eq 0 ]; then
    echo installing requests...
    pip3 install requests
else
    echo requests is already installed
fi

echo
echo -e ${BOLD}Checking pysocks...${NORMAL}
test=$(pip freeze | grep -w PySocks)
n=${#test}

if [ $n -eq 0 ]; then
    echo installing pysocks...
    pip3 install pysocks
else
    echo pysocks is already installed
fi

echo
echo -e ${BOLD}Checking pyyaml...${NORMAL}
test=$(pip3 freeze | grep -w PyYAML)
n=${#test}

if [ $n -eq 0 ]; then
    echo installing pyyaml...
    pip3 install pyyaml
    
else
    echo pyyaml is already installed
fi
