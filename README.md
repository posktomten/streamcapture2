 # streamCapture2
&copy; Coppyright Ingemar Ceicer GPL v3 2016 - 2024<br><br>

# [**Read more and download**](https://gitlab.com/posktomten/streamcapture2/-/wikis/home)


# [**En hemsida på svenska**](https://ceicer.eu/streamcapture2/index.php)

@posktomten

A program to save streaming video to your computer. A graphical shell for the command line program svtplay-dl
The program works with Linux, Windows and MacOS X. The program is written in C++ and uses Qt5 or Qt6 graphic library. There is a setup program for Windows. AppImage is available for Linux and Portable version for Windows. No installation required.

[Tested](https://gitlab.com/posktomten/streamcapture2/-/wikis/Home) with several operating systems.

The program is translated into English, Italian and Swedish.

A [translation](https://gitlab.com/posktomten/streamcapture2/-/wikis/Translate) into additional languages would be appreciated!

# streamCapture2 is a graphical shell for the command line program [svtplay-dl](https://svtplay-dl.se/).


You might also be interested in
- [checkversionsvtplaydl](https://gitlab.com/posktomten/checkversionsvtplaydl)


[FFmpeg](https://www.ffmpeg.org/) is used in turn by svtplay-dl


The program uses these libraries

- [libabout](https://gitlab.com/posktomten/libabout)
- [download_install](https://gitlab.com/posktomten/download_offline_installer)
- [libcheckforupdates](https://gitlab.com/posktomten/libcheckforupdates)
- [libcheckupdcli](https://gitlab.com/posktomten/libcheckupdcli)
- [libselectfont](https://gitlab.com/posktomten/libselectfont)
- [libzsyncupdateappimage](https://gitlab.com/posktomten/libzsyncupdateappimage)

And this program

- [download-svtplay-dl-from-ceicer](https://gitlab.com/posktomten/download-svtplay-dl-from-ceicer)

And this command line program

- [downloadandremove](https://gitlab.com/posktomten/downloadandremove)


Tools for AppImage and for updating AppImage

- [linuxdeploy](https://github.com/linuxdeploy/linuxdeploy)
- [appimagetool](https://github.com/AppImage/AppImageKit)
- [zsync](http://zsync.moria.org.uk/)


