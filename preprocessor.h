//  <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
//
//    		Program name
//    		Copyright (C) 2024 Ingemar Ceicer
//    		https://gitlab.com/posktomten
//    		programming@ceicer.com
//
//   This program is free software: you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 3 as published by
//   the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License for more details.
//#define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.35/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt

// <(((*> <(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*> ><(((*>
#ifndef TEST_H
#define TEST_H

#endif // TEST_H

#if defined(Q_PROCESSOR_X86_64) // 64 bit
#if (__GLIBC_MINOR__ == 27)

#define COMPILEDON "Lubuntu 18.04.6 LTS 64-bit, GLIBC 2.27"
    #if defined(FFMPEG)
        #define ARG1 EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage"
        #if defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/BETA/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
        #elif !defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
        #endif // BETA


    #elif !defined(FFMPEG)
        #define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
        #if defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
        #elif !defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
        #endif // BETA
    #endif // FFMPEG

#endif // __GLIBC_MINOR__ == 27

#if (__GLIBC_MINOR__ == 31)

#define COMPILEDON "Lubuntu 20.04.6 LTS 64-bit, GLIBC 2.31"
    #if defined(FFMPEG)
    #define ARG1 EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage"
        #if defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/BETA/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.31/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
        #elif !defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.31/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
        #endif // BETA
    #elif !defined(FFMPEG)
    #define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
        #if defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.31/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
        #elif !defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.31/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.31/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
        #endif // BETA
    #endif // FFMPEG

#endif // __GLIBC_MINOR__ == 31

#if  (__GLIBC_MINOR__ == 35)

#define COMPILEDON "Ubuntu 22.04.4 LTS 64-bit, GLIBC 2.35"
    #if defined(FFMPEG)
        #define ARG1 EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage"
        #if defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/BETA/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.35/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
        #elif !defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.35/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
        #endif // BETA
    #elif !defined(FFMPEG)
        #define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
        #if defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.35/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
        #elif !defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.35/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.35/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
        #endif // BETA
    #endif // FFMPEG

#endif // __GLIBC_MINOR__ == 35

#if  (__GLIBC_MINOR__ == 39)
#define COMPILEDON "Ubuntu 24.04.1 LTS, GLIBC 2.39"
    #if defined(FFMPEG)
    #define ARG1 EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage"
    #if defined(BETA)
        #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/BETA/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
        #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.39/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
    #elif !defined(BETA)
        #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.zsync"
        #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.39/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-x86_64.AppImage.txt"
    #elif !defined(FFMPEG)
    #define ARG1 EXECUTABLE_NAME "-x86_64.AppImage"
    #if defined(BETA)
        #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/BETA/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
        #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.39/BETA/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
    #elif !defined(BETA)
        #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.39/" EXECUTABLE_NAME "-x86_64.AppImage.zsync"
        #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.39/.invisible_checksum_" EXECUTABLE_NAME "-x86_64.AppImage.txt"
    #endif // BETA
#endif // FFMPEG

#endif // __GLIBC_MINOR__ == 39

#elif defined(Q_PROCESSOR_X86_32) // 32 bit

#define COMPILEDON "Lubuntu 18.04.6 LTS 32-bit, GLIBC 2.27"
    #if defined(FFMPEG)
        #define ARG1 EXECUTABLE_NAME "-ffmpeg-i386.AppImage"
        #if defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/BETA/" EXECUTABLE_NAME "-ffmpeg-i386.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/BETA/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-i386.AppImage.txt"
        #elif !defined(BETA)
            #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-ffmpeg-i386.AppImage.zsync"
            #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/.invisible_checksum_" EXECUTABLE_NAME "-ffmpeg-i386.AppImage.txt"
        #endif //BETA
    #elif !defined(FFMPEG)
        #define ARG1 EXECUTABLE_NAME "-i386.AppImage"
            #if defined(BETA)
                #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/BETA/" EXECUTABLE_NAME "-i386.AppImage.zsync"
                #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/BETA/.invisible_checksum_" EXECUTABLE_NAME "-i386.AppImage.txt"
            #elif !defined(BETA)
                #define ARG2 "http://bin.ceicer.com/zsync/" EXECUTABLE_NAME "/GLIBC2.27/" EXECUTABLE_NAME "-i386.AppImage.zsync"
                #define CHECKSUM "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/linux/GLIBC2.27/.invisible_checksum_" EXECUTABLE_NAME "-i386.AppImage.txt"
            #endif //BETA
    #endif // FFMPEG

#endif // Q_PROCESSOR

#elif defined(Q_OS_WINDOWS) // WINDOWS

#define FONTSIZE 10
#if defined(OFFLINE_INSTALLER)
#if defined(PYTHON38)
#define PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/windows/obsolete/"
#if defined(Q_PROCESSOR_X86_64) // Windows 64 bit
#define FILENAME DISPLAY_NAME "_setup.exe"
#elif defined (Q_PROCESSOR_X86_32) // Windows 32 bit
#define FILENAME DISPLAY_NAME "_32-bit_setup.exe"
#endif

#elif !defined(PYTHON38)

#define PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/bin/windows/"
#define FILENAME DISPLAY_NAME "_setup.exe"
#endif // PYTHON38
#endif // OFFLINE_INSTALLER

#define VERSION_PATH "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_windows.txt"
#define USER_MESSAGE "https://bin.ceicer.com/" EXECUTABLE_NAME "/user_windows.txt"

#define VERSION_PATH_SV "https://bin.ceicer.com/" EXECUTABLE_NAME "/version_windows_sv.txt"
#define USER_MESSAGE_SV "https://bin.ceicer.com/" EXECUTABLE_NAME "/user_windows_sv.txt"

#if defined(Q_PROCESSOR_X86_64) // Windows 64 bit
#if defined(PYTHON38)
#define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Windows/64bit_python3.8/version_stable.txt"
#define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Windows/64bit_python3.8/version.txt"
#elif !defined(PYTHON38)
#define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Windows/64bit/version_stable.txt"
#define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Windows/64bit/version.txt"
#endif
#define COMPILEDON "Microsoft Windows 11 Pro Version: 24H2<br>OS build: 26100.2033<br>System type: 64-bit operating system, x64-based processor"

#elif defined (Q_PROCESSOR_X86_32) // Windows 32 bit
#define CHECK_LATEST_STABLE "https://bin.ceicer.com/svtplay-dl/Windows/32bit/version_stable.txt"
#define CHECK_LATEST "https://bin.ceicer.com/svtplay-dl/Windows/32bit/version.txt"
#define COMPILEDON "Microsoft Windows 11 Pro Version: 24H2<br>OS build: 26100.2033<br>System type: 64-bit operating system, x64-based processor"

#endif // Q_PROCESSOR